/**
 * Перевести время в пути полученное из системы в читаемый вид
 * @param string traveltime
 * @return string
 */
function timeInFlight(traveltime) {
    let arr = traveltime.split("H");
    let hours = arr[0].split("T");
    hours = hours[1];
    let minutes = arr[1].split("M");
    minutes = minutes[0];

    let text = "";
    if(hours>0) {
        text += hours + "ч.";
    }
    if(minutes>0) {
        text += " " + minutes + "мин.";
    }
    return text;
}

/**
 * Перевести дату в формат чч:мм
 * @param time timestamp
 * @return string
 */
function timePerUnix(timestamp) {
    var date = parseISO8601(timestamp);
    var hours = date[4];
    if(hours<10) {
        hours = "0"+hours;
    }
    var minutes = date[5];
    if(minutes<10) {
        minutes = "0"+minutes;
    }

    return hours + ':' + minutes;
}

/**
 * Перевести в формат ISO8601
 * @param string s
 * @return string
 */
function parseISO8601(s) {
    var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:.(\d+))?(Z|[+-]\d{2})(?::(\d{2}))?/,
        d = s.match(re);
    if(!d) return null;
    for(var i in d)
        d[i] = ~~d[i];

    return d;
}

/**
 * Разделить число на тысячные
 * @param string num
 * @return string
 */
function delimeterThousand(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

/**
 * Read file
 */
function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

function escapeHtml(text) {
    var map = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function get_cookie(cookie_name) {
    var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
    if ( results )
        return ( unescape ( results[2] ) );
    else
        return null;
}