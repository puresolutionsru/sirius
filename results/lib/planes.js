var planes = {
    "X85": {
        "map": {
            "1A": {
                "top": 64,
                "left": 219,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 219,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 283,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 283,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 314,
                "route": "bottomleft",
            },
            "4A": {
                "top": 63,
                "left": 347,
                "route": "bottomcenter",
            },
            "4B": {
                "top": 4,
                "left": 346,
                "route": "right",
            },
            "5A": {
                "top": 63,
                "left": 380,
                "route": "bottomright",
            },
        },
        "mapCabin": {
            "0": {
                "1A": true,
                "1B": true,
                "2A": true,
                "2B": true,
            },
            "1": {
                "3A": true,
                "4A": true,
                "4B": true,
                "5A": true,
            }
        },
        "mapCabinOptions": {
            "0": {
                "left": "219px",
                "top": "3px",
                "width": "96px",
                "height": "94px"
            },
            "1": {
                "left": "316px",
                "top": "3px",
                "width": "97px",
                "height": "94px"
            },
        },
        "aircraft": {
            "left": "153px",
            "top": "3px",
            "width": "259px",
            "height": "94px"
        }
    },
    "EM6-1": {
        "map": {
            "1A": {
                "top": 64,
                "left": 218,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 218,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 282,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 282,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 325,
                "route": "left",
            },
            "3B": {
                "top": 4,
                "left": 325,
                "route": "left",
            },
            "4A": {
                "top": 63,
                "left": 389,
                "route": "right",
            },
            "4B": {
                "top": 4,
                "left": 389,
                "route": "right",
            },
            "5A": {
                "top": 63,
                "left": 432,
                "route": "left",
            },
            "5B": {
                "top": 5,
                "left": 431.5,
                "route": "topleft",
            },
            "6B": {
                "top": 5,
                "left": 463.5,
                "route": "topcenter",
            },
            "7A": {
                "top": 63,
                "left": 496,
                "route": "right",
            },
            "7B": {
                "top": 5,
                "left": 495,
                "route": "topright",
            },
        },
        "mapCabin": {
            "0": {
                "1A": true,
                "1B": true,
                "2A": true,
                "2B": true,
            },
            "1": {
                "3A": true,
                "3B": true,
                "4A": true,
                "4B": true,
            },
            "2": {
                "5A": true,
                "5B": true,
                "6B": true,
                "7A": true,
                "7B": true,
            }
        },
        "mapCabinOptions": {
            "0": {
                "left": "218px",
                "top": "3px",
                "width": "96px",
                "height": "94px"
            },
            "1": {
                "left": "320px",
                "top": "3px",
                "width": "106px",
                "height": "94px"
            },
            "2": {
                "left": "431px",
                "top": "3px",
                "width": "97px",
                "height": "94px"
            },
        },
        "aircraft": {
            "left": "153px",
            "top": "3px",
            "width": "376px",
            "height": "94px"
        }
    },
    "EM6-2": {
        "map": {
            "1A": {
                "top": 64,
                "left": 218,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 218,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 282,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 282,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 325,
                "route": "left",
            },
            "3B": {
                "top": 30,
                "left": 325,
                "route": "left",
            },
            "4A": {
                "top": 63,
                "left": 389,
                "route": "right",
            },
            "4B": {
                "top": 30,
                "left": 389,
                "route": "right",
            },
            "5A": {
                "top": 63,
                "left": 432,
                "route": "left",
            },
            "5B": {
                "top": 5,
                "left": 431.5,
                "route": "topleft",
            },
            "6B": {
                "top": 5,
                "left": 463.5,
                "route": "topcenter",
            },
            "7A": {
                "top": 63,
                "left": 496,
                "route": "right",
            },
            "7B": {
                "top": 5,
                "left": 495,
                "route": "topright",
            },
        },
        "mapCabin": {
            "0": {
                "1A": true,
                "1B": true,
                "2A": true,
                "2B": true,
            },
            "1": {
                "3A": true,
                "3B": true,
                "4A": true,
                "4B": true,
            },
            "2": {
                "5A": true,
                "5B": true,
                "6B": true,
                "7A": true,
                "7B": true,
            }
        },
        "mapCabinOptions": {
            "0": {
                "left": "218px",
                "top": "3px",
                "width": "96px",
                "height": "94px"
            },
            "1": {
                "left": "320px",
                "top": "3px",
                "width": "106px",
                "height": "94px"
            },
            "2": {
                "left": "431px",
                "top": "3px",
                "width": "97px",
                "height": "94px"
            },
        },
        "aircraft": {
            "left": "153px",
            "top": "3px",
            "width": "376px",
            "height": "94px"
        }
    },
    "EM6-3": {
        "map": {
            "1A": {
                "top": 64,
                "left": 218,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 218,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 282,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 282,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 325,
                "route": "left",
            },
            "3B": {
                "top": 30,
                "left": 325,
                "route": "left",
            },
            "4A": {
                "top": 63,
                "left": 389,
                "route": "right",
            },
            "4B": {
                "top": 30,
                "left": 389,
                "route": "right",
            },
            "5A": {
                "top": 63,
                "left": 432,
                "route": "left",
            },
            "5B": {
                "top": 5,
                "left": 431.5,
                "route": "topleft",
            },
            "6B": {
                "top": 5,
                "left": 463.5,
                "route": "topcenter",
            },
            "7A": {
                "top": 63,
                "left": 496,
                "route": "right",
            },
            "7B": {
                "top": 5,
                "left": 495,
                "route": "topright",
            },
        },
        "mapCabin": {
            "0": {
                "1A": true,
                "1B": true,
                "2A": true,
                "2B": true,
            },
            "1": {
                "3A": true,
                "3B": true,
                "4A": true,
                "4B": true,
            },
            "2": {
                "5A": true,
                "5B": true,
                "6B": true,
                "7A": true,
                "7B": true,
            }
        },
        "mapCabinOptions": {
            "0": {
                "left": "218px",
                "top": "3px",
                "width": "96px",
                "height": "94px"
            },
            "1": {
                "left": "320px",
                "top": "3px",
                "width": "106px",
                "height": "94px"
            },
            "2": {
                "left": "431px",
                "top": "3px",
                "width": "97px",
                "height": "94px"
            },
        },
        "aircraft": {
            "left": "153px",
            "top": "3px",
            "width": "376px",
            "height": "94px"
        }
    },
}