<?
require($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

$planes = array();
$arFilter = Array("IBLOCK_ID"=>7);
$res = CIBlockElement::GetList(Array(), $arFilter, false);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProperties = $ob->GetProperties();

    $id = $arProperties["ID_IBE"]["VALUE"];
    if($id) {
        $planes[$id]["name"] = $arProperties["PLANE"]["VALUE"];
        $planes[$id]["number"] = $arFields["NAME"];
        $planes[$id]["description"] = $arProperties["SHORT_DESCRIPTION"]["~VALUE"]["TEXT"];
        $planes[$id]["descriptionCabin"] = $arProperties["SHORT_DESCRIPTION_CAUTA"]["~VALUE"]["TEXT"];
        $planes[$id]["descriptionAircraft"] = $arProperties["SHORT_DESCRIPTION_SALON"]["~VALUE"]["TEXT"];


        $planes[$id]["class"] = $arProperties["CLASS"]["VALUE"];
        $planes[$id]["count"] = $arProperties["VMEST"]["VALUE"];
        $planes[$id]["configurationImage"] = CFile::GetPath($arProperties["CONFIGURATION"]["VALUE"]);
        $planes[$id]["smallImage"] = CFile::GetPath($arProperties["SMALLIMAGE"]["VALUE"]);
        $planes[$id]["smallPhoto"] = CFile::GetPath($arProperties["PIC_RAM"]["VALUE"]);
        $planes[$id]["smallPhotoResize"] = CFile::ResizeImageGet($arProperties["PIC_RAM"]["VALUE"], array('width'=>213, 'height'=>56));
        $planes[$id]["smallPhotoResize"] = $planes[$id]["smallPhotoResize"]["src"];
        // $planes[$id]["map"] = $arProperties["COMPONOVKA"]["~VALUE"]["TEXT"];
        $planes[$id]["countCabin"] = $arProperties["COUNT_CABIN"]["VALUE"];
        // $planes[$id]["mapCabin"] = $arProperties["COMPONOVKA_CABINS"]["VALUE"]["TEXT"];
        // $planes[$id]["mapCabinOptions"] = $arProperties["POSITIONS_CABINS"]["VALUE"]["TEXT"];
        // $planes[$id]["aircraft"] = $arProperties["POSITION_SALON"]["VALUE"]["TEXT"];
        $planes[$id]["detailUrl"] = "//" . $_SERVER["SERVER_NAME"] . $arFields["DETAIL_PAGE_URL"];

        $photos = $arProperties["GALLERY"]["VALUE"];
        foreach($photos as $key) {
            $planes[$id]["photos"][] = CFile::GetPath($key);
        }
        foreach($photos as $key) {
            $p = CFile::ResizeImageGet($key, array('width'=>169, 'height'=>113));
            $planes[$id]["photosResize"][] = $p["src"];
        }
    }
}


file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/results/lib/planes.json', json_encode($planes, JSON_UNESCAPED_UNICODE));