<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');
?>
    <link rel="stylesheet" href="/results/assets/icons/font-awesome/css/font-awesome.min.css">
<?
$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);

$dayofweek = array(
    '', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'
);
if(CModule::IncludeModule('iblock')) {
    $prop = [];
    $prop['STATUS'] = ['PROPERTY_VALUES' => 3];
    $prop['TICKET_NUMBER'] = $_GET['ticket'];
    $prop['LOCATOR'] = $_GET['loc'];

    $el = new CIBlockElement;
    $el->SetPropertyValuesEx($data->orderid, false, $prop);
}
$_monthsList = array(
    "1"=>"января",
    "2"=>"февраля",
    "3"=>"марта",
    "4"=>"апреля",
    "5"=>"мая",
    "6"=>"июня",
    "7"=>"июля",
    "8"=>"августа",
    "9"=>"сентября",
    "10"=>"октября",
    "11"=>"ноября",
    "12"=>"декабря"
);

$date = $data->selectFlight->date;
$month = $_monthsList[date("n", strtotime($date))];
$day = $dayofweek[date("N", strtotime($date))];

$servPrice = 0;
if($data->tarif == "Место"):
    foreach($data->service as $value) {
        $servPrice += $value->ActualPrice;
    }
endif;
$total = $data->price + $servPrice;
?>




    <duv style="position:absolute;top:0;right:0;border: solid 2px;width:200px;height:70px; text-align:left;background:#ffffff;border-radius:0px 0px 0px 5px;padding:0px 0px 0px 15px;font-size:14px;">
        <?
        if(empty($_COOKIE['user_id'])){
            ?>
            <div style="float:left; margin:20px 10px 0px 0px;">
                <a href="http://lk.sirius-aero.ru/">Авторизоваться</a>
            </div>
            <div style="parring-left:10px;margin-top:20px"><a href="http://lk.sirius-aero.ru/"><span class="fa fa-sign-in" style="font-size:18px"></span></a></div>
            <?
        }else{
            ?>
            <div style="float:left;margin-top:10px">
                USER ID: <?=$_COOKIE['user_id']?><br>
                REQUESTSITES ID: <?=$_COOKIE['requisites_id']?></div>
            <div style="margin-top:10px"><a href="http://lk.sirius-aero.ru/"><span class="fa fa-sign-out" style="font-size:18px"></span></a></div>
            <?
        }
        ?>
    </duv>
<div class="breadcrumbs" style="display:block;margin-top:0px;">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper order-success">
    <div class="order-success__t">Благодарим за покупку.</div>
    <div class="order-success__tx">Ваш заказ № <?=$_GET['ticket']?> успешно оплачен.</div>
    <div class="data-sender order-success__data-sender">
        <div class="data-sender__ticket order-success__data-sender__ticket">
            <div class="data-sender__ticket__info order-success__data-sender__ticket__info">
                <div class="data-sender__ticket__info__order order-success__data-sender__ticket__info__order">
                    <div class="data-sender__ticket__info__order__t order-success__data-sender__ticket__info__order__t">номер билета:</div>
                    <div class="data-sender__ticket__info__order__tx order-success__data-sender__ticket__info__order__tx"><?=$_GET['ticket']?></div>
                </div>
                <div class="data-sender__ticket__info__code order-success__data-sender__ticket__info__code">
                    <div class="data-sender__ticket__info__code__t order-success__data-sender__ticket__info__code__t">локатор:</div>
                    <div class="data-sender__ticket__info__code__tx order-success__data-sender__ticket__info__code__tx"><span><?=$_GET['loc']?></span></div>
                </div>
            </div>
            <div class="data-sender__ticket__price order-success__data-sender__ticket__price">
                <div class="data-sender__ticket__price__t order-success__data-sender__ticket__price__t">пассажиров:</div>
                <div class="data-sender__ticket__price__tx order-success__data-sender__ticket__price__tx">1</div>
                <div class="data-sender__ticket__price__t order-success__data-sender__ticket__price__t order-success__data-sender__ticket__price__t_next">цена:</div>
                <div class="data-sender__ticket__price__tx order-success__data-sender__ticket__price__tx"><?=$total?> <?=$data->currency?></div>
            </div>
        </div>
        <div>
            <div class="data-sender__flight order-success__data-sender__flight">
                <div class="data-sender__flight__top">
                    <div class="data-sender__flight__info">
                        <div class="data-sender__flight__info__t"><?=$data->selectFlight->fromCity?> &#8594; <?=$data->selectFlight->toCity?></div>
                        <div class="data-sender__flight__info__timeofflight">Прямой рейс. Время в пути <?=$data->selectFlight->timeFlight?></div>
                        <div class="data-sender__flight__info__time">
                            <div class="data-sender__flight__info__time__clock"><?=$data->selectFlight->timeDeparture?> – <?=$data->selectFlight->timeArrival?></div>
                            <div class="data-sender__flight__info__time__day"><?=date("d", strtotime($date))?> <?=$month?>, <?=$day?></div>
                        </div>
                    </div>
                    <img src="/results/img/sirius.jpg" alt="">
                </div>
                <div class="data-sender__flight__bottom">
                    <div class="data-sender__flight__tarif">
                        тариф:
                        <span><?=$data->tarif?></span>
                    </div>
                    <div class="data-sender__flight__plane">
                        <div class="data-sender__flight__plane__params">
                            <?=$data->plane->class?><br>
                            <?=$data->plane->name?><br>
                            <?=$data->plane->number?>
                        </div>
                        <img src="/results/img/data/pla.jpg" alt="">
                    </div>
                </div>
                <div class="data-sender__enter">Выставлено на продажу: <span>0</span></div>
            </div>
            <div class="data-itog-dop">Дополнительные услуги: <span>0</span></div>
        </div>
    </div>
    <div class="next-step"></div>
</div>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>