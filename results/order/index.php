<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');

$planes = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/results/lib/planes.json');
$planes = json_decode($planes);

$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);

$get_data = '';

if(isset($_GET['data'])){
    $get_data = $_GET['data'];
    $path = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    $path = explode('&', $path);

    $path = $path[0];
}
$url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$APPLICATION->IncludeComponent(
    "ibe:auth.form",
    ".default",
    Array(
        'API_DATA' => $get_data,
        'URL' => $url
    ),
    false
);
global $COMPONENT_AUTH;
isset($_GET['data'])?header("Location: $path"):null;

$dayofweek = array(
    '', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'
);

$_monthsList = array(
    "1"=>"января",
    "2"=>"февраля",
    "3"=>"марта",
    "4"=>"апреля",
    "5"=>"мая",
    "6"=>"июня",
    "7"=>"июля",
    "8"=>"августа",
    "9"=>"сентября",
    "10"=>"октября",
    "11"=>"ноября",
    "12"=>"декабря"
);

$date = $data->selectFlight->date;
$month = $_monthsList[date("n", strtotime($date))];
$day = $dayofweek[date("N", strtotime($date))];

if($COMPONENT_AUTH['API']['user_id']) :
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/balance/");
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('auth_key' => 'awgwagawfaw23cr3x23rc432cfx3f2', 'user_id' => $COMPONENT_AUTH['API']['user_id'], 'contract_id' => $COMPONENT_AUTH['API']['contract_id']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3000);
    $balance = json_decode(curl_exec($ch));
    curl_close($ch);

    if(CModule::IncludeModule('iblock')) {
            $url = 'https://corp.rusline.aero/rest/v1/cabinet_sirius/user/';
            $ch = curl_init($url);
            $curl = [
                "auth_key" => 'awgwagawfaw23cr3x23rc432cfx3f2',
                "user_id" => (int)$COMPONENT_AUTH['API']['user_id']
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curl));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $curlResult = curl_exec($ch);

            curl_close($ch);

            $arr = json_decode($curlResult, true);

            $el = new CIBlockElement;
            $el->SetPropertyValueCode($data->orderid, "BROKER", $arr['email']);

//        $upd = isset($COMPONENT_AUTH['API']['user_id']) ?? $COMPONENT_AUTH['API']['user_id'];
//        $el = new CIBlockElement;
//        $el->SetPropertyValueCode($data->orderid, "BROKER", $upd);
    }
endif;
?>



<div class="breadcrumbs">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper">
    <div class="data-sender">
        <div class="data-sender__ticket">
            <div class="data-sender__ticket__info">
                <div class="data-sender__ticket__info__order">
                    <div class="data-sender__ticket__info__order__t">заказ:</div>
                    <div class="data-sender__ticket__info__order__tx">516478355</div>
                </div>
                <div class="data-sender__ticket__info__code">
                    <div class="data-sender__ticket__info__code__t">код доступа:</div>
                    <div class="data-sender__ticket__info__code__tx"><span>34516D</span></div>
                </div>
            </div>
            <div class="data-sender__ticket__price">
                <div class="data-sender__ticket__price__t">цена:</div>
                <?if($data->tarif === "Место"):
                    $servPrice = 0;
                    foreach($data->service as $value) {
                        $servPrice += $value->ActualPrice;
                    }
                    ?>
                    <div class="data-sender__ticket__price__tx"><?=($data->price + $servPrice)?> <?=$data->currency?></div>
                <?else:?>
                    <div class="data-sender__ticket__price__tx"><?=$data->price?> <?=$data->currency?></div>
                <?endif;?>
            </div>
        </div>
        <div class="data-sender__flight">
            <div class="data-sender__flight__top">
                <div class="data-sender__flight__info">
                    <div class="data-sender__flight__info__t"><?=$data->selectFlight->fromCity?> &#8594; <?=$data->selectFlight->toCity?></div>
                    <div class="data-sender__flight__info__timeofflight">Прямой рейс. Время в пути <?=$data->selectFlight->timeFlight?></div>
                    <div class="data-sender__flight__info__time">
                        <div class="data-sender__flight__info__time__clock"><?=$data->selectFlight->timeDeparture?> – <?=$data->selectFlight->timeArrival?></div>
                        <div class="data-sender__flight__info__time__day"><?=date("d", strtotime($date))?> <?=$month?>, <?=$day?></div>
                    </div>
                </div>
                <img src="/results/img/sirius.jpg" alt="">
            </div>
            <div class="data-sender__flight__bottom">
                <div class="data-sender__flight__tarif">
                    тариф:
                    <span><?=$data->tarif?></span>
                </div>
                <div class="data-sender__flight__plane">
                    <div class="data-sender__flight__plane__params">
                        <?=$planes->{$data->selectFlight->Equipment}->class?><br>
                        <?=$planes->{$data->selectFlight->Equipment}->name?><br>
                        <?=$planes->{$data->selectFlight->Equipment}->number?>
                    </div>
                    <img src="<?=$planes->{$data->selectFlight->Equipment}->smallImage?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="data-passenger">
        <div class="data-passenger__items" data-count="1">
            <div class="data-passenger__t">Пассажир 1</div>
            <div class="data-passenger__row">
                <input type="text" class="form-it data-firstname" placeholder="Имя">
                <input type="text" class="form-it" placeholder="Отчество">
                <input type="text" class="form-it data-lastname" placeholder="Фамилия">
                <input type="radio" name="sex" id="sex1" class="form-it" value="Муж">
                <label for="sex1">Муж</label>
                <input type="radio" name="sex" id="sex2" class="form-it" value="Жен">
                <label for="sex2">Жен</label>
                <input type="text" class="form-it" placeholder="Дата рождения">
            </div>
            <div class="data-passenger__row data-passenger__for">
                <input type="text" class="form-it" placeholder="Страна">
                <input type="text" class="form-it" placeholder="Документ">
                <input type="text" class="form-it" placeholder="Номер">
                <input type="text" class="form-it" placeholder="Годен до">
            </div>
        </div>
        <div class="data-passenger__add">Добавить пассажира</div>
    </div>
    <div class="data-buyer">
        <div class="data-buyer__t">Покупатель</div>
        <div class="data-buyer__tx">Оставьте свой e-mail и телефон, и мы будем сообщать вам обо всех изменениях в вашем бронировании или <br>статусе рейса</div>
        <div class="data-buyer__form">
            <div class="data-buyer__form__row">
                <input type="text" class="form-it" placeholder="Ваше имя">
                <input type="text" class="form-it" placeholder="Телефон">
            </div>
            <div class="data-buyer__form__row">
                <div class="form-it form-it-block">
                    Создать аккаунт
                </div>
                <input type="text" class="form-it" placeholder="E-mail">
            </div>
        </div>
    </div>
    <?if($COMPONENT_AUTH['API']['user_id']):?>
        <div class="data-pay b-data">
            <div class="data-pay__t b-data__t">Метод оплаты</div>
            <div class="b-data__form">
                <div class="b-data__form__row">
                    <input type="radio" name="type-of-pay" id="pay-card" class="type-of-pay" value="card" checked>
                    <label for="pay-card">Банковская карта</label>
                    <input type="radio" name="type-of-pay" id="pay-bill" class="type-of-pay" value="bill" <?=($balance->balance < ($data->price + $servPrice))?"disabled":"";?>>
                    <label for="pay-bill">счет<br><span>Доступно <?=$balance->balance?> <?=$balance->balance_currency?></span></label>
                    <?if($balance->balance < ($data->price + $servPrice) && $balance->credit_limit >= ($data->price + $servPrice)):?>
                        <input type="radio" name="type-of-pay" id="pay-credit" class="type-of-pay" value="bill" <?=($balance->balance < ($data->price + $servPrice))?"disabled":"";?>>
                        <label for="pay-credit">Кредитный лимит<br><span>Доступно <?=$balance->credit_limit?> <?=$balance->credit_limit_currency?></span></label>
                    <?endif;?>
                </div>
            </div>
        </div>
    <?endif;?>
    <div class="next-step">
        <div class="next-step__w">
            <a href="/order/payment/?order=<?=$_GET['order']?>" class="next-step__lk next-step__lk_create next-step__lk_active"><span>Забронировать</span><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></a>
            <div class="next-step__tx">Шаг 3. Опции</div>
        </div>
    </div>
</div>



<script>
    let session = "<?=$data->SessionID?>";
    let fare = <?=$data->Fare?>;
    let token = "<?=$_GET['order']?>";
    let orderid = <?=$data->orderid?>;
</script>


<script src="/results/functions.js"></script>
<script src="/results/order/script.js"></script>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>