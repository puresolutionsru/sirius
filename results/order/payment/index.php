<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');
$APPLICATION->IncludeComponent(
    "ibe:auth.form",
    ".default",
    Array(
        'API_DATA' => '',
    ),
    false
);
global $COMPONENT_AUTH;
$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);


$servPrice = 0;
if($data->tarif == "Место"):
    foreach($data->service as $value) {
        $servPrice += $value->ActualPrice;
    }
endif;
$total = $data->price + $servPrice;

//отправка письма об успешном бронировании рейса
\Bitrix\Main\Mail\Event::send([
    "EVENT_NAME" => "IBE_FLIGHT_BOOKED",
    'MESSAGE_ID' => 15,
    "LID" => "ib",
    "C_FIELDS" => [
        "TITLE" => "Бронирование рейса",
        "EMAIL" => "a.lodyanoy@rusline.aero",
        "FROM_TO" => ''.$data->selectFlight->fromCity.' - '.$data->selectFlight->toCity,
        "NUMBER_FLIGHT" => $data->selectFlight->numberFlight,
        "DATE_TO_DEPARTURE" => $data->selectFlight->date.' '.$data->selectFlight->timeDeparture,
        "TIME_FLIGHT" => $data->selectFlight->timeFlight
    ]
]);

//отправка маршрутной квитанции на почту
\Bitrix\Main\Mail\Event::send([
    "EVENT_NAME" => "IBE_ITINERARY_RECEIPT",
    'MESSAGE_ID' => 16,
    "LID" => "ib",
    "C_FIELDS" => [
        "TITLE" => "Маршрутная квитанция рейса",
        "EMAIL" => "a.lodyanoy@rusline.aero",
        "DEPARTURE_FROM" => $data->selectFlight->fromCity,
        "ARRIVAL_TO" => $data->selectFlight->toCity,
        "NUMBER_FLIGHT" => $data->selectFlight->numberFlight,
        "DATE_TO_DEPARTURE" => $data->selectFlight->date.' '.$data->selectFlight->timeDeparture,
        "DATE_TO_ARRIVAL" => $data->selectFlight->date.' '.$data->selectFlight->timeArrival,
        "TIME_FLIGHT" => $data->selectFlight->timeFlight,
        "PLANE" => $data->plane->name
    ]
]);

if($COMPONENT_AUTH['API']['user_id']) :
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/balance/");
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('auth_key' => 'awgwagawfaw23cr3x23rc432cfx3f2', 'user_id' => $COMPONENT_AUTH['API']['user_id'], 'contract_id' => $COMPONENT_AUTH['API']['contract_id']));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3000);
    $balance = curl_exec($ch);
    $balance = json_decode($balance);
    curl_close($ch);
endif;

$type = $_GET['type'];


if($_GET['orderId'] || $_GET['type'] == 'hold') { ?>
    <div class="toptop"></div>
    <style>
        .toptop {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 10;
            background-color: rgba(0,0,0,.3);
        }
    </style>
    <script>
        var session = "<?=$data->SessionID?>";
        var fare = <?=$data->Fare?>;
        var token = "<?=$_GET['order']?>";
        var firstname = "<?=$data->firstname?>";
        var lastname = "<?=$data->lastname?>";
        var servprice = "<?=$data->servprice?>";
        servprice = Number(servprice);
        var remarkref = "<?=$data->service->RemarkRef?>";
        var servicecode = "<?=$data->service->ServiceCode?>";
        var servicetype = "<?=$data->service->ServiceType?>";
        var place = "<?=$data->seat?>";
        var type = "<?=$_GET['type']?>";
        var user_id = "<?=$COMPONENT_AUTH['API']['user_id']?>";
        var requisites_id = "<?=$COMPONENT_AUTH['API']['requisites_id']?>";
        var contract_id = "<?=$COMPONENT_AUTH['API']['contract_id']?>";
        var amount = "<?=$total?>";
    </script>
    <script>
            // var xhr = new XMLHttpRequest();
            // xhr.open('GET', 'https://sirius-aero.ru/enginetais/Describes/set_flight/?flight='+numberFlight+'&fare='+fare+'&session='+session, true);
            // xhr.send();
            // xhr.onreadystatechange = function() {
            //     if (xhr.readyState != 4) return;
            //     if (xhr.status != 200) {} 
            //     else {
            //         var responseText = JSON.parse(xhr.responseText);
            //         if(responseText.Result === "SUCCESS") {
            if(type == "hold") {
                var hold = new XMLHttpRequest();
                console.log('https://sirius-aero.ru/enginetais/Requests/pay_deposit/?user_id='+user_id+'&requisites_id='+requisites_id+'&contract_id='+contract_id+'&amount='+amount);
                hold.open('GET', 'https://sirius-aero.ru/enginetais/Requests/pay_deposit/?user_id='+user_id+'&requisites_id='+requisites_id+'&contract_id='+contract_id+'&amount='+amount, true);
                hold.send();
                hold.onreadystatechange = function() {
                    var dataPass = hold.responseText;
                    console.log(dataPass);
                }
            }
                        var addpass = new XMLHttpRequest();
                        addpass.open('GET', 'https://sirius-aero.ru/enginetais/Describes/booking_place/?flight=2&firstname='+firstname+'&lastname='+lastname+'&session='+session+'&token='+token, true);
                        addpass.send();
                        addpass.onreadystatechange = function() {
                            if (addpass.readyState != 4) return;
                            if (addpass.status != 200) {}
                            else {
                                var dataPass = JSON.parse(addpass.responseText);
                                console.log(dataPass);

                                let bookingreference = dataPass['GeneralInfo']['@attributes']['BookingReference'];
                                let leadpassenger = null;
                                let paxref = null;
                                if(typeof dataPass['Passengers']['Passenger'].length !== "undefined") {
                                    leadpassenger = dataPass['Passengers']['Passenger'][0]['@attributes']['LastName'];
                                    paxref = "";
                                    for(let i=0; i<dataPass['Passengers']['Passenger'].length; i++) {
                                        paxref += dataPass['Passengers']['Passenger'][i]['@attributes']['PaxRef'] + ",";
                                    }
                                } else {
                                    leadpassenger = dataPass['Passengers']['Passenger']['@attributes']['LastName'];
                                    paxref = dataPass['Passengers']['Passenger']['@attributes']['PaxRef'];
                                }
                                let currency = null;
                                let amount = null;
                                if(typeof dataPass['FareInfo']['Fares']['Fare'].length !== "undefined") {
                                    currency = dataPass['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Currency'];
                                    amount = dataPass['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Total'];
                                } else {
                                    currency = dataPass['FareInfo']['Fares']['Fare']['Price']['@attributes']['Currency'];
                                    amount = dataPass['FareInfo']['Fares']['Fare']['Price']['@attributes']['Total'];
                                }
                                total = (amount+servprice);
                                let flightref = dataPass["Itinerary"]["FlightSegment"]["@attributes"]["FlightRef"];

                                var addserv = new XMLHttpRequest();
                                console.log('https://sirius-aero.ru/enginetais/Describes/add_services/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&session='+session+'&remarkref='+remarkref+'&flightref='+flightref+'&paxref='+paxref+'&servicecode='+servicecode+'&servicetype='+servicetype+'&place='+place+'&token='+token);
                                addserv.open('GET', 'https://sirius-aero.ru/enginetais/Describes/add_services/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&session='+session+'&remarkref='+remarkref+'&flightref='+flightref+'&paxref='+paxref+'&servicecode='+servicecode+'&servicetype='+servicetype+'&place='+place+'&token='+token, true);
                                addserv.send();
                                addserv.onreadystatechange = function() {
                                    if (addserv.readyState != 4) return;
                                    if (addserv.status != 200) {}
                                    else {
                                        var servData = JSON.parse(addserv.responseText);
                                        console.log(servData);

                                        var issue = new XMLHttpRequest();
                                        console.log('https://sirius-aero.ru/enginetais/Describes/issue_ticket/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&session='+session+'&currency='+currency+'&amount='+amount);
                                        issue.open('GET', 'https://sirius-aero.ru/enginetais/Describes/issue_ticket/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&session='+session+'&currency='+currency+'&amount='+amount+'&type='+type, true);
                                        issue.send();
                                        issue.onreadystatechange = function() {
                                            if (issue.readyState != 4) return;
                                            if (issue.status != 200) {}
                                            else {
                                                let tick = JSON.parse(issue.responseText);
                                                console.log(tick);
                                                let ticket = null;
                                                let rloc = null;
                                                if(typeof tick['TicketInfo']['Ticket'].length !== "undefined") {
                                                    ticket = tick['TicketInfo']['Ticket'][0]['@attributes']['TicketNumber'];
                                                    rloc = tick['TicketInfo']['Ticket'][0]['TicketData']['IssueData']['@attributes']['IssueRLOC'];
                                                } else {
                                                    ticket = tick['TicketInfo']['Ticket']['@attributes']['TicketNumber'];
                                                    rloc = tick['TicketInfo']['Ticket']['TicketData']['IssueData']['@attributes']['IssueRLOC'];
                                                }
                                                let sess = tick['@attributes']['SessionID'];

                                                var issueServ = new XMLHttpRequest();
                                                console.log('https://sirius-aero.ru/enginetais/Describes/issue_services/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&currency='+currency+'&amount='+servprice+'&session='+sess+'&token='+token);
                                                issueServ.open('GET', 'https://sirius-aero.ru/enginetais/Describes/issue_services/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&currency='+currency+'&amount='+servprice+'&session='+sess+'&token='+token+'&type='+type, true);
                                                issueServ.send();
                                                issueServ.onreadystatechange = function() {
                                                    if (issueServ.readyState != 4) return;
                                                    if (issueServ.status != 200) {}
                                                    else {
                                                        let serv = JSON.parse(issueServ.responseText);
                                                        console.log(serv);
                                                        let lk = 'https://ibe.sirius-aero.ru/order/success/?order=' + token + '&ticket=' + ticket + '&loc=' + rloc;
                                                        document.location.href = lk;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
            // }
            //     }
            // }
    </script>
<?}?>
<div class="breadcrumbs">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper order-payment">
    <!-- <div class="bank-card">
        <div class="bank-card__top">
            <div class="bank-card__types">
                <img src="/bitrix/templates/ibe/img/visa.svg" alt="">
                <img src="/bitrix/templates/ibe/img/master.svg" alt="">
                <img src="/bitrix/templates/ibe/img/f.svg" alt="">
                <img src="/bitrix/templates/ibe/img/jcb.svg" alt="">
                <img src="/bitrix/templates/ibe/img/maestro.svg" alt="">
                <img src="/bitrix/templates/ibe/img/mir.svg" alt="">
            </div>
            <div>
                <input type="text" placeholder="Номер карты">
            </div>
            <div class="bank-card__date">
                <input class="small" type="text" placeholder="мм"> / <input class="small" type="text" placeholder="гг">
            </div>
            <div>
                <input type="text" placeholder="ИМЯ И ФАМИЛИЯ, КАК НА КАРТЕ">
            </div>
        </div>
        <div class="bank-card__bottom">
            <div class="bank-card__line"></div>
            <div class="bank-card__cvc">CVV2 / CVC2</div>
            <div class="bank-card__cvc-code">
                <input type="password" placeholder="">
            </div>
        </div>
    </div>
    <div class="payment-info">
        <div class="payment-info__data">
            <div class="payment-info__data__i">
                получатель платежа:
                <span>ООО АК «Сириус-Аэро»</span>
            </div>
            <div class="payment-info__data__i">
                сумма к оплате:
                <span class="price"><?=$data->onlyPrice?> €</span>
            </div>
        </div>
        <div class="payment-info__btn">Оплатить</div>
    </div> -->
</div>

<script>
    var session = "<?=$data->SessionID?>";
    var fare = <?=$data->Fare?>;
    var token = "<?=$_GET['order']?>";
</script>
<script src="/results/order/payment/script.js"></script>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');