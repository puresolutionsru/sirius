<?
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

$userName = "T7717294942-api";
$password = "T7717294942";
$address = "https://3dsec.sberbank.ru/payment/rest/register.do";
$addressCheck = "https://3dsec.sberbank.ru/payment/rest/getOrderStatusExtended.do";


$token = $_GET['token'];
$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$token.'.json');
$data = json_decode($data);

$orderNumber = file_get_contents('https://ibe.sirius-aero.ru/order/payment/number.txt');
$amount = ($data->price + $data->servprice) * 89 * 100;
$currency = 643;
$returnUrl = "https://ibe.sirius-aero.ru/order/payment/?order=".$token;

// отправляем запрос на оплату
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $address);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, "userName=".$userName."&password=".$password."&orderNumber=".$orderNumber."&amount=".$amount."&currency=".$currency."&returnUrl=".$returnUrl);
curl_setopt($ch, CURLOPT_HEADER, 0);
$regOrder = curl_exec($ch);
$regOrder = json_decode($regOrder);
curl_close($ch);

use Bitrix\Main\Application;

$api = Application::getInstance()->getContext()->getRequest()->getCookie("API_DATA");
$output = [];

if(!empty($api)){
    $API = openssl_decrypt(base64_decode($api), 'BF-CBC', 'ab86d144e3f080b61c7c2e43');
    parse_str($API, $output);

    //проверяем оплату
    $check = curl_init();
    curl_setopt($check, CURLOPT_URL, $addressCheck);
    curl_setopt($check, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($check, CURLOPT_POST, true);
    curl_setopt($check, CURLOPT_POSTFIELDS, "userName=".$userName."&password=".$password."&orderId=".$regOrder->orderId);
    curl_setopt($check, CURLOPT_HEADER, 0);
    $checkOrder = curl_exec($check);
    $checkOrder = json_decode($checkOrder, true);

    //добавляем данные по заказу в ЛК брокера
    $err = $checkOrder['errorMessage'];

    if($err === 'Успешно'){
        $auth_key = 'awgwagawfaw23cr3x23rc432cfx3f2';
        $user_id =  $output['user_id'];
        $contract_id =  $output['contract_id'];
        $arr = [
            'auth_key' => $auth_key,
            'user_id' => $user_id,
            'contract_id' => $contract_id,
            'service_id' => 2853880,
            'status' => 'in_progress',
            'type_operation' => 'debit',
            'form_of_payment' => 'card',
            'amount' => $data->price,
            'card_number' => $checkOrder['cardAuthInfo']['pan'],
            'card_owner' => $checkOrder['cardAuthInfo']['cardholderName'],
            'card_date' => $checkOrder['authDateTime']
        ];
        $chAdd = curl_init();
        curl_setopt($chAdd, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/billing/add/");
        curl_setopt($chAdd, CURLOPT_POSTFIELDS, $arr);
        curl_setopt($chAdd, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chAdd, CURLOPT_CONNECTTIMEOUT, 3000);
        $billing = json_decode(curl_exec($chAdd), true);
        $billingId = $billing['billing_id'];
        curl_close($chAdd);

        $status = 'cancelled';
        if($billingId) {
            $status = 'success';
        }
        $arrChangeStatus = [
            'auth_key' => $auth_key,
            'user_id' => $user_id,
            'status' => $status
        ];

    //меняем статус заказа в ЛК

        $chs = curl_init();
        curl_setopt($chs, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/billing/edit/$billingId");
        curl_setopt($chs, CURLOPT_POSTFIELDS, $arrChangeStatus);
        curl_setopt($chs, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($chs, CURLOPT_CONNECTTIMEOUT, 3000);
        curl_exec($chs);
        curl_close($chs);
    }
}


// номер заказа
$orderNumber++;
file_put_contents($_SERVER['DOCUMENT_ROOT'].'/order/payment/number.txt', $orderNumber);

echo $regOrder->formUrl;