function templateItems() {
    console.log(dataCome);

    for(let index in dataCome) {
        let info = dataCome[index]['SEAT']['ShopOption']['ItineraryOption'];

        listFlightsData[index] = {
            'date': day+'.'+month+'.'+year,
            'fromCity': fromCity,
            'toCity': toCity,
            'Equipment': dataCome[index]['equipment'],
            'timeFlight': timeInFlight(info["@attributes"]["TravelTime"]),
            'numberFlight': info["FlightSegment"]["@attributes"]["Flight"],
            'timeDeparture': timePerUnix(info["FlightSegment"]["Departure"]["@attributes"]["Time"]),
            'timeArrival': timePerUnix(info["FlightSegment"]["Arrival"]["@attributes"]["Time"]),
            'airportDeparture': info["FlightSegment"]["Departure"]["@attributes"]["Airport"],
            'airportArrival': info["FlightSegment"]["Arrival"]["@attributes"]["Airport"],
        };
        let thisPlane = planes[listFlightsData[index].Equipment];
        let plane = planesData[listFlightsData[index].Equipment];


        let html = '<div class="vydacha-items__i" data-type="Shuttle" data-flight="'+index+'">'+
                        '<div class="vydacha-items__i__row vydacha-items__i__top">'+
                            // инфо о самолете и направлении
                            '<div class="vydacha-items__i__cell-left vydacha-items__i__info vydacha-items__i__info-flight">'+
                                '<div class="vydacha-items__i__info__plane">'+
                                    '<img src="'+plane.smallImage+'" alt="">'+
                                    '<div>'+plane.name+'</div>'+
                                    '<div>'+plane.number+'</div>'+
                                '</div>'+
                                '<div class="vydacha-items__i__info__time">'+
                                    '<div class="vydacha-items__i__info__time__top">'+
                                        '<div class="vydacha-items__i__info__time__t">'+listFlightsData[index].timeDeparture+'</div>'+
                                        '<div class="vydacha-items__i__info__time__direct">№ рейса: '+listFlightsData[index].numberFlight+'</div>'+
                                        '<div class="vydacha-items__i__info__time__t align-right">'+listFlightsData[index].timeArrival+'</div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__info__time__center">'+
                                        '<div class="vydacha-items__i__info__time__line"></div>'+
                                        '<div class="vydacha-items__i__info__time__img-plane"></div>'+
                                        '<div class="vydacha-items__i__info__time__line"></div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__info__time__bottom">'+
                                        '<div class="vydacha-items__i__info__time__code">'+listFlightsData[index].airportDeparture+'</div>'+
                                        '<div class="vydacha-items__i__info__time__path">в пути '+listFlightsData[index].timeFlight+'</div>'+
                                        '<div class="vydacha-items__i__info__time__code align-right">'+listFlightsData[index].airportArrival+'</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            // инфо по брендам с ценой
                            '<div class="vydacha-items__i__cell-right vydacha-items__i__tarifs">';
                                if(typeof dataCome[index]['SEAT'] !== "undefined") {
                                    let session = dataCome[index]['SEAT']['attributes']['SessionID'];
                                    let option = dataCome[index]['SEAT']['ShopOption']['@attributes']['OptionRef'];
                                    let itinerary = dataCome[index]['SEAT']['ShopOption']['ItineraryOption']['@attributes']['ItineraryRef'];
                                    let currencyBase = null;
                                    let price = null;
                                    let fare = null;
                                    if(typeof dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare'].length !== "undefined") {
                                        currencyBase = currency[dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['@attributes']['FareRef'];
                                    } else {
                                        currencyBase = currency[dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['SEAT']['ShopOption']['FareInfo']['Fares']['Fare']['@attributes']['FareRef'];
                                    }

                                    html +=
                                    '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_sh" data-ref="'+fare+'" data-session="'+session+'" data-itineraryref="'+itinerary+'" data-optionref="'+option+'"  data-id="#configration-seat-'+index+'" data-tarif="Место" data-price="'+price+'" data-currency="'+currencyBase+'" data-price-serv="0">'+
                                        '<input type="radio" name="" id="shuttle" class="vydacha-items__i__tarifs__i__radio">'+
                                        '<label for="shuttle" class="vydacha-items__i__tarifs__i__label"></label>'+
                                        '<div class="vydacha-items__i__tarifs__i__t">Место</div>'+
                                        '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(price)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                    '</div>';
                                } else {
                                    html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Место»</div>';
                                }

                                if(typeof dataCome[index]['CABIN'] !== "undefined") {
                                    let session = dataCome[index]['CABIN']['attributes']['SessionID'];
                                    let option = dataCome[index]['CABIN']['ShopOption']['@attributes']['OptionRef'];
                                    let itinerary = dataCome[index]['CABIN']['ShopOption']['ItineraryOption']['@attributes']['ItineraryRef'];
                                    let currencyBase = null;
                                    let price = null;
                                    let fare = null;
                                    if(typeof dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare'].length !== "undefined") {
                                        currencyBase = currency[dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare'][0]['@attributes']['FareRef'];
                                    } else {
                                        currencyBase = currency[dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['CABIN']['ShopOption']['FareInfo']['Fares']['Fare']['@attributes']['FareRef'];
                                    }

                                    html +=
                                    '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_em" data-id="#configration-cabin-'+index+'" data-ref="'+fare+'" data-session="'+session+'" data-itineraryref="'+itinerary+'" data-optionref="'+option+'" data-tarif="Каюта" data-price="'+price+'" data-currency="'+currencyBase+'" data-price-serv="0">'+
                                        '<input type="radio" name="" id="shuttle" class="vydacha-items__i__tarifs__i__radio">'+
                                        '<label for="shuttle" class="vydacha-items__i__tarifs__i__label"></label>'+
                                        '<div class="vydacha-items__i__tarifs__i__t">Каюта</div>'+
                                        '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(price)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                    '</div>';
                                } else {
                                    html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Каюта»</div>';
                                }

                                if(typeof dataCome[index]['AIRCRAFT'] !== "undefined") {
                                    let session = dataCome[index]['AIRCRAFT']['attributes']['SessionID'];
                                    let option = dataCome[index]['AIRCRAFT']['ShopOption']['@attributes']['OptionRef'];
                                    let itinerary = dataCome[index]['AIRCRAFT']['ShopOption']['ItineraryOption']['@attributes']['ItineraryRef'];
                                    let currencyBase = null;
                                    let price = null;
                                    let fare = null;
                                    if(typeof dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare'].length !== "undefined") {
                                        currencyBase = currency[dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare'][0]['@attributes']['FareRef'];
                                    } else {
                                        currencyBase = currency[dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Currency']];
                                        price = Number(dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare']['Price']['@attributes']['Total']);
                                        fare = dataCome[index]['AIRCRAFT']['ShopOption']['FareInfo']['Fares']['Fare']['@attributes']['FareRef'];
                                    }

                                    html +=
                                    '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_ai" data-ref="'+fare+'" data-session="'+session+'" data-itineraryref="'+itinerary+'" data-optionref="'+option+'"  data-id="#configration-aircraft-'+index+'" data-tarif="Салон" data-price="'+price+'" data-currency="'+currencyBase+'" data-price-serv="0">'+
                                        '<input type="radio" name="" id="shuttle" class="vydacha-items__i__tarifs__i__radio">'+
                                        '<label for="shuttle" class="vydacha-items__i__tarifs__i__label"></label>'+
                                        '<div class="vydacha-items__i__tarifs__i__t">Салон</div>'+
                                        '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(price)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                    '</div>';
                                } else {
                                    html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Салон»</div>';
                                }
                            html += '</div>'+
                        '</div>'+












































                        










                            '<div class="vydacha-items__i__bottom">'+
                                '<div class="vydacha-items__i__row vydacha-items__i__about-flight">'+
                                    '<div class="vydacha-items__i__cell-left vydacha-items__i__about-flight__left">'+
                                        '<div class="vydacha-items__i__info-plane">'+
                                            '<div class="vydacha-items__i__info-plane__transfer-company"><img src="/bitrix/templates/ibe/img/logo.svg" alt=""></div>'+
                                            '<div class="vydacha-items__i__info-plane__img-plane"><a href="'+plane.smallPhoto+'" class="fancybox"><img src="'+plane.smallPhotoResize+'" alt=""></a></div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__info-fly">'+
                                            '<div class="vydacha-items__i__info-fly__top">'+
                                                '<span>вылет</span>'+
                                                '<span>прилет</span>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__info-fly__bottom">'+
                                                '<span class="icon-from">'+listFlightsData[index].fromCity+'</span>'+
                                                '<span class="icon-centerroute"></span>'+
                                                '<span class="icon-to">'+listFlightsData[index].toCity+'</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__cell-right vydacha-items__i__tarifs-list">'+
                                        // Добавляем услуги для тарифов из файла для более быстрой загрузки, после полной загрузки отправим ajax для сравнения и обновления файла
                                        '<div class="vydacha-items__i__tarifs-list__i" id="services-seat">';
                                            if(typeof dataCome[index]['SEAT'] !== "undefined") {
                                                for(i in dataCome[index]['SEAT'].Services.ServiceInfo) {
                                                    let typeTar = '';
                                                    let show = true;
                                                    if(dataCome[index]['SEAT'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Included') {

                                                    } else if (dataCome[index]['SEAT'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Available') {
                                                        typeTar = 'list-services_pay';
                                                    } else {
                                                        show = false;
                                                    }
                                                    if(show) {
                                                        html += '<div class="list-services '+typeTar+'">'+dataCome[index]['SEAT'].Services.ServiceInfo[i].ServiceDetail.Name.toLowerCase()+'</div>';
                                                    }
                                                }
                                            }
                                        html += '</div>'+
                                        '<div class="vydacha-items__i__tarifs-list__i">';
                                            if(typeof dataCome[index]['CABIN'] !== "undefined") {
                                                // if(typeof dataCome[index]['CABIN'].length !== "undefined") {
                                                    for(i in dataCome[index]['CABIN'].Services.ServiceInfo) {
                                                        let typeTar = '';
                                                        let show = true;
                                                        if(dataCome[index]['CABIN'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Included') {

                                                        } else if (dataCome[index]['CABIN'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Available') {
                                                            typeTar = 'list-services_pay';
                                                        } else {
                                                            show = false;
                                                        }
                                                        if(show) {
                                                            html += '<div class="list-services '+typeTar+'">'+dataCome[index]['CABIN'].Services.ServiceInfo[i].ServiceDetail.Name.toLowerCase()+'</div>';
                                                        }
                                                    }
                                                // }
                                            }
                                        html += '</div>'+
                                        '<div class="vydacha-items__i__tarifs-list__i">';
                                            if(typeof dataCome[index]['AIRCRAFT'] !== "undefined") {
                                                for(i in dataCome[index]['AIRCRAFT'].Services.ServiceInfo) {
                                                    let typeTar = '';
                                                    let show = true;
                                                    if(dataCome[index]['AIRCRAFT'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Included') {

                                                    } else if (dataCome[index]['AIRCRAFT'].ServiceSets.ServiceSet.Service[i]['@attributes'].Status == 'Available') {
                                                        typeTar = 'list-services_pay';
                                                    } else {
                                                        show = false;
                                                    }
                                                    if(show) {
                                                        html += '<div class="list-services '+typeTar+'">'+dataCome[index]['AIRCRAFT'].Services.ServiceInfo[i].ServiceDetail.Name.toLowerCase()+'</div>';
                                                    }
                                                }
                                            }
                                        html += '</div>'+
                                    '</div>'+
                                '</div>'+












                                '<div class="vydacha-items__i__row vydacha-items__i__about-plane">'+
                                    '<div class="vydacha-items__i__cell-left">'+
                                        '<div class="vydacha-items__i__about-plane__gallery">';
                                            for(let i in plane.photos) {
                                                html += '<a href="'+plane.photos[i]+'" class="fancybox" data-fancybox="gallery-'+index+'"><img src="'+plane.photosResize[i]+'" alt=""></a>';
                                            }
                                        html += '</div>'+
                                        '<div class="vydacha-items__i__about-plane__tx vydacha-items__i__about-plane__tx_configration-seat-'+index+' vydacha-items__i__about-plane__tx_active">'+plane.description+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__tx vydacha-items__i__about-plane__tx_configration-cabin-'+index+'">'+plane.descriptionCabin+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__tx vydacha-items__i__about-plane__tx_configration-aircraft-'+index+'">'+plane.descriptionAircraft+'</div>'+
                                        '<div class="cl"></div>'+
                                        '<a href="'+plane.detailUrl+'" target="_blank" class="btn-all">Подробнее</a>'+
                                    '</div>'+
                                    // Конфигурация бренда место
                                    '<div class="vydacha-items__i__cell-right configuration-item configuration-item_active configuration-item_first" id="configration-seat-'+index+'">'+
                                        '<div class="vydacha-items__i__about-plane__t">Конфигурация салона '+plane.name+' '+plane.number+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__change">Ваш выбор: «<span>Место</span>»</div>'+
                                        '<div class="vydacha-items__i__about-plane__salon">'+
                                            '<div class="vydacha-items__i__about-plane__salon__img">';
                                                for(numberSeat in thisPlane.map) {
                                                    html += '<div class="vydacha-items__i__about-plane__salon__place vydacha-items__i__about-plane__salon__place_available vydacha-items__i__about-plane__salon__place_'+thisPlane.map[numberSeat].route+'" data-number="'+numberSeat+'" style="top:'+thisPlane.map[numberSeat].top+'px;left:'+thisPlane.map[numberSeat].left+'px;"></div>';
                                                }
                                                html += '<img src="'+plane.configurationImage+'" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__img__loader vydacha-items__i__about-plane__salon__img__loader_active" style="left:'+thisPlane.aircraft.left+';top:'+thisPlane.aircraft.top+';width:'+thisPlane.aircraft.width+';height:'+thisPlane.aircraft.height+';"><div class="lds-hourglass"></div></div>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__about-plane__ch">'+
                                                '<img src="/bitrix/templates/ibe/img/people.svg" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__tx">Количество мест</div>'+
                                                '<div class="vydacha-items__i__about-plane__salon__count">'+plane.count+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__about-plane__available">Свободных мест: <span class="vydacha-items__i__about-plane__available__count">Х</span> <span>/ '+plane.count+'</span></div>'+
                                    '</div>'+
                                    // Конфигурация бренда каюта
                                    '<div class="vydacha-items__i__cell-right configuration-item configuration-item_cabin" id="configration-cabin-'+index+'">'+
                                        '<div class="vydacha-items__i__about-plane__t">Конфигурация салона '+plane.name+' '+plane.number+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__change">Ваш выбор: «<span>Каюта</span>»</div>'+
                                        '<div class="vydacha-items__i__about-plane__salon">'+
                                            '<div class="vydacha-items__i__about-plane__salon__img">';
                                                for(numberSeat in thisPlane.map) {
                                                    html += '<div class="vydacha-items__i__about-plane__salon__place vydacha-items__i__about-plane__salon__place_available vydacha-items__i__about-plane__salon__place_'+thisPlane.map[numberSeat].route+'" data-number="'+numberSeat+'" style="top:'+thisPlane.map[numberSeat].top+'px;left:'+thisPlane.map[numberSeat].left+'px;"></div>';
                                                }
                                                html += '<img src="'+plane.configurationImage+'" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__img__loader vydacha-items__i__about-plane__salon__img__loader_active" style="left:'+thisPlane.aircraft.left+';top:'+thisPlane.aircraft.top+';width:'+thisPlane.aircraft.width+';height:'+thisPlane.aircraft.height+';"><div class="lds-hourglass"></div></div>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__about-plane__ch">'+
                                                '<img src="/bitrix/templates/ibe/img/people.svg" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__tx">Количество мест</div>'+
                                                '<div class="vydacha-items__i__about-plane__salon__count">'+plane.count+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__about-plane__available">Свободных кают: <span class="vydacha-items__i__about-plane__available__count">Х</span> <span>/ '+plane.countCabin+'</span></div>'+
                                    '</div>'+
                                    // Конфигурация бренда салон
                                    '<div class="vydacha-items__i__cell-right configuration-item" id="configration-aircraft-'+index+'">'+
                                        '<div class="vydacha-items__i__about-plane__t">Конфигурация салона '+plane.name+' '+plane.number+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__change">Ваш выбор: «<span>Салон</span>»</div>'+
                                        '<div class="vydacha-items__i__about-plane__salon">'+
                                            '<div class="vydacha-items__i__about-plane__salon__img">';
                                                for(numberSeat in thisPlane.map) {
                                                    html += '<div class="vydacha-items__i__about-plane__salon__place vydacha-items__i__about-plane__salon__place_selected vydacha-items__i__about-plane__salon__place_'+thisPlane.map[numberSeat].route+'" style="top:'+thisPlane.map[numberSeat].top+'px;left:'+thisPlane.map[numberSeat].left+'px;"></div>';
                                                }
                                                html += '<img src="'+plane.configurationImage+'" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__img__area vydacha-items__i__about-plane__salon__img__area_selected" style="left:'+thisPlane.aircraft.left+';top:'+thisPlane.aircraft.top+';width:'+thisPlane.aircraft.width+';height:'+thisPlane.aircraft.height+';"></div>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__about-plane__ch">'+
                                                '<img src="/bitrix/templates/ibe/img/people.svg" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__tx">Количество мест</div>'+
                                                '<div class="vydacha-items__i__about-plane__salon__count">'+plane.count+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__about-plane__available">В салоне <span>'+plane.count+'</span> мест и <span>'+plane.countCabin+'</span> каюты</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="vydacha-items__i__open" data-flight="'+index+'"></div>'+
                        '</div>';

            itemsblock.innerHTML += html;
    }

    seatMap(0);
}