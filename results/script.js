let itemsblock = document.getElementsByClassName('vydacha-items')[0]; // контейнер для списка рейсов
let companyCode = "7R"; // код авиакомпании
let dataCome = Array(); // сформированные результаты поиска
let listFlightsData = Array();
let countSeatFlight = {};
let services = {}; // информация об rfisk
let selectData = {}; // данные выбранного места
let planesData = null;
let arrayKeyObject = Array();






/**
 * Получаем данные по выбранному направлению из системы
 */
function loadData() {
    readTextFile("https://sirius-aero.ru/results/lib/planes.json", function(text){
        planesData = JSON.parse(text);
    });
    // получаем места
    var xhr = new XMLHttpRequest();
    var params = {};
    params.company  = companyCode;
    params.from     = from;
    params.to       = to;
    params.date     = formatDate;
    params.brand    = "SEAT";
    params.count    = 1;
    xhr.open('POST', 'https://sirius-aero.ru/enginetais/Describes/get_results/?params='+JSON.stringify(params), true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {}
        else {
            let data = JSON.parse(xhr.responseText);
            createParams("SEAT", data);
            for(let index in dataCome) {
                arrayKeyObject.push(index);
            }

            createEquipment(0);
        }
    }
}
loadData();
createEquipment = function(el) {
    if(typeof arrayKeyObject[el] !== "undefined") {
        var equi = {};
        equi.SessionID      = dataCome[arrayKeyObject[el]]['SEAT']['attributes']['SessionID'];
        equi.OptionRef      = dataCome[arrayKeyObject[el]]['SEAT']['ShopOption']['@attributes']['OptionRef'];
        equi.ItineraryRef   = dataCome[arrayKeyObject[el]]['SEAT']['ShopOption']['ItineraryOption']['@attributes']['ItineraryRef'];
        var equipmentType   = new XMLHttpRequest();
        equipmentType.open('POST', 'https://sirius-aero.ru/enginetais/Describes/get_equipment/?params='+JSON.stringify(equi), true);
        equipmentType.send();
        equipmentType.onreadystatechange = function() {
            if (equipmentType.readyState != 4) return;
            if (equipmentType.status != 200) {}
            else {
                let data = JSON.parse(equipmentType.responseText);
                dataCome[arrayKeyObject[el]]["equipment"] = data["Itinerary"]["FlightSegment"]["@attributes"]["PlaneMapName"];
                createEquipment(++el);
            }
        }
    } else {
        loadDataContinue();
    }
}
function loadDataContinue() {
    var params = {};
    params.company  = companyCode;
    params.from     = from;
    params.to       = to;
    params.date     = formatDate;
    // получаем салон
    for(let i in countSeatFlight) {
        // console.log(i);
    }
    var xhrSalon = new XMLHttpRequest();
    params.brand = "AIRCRAFT";
    params.count = 8;
    xhrSalon.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_results/?params='+JSON.stringify(params), true);
    xhrSalon.send();
    xhrSalon.onreadystatechange = function() {
        if (xhrSalon.readyState != 4) return;
        if (xhrSalon.status != 200) {}
        else {
            let data = JSON.parse(xhrSalon.responseText);
            createParams("AIRCRAFT", data);

            // получаем каюты
            var xhrCabin = new XMLHttpRequest();
            params.brand = "CABIN";
            params.count = 4;
            xhrCabin.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_results/?params='+JSON.stringify(params), true);
            xhrCabin.send();
            xhrCabin.onreadystatechange = function() {
                if (xhrCabin.readyState != 4) return;
                if (xhrCabin.status != 200) {}
                else {
                    let data = JSON.parse(xhrCabin.responseText);
                    createParams("CABIN", data);

                    // по завершению формируем html
                    $(".main-preloader").addClass("main-preloader_close");
                    templateItems();

                    $(".fancybox").fancybox({
                        openEffect	: 'none',
                        closeEffect	: 'none'
                    });
                }
            }
        }
    }
}






function createParams(type, data) {
    if(data['ShopOptions'] !== null && data['ShopOptions'].length !== 0) {
        if(typeof data['ShopOptions']['ShopOption'].length !== "undefined") {
            for(let ShopOptionIndex in data['ShopOptions']['ShopOption']) {
                if(typeof data['ShopOptions']['ShopOption'][ShopOptionIndex]['ItineraryOptions']['ItineraryOption'].length !== "undefined") {
                    for(let ItineraryOptionIndex in data['ShopOptions']['ShopOption'][ShopOptionIndex]['ItineraryOptions']['ItineraryOption']) {
                        addData(ShopOptionIndex, ItineraryOptionIndex, type, data);
                        if(type==="SEAT") {
                            countSeatFlight[data['ShopOptions']['ShopOption'][ShopOptionIndex]['ItineraryOptions']['ItineraryOption'][ItineraryOptionIndex]['FlightSegment']['@attributes']['Equipment']] = true;
                        }
                    }
                } else {
                    addData(ShopOptionIndex, null, type, data);
                    if(type==="SEAT") {
                        countSeatFlight[data['ShopOptions']['ShopOption'][ShopOptionIndex]['ItineraryOptions']['ItineraryOption']['FlightSegment']['@attributes']['Equipment']] = true;
                    }
                }
            }
        } else {
            if(typeof data['ShopOptions']['ShopOption']['ItineraryOptions']['ItineraryOption'].length !== "undefined") {
                for(let ItineraryOptionIndex in data['ShopOptions']['ShopOption']['ItineraryOptions']['ItineraryOption']) {
                    addData(null, ItineraryOptionIndex, type, data);
                    if(type==="SEAT") {
                        countSeatFlight[data['ShopOptions']['ShopOption']['ItineraryOptions']['ItineraryOption'][ItineraryOptionIndex]['FlightSegment']['@attributes']['Equipment']] = true;
                    }
                }
            } else {
                addData(null, null, type, data);
                if(type==="SEAT") {
                    countSeatFlight[data['ShopOptions']['ShopOption']['ItineraryOptions']['ItineraryOption']['FlightSegment']['@attributes']['Equipment']] = true;
                }
            }
        }
    }
}
function addData(shop, itinerary, type, data) {
    let pathShop = data['ShopOptions']['ShopOption'];
    if(shop) {
        pathShop = pathShop[shop];
    }
    let path = pathShop['ItineraryOptions']['ItineraryOption'];
    if(itinerary) {
        path = path[itinerary];
    }
    let flight = path['FlightSegment']['@attributes']['Flight'];

    if(!dataCome[flight]) {
        dataCome[flight] = Array();
    }
    dataCome[flight][type] = Array();
    dataCome[flight][type]['ServiceSets'] = data['ServiceSets'];
    dataCome[flight][type]['Services'] = data['Services'];
    dataCome[flight][type]['attributes'] = data['attributes'];
    dataCome[flight][type]['ShopOption'] = Array();
    dataCome[flight][type]['ShopOption']['@attributes'] = pathShop['@attributes'];
    dataCome[flight][type]['ShopOption']['BookingGuidelines'] = pathShop['BookingGuidelines'];
    dataCome[flight][type]['ShopOption']['FareInfo'] = pathShop['FareInfo'];
    dataCome[flight][type]['ShopOption']['ItineraryOption'] = path;
}

function setSlider(selectContainer) {
    let gallery = $(selectContainer).find(".vydacha-items__i__about-plane__gallery");
    if(!gallery.hasClass("slick-slider")) {
        $(gallery).slick({
            dots: false,
            infinite: true,
            speed: 200,
            slidesToShow: 1
        });
    }
}








































function seatMap(num) {
    let el = $('.vydacha-items__i__tarifs__i_sh:eq('+num+')');
    if(typeof el[0] !== "undefined") {
        let id = el.attr("data-id");
        let flight = el.parents(".vydacha-items__i").attr("data-flight");
        let session = el.attr("data-session");
        let itineraryref = el.attr("data-itineraryref");
        let option = el.attr("data-optionref");

        var xhr = new XMLHttpRequest();
        let params = {};
        params.SessionID = session;
        params.ItineraryRef = itineraryref;
        params.OptionRef = option;
        xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_flight_for_seat/?params='+JSON.stringify(params), true);
        xhr.send();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {}
            else {
                let data = JSON.parse(xhr.responseText);

                var xhrServices = new XMLHttpRequest();
                xhrServices.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_air_services/?params='+JSON.stringify(params), true);
                xhrServices.send();
                xhrServices.onreadystatechange = function() {
                    if (xhrServices.readyState != 4) return;
                    if (xhrServices.status != 200) {}
                    else {
                        let responseText = JSON.parse(xhrServices.responseText);
                        let remarks = {};
                        let setSeat = {
                            "total": null,
                            "seat": null,
                            "price": null,
                        };
                        let countAvSeat = 0;
                        for(let index in responseText['SIG_AirServicesRS']['Remarks']['Remark']) {
                            remarks[responseText['SIG_AirServicesRS']['Remarks']['Remark'][index]["@attributes"]["ServiceRef"]] = responseText['SIG_AirServicesRS']['Remarks']['Remark'][index];
                            remarks[responseText['SIG_AirServicesRS']['Remarks']['Remark'][index]["@attributes"]["ServiceRef"]]["@attributes"]["RemarkRef"] = index;
                        }

                        for(let element in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row']) {
                            if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['@attributes']['RowProps'] != "NoRow") {
                                for(let i in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat']) {
                                    if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatAvailability'] !== "Available") {
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel']+"']").removeClass("vydacha-items__i__about-plane__salon__place_available");
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel']+"']").addClass("vydacha-items__i__about-plane__salon__place_noavailable");
                                    } else {
                                        let price = null;
                                        let total = $(el).attr("data-price");
                                        let service = responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['ServiceRef'];
                                        let SeatLabel = responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel'];

                                        if(typeof service !== "undefined") {
                                            price = Number(remarks[service]["@attributes"]["Price"]);
                                            total = Number($(el).attr("data-price"))+price;
                                        }
                                        if(setSeat.total >= total || setSeat.total == null) {
                                            setSeat.price = $(el).attr("data-price");
                                            setSeat.seat = SeatLabel;
                                            setSeat.total = total;
                                        }
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+SeatLabel+"']")
                                            .attr("data-price", price)
                                            .attr("data-services", service)
                                            .html("<span class='vydacha-items__i__about-plane__salon__place__hover'></span><span class='vydacha-items__i__about-plane__salon__place__price'>"+total+" "+$(el).attr("data-currency")+"</span>");

                                        if(typeof services[flight] === "undefined") {
                                            services[flight] = {};
                                        }
                                        if(typeof services[flight]["SEAT"] === "undefined") {
                                            services[flight]["SEAT"] = {};
                                        }
                                        if(typeof services[flight]["SEAT"][SeatLabel] === "undefined") {
                                            services[flight]["SEAT"][SeatLabel] = {};
                                        }

                                        if(service) {
                                            services[flight]["SEAT"][SeatLabel] = remarks[service]["@attributes"];
                                        }

                                        countAvSeat++;
                                    }
                                }
                            }
                        }

                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+setSeat.seat+"']")
                            .attr("data-price", "0")
                            .html("<span class='vydacha-items__i__about-plane__salon__place__hover'></span><span class='vydacha-items__i__about-plane__salon__place__price'>"+setSeat.price+" "+$(el).attr("data-currency")+"</span>")
                            .attr("data-services", "")
                            .addClass("vydacha-items__i__about-plane__salon__place_selected");

                        $(id + " .vydacha-items__i__about-plane__available__count").html(countAvSeat);
                        $(id + " .vydacha-items__i__about-plane__salon__img__loader").removeClass("vydacha-items__i__about-plane__salon__img__loader_active");

                        num++;
                        seatMap(num);
                    }
                }
            }
        }
    } else {
        cabinMap(0);
    }
}
function cabinMap(num) {
    let el = $('.vydacha-items__i__tarifs__i_em:eq('+num+')');
    if(typeof el[0] !== "undefined") {
        let id = el.attr("data-id");
        let flight = el.parents(".vydacha-items__i").attr("data-flight");
        let session = el.attr("data-session");
        let itineraryref = el.attr("data-itineraryref");
        let option = el.attr("data-optionref");

        var xhr = new XMLHttpRequest();
        let params = {};
        params.SessionID = session;
        params.ItineraryRef = itineraryref;
        params.OptionRef = option;
        xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_flight_for_seat/?params='+JSON.stringify(params), true);
        xhr.send();
        xhr.onreadystatechange = function() {
            if (xhr.readyState != 4) return;
            if (xhr.status != 200) {}
            else {
                let data = JSON.parse(xhr.responseText);

                var xhrServices = new XMLHttpRequest();
                xhrServices.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_air_services/?params='+JSON.stringify(params), true);
                xhrServices.send();
                xhrServices.onreadystatechange = function() {
                    if (xhrServices.readyState != 4) return;
                    if (xhrServices.status != 200) {}
                    else {
                        let responseText = JSON.parse(xhrServices.responseText);
                        let remarks = {};
                        let ava = {};

                        for(let index in responseText['SIG_AirServicesRS']['Remarks']['Remark']) {
                            remarks[responseText['SIG_AirServicesRS']['Remarks']['Remark'][index]["@attributes"]["ServiceRef"]] = responseText['SIG_AirServicesRS']['Remarks']['Remark'][index];
                            remarks[responseText['SIG_AirServicesRS']['Remarks']['Remark'][index]["@attributes"]["ServiceRef"]]["@attributes"]["RemarkRef"] = index;
                        }

                        for(let element in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row']) {
                            if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['@attributes']['RowProps'] != "NoRow") {
                                for(let i in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat']) {
                                    if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatAvailability'] !== "Available") {
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel']+"']").removeClass("vydacha-items__i__about-plane__salon__place_available");
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel']+"']").addClass("vydacha-items__i__about-plane__salon__place_noavailable");
                                    } else {
                                        let service = responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['ServiceRef'];
                                        let SeatLabel = responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel'];

                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+SeatLabel+"']").attr("data-services", service);
                                        ava[SeatLabel] = true;

                                        if(typeof services[flight] === "undefined") {
                                            services[flight] = {};
                                        }
                                        if(typeof services[flight]["CABIN"] === "undefined") {
                                            services[flight]["CABIN"] = {};
                                        }
                                        if(typeof services[flight]["CABIN"][SeatLabel] === "undefined") {
                                            services[flight]["CABIN"][SeatLabel] = {};
                                        }

                                        if(service) {
                                            service = service.split(' ')[0];
                                            services[flight]["CABIN"][SeatLabel] = remarks[service]["@attributes"];
                                        }
                                    }
                                }
                            }
                        }
                        let equipment = responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['@attributes']['PlaneMapName'];
                        let activeCabin = true;
                        let countAvailableCabin = 0;
                        for(let sa in planes[equipment].mapCabin) {
                            let result = true;
                            for(let seat in planes[equipment].mapCabin[sa]) {
                                if($(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+seat+"']").hasClass("vydacha-items__i__about-plane__salon__place_noavailable")) {
                                    result = false;
                                }
                            }
                            if(result) {
                                countAvailableCabin++;
                                let top = planes[equipment].mapCabinOptions[sa].top;
                                let left = planes[equipment].mapCabinOptions[sa].left;
                                let width = planes[equipment].mapCabinOptions[sa].width;
                                let height = planes[equipment].mapCabinOptions[sa].height;
                                $(id + " .vydacha-items__i__about-plane__salon__img").append('<div class="vydacha-items__i__about-plane__salon__img__area" style="top:'+top+';left:'+left+';width:'+width+';height:'+height+';" data-places="'+sa+'"></div>');

                                if(activeCabin) {
                                    for(let seat in planes[equipment].mapCabin[sa]) {
                                        $(id + " .vydacha-items__i__about-plane__salon__place[data-number='"+seat+"']").addClass("vydacha-items__i__about-plane__salon__place_selected");
                                    }
                                    $(id + " .vydacha-items__i__about-plane__salon__img").find(".vydacha-items__i__about-plane__salon__img__area").addClass("vydacha-items__i__about-plane__salon__img__area_selected");
                                    activeCabin = false;
                                }
                            }
                        }
                        $(id + " .vydacha-items__i__about-plane__available__count").html(countAvailableCabin);
                        $(id + " .vydacha-items__i__about-plane__salon__img__loader").removeClass("vydacha-items__i__about-plane__salon__img__loader_active");

                        num++;
                        cabinMap(num);
                    }
                }
            }
        }
    }
}



































// наведение на каюту
$("body").on("mouseover", ".configuration-item_cabin .vydacha-items__i__about-plane__salon__img__area", function() {
    let places = $(this).attr("data-places");
    let containerImg = $(this).parents(".vydacha-items__i__about-plane__salon__img");
    let container = $(this).parents(".vydacha-items__i");
    let flight = container.attr("data-flight");
    let equipment = dataCome[flight]['equipment'];
    for(let seat in planes[equipment].mapCabin[places]) {
        containerImg.find(".vydacha-items__i__about-plane__salon__place[data-number='"+seat+"']").addClass('vydacha-items__i__about-plane__salon__place_hover');
    }
});
// убрать курсор с каюты
$("body").on("mouseout", ".configuration-item_cabin .vydacha-items__i__about-plane__salon__img__area", function() {
    let places = $(this).attr("data-places");
    let containerImg = $(this).parents(".vydacha-items__i__about-plane__salon__img");
    let container = $(this).parents(".vydacha-items__i");
    let flight = container.attr("data-flight");
    let equipment = dataCome[flight]['equipment'];
    for(let seat in planes[equipment].mapCabin[places]) {
        containerImg.find(".vydacha-items__i__about-plane__salon__place[data-number='"+seat+"']").removeClass('vydacha-items__i__about-plane__salon__place_hover');
    }
});
// клик по каюте
$("body").on("click", ".configuration-item_cabin .vydacha-items__i__about-plane__salon__img__area", function() {
    let places = $(this).attr("data-places");
    let containerImg = $(this).parents(".vydacha-items__i__about-plane__salon__img");
    let container = $(this).parents(".vydacha-items__i");
    let flight = container.attr("data-flight");
    let equipment = dataCome[flight]['equipment'];
    containerImg.find(".vydacha-items__i__about-plane__salon__place_selected").removeClass("vydacha-items__i__about-plane__salon__place_selected");
    containerImg.find(".vydacha-items__i__about-plane__salon__img__area_selected").removeClass("vydacha-items__i__about-plane__salon__img__area_selected");
    $(this).addClass("vydacha-items__i__about-plane__salon__img__area_selected");
    for(let seat in planes[equipment].mapCabin[places]) {
        containerImg.find(".vydacha-items__i__about-plane__salon__place[data-number='"+seat+"']").addClass('vydacha-items__i__about-plane__salon__place_selected');
    }
});
// клик по стрелке развернуть
$("body").on("click", ".vydacha-items__i__open", function(e) {
    let check = $(this).parents(".vydacha-items__i").hasClass("vydacha-items__i_active");
    $(".vydacha-items__i").removeClass("vydacha-items__i_active");
    if(!check) {
        $(this).parents(".vydacha-items__i").addClass("vydacha-items__i_active");
    }

    let selectContainer = $(this).parents(".vydacha-items__i");
    let select = selectContainer.find(".vydacha-items__i__tarifs__i_selected");
    if($(selectContainer).hasClass("vydacha-items__i_active") && !select[0]) {
        createSelectData(selectContainer.find(".vydacha-items__i__tarifs__i_sh"));
    }
});
// клик по тарифу
$("body").on("click", ".vydacha-items__i__tarifs__i", function(e) {
    if(!$(this).hasClass("vydacha-items__i__tarifs__i_disabled")) {
        createSelectData($(this))
    }
});
function createSelectData(selectTarif) {
    let selectContainer = selectTarif.parents(".vydacha-items__i");
    let flight = selectContainer.attr('data-flight');
    // показываем нужную схему самолета
    let id = $(selectTarif).attr("data-id");
    $(".configuration-item").removeClass("configuration-item_active");
    $(".configuration-item_first").addClass("configuration-item_active");
    selectContainer.find(".configuration-item_first").removeClass("configuration-item_active");
    $(id).addClass("configuration-item_active");
    $(".vydacha-items__i").removeClass("vydacha-items__i_active");
    selectContainer.addClass("vydacha-items__i_active");
    // показываем нужное описание тарифа
    selectContainer.find(".vydacha-items__i__about-plane__tx_active").removeClass("vydacha-items__i__about-plane__tx_active");
    $(".vydacha-items__i__about-plane__tx_" + id.replace("#", "")).addClass("vydacha-items__i__about-plane__tx_active");
    // записываем выбранные данные
    selectData.selectFlight = listFlightsData[flight];
    selectData.ItineraryRef = selectTarif.attr('data-itineraryref');
    selectData.OptionRef = selectTarif.attr('data-optionref');
    selectData.Fare = selectTarif.attr('data-ref');
    selectData.SessionID = selectTarif.attr('data-session');
    selectData.tarif = selectTarif.attr('data-tarif');
    selectData.price = selectTarif.attr('data-price');
    selectData.servprice = selectTarif.attr('data-price-serv');
    selectData.currency = selectTarif.attr('data-currency');
    selectData.plane = planes[listFlightsData[flight].Equipment];
    // закрываем активные тарифы и устанавливаем выбранный
    $(".vydacha-items__i__tarifs__i").removeClass("vydacha-items__i__tarifs__i_selected");
    selectTarif.addClass("vydacha-items__i__tarifs__i_selected");
    $(".vydacha-items__i__tarifs__i__radio").attr("checked", false);
    selectTarif.find(".vydacha-items__i__tarifs__i__radio").attr("checked", "checked");

    // обновляем слайдер
    setSlider(selectContainer);

    // устанавливаем выбранные параметры в плашку с информацией о выбранном рейсе
    $(".data-select-pl__plane__img").attr("src", planes[selectData.selectFlight.Equipment].smallImage);
    $(".data-select-pl__plane__t").html(planes[selectData.selectFlight.Equipment].class);
    $(".data-select-pl__plane__engine").html(planes[selectData.selectFlight.Equipment].name);
    $(".data-select-pl__info__date").html(selectData.selectFlight.date);
    $(".data-select-pl__info__time").html(selectData.selectFlight.timeDeparture);
    $(".data-select-pl__info__route").html(selectData.selectFlight.fromCity + " - " + selectData.selectFlight.toCity);
    $(".data-select-pl__tarif").html("«"+selectData.tarif+"»");
    $(".data-select-pl__price__amount").html(delimeterThousand(Number(selectData.price) + Number(selectData.servprice)));
    $(".data-select-pl__price__currency").html(selectData.currency);
    $(".data-select-pl").addClass("data-select-pl_active");
}







































// выбор места
$("body").on("click", ".vydacha-items__i__about-plane__salon__place_available", function() {
    let price = $(this).attr("data-price");
    let service = $(this).attr("data-services");
    let place = $(this).attr("data-number");
    let flight = $(this).parents(".vydacha-items__i").attr("data-flight");

    let tarif = $(this).parents(".vydacha-items__i__cell-right").attr("id");
    tarif = $("div[data-id='#"+tarif+"'");
    total = Number(tarif.attr("data-price")) + Number(price);
    tarif.attr("data-price-serv", price);
    tarif.attr("data-services", service);
    tarif.find(".vydacha-items__i__tarifs__i__price__amount").html(delimeterThousand(total));
    $('.data-select-pl__price__amount').html(delimeterThousand(total));

    let parent = $(this).parents(".vydacha-items__i__about-plane__salon__img");
    parent.find(".vydacha-items__i__about-plane__salon__place_selected").removeClass("vydacha-items__i__about-plane__salon__place_selected");
    $(this).addClass("vydacha-items__i__about-plane__salon__place_selected");


    console.log(services[flight]["SEAT"][place]);
});


// После выбора рейса жмем кнопку продолжить
$(".data-select-pl__btns__next").click(function(e){
    e.preventDefault();
    // добавляем на кнопку прелоадер
    $(".data-select-pl__btns__next .lds-ellipsis").addClass("lds-ellipsis_active");
    $(".data-select-pl__btns__next span").css("display", "none");
    //
    if(selectData.tarif == "Место") {
        let number = $(".vydacha-items__i__tarifs__i_selected").parents(".vydacha-items__i").find(".configuration-item_active .vydacha-items__i__about-plane__salon__place_selected").attr('data-number');
        selectData.price = $(".vydacha-items__i__tarifs__i_selected").attr('data-price');
        selectData.service = services[selectData.selectFlight.numberFlight]["SEAT"][number];
        selectData.service.ActualPrice = $(".vydacha-items__i__tarifs__i_selected").attr('data-price-serv');
        selectData.service.Seat = number;
        let data = selectData.service;
        selectData.service = {};
        selectData.service[0] = data;
    } else if(selectData.tarif == "Каюта") {
        selectData.service = {};
        $(".vydacha-items__i__tarifs__i_selected").parents(".vydacha-items__i").find(".configuration-item_active .vydacha-items__i__about-plane__salon__place_selected").each(function(index) {
            let number = $(".configuration-item_active .vydacha-items__i__about-plane__salon__place_selected:eq("+index+")").attr("data-number");
            if(typeof selectData.service[index] === "undefined") {
                selectData.service[index] = {};
            }
            selectData.service[index] = services[selectData.selectFlight.numberFlight]["CABIN"][number];
            console.log(selectData);
            selectData.service[index].Seat = number;
        });
    } else {
        selectData.service = {};
    }
    // if(typeof selectData.service === "undefined") {
    //     selectData.service = {};
    //     selectData.seat = {};
    //     selectData.servprice = 0;
    //     $(".vydacha-items__i__cell-right_active .vydacha-items__i__about-plane__salon__place_selected").each(function(index) {
    //         let number = $(".vydacha-items__i__cell-right_active .vydacha-items__i__about-plane__salon__place_selected:eq("+index+")").attr("data-number");
    //         selectData.service[index] = services[selectData.selectFlight.numberFlight]["CABIN"][number];
    //         selectData.seat[index] = number;
    //     });
    // }
    console.log(selectData);

    // переводим выбранные данные в json и отдаем на сервер, после получения ответа перенаправляем на следующую страницу
    var data = JSON.stringify(selectData);
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_ordersdata/?data='+data, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {}
        else {
            // console.log(xhr.responseText);
            document.location.href = "/order/?order="+xhr.responseText;
        }
    }
});