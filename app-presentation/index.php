<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Sirius Aero Card");
?>

<div class="b-content _page">
    <div class="first-page wow fadeIn">
        <div class="first-page__title">У нас к вам приложение</div>

    </div>
    <h2 class="app-prs_title wow fadeIn">Услуги Sirius Aero всегда под рукой</h2>
    <div class="app-prs-block">
        <div class="app-prs-block__bg">
            <h2 class="app-prs_title wow fadeIn">Возвратные рейсы</h2>
            <div class="app-prs-block__bg-text important-text">
                Экономте до 75% от стоимости обычного чартера.
            </div>
            <div class="app-prs-block__bg-text">
                Возвратный рейс - это рейс без пассажиров. Это происходит, когда воздушное судно высаживает пассажиров 
                в аэропорту назначения и возвращается на аэродром приписки или когда самолет летит, чтобы забрать 
                пассажиров в другом аэропорту. 
            </div>
        </div>
    </div>
    <h2 class="app-prs_title wow fadeIn">Уведомления о возвратных рейсах</h2>
    <!-- <div class="qualities">
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.1s" >Стабильность</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.2s" >Оперативность</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.3s" >Качество</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.4s" >Безупречный cервис</div>
    </div>


    <div class="advanteges siriusaerocard-wrapper">
        <h2 class="wow fadeIn">Преимущества</h2>
        <div class="advanteges__cards">
            <div class="advanteges__item wow fadeInLeft">Фиксация стоимости перелета на весь период действия</div>
            <div class="advanteges__item wow fadeInRight">Возможность замены борта из собственного флота и повышение класса ВС</div>
        </div>
    </div>


    <div class="appeal  wow fadeIn">
        <div class="appeal__text  wow fadeIn"  data-wow-delay="0.2s">
            Будем рады Вашему обращению к нам!
        </div>
        <a href="tel:+79261183323" class="phone-icon wow fadeIn"  data-wow-delay="0.4s">
            <div  class="phone-circle"></div>
        </a>
        <a href="tel:+79261183323" class="appeal__phone wow fadeIn"  data-wow-delay="0.6s">
            +7 926 118-33-23
        </a>
    </div> -->
</div>


<script src="js/wow.js"></script>
<script>
var wow = new WOW(
  {    
    mobile: false,
    offset: 400,  
  }
);
    // new WOW({mobile: false}).init();
wow.init();
</script>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>