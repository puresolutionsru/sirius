<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Sirius Aero Card");
?>

<div class="b-content _page">
    <div class="slogan-block wow fadeIn">
        <div class="slogan-block__label wow fadeIn" data-wow-delay="0.5s">
            <span>Sirius Aero</span>
            <span>business aviation</span>
        </div>
        <div class="slogan-block__slogan">Ключевым клиентам  Ключевым клиентам</div>
        <!-- <div class="slogan-block__slogan2">Ключевым клиентам</div> -->

    </div>
    <div class="qualities">
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.1s" >Стабильность</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.2s" >Оперативность</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.3s" >Качество</div>
        <div class="qualities__it wow fadeInRight" data-wow-delay="0.4s" >Безупречный cервис</div>
    </div>
    <div class="prices siriusaerocard-wrapper wow fadeIn">
        <table>
        <thead>
            <tr>
                <td>Sirius Aero Card</td>
                <td>
                    <div class="plane-class">Mid-size Jet</div>
                    <div class="per-h-plane">€ 4,600 <span>/hour</span> </div>
                </td>
                <td>
                    <div class="plane-class">Heavy Jet</div>
                    <div class="per-h-plane">€ 6,300 <span>/hour</span> </div>
                </td>
            </tr>
        </thead>
            <tr>
                <td>Минимальное количество часов, приобретаемое по карте</td>
                <td colspan="2">50 часов</td>
            </tr>
            <tr>
                <td>Ставка, применяемая при коротких рейсах (летное время до 2,5ч)</td>
                <td>€ 6,300</td>
                <td>€ 10,300</td>
            </tr>
            <tr>
                <td>Ставка, применяемая при длинных рейсах (летное время более 6ч)</td>
                <td>€ 3,600</td>
                <td>€ 4,900</td>
            </tr>
            <tr>
                <td>Срок действия карты</td>
                <td colspan="2">1 год</td>
            </tr>
            <tr>
                <td>Кол-во пассажиров, включенных в стоимость</td>
                <td>2</td>
                <td>4</td>
            </tr>
            <tr>
                <td>Каждый дополнительный пассажир</td>
                <td colspan="2">€ 500/ участок</td>
            </tr>
            <tr>
                <td>Максимальное кол-во ночевок в рейсе, включенных в программу</td>
                <td colspan="2">1 ночевка</td>
            </tr>

            <tr>
                <td>Каждая дополнительная ночевка</td>
                <td>€ 2500</td>
                <td>€ 3500</td>
            </tr>
            <tr>
                <td>Плата за подлет</td>
                <td>€ 6500</td>
                <td>€ 10000</td>
            </tr>
            <tr>
                <td>Кол-во пассажиров, включенных в стоимость</td>
                <td>2</td>
                <td>4</td>
            </tr>
            <tr>
                <td>Минимальное время подачи заявки</td>
                <td colspan="2">24 часа</td>
            </tr>
        </table>
    </div>

    <div class="include-price__bg wow fadeIn">
        <div class="siriusaerocard-wrapper">
            <div class="include-price">
                <div class="include-price__positions">
                    <h2>Стоимость летного часа включает в себя</h2>
                    <p>Заработную плату. суточные и проживание летного состава</p>
                    <p>Аэронавигационное и метеообеспечение по маршруту полётов</p>
                    <p>Обеспечение полетов диспетчерской службой авиакомпании</p>
                    <p>Техническое обслуживание ВС и двигателей</p>
                    <p>Страхование ВС, пассажиров, багажа, груза и ответственности перед третьими лицами в лимитах, установленных для внутренних полётов</p>
                    <p>Обновление баз данных FMS и EGPWS самолёта, сборников аэронавигационной информации, подписки на электронные технические библиотеки и лётную документацию</p>
                    <p>Все прямые переменные операционные расходы по маршрутам полётов</p>
                    <p>Топливо по маршрутам полётов</p>
                    <p>Аэропортовые и хендлинговые сборы</p>
                    <p>VIP бортовое питание (по согласованному меню) и обслуживание пассажиров в VIP залах</p>
                </div>
                <div class="include-price__side-box">
                    <div class="include-price__side-box-item bank-card wow fadeInRight" data-wow-delay="0.4s" >
                        Единовременный платеж осуществляется не позднее, чем за 5 дней до начала программы.
                    </div>
                    <div class="include-price__side-box-item dashed-border wow fadeInRight" data-wow-delay="0.4s">
                        <span>Не включены в стоимость</span>
                        Антиобледенительная обработка ВС Продление регламентов работы аэропортов
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="advanteges siriusaerocard-wrapper">
        <h2 class="wow fadeIn">Преимущества</h2>
        <div class="advanteges__cards">
            <div class="advanteges__item wow fadeInLeft">Фиксация стоимости перелета на весь период действия</div>
            <div class="advanteges__item wow fadeInRight">Возможность замены борта из собственного флота и повышение класса ВС</div>
        </div>
    </div>

    <div class="card-fleet wow fadeIn">
        <div class="siriusaerocard-wrapper">
            <div class="card-fleet__content">
                <h2>Флот</h2>
                <p>У нас самый крупный в России, уверенно растущий флот бизнесджетов, и более 500 привлеченных самолетов</p>
                <div class="card-fleet__amount wow fadeInRight" data-wow-delay="0.4s">
                    <div>20</div>
                    <div>
                        самолетов типа Mid-size Jet, Large Jet
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"aerocard",
            	array(
                    "TITLE_H2" => "Middle-size Jet",
                    "INFO_FLOAT" => "LEFT",
                    "SUB_CATEGORY" => "1",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"ADD_SECTIONS_CHAIN" => "N",
            		"AJAX_MODE" => "N",
            		"AJAX_OPTION_ADDITIONAL" => "",
            		"AJAX_OPTION_HISTORY" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "36000000",
            		"CACHE_TYPE" => "A",
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"DISPLAY_DATE" => "Y",
            		"DISPLAY_NAME" => "Y",
            		"DISPLAY_PICTURE" => "Y",
            		"DISPLAY_PREVIEW_TEXT" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"FIELD_CODE" => array(
            			0 => "",
            			1 => "",
            		),
            		"FILTER_NAME" => "",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"IBLOCK_ID" => "7",
            		"IBLOCK_TYPE" => "-",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"MESSAGE_404" => "",
            		"NEWS_COUNT" => "100",
            		"PAGER_BASE_LINK_ENABLE" => "N",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => ".default",
            		"PAGER_TITLE" => "",
            		"PAGER_TITLE_ALL" => "",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"PROPERTY_CODE" => array(
            			0 => "VMEST",
            			1 => "DAL",
            			2 => "VYSOTA",
            			3 => "SHIRINA",
            			4 => "DLINA",
            			5 => "OB",
            		),
            		"SET_BROWSER_TITLE" => "Y",
            		"SET_LAST_MODIFIED" => "N",
            		"SET_META_DESCRIPTION" => "Y",
            		"SET_META_KEYWORDS" => "Y",
            		"SET_STATUS_404" => "N",
            		"SET_TITLE" => "Y",
            		"SHOW_404" => "N",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_BY2" => "ID",
            		"SORT_ORDER1" => "DESC",
            		"SORT_ORDER2" => "DESC",
            		"STRICT_SECTION_CHECK" => "N",
            		"COMPONENT_TEMPLATE" => "aerocard"
            	),
            	false
    );?>

<?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"aerocard",
            	array(
                    "TITLE_H2" => "Heavy Jet",
                    "INFO_FLOAT" => "RIGHT",
                    "SUB_CATEGORY" => "2",
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"ADD_SECTIONS_CHAIN" => "N",
            		"AJAX_MODE" => "N",
            		"AJAX_OPTION_ADDITIONAL" => "",
            		"AJAX_OPTION_HISTORY" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "36000000",
            		"CACHE_TYPE" => "A",
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"DISPLAY_DATE" => "Y",
            		"DISPLAY_NAME" => "Y",
            		"DISPLAY_PICTURE" => "Y",
            		"DISPLAY_PREVIEW_TEXT" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"FIELD_CODE" => array(
            			0 => "",
            			1 => "",
            		),
            		"FILTER_NAME" => "",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"IBLOCK_ID" => "7",
            		"IBLOCK_TYPE" => "-",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"MESSAGE_404" => "",
            		"NEWS_COUNT" => "100",
            		"PAGER_BASE_LINK_ENABLE" => "N",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => ".default",
            		"PAGER_TITLE" => "",
            		"PAGER_TITLE_ALL" => "",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"PROPERTY_CODE" => array(
            			0 => "VMEST",
            			1 => "DAL",
            			2 => "VYSOTA",
            			3 => "SHIRINA",
            			4 => "DLINA",
            			5 => "OB",
            		),
            		"SET_BROWSER_TITLE" => "Y",
            		"SET_LAST_MODIFIED" => "N",
            		"SET_META_DESCRIPTION" => "Y",
            		"SET_META_KEYWORDS" => "Y",
            		"SET_STATUS_404" => "N",
            		"SET_TITLE" => "Y",
            		"SHOW_404" => "N",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_BY2" => "ID",
            		"SORT_ORDER1" => "DESC",
            		"SORT_ORDER2" => "DESC",
            		"STRICT_SECTION_CHECK" => "N",
            		"COMPONENT_TEMPLATE" => "aerocard"
            	),
            	false
    );?>

    <div class="map-aria  wow fadeIn">
        <p>
            Мы обеспечиваем полеты по РФ, Европе и СНГ
        </p>
    </div>
    <div class="benefits">
        <div class="benefits__item  wow mySliseUp">
            <div class="benefits__text">Мы сохраняем приватность и эксклюзивность, уделяя максимум внимания каждому аспекту</div>
        </div>
        <div class="benefits__item  wow mySliseDown">
            <div class="benefits__text">Мы всегда отдаем предпочтение качеству, поэтому число наших клиентов лимитировано</div>
        </div>
    </div>
    <div class="appeal  wow fadeIn">
        <div class="appeal__text  wow fadeIn"  data-wow-delay="0.2s">
            Будем рады Вашему обращению к нам!
        </div>
        <a href="tel:+79261183323" class="phone-icon wow fadeIn"  data-wow-delay="0.4s">
            <div  class="phone-circle"></div>
        </a>
        <a href="tel:+79261183323" class="appeal__phone wow fadeIn"  data-wow-delay="0.6s">
            +7 926 118-33-23
        </a>
    </div>
</div>


<script src="js/wow.js"></script>
<script>
var wow = new WOW(
  {    
    mobile: false,
    offset: 400,  
  }
);
    // new WOW({mobile: false}).init();
wow.init();
</script>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>