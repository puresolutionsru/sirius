let rusWidget = {

    // main params
    tickets: null,
    companyCode: null,
    lang: null,

    // count passengers
    countPass: 1,
    typePass: {
        "A": 1, // взрослые
        "B": 0, // дети от 2 до 11
        "C": 0, // дети до 2
        "D": 0, // дети до 2 с местом
    },
    maxPass: 6,
    counter: document.getElementsByClassName("ruswidget-counter"),
    counterA: 0, // position of input with type A

    // codes of airports
    routeFrom: null,
    routeTo: null,
    
    // date value
    dateFrom: {value: null},
    dateTo: {value: null},

    // other params
    fullListCities: {},
    fullListCitiesWithCodeCity: {},
    fromCity: document.getElementById("ruswidget-from-city"),
    fromCityBox: document.getElementById("outFromCities"),
    toCity: document.getElementById("ruswidget-to-city"),
    toCityBox: document.getElementById("outToCities"),
    calendarFrom: document.getElementById("ruswidget-calendar-from"),
    calendarFromBox: document.getElementById("outFromCalendars"),
    calendarTo: document.getElementById("ruswidget-calendar-to"),
    calendarToBox: document.getElementById("outToCalendars"),
    passengers: document.getElementById("ruswidget-passengers"),
    passengersBox: document.getElementById("ruswidget-choise-passengers"),
    sendBtn: document.getElementById("ruswidget-send"),

    listCloseWindows: [], // boxes that don't close on click
    calendar: {}, // params of calendar

    init: function(tickets, companyCode, lang) {
        this.tickets = tickets;
        this.companyCode = companyCode;
        this.lang = lang;
        for(let i=0;i<this.counter.length;i++) {
            if(this.counter[i].getElementsByClassName("ruswidget-counter__number")[0].getAttribute("data-type") == "A") {
                this.counterA = i;
                break;
            }
        }
        this.events();
        this.getAndAddListCities();
        this.listCloseWindows = [this.fromCityBox, this.toCityBox, this.calendarFromBox, this.calendarToBox, this.passengersBox];
        if(lang=='ru') {
            this.calendar.monthName = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
            this.calendar.monthNameWN = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
            this.calendar.dayName = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
        } else {
            this.calendar.monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            this.calendar.monthNameWN = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
            this.calendar.dayName = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU'];
        }
        this.calendar.selectedDate = {
            'Day': new Date().getDate(),
            'Month': parseInt(new Date().getMonth()) + 1,
            'Year': new Date().getFullYear()
        };
    },
    events: function() {
        document.addEventListener('click', function(e) {
            // console.log(e.path);
            // for(let element in rusWidget.listCloseWindows) {
            //     if(e.path.indexOf(rusWidget.listCloseWindows[element]) == -1) {
            //         rusWidget.listCloseWindows[element].classList.remove("ruswidget__i__window_active");
            //     }
            // }
            // e.stopPropagation();
        }, false);
        this.fromCity.addEventListener('click', function(e) {
            rusWidget.fromCityBox.classList.toggle("ruswidget__i__window_active");
        }, false);
        this.toCity.addEventListener('click', function(e) {
            if(!rusWidget.routeFrom) {
                rusWidget.fromCity.focus();
                rusWidget.fromCityBox.classList.toggle("ruswidget__i__window_active");
            } else {
                rusWidget.toCityBox.classList.toggle("ruswidget__i__window_active");
            }
        }, false);
        this.calendarFrom.addEventListener('click', function(e) {
            let time = new Date();
            rusWidget.drawCalendar(rusWidget.calendarFromBox, (time.getMonth()+1), time.getFullYear(), rusWidget.calendarFrom, rusWidget.dateFrom);
        }, false);
        this.calendarTo.addEventListener('click', function(e) {
            let time = new Date();
            rusWidget.drawCalendar(rusWidget.calendarToBox, (time.getMonth()+1), time.getFullYear(), rusWidget.calendarTo, rusWidget.dateTo);
        }, false);
        this.passengers.addEventListener('click', function(e) {
            rusWidget.passengersBox.classList.toggle("ruswidget__i__window_active");
        }, false);
        this.sendBtn.addEventListener('click', function(e) {
            rusWidget.checkAndRedirect();
        }, false);
        for(let i=0;i<this.counter.length;i++) {
            let number = this.counter[i].getElementsByClassName("ruswidget-counter__number")[0];
            this.counter[i].getElementsByClassName("ruswidget-counter__minus")[0].addEventListener('click', function(btn) {
                rusWidget.changeCountPass("minus", number);
            }, false);
            this.counter[i].getElementsByClassName("ruswidget-counter__plus")[0].addEventListener('click', function(btn) {
                rusWidget.changeCountPass("plus", number);
            }, false);
        }
    },
    changeCountPass: function(type, number) {
        if(type=="minus") {
            if(this.typePass[number.getAttribute("data-type")] > number.getAttribute("data-min"))
                this.typePass[number.getAttribute("data-type")] -= 1;
                number.innerHTML = this.typePass[number.getAttribute("data-type")];
        } else if (type=="plus") {
            if(this.countPass < this.maxPass)
                this.typePass[number.getAttribute("data-type")] += 1;
                number.innerHTML = this.typePass[number.getAttribute("data-type")];
        }
        this.countPass = 0;
        for(let el in this.typePass) {
            this.countPass += this.typePass[el];
        }
        for(let i=0;i<this.counter.length;i++) {
            let count = this.counter[i].getElementsByClassName("ruswidget-counter__number")[0];
            if(this.countPass >= this.maxPass) {
                this.counter[i].getElementsByClassName("ruswidget-counter__plus")[0].classList.remove('ruswidget-counter__icon_active');
            } else {
                this.counter[i].getElementsByClassName("ruswidget-counter__plus")[0].classList.add('ruswidget-counter__icon_active');
            }
            if(this.typePass[count.getAttribute("data-type")] <= count.getAttribute("data-min")) {
                this.counter[i].getElementsByClassName("ruswidget-counter__minus")[0].classList.remove('ruswidget-counter__icon_active');
            } else {
                this.counter[i].getElementsByClassName("ruswidget-counter__minus")[0].classList.add('ruswidget-counter__icon_active');
            }

            if(count.getAttribute("data-type") == "C" && this.typePass[count.getAttribute("data-type")] >= this.typePass["A"]) {
                this.counter[i].getElementsByClassName("ruswidget-counter__plus")[0].classList.remove('ruswidget-counter__icon_active');
                this.counter[this.counterA].getElementsByClassName("ruswidget-counter__minus")[0].classList.remove('ruswidget-counter__icon_active');
            }
        }
        let words = ["passenger", "passengers", "passengers"];
        if(this.lang == "ru")
            words = ["пассажир", "пассажира", "пассажиров"];
        this.passengers.value = this.countPass + " " + this.declension(this.countPass, words);
    },
    declension: function(number, wordsArray) {
        let cases = [2, 0, 1, 1, 1, 2];
        return wordsArray[(number%100>4 && number%100<20) ? 2 : cases[(number%10<5) ? number % 10 : 5]];
    },
    getAndAddListCities: function() {
        if(!Object.keys(rusWidget.fullListCities).length) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'https://www.rusline.aero/engine/Describes/get_company_routes/?company='+this.companyCode+'&lang='+this.lang, true);
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) return;
                if (xhr.status != 200) {

                } else {
                    let elements = JSON.parse(xhr.responseText);
                    for(let element in elements) {
                        rusWidget.fullListCities[elements[element]['infoAirports']['code']] = elements[element];
                        rusWidget.fullListCitiesWithCodeCity[elements[element]['infoCity']['name']] = elements[element];
                        let div = document.createElement('div');
                        div.className = "ruswidget__list__i";
                        div.innerHTML = '<div class="ruswidget__list__i__t">'+elements[element]['infoCity']['name']+'<span class="ruswidget-info ruswidget__list__info">'+elements[element]['infoCity']['fullCountryName']+'</span></div><div class="ruswidget__list__i__air">'+elements[element]['infoAirports']['code']+'</div>';
                        rusWidget.fromCityBox.append(div);
                        div.addEventListener('click', function(e) {
                            rusWidget.routeFrom = elements[element]['infoAirports']['code'];
                            rusWidget.fromCity.value = elements[element]['infoCity']['name'] + ', ' + elements[element]['infoCity']['fullCountryName'];
                            rusWidget.fromCityBox.classList.remove("ruswidget__i__window_active");
                            rusWidget.toCityBox.classList.add("ruswidget__i__window_active");
                            rusWidget.toCity.focus();
                            rusWidget.createListCitiesTo();
                        }, false);
                    }
                    rusWidget.fromCityBox.getElementsByClassName("ruswidget__preloader")[0].classList.add("ruswidget__preloader_hide");
                }
            }
        }
    },
    createListCitiesTo: function() {
        rusWidget.toCityBox.classList.add("ruswidget__i__window_active");
        rusWidget.toCityBox.innerHTML = "<div class='ruswidget__preloader'></div>";
        rusWidget.toCityBox.getElementsByClassName("ruswidget__preloader")[0].classList.add("ruswidget__preloader_hide");
        for(let element in this.fullListCities[this.routeFrom].way) {
            let way = this.fullListCities[this.routeFrom].way[element];
            let city = way.city.name;
            let country = rusWidget.fullListCitiesWithCodeCity[city]['infoCity']['fullCountryName'];
            let code = rusWidget.fullListCitiesWithCodeCity[city]['infoAirports']['code'];

            let direct = "";
            if(!way.direct) direct = "с пересадкой";

            let div = document.createElement('div');
            div.className = "ruswidget__list__i";
            div.innerHTML = '<div class="ruswidget__list__i__t">'+city+'<span class="ruswidget-info ruswidget__list__info">'+country+'</span> <span class="ruswidget-info ruswidget__list__info">'+direct+'</span></div><div class="ruswidget__list__i__air">'+code+'</div>';
            rusWidget.toCityBox.append(div);

            div.addEventListener('click', function(e) {
                rusWidget.routeTo = rusWidget.fullListCities[code]['infoAirports']['code'];
                rusWidget.toCity.value = rusWidget.fullListCities[code]['infoCity']['name'] + ', ' + rusWidget.fullListCities[code]['infoCity']['fullCountryName'];
                rusWidget.toCityBox.classList.remove("ruswidget__i__window_active");

                let time = new Date();
                rusWidget.drawCalendar(rusWidget.calendarFromBox, (time.getMonth()+1), time.getFullYear(), rusWidget.calendarFrom, rusWidget.dateFrom);
            }, false);
        }
    },
    drawCalendar: function(container, month, year, input, dateT) {
        var calendarHTML = '';
        calendarHTML += '<table class="calendarTable" cellspacing="0" cellpadding="0">';

        // Месяц и навигация
        calendarHTML += '<tr>';
        calendarHTML += '<td class="navigation navigation_to_left" month="' + (month > 1 ? (+month - 1) : 12) + '" year="' + (month > 1 ? year : (+year - 1)) + '">&#9668;<\/td>';
        calendarHTML += '<td colspan="5" class="navigation navigation_to_this_day" month="' + this.calendar.selectedDate.Month + '" year="' + this.calendar.selectedDate.Year + '">' + this.calendar.monthName[(month - 1)] + '&nbsp;-&nbsp;' + year + '<\/td>';
        calendarHTML += '<td class="navigation navigation_to_right" month="' + (month < 12 ? (+month + 1) : 1) + '" year="' + (month < 12 ? year : (+year + 1)) + '">&#9658;<\/td>';
        calendarHTML += '<\/tr>';

        // Шапка таблицы с днями недели
        calendarHTML += '<tr>';
        calendarHTML += '<th>' + this.calendar.dayName[0] + '<\/th>';
        calendarHTML += '<th>' + this.calendar.dayName[1] + '<\/th>';
        calendarHTML += '<th>' + this.calendar.dayName[2] + '<\/th>';
        calendarHTML += '<th>' + this.calendar.dayName[3] + '<\/th>';
        calendarHTML += '<th>' + this.calendar.dayName[4] + '<\/th>';
        calendarHTML += '<th class="holiday">' + this.calendar.dayName[5] + '<\/th>';
        calendarHTML += '<th class="holiday">' + this.calendar.dayName[6] + '<\/th>';
        calendarHTML += '<\/tr>';

        // Количество дней в месяце
        var total_days = 32 - new Date(year, (month - 1), 32).getDate();
        // Начальный день месяца
        var start_day = new Date(year, (month - 1), 1).getDay();
        if (start_day == 0) {
            start_day = 7;
        }
        start_day--;
        // Количество ячеек в таблице
        var final_index = Math.ceil((total_days + start_day) / 7) * 7;

        var day = 1;
        var index = 0;
        do {
            // Начало строки таблицы
            if (index % 7 == 0) {
                calendarHTML += '<tr>';
            }
            // Пустые ячейки до начала месяца или после окончания
            if ((index < start_day) || (index >= (total_days + start_day))) {
                calendarHTML += '<td class="grayed">&nbsp;<\/td>';
            }
            else {
                var class_name = '';
                // Выбранный день
                if (this.calendar.selectedDate.Day == day &&
                    this.calendar.selectedDate.Month == month &&
                    this.calendar.selectedDate.Year == year) {
                    class_name = 'selected';
                }
                // Праздничный день
                else if (index % 7 == 6 || index % 7 == 5) {
                    class_name = 'holiday';
                }
                // Ячейка с датой
                calendarHTML += '<td class="td_day_active ' + class_name + '" day="' + day + '" month="' + month + '" year="' + year + '">' + day + '</td>';
                day++;
            }
            // Конец строки таблицы
            if (index % 7 == 6) {
                calendarHTML += '<\/tr>';
            }
            index++;
        }
        while (index < final_index);

        calendarHTML += '<\/table>';

        container.innerHTML = calendarHTML;
        container.classList.add("ruswidget__i__window_active");

        /**
         * Клик по навигации и дням
         */
        document.querySelector('.navigation_to_left').addEventListener('click', function (e) {
            rusWidget.drawCalendar(rusWidget.calendarFromBox, this.getAttribute('month'), this.getAttribute('year'), input, dateT);
            e.stopPropagation();
        }, false);
        document.querySelector('.navigation_to_this_day').addEventListener('click', function (e) {

        }, false);
        document.querySelector('.navigation_to_right').addEventListener('click', function (e) {
            rusWidget.drawCalendar(rusWidget.calendarFromBox, this.getAttribute('month'), this.getAttribute('year'), input, dateT);
            e.stopPropagation();
        }, false);

        var all_td_day_active = document.querySelectorAll('.td_day_active'),
            ind_td_day_active, btn_td_day_active;
        for(ind_td_day_active = 0; ind_td_day_active < all_td_day_active.length; ind_td_day_active++) {
            btn_td_day_active = all_td_day_active[ind_td_day_active];
            btn_td_day_active.addEventListener('click', function (e) {
                rusWidget.selectDate(e.target.getAttribute('day'), e.target.getAttribute('month'), e.target.getAttribute('year'), input, dateT);

                // document.querySelector('input[calendar=' + classInput + ']').value = this.calendar.selectedDate.Day + '.' + this.calendar.selectedDate.Month + '.' + this.calendar.selectedDate.Year;
                // document.querySelector('.calendarOut').parentNode.removeChild(document.querySelector('.calendarOut'));
                container.classList.remove("ruswidget__i__window_active");
                e.stopPropagation();
            }, false);
        }
    },
    selectDate: function(day, month, year, input, dateT) {
        if(day<10) day = '0' + day;
        if(month<10) month = '0' + month;
        let d = new Date(year, (month-1), day);

        dateT.value = year+""+month+""+day;

        let dayShort = d.getDay()-1;
        if(d.getDay()==0) {
            dayShort = 6;
        }
        input.value = d.getDate()+" "+this.calendar.monthNameWN[month-1]+", "+this.calendar.dayName[dayShort].toLowerCase();
    },
    checkAndRedirect: function() {
        if(this.validate()) {
            return;
        } else {
            let link = this.tickets + "results/";

            link += "a" + this.routeFrom;
            link += "a" + this.routeTo;

            link += this.dateFrom.value;
            if(this.dateTo.value)
                link += this.dateTo.value;

            link += "ADT" + this.typePass["A"];
            if(this.typePass["B"] > 0)
                link += "CLD" + this.typePass["B"];
            if(this.typePass["C"] > 0)
                link += "INF" + this.typePass["C"];
            if(this.typePass["D"] > 0)
                link += "INS" + this.typePass["D"];

            link += "-class=Economy";
            link += "?changelang=" + this.lang;

            document.location.href = link;
        }
    },
    validate: function() {
        let error = false;
        if(this.countPass < 1) { error = true; this.passengers.classList.add("error"); }
        if(!this.routeFrom) { error = true; this.fromCity.classList.add("error"); }
        if(!this.routeTo) { error = true; this.toCity.classList.add("error"); }
        if(!this.dateFrom.value) { error = true; this.calendarFrom.classList.add("error"); }

        return error;
    }
}