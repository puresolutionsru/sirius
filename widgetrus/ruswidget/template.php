<div class="ruswidget" id="ruswidget">
    <div class="ruswidget__main-fields ruswidget__routes">
        <div class="ruswidget__i">
            <input type="text" class="ruswidget__input ruswidget__enter" id="ruswidget-from-city" placeholder="Город или аэропорт вылета">
            <div class="ruswidget__i__window ruswidget__list ruswidget__list_from" id="outFromCities">
                <div class="ruswidget__preloader"></div>
            </div>
        </div>
        <div class="ruswidget__i">
            <input type="text" class="ruswidget__input ruswidget__enter" id="ruswidget-to-city" placeholder="Город или аэропорт прилета">
            <div class="ruswidget__i__window ruswidget__list ruswidget__list_to" id="outToCities">
                <div class="ruswidget__preloader"></div>
            </div>
        </div>
    </div>
    <div class="ruswidget__main-fields ruswidget__dates">
        <div class="ruswidget__i ruswidget__i_calendar">
            <input type="text" class="ruswidget__input ruswidget__date" id="ruswidget-calendar-from" placeholder="Туда" readonly>
            <div class="ruswidget__i__window ruswidget__calendar ruswidget__calendar_to" id="outFromCalendars"></div>
        </div>
        <div class="ruswidget__i ruswidget__i_calendar">
            <input type="text" class="ruswidget__input ruswidget__date" id="ruswidget-calendar-to" placeholder="Обратно" readonly>
            <div class="ruswidget__i__window ruswidget__calendar ruswidget__calendar_from" id="outToCalendars"></div>
        </div>
    </div>
    <div class="ruswidget__main-fields ruswidget__pass">
        <div class="ruswidget__i ruswidget__i_pass">
            <input type="text" class="ruswidget__input ruswidget__passengers" id="ruswidget-passengers" value="1 пассажир" data-value="1" readonly>
            <div class="ruswidget__i__window ruswidget__list ruswidget__choise-passengers" id="ruswidget-choise-passengers">
                <div class="ruswidget__list__i">
                    <div class="ruswidget__list__i__t">Взрослые <span class="ruswidget-info ruswidget__list__info">от 12 лет</span></div>
                    <div class="ruswidget-counter ruswidget__list__i__counter">
                        <div class="ruswidget-counter__icon ruswidget-counter__minus ruswidget-counter__icon_disabled"></div>
                        <div class="ruswidget-counter__number" data-type="A" data-min="1">1</div>
                        <div class="ruswidget-counter__icon ruswidget-counter__plus ruswidget-counter__icon_active"></div>
                    </div>
                </div>
                <div class="ruswidget__list__i">
                    <div class="ruswidget__list__i__t">Дети <span class="ruswidget-info ruswidget__list__info">от 2 до 11 лет</span></div>
                    <div class="ruswidget-counter ruswidget__list__i__counter">
                        <div class="ruswidget-counter__icon ruswidget-counter__minus ruswidget-counter__icon_disabled"></div>
                        <div class="ruswidget-counter__number" data-type="B" data-min="0">0</div>
                        <div class="ruswidget-counter__icon ruswidget-counter__plus ruswidget-counter__icon_active"></div>
                    </div>
                </div>
                <div class="ruswidget__list__i">
                    <div class="ruswidget__list__i__t">Младенцы <span class="ruswidget-info ruswidget__list__info">до 2 лет</span></div>
                    <div class="ruswidget-counter ruswidget__list__i__counter">
                        <div class="ruswidget-counter__icon ruswidget-counter__minus ruswidget-counter__icon_disabled"></div>
                        <div class="ruswidget-counter__number" data-type="C" data-min="0">0</div>
                        <div class="ruswidget-counter__icon ruswidget-counter__plus ruswidget-counter__icon_active"></div>
                    </div>
                </div>
                <div class="ruswidget__list__i">
                    <div class="ruswidget__list__i__t">Младенцы с местом <span class="ruswidget-info ruswidget__list__info">до 2 лет</span></div>
                    <div class="ruswidget-counter ruswidget__list__i__counter">
                        <div class="ruswidget-counter__icon ruswidget-counter__minus ruswidget-counter__icon_disabled"></div>
                        <div class="ruswidget-counter__number" data-type="D" data-min="0">0</div>
                        <div class="ruswidget-counter__icon ruswidget-counter__plus ruswidget-counter__icon_active"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ruswidget__main-fields ruswidget__search">
        <div class="ruswidget__i">
            <input type="submit" class="ruswidget__input ruswidget__btn" id="ruswidget-send" value="Поиск">
        </div>
    </div>
</div>