<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

class AuthForm extends CBitrixComponent
{

    public function __construct($component = null)
    {
        parent::__construct($component);

    }

    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
            'CACHE_TIME' => isset($arParams['CACHE_TIME']) ?$arParams['CACHE_TIME']: 36000000,
            'API_DATA' => $arParams['API_DATA'],
            'URL' => $arParams['URL'],
        );
        return $result;
    }

    public function formAction($arr){
//        print_r($arr);
//        return $this->getAuth();
        return array_key_exists('user_id', $arr)?$this->getAuthData($arr):$this->getAuth();
    }


    /**
     * @return string
     */
    private function getAuth(): string
    {
        $path = $this->arParams['URL'];
        $url = [];
        $url['URL'] = '<link rel="stylesheet" href="/results/assets/icons/font-awesome/css/font-awesome.min.css">';
        $url['URL'] .= '<div style="position:absolute;top:0;right:0;width:450px;text-align:left;font-size:14px;color:font-color: #1d3156;display:flex;margin:10px auto 0px auto">';
        $url['URL'] .= '<div style="float:left;margin-top:10px;width:120px;height:40px;padding-top:5px;">
                <a href="http://lk.sirius-aero.ru/?url_data='.$path.'">Авторизоваться</a>
            </div>';
        $url['URL'] .= '</div>';

        return $url['URL'];
    }


    /**
     * @param $arr
     * @return string
     */
    private function getAuthData($arr): string
    {


        $arrAuth = $this->checkAuth($arr['user_id']);

        if(array_key_exists('email', $arrAuth) && $arrAuth['online'] === true){
            $name = $this->getRequisites($arr['requisites_id']);
            $url = [];
            $url['URL'] = '<link rel="stylesheet" href="/results/assets/icons/font-awesome/css/font-awesome.min.css">';
            $url['URL'] .= '<div style="position:absolute;top:0;right:0;width:450px;text-align:left;font-size:17px;color: #1d3156;display:flex;margin:10px auto 0px auto">';
            $url['URL'] .= '<div style="float:left;margin-top:10px;width:140px;height:40px;padding-top:5px;">
                            <a href="http://lk.sirius-aero.ru/" >Личный кабинет</a>
                            </div>
                            <div style="padding-left:10px;border-left:solid 1px #1d3156;">
                                <span>'.$arrAuth['email'].'</span><br>
                                '.$name['name'].'
                </div>';
            $url['URL'] .= '</div>';
        }else{
            $url['URL'] = $this->getAuth();
        }


        return $url['URL'];
    }

    private function getRequisites($id){
        $id = (int)$id;
        $url = 'https://corp.rusline.aero/rest/v1/cabinet_sirius/requisite/get/'.$id;

        $ch = curl_init($url);

        $data = [
            "auth_key" => 'awgwagawfaw23cr3x23rc432cfx3f2'
        ];

        $post = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $curlResult = curl_exec($ch);

        curl_close($ch);

        return json_decode($curlResult, true);
    }


    /**
     * @param $user_id
     * @return mixed
     */
    private function checkAuth($user_id)
    {
        $url = 'https://corp.rusline.aero/rest/v1/cabinet_sirius/user/';
        $ch = curl_init($url);
        $data = [
            "auth_key" => 'awgwagawfaw23cr3x23rc432cfx3f2',
            "user_id" => (int)$user_id
        ];

        $post = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $curlResult = curl_exec($ch);

        curl_close($ch);

        return json_decode($curlResult, true);

    }

    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        global $COMPONENT_AUTH;
        if(!empty($this->arParams['API_DATA'])){
            $cookie = new Cookie("API_DATA", $this->arParams['API_DATA']);
            Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
        }
        $this->arResult['API'] = Application::getInstance()->getContext()->getRequest()->getCookie("API_DATA");
        $API = '';
        $output = [];
        if(!empty($this->arResult['API'])){
            $API = openssl_decrypt(base64_decode($this->arResult['API']), 'BF-CBC', 'ab86d144e3f080b61c7c2e43');
            parse_str($API, $output);
            $checkAuth = $this->checkAuth($output['user_id']);
            if($checkAuth['online'] === true){
                $COMPONENT_AUTH['API'] = $output;
            }
        }
            $this->arResult['URL'] = $this->formAction($output);
            $this->includeComponentTemplate();
    }
}