<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

class SearchResult extends CBitrixComponent implements Controllerable
{

    public function configureActions()
    {
        return [
            'form' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                ],
                'postfilters' => []
            ]
        ];
    }

    public function formAction(){
        return empty($_COOKIE['user_id'])?$this->getAuth():$this->getAuthData($_COOKIE['user_id']);
    }


    /**
     * @return string
     */
    private function getAuth(): string
    {

        $url = [];
        $url['URL'] = '<link rel="stylesheet" href="/results/assets/icons/font-awesome/css/font-awesome.min.css">';
        $url['URL'] .= '<duv style="position:absolute;top:0;right:0;border: solid 2px;width:200px;height:70px; text-align:left;background:#ffffff;border-radius:0px 0px 0px 5px;padding:0px 0px 0px 15px;font-size:14px;">';
        $url['URL'] .= '<div style="float:left; margin:20px 10px 0px 0px;">
                <a href="http://lk.sirius-aero.ru/">Авторизоваться</a>
            </div>
            <div style="parring-left:10px;margin-top:20px"><a href="http://lk.sirius-aero.ru/"><span class="fa fa-sign-in" style="font-size:18px"></span></a></div>';
        $url['URL'] .= '</div>';

        return $url['URL'];
    }


    /**
     * @param $user_id
     * @return string
     */
    private function getAuthData($user_id): string
    {

        $arr = $this->checkAuth($user_id);

        if(array_key_exists('email', $arr) && $arr['online'] === true){
            $url = [];
            $url['URL'] = '<link rel="stylesheet" href="/results/assets/icons/font-awesome/css/font-awesome.min.css">';
            $url['URL'] .= '<duv style="position:absolute;top:0;right:0;border: solid 2px;width:250px;height:100px; text-align:left;background:#ffffff;border-radius:0px 0px 0px 5px;padding:0px 0px 0px 15px;font-size:14px;">';
            $url['URL'] .= '<div style="float:left;margin-top:10px">
                LOGIN: '.$arr['email'].'<br>
                REQUISITES ID: '.$_COOKIE['requisites_id'].'<br>
                <a href="http://lk.sirius-aero.ru/">Личный кабинет</a>
                </div>';
            $url['URL'] .= '</div>';
        }else{
            $url['URL'] = $this->getAuth();
        }


        return $url['URL'];
    }


    /**
     * @param $user_id
     * @return string
     */
    private function checkAuth($user_id)
    {
        $url = 'https://corp.rusline.aero/rest/v1/cabinet_sirius/user/';
        $ch = curl_init($url);
        $data = [
            "auth_key" => 'awgwagawfaw23cr3x23rc432cfx3f2',
            "user_id" => (int)$user_id
        ];

        $post = json_encode($data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $curlResult = curl_exec($ch);

        curl_close($ch);

        return json_decode($curlResult, true);

    }

    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }

}
