

var data = {
    param1: 'eee',
    SITE_ID: 'ib',
};

// var query = {
//     c: 'ibv:results',
//     action: 'result',
//     mode: 'class'
// };

// var request = $.ajax({
//     url: '/bitrix/services/main/ajax.php?' + $.param(query, true),
//     method: 'POST',
//     data: data
// });

// request.done(function (response) {
//     console.log(response);
// });

var Result = {
    c: 'ibv:results',
    action: 'result',
    mode: 'class'
};

var request = $.ajax({
    url: '/bitrix/services/main/ajax.php?' + $.param(Result, true),
    method: 'POST',
    dataType: "html",
    data: data,
    success: function(data){
        data = JSON.parse(data);
        $('.js-panel-btn .header__result-badge').html(data.data)
    }
});
