<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// разбираем url
//$url = $_SERVER["REQUEST_URI"];
//$url = explode("/", $url);
//if ($url[count($url) - 1]) {
//    $data = $url[count($url) - 1];
//} else {
//    $data = $url[count($url) - 2];
//}
//$data = explode("a", $data);

// дата
//$date = $data[3];
//$day = substr($date, 0, 2);
//$month = substr($date, 2, 2);
//$year = substr($date, 4, 4);
//$formatCal = $year . "-" . $month . "-" . $day;

//$date = new DateTime($formatCal . "T00:00:00+00:00");
//$dateFrom = $date->getTimestamp();

$dayofweek = array(
    '', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'
);

$_monthsList = array(
    "1" => "января",
    "2" => "февраля",
    "3" => "марта",
    "4" => "апреля",
    "5" => "мая",
    "6" => "июня",
    "7" => "июля",
    "8" => "августа",
    "9" => "сентября",
    "10" => "октября",
    "11" => "ноября",
    "12" => "декабря"
);

// получаем города и страны
$cities = file($_SERVER["DOCUMENT_ROOT"] . '/enginetais/files/city.json');
$cities = json_decode($cities[0], true);
$countries = file($_SERVER["DOCUMENT_ROOT"] . '/enginetais/files/country.json');
$countries = json_decode($countries[0], true);


//// данные для формы поиска поля откуда
//$fromInput = "";
//$country = "";
//$fromCity = "";
//foreach ($cities as $item) {
//    if ($item['codeen'] == $data[1]) {
//        $fromCity = mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE);
//        $fromInput .= mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE) . ', ';
//        $country = $item['country'];
//        break;
//    }
//}
//$fromInput .= mb_convert_case(mb_strtolower($countries[$country]['name']), MB_CASE_TITLE) . ', ' . $data[1];
//
//
//// данные для формы поиска поля куда
//$toInput = "";
//$country = "";
//$toCity = "";
//foreach ($cities as $item) {
//    if ($item['codeen'] == $data[2]) {
//        $toCity = mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE);
//        $toInput .= mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE) . ', ';
//        $country = $item['country'];
//        break;
//    }
//}
//$toInput .= mb_convert_case(mb_strtolower($countries[$country]['name']), MB_CASE_TITLE) . ', ' . $data[2];

echo '
        <div class="just-photo decstop-img just-adaptive">
        <script>
            var justphoto = document.getElementsByClassName(\'just-photo\')[0];
            var justphotoAdaptive = document.getElementsByClassName(\'photo-for-adaptive\')[0];
            var clientWidth = document.body.clientWidth;
            var percentWidth = clientWidth/1920;
            var percentHeight = Math.floor(630*percentWidth);
        </script>
        <div class="main-order-block">
            <div class="main-order-block__form">
                <div class="mof-input departure-inp">
                    <label class="input-label" for="order_from">Откуда</label>
                    <div>
                        <input type ="text" class="mof-input-item city required" id="order_from" name="order_from" autocomplete="off">
                        <div class="modal-city-from modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input arrival-inp">
                    <label class="input-label" for="order_to">Куда</label>
                    <div>
                        <input type="text" class="mof-input-item city required" id="order_to" autocomplete="off">
                        <div class="modal-city-to modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input">
                    <label class="input-label "  for="order_attrebute_date_0" >Дата</label>
                    <div>
                        <? if($MainConstruct->isPhone) : ?>
                            <input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_0" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
                        <? else : ?>
                            <input class="mof-input-item mof-input-item_date datepicker datepicker2 required" data-dp-class="custom" id="order_attrebute_date_0" autocomplete="off"/>
                        <? endif; ?>
                    </div>
                </div>
                <div class="mof-input leg-pass">
                    <div class="mof-input_add-pass">
                        <div class="mof-input_add-pass-btn"></div>
                        <div class="mof-input_add-pass-btn"></div>
                    </div>
                    <label class="input-label" for="add_pass">Пассажиры</label>
                    <div><input class="mof-input-item pass required" id="add_pass" autocomplete="off" type="number"></div>
                </div>
                <? if($MainConstruct->isPhone) : ?>
                    <div class="for-cryak">
                        <div class="for-cryak__routes"></div>
                        <div class="for-cryak__date"></div>
                    </div>
                <? endif; ?>
            </div>



            <div class="main-order-block__form display-none">
                <div class="mof-input departure-inp">
                    <label class="input-label" for="order_from2"><?= GetMessage(\'from\') ?></label>
                    <div>
                        <input type ="text" class="mof-input-item city required" id="order_from2" autocomplete="off">
                        <div class="modal-city-from2 modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input arrival-inp">
                    <label class="input-label" for="order_to2"><?= GetMessage(\'to\') ?></label>
                    <div>
                        <input type="text" class="mof-input-item city required" id="order_to2" autocomplete="off">
                        <div class="modal-city-to2 modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input">
                    <label class="input-label" for="order_attrebute_date_2"><?= GetMessage(\'date\') ?></label>
                    <div>
                        <? if($MainConstruct->isPhone) : ?>
                            <input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_2" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
                        <? else : ?>
                            <input class="mof-input-item mof-input-item_date datepicker required" data-dp-class="custom" id="order_attrebute_date_2" autocomplete="off"/>
                        <? endif; ?>
                    </div>
                </div>
                <div class="mof-input leg-pass">
                    <div class="mof-input_add-pass">
                        <div class="mof-input_add-pass-btn"></div>
                        <div class="mof-input_add-pass-btn"></div>
                    </div>
                    <label class="input-label" for="add_pass2"><?= GetMessage(\'passengers\') ?></label>
                    <div><input class="mof-input-item pass required" id="add_pass2" autocomplete="off"></div>
                </div>
                <? if($MainConstruct->isPhone) : ?>
                    <div class="for-cryak">
                        <div class="for-cryak__routes"></div>
                        <div class="for-cryak__date"></div>
                    </div>
                <?endif;?>
                <div class="del-block"></div>
            </div>


            <div class="main-order-block__form display-none">
                <div class="mof-input departure-inp">
                    <label class="input-label" for="order_from3"><?= GetMessage(\'from\') ?></label>
                    <div>
                        <input type ="text" class="mof-input-item city required" id="order_from3" autocomplete="off">
                        <div class="modal-city-from3 modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input arrival-inp">
                    <label class="input-label" for="order_to3"><?= GetMessage(\'to\') ?></label>
                    <div>
                        <input type="text" class="mof-input-item city required" id="order_to3" autocomplete="off">
                        <div class="modal-city-to3 modal-city-it"></div>
                    </div>
                </div>
                <div class="mof-input">
                    <label class="input-label" for="order_attrebute_date_3"><?= GetMessage(\'date\') ?></label>
                    <div>
                        <? if($MainConstruct->isPhone) : ?>
                            <input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_3" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
                        <? else : ?>
                            <input class="mof-input-item mof-input-item_date datepicker required" data-dp-class="custom" id="order_attrebute_date_3" autocomplete="off"/>
                        <? endif; ?>
                    </div>
                </div>
                <div class="mof-input leg-pass">
                    <div class="mof-input_add-pass">
                        <div class="mof-input_add-pass-btn"></div>
                        <div class="mof-input_add-pass-btn"></div>
                    </div>
                    <label class="input-label" for="add_pass3"><?= GetMessage(\'passengers\') ?></label>
                    <div><input class="mof-input-item pass required" id="add_pass3" type="number" autocomplete="off"></div>
                </div>
                <? if($MainConstruct->isPhone) : ?>
                    <div class="for-cryak">
                        <div class="for-cryak__routes"></div>
                        <div class="for-cryak__date"></div>
                    </div>
                <?endif;?>
                <div class="del-block"></div>
            </div>


            <a href="#main-form-order" class="mof-btn">Найти</a>
            <!-- <div class="main-order-block__manage-btns">
                <div class="manage-btn main-order-block__add-transfer">Добавить перелет</div>
                <div class="manage-btn main-order-block__add-days">± 3 дня</div>
                <div class="manage-btn main-order-block__add-days">± 15 дней</div>
            </div> -->
        </div>


        <script>
            //добавляем пассажиров (как смог)
            let legPass = document.getElementsByClassName(\'leg-pass\'); // дивы с пассажирами
            let passangers = document.getElementsByClassName(\'pass\'); // инпуты с пассажирами
            for (let i = 0 ; i < legPass.length; i++){
                legPass[i].firstElementChild.firstElementChild.onmousedown = (function(){

                    let formNumber = i;
                    return function(){
                        let PasAmount = legPass[formNumber].getElementsByClassName(\'pass\')[0];
                        let passValue = PasAmount.value;
                        PasAmount.value = (PasAmount.value == "") ? 1 : ++passValue;


                        legPass[formNumber].children[1].classList.add("label-active");
                    }
                } )();
            }

            //убавляем пассажиров (как смог)
            for (let i = 0 ; i < legPass.length; i++){
                legPass[i].firstElementChild.lastElementChild.onclick = (function(){

                    let formNumber = i;
                    return function(){
                        let PasAmount = legPass[formNumber].getElementsByClassName(\'pass\')[0];
                        let passValue = PasAmount.value;
                        PasAmount.value = passValue > 1 ? --passValue : "";
                    }
                } )();
            }


            let mainOrderBlock = document.getElementsByClassName(\'main-order-block\')[0];// блок со всеми формами
            let forms = document.getElementsByClassName(\'main-order-block__form\'); // формы
            let dalee = document.getElementsByClassName(\'mof-btn\')[0]; // кнопка далее
            let comeback = document.getElementsByClassName(\'comeback\')[0]; //кнопка добавить обратный рейс
            let addTransfer = document.getElementsByClassName(\'main-order-block__add-transfer\')[0];//кнопка добавить пересадку
            let addPreferences = document.getElementById(\'prefencesSubmit\');//кнопка добавить предпочтения

            $(".main-order-block__add-days").click(function(){
                $(this).toggleClass("main-order-block__add-days_active");
            });


            // добавляем пересадочку
            var formCount = 1;
            var centerBlockHid = 0;

            //делаем активный лейбл меньше
            let addActiveLabel = function(input){
                input.parentElement.previousElementSibling.classList.add("label-active");
            }

            $(\'body\').on(\'click\', \'.add-preferences\', function(e){
                e.preventDefault();
                $(\'#formMainOrderPreferences\').addClass(\'form-orderMtl_active\');
            });

            var mofInputs = document.getElementsByClassName(\'mof-input-item\');
            for (var i = 0; i < mofInputs.length; i++){
                mofInputs[i].value = "";
                mofInputs[i].onfocus = (function(){
                    var x = i;
                    return function(){
                        mofInputs[x].parentElement.previousElementSibling.classList.add("label-active");
                    }
                })();
            }
            let checkLebels = function(){
                for (var i = 0; i < mofInputs.length; i++){
                    if(mofInputs[i].value==""){
                        mofInputs[i].parentElement.previousElementSibling.classList.remove("label-active");
                        mofInputs[i].parentElement.previousElementSibling.classList.remove("input-label__error");
                    }
                }
            }
            var isDatepicker = function(element, className){
                while(element){
                    if(element.matches(className)){
                        return true;
                        break;
                    }else{
                        element = element.parentElement;
                    }
                }
                return false;
            }
            let doImputs = function(e){
                if(isDatepicker(e.target,\'.ui-datepicker\')||isDatepicker(e.target,\'.ui-datepicker-header\')||e.target.classList.contains(\'mof-btn\')){
                }else if(e.target.matches(\'.mof-input-item\')){
                    checkLebels();
                    e.target.parentElement.previousElementSibling.classList.add("label-active");
                }else{
                    checkLebels();
                }
            }
            let doImputsOnPress = function(e){
                if(e.key == "Tab"){
                    checkLebels();
                }
            }
            window.addEventListener(\'click\', doImputs);
            dalee.addEventListener(\'click\', checkLebels);
            window.addEventListener(\'keydown\', doImputsOnPress);
        </script>
    </div>
    <script>
        setTimeout(function(){
            document.getElementsByClassName(\'main-order-block\')[0].classList.add("main-order-block_active");
        }, 300);
    </script>
        ';

echo '
<div class="js-panel-btn">
    <div class="header__result-badge">  
    </div>
 </div>
';