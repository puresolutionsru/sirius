<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

class SearchResult extends CBitrixComponent implements Controllerable
{

    public function configureActions()
    {
        return [
            'result' => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                        array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    ),
                ],
                'postfilters' => []
            ]
        ];
    }

    public function getDataAction()
    {
        return $this->getDataResult();
    }


    public function getDataResult(){
        return 'result data';
    }

    /**
     * @return mixed
     */
    public function resultAction()
    {
        return 'result';
    }

    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate();
    }

}
