<?
use Bitrix\Main\Localization\Loc;

if(!check_bitrix_sessid())
	return;

if($ex = $APPLICATION->GetException()) :
	echo CAdminMessage::ShowMessage(array(
		"TYPE" 		=> "ERROR",
		"MESSAGE" 	=> Loc::getMessage("RP_TURBO_UNINST_ERR"),
		"DETAILS"	=> $ex->GetString(),
		"HTML"		=> true,
	));
else :
	echo CAdminMessage::ShowNote(Loc::getMessage("RP_TURBO_UNINST_OK"));
endif;
?>

<form action="<?= $APPLICATION->GetCurPage(); ?>">
	<input type="hidden" name="lang" value="<?= LANGUAGE_ID; ?>" />
	<input type="submit" value="<?= Loc::getMessage("RP_TURBO_UNSTEP_SUBMIT_BACK"); ?>">
</form>