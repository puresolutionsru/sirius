<?
use Bitrix\Main\Localization\Loc;

if(!check_bitrix_sessid())
	return;

echo CAdminMessage::ShowMessage(array(
	"TYPE" 		=> "ERROR",
	"MESSAGE" 	=> Loc::getMessage("RP_TURBO_UNINST_WAR"),
	"DETAILS"	=> '',
	"HTML"		=> true,
));
?>

<form action="<?= $APPLICATION->GetCurPage(); ?>">
	<?= bitrix_sessid_post() ?>
	<input type="hidden" name="lang" value="<?= LANGUAGE_ID; ?>">
	<input type="hidden" name="id" value="rp.turboyandex">
	<input type="hidden" name="uninstall" value="Y">
	<input type="hidden" name="step" value="2">
	<p><?= Loc::getMessage("RP_TURBO_SAVE_INFO") ?></p>
	<p>
		<input type="checkbox" id="savedata" name="savedata" checked>
		<label for="savedata"><?= Loc::getMessage("RP_TURBO_SAVE_LABEL"); ?></label>
	</p>
	<input type="submit" value="<?= Loc::getMessage("RP_TURBO_UNSTEP_SUBMIT"); ?>">
</form>