<?
use Bitrix\Main\Localization\Loc;

if(!check_bitrix_sessid())
	return;

if($ex = $APPLICATION->GetException()) :
	echo CAdminMessage::ShowMessage(array(
		"TYPE" 		=> "ERROR",
		"MESSAGE" 	=> Loc::getMessage("RP_TURBO_INST_ERR"),
		"DETAILS"	=> $ex->GetString(),
		"HTML"		=> true,
	));
else :
	echo CAdminMessage::ShowNote(Loc::getMessage("RP_TURBO_INST_OK"));
endif;
?>

<form action="<?= $APPLICATION->GetCurPage(); ?>">
	<input type="hidden" name="lang" value="<?= LANGUAGE_ID; ?>">
	<input type="submit" value="<?= Loc::getMessage("RP_TURBO_STEP_SUBMIT_BACK"); ?>">
</form>