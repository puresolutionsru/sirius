<?
/**
 * Created by RP
 * User: Vitaly Kolosov 
 * rp.ru
 * @ Web-studio
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;

Loc::loadMessages(__FILE__);

class rp_turboyandex extends CModule {

    public $exclusionAdminFiles;

    public function __construct() {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");

        $this->MODULE_ID           = str_replace("_", ".", get_class($this));
        $this->MODULE_VERSION 	   = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME 		   = Loc::getMessage("RP_TURBO_NAME");
        $this->MODULE_DESCRIPTION  = Loc::getMessage("RP_TURBO_DESCRIPTION");
        $this->PARTNER_NAME 	   = Loc::getMessage("RP_TURBO_PARTNER_NAME");
        $this->PARTNER_URI  	   = Loc::getMessage("RP_TURBO_PARTNER_URI");

        $this->exclusionAdminFiles = array(
            '..',
            '.',
            'menu.php',
            'operation_description.php',
            'task_description.php',
        );

        $this->AddLinksInMenu();
    }

    /**
     * Install module
     */
    public function DoInstall() {
        global $APPLICATION;
        if($this->isVersionD7()) :
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
            ModuleManager::registerModule($this->MODULE_ID);
        else :
            $APPLICATION->ThrowException(Loc::getMessage("RP_TURBO_INSTALL_ERROR_VERSION"));
        endif;

        $APPLICATION->IncludeAdminFile(Loc::getMessage("RP_TURBO_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    public function InstallFiles($arParams = array()) {
        CopyDirFiles($this->GetPath() . "/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);

        if(Directory::isDirectoryExists($path = $this->GetPath() . "/admin")) :
            if($dir = opendir($path)) :
                while(false != $item = readdir($dir)) {
                    if(in_array($item, $this->exclusionAdminFiles))
                        continue;
                    file_put_contents(
                        $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $this->MODULE_ID . "_" . $item,
                        '<? require($_SERVER["DOCUMENT_ROOT"] . "' . $this->GetPath(true) . '/admin/' . $item . '"); ?>'
                    );
                }
                closedir($dir);
            endif;
        endif;

        return true;
    }

    public function InstallEvents() {
        // EventManager::getInstance()->registerEventHandler(
        //     "main",
        //     "OnBeforeEndBufferContent",
        //     $this->MODULE_ID,
        //     "Falbar\ToTop\Main",
        //     "appendScriptsToPage"
        // );
    }

    public function InstallDB() {
        
    }

    /**
     * Uninstall module
     */
    public function DoUninstall() {
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if($request["step"] < 2) :
            $APPLICATION->IncludeAdminFile(Loc::getMessage("RP_TURBO_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep1.php");
        elseif($request["step"] == 2) :
            $this->UnInstallFiles();
            $this->UnInstallEvents();

            if($request["savedata"] != "Y")
                $this->UnInstallDB();

            ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage("RP_TURBO_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep2.php");
        endif;
    }

    public function UnInstallFiles() {
        Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/rp/");
        if(Directory::isDirectoryExists($path = $this->GetPath() . '/admin')) :
            DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . $this->GetPath() . "/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
            if($dir = opendir($path)) :
                while(false !== $item = readdir($dir)) {
                    if(in_array($item, $this->exclusionAdminFiles))
                        continue;
                    File::deleteFile($_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/" . $this->MODULE_ID . "_" . $item);
                }
                closedir($dir);
            endif;
        endif;

        return true;
    }

    public function UnInstallEvents(){
        // EventManager::getInstance()->unRegisterEventHandler(
        //     "main",
        //     "OnBeforeEndBufferContent",
        //     $this->MODULE_ID,
        //     "Falbar\ToTop\Main",
        //     "appendScriptsToPage"
        // );
    }

    public function UnInstallDB() {
        Option::delete($this->MODULE_ID);
    }

    /**
     * Check if the version supports D7
     * @return boolean
     */
    public function isVersionD7() {
        return CheckVersion(ModuleManager::getVersion("main"), "14.00.00");
    }

    /**
     * Function return path to the module folder
     * @return string
     */
    public function GetPath($notDocumentRoot = false) {
        if($notDocumentRoot) :
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else :
            return dirname(__DIR__);
        endif;
    }

    /**
     * Function return path to the module folder
     * @return string
     */
    public function GetModuleRightList() {
        return array(
            "reference_id" => array("D", "K", "S", "W"),
            "reference" => array(
                "[D] " . "Текст прав D" . Loc::getMessage("RP_TURBO_REF_DENIED"),
                "[K] " . "Текст прав K" . Loc::getMessage("RP_TURBO_REF_READ_COMPONENT"),
                "[S] " . "Текст прав S" . Loc::getMessage("RP_TURBO_REF_WRITE_SETTINGS"),
                "[W] " . "Текст прав W" . Loc::getMessage("RP_TURBO_REF_FULL")
            )
        );
    }






















    public function AddLinksInMenu() {
        AddEventHandler("main", "OnBuildGlobalMenu", function(&$adminMenu, &$moduleMenu){
            $moduleMenu[] = array(
                "parent_menu" 	=> "global_menu_services",
                "section" 	=> "ваше название аля модуля",
                "sort"			=> 1000,
                "url"			=> $this->MODULE_ID . "_page.php?lang=" . LANG . "&mid=rp.turboyandex",
                "text"			=> 'Yandex Turbo',
                "title"       => 'Курс Валют ЦБР', // текст всплывающей подсказки
                "icon"		=> "form_menu_icon", // малая иконка
                "page_icon"	=> "form_page_icon", // большая иконка
                "items_id"    => "menu_ваше название аля модуля",  // идентификатор ветви
                "items"       => array()          // остальные уровни меню сформируем ниже.
            );
        });
    }


    
}