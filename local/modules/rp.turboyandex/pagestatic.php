<?php
use Bitrix\Main\Application;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
include 'lib/simple_html_dom.php';

$rss = '<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" xmlns:turbo="http://turbo.yandex.ru" version="2.0"><channel>';

$pages = array(
    'https://www.rusline.aero/read/rules_tariff/brand_tariffes/' => 'true',
);


foreach($pages as $key=>$value) :
    $gethtml = file_get_contents($key);

    $domOb = new DOMDocument();
    $html = $domOb->loadHTML($gethtml);
    $domOb->preserveWhiteSpace = false;
    
    $container = $domOb->getElementsByTagName('title');
    $title = $container->item(0)->nodeValue; 
    
    $container = $domOb->getElementsByTagName('h1');
    $h1 = $container->item(0)->nodeValue;

    $content = explode('<!-- FORRSS -->', $gethtml);
    $content[1] = str_replace('href="/', 'href="' . $_SERVER["HTTP_X_FORWARDED_PROTO"] . '://' . $_SERVER["HTTP_HOST"] . '/', $content[1]);
    $content[1] = str_replace('src="/', 'src="' . $_SERVER["HTTP_X_FORWARDED_PROTO"] . '://' . $_SERVER["HTTP_HOST"] . '/', $content[1]);
    // $content[1] = trim(str_replace(array("\r", "\n", "\t"), '', $content[1]));

    $rss .= '<item turbo="'.$value.'">';
    $rss .= '<link>'.$key.'</link>';
    $rss .= '<title>'.$title.'</title>';
    $rss .= '<turbo:content><![CDATA[<header><h1>'.$h1.'</h1></header>';
    $rss .= $content[1];
    $rss .= ']]></turbo:content>';
    $rss .= '</item>';

endforeach;

$rss .= '</channel></rss>';

$createFile = file_put_contents($_SERVER["DOCUMENT_ROOT"] . str_ireplace(Application::getDocumentRoot(), '', __DIR__) . '/pagestatic.rss', $rss);