<?
$MESS["RP_TURBO_NAME"]                      = "Турбо-страницы Yandex";
$MESS["RP_TURBO_DESCRIPTION"]               = "Модуль создания RSS-канала для использования Турбо-страниц в результатах поиска Яндекса.";
$MESS["RP_TURBO_PARTNER_NAME"]              = "rp";
$MESS["RP_TURBO_PARTNER_URI"]               = "https://rp.su/";
$MESS["RP_TURBO_INSTALL_ERROR_VERSION"]     = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["RP_TURBO_INSTALL_TITLE"]             = "Установка модуля";
$MESS["RP_TURBO_UNINSTALL_TITLE"]           = "Деинсталляция модуля";
