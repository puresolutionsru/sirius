<?
$MESS["RP_TURBO_OPTIONS_TAB_NAME"] 		 	    = "Настройки";
$MESS["RP_TURBO_OPTIONS_YA_PARAM"]       	 	= "Параметры подключения яндекса";
$MESS["RP_TURBO_OPTIONS_TAB_AUTO_SEND"]       	= "При отмеченной опции и указанном токене документ будет отправляться в яндекс";
$MESS["RP_TURBO_OPTIONS_TAB_AUTH"]       	 	= "Токен";

$MESS["RP_TURBO_OPTIONS_INPUT_APPLY"]           = "Применить";
$MESS["RP_TURBO_OPTIONS_INPUT_DEFAULT"]         = "По умолчанию";
