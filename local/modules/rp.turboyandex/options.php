<?
use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");
Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

if($APPLICATION->GetGroupRight($module_id) < "S")
	$APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));

Loader::includeModule($module_id);

# Options
$aTabs = array(
	array(
		"DIV" 	  => "edit",
		"TAB" 	  => Loc::getMessage("RP_TURBO_OPTIONS_TAB_NAME"),
		"TITLE"   => Loc::getMessage("RP_TURBO_OPTIONS_TAB_NAME"),
		"OPTIONS" => array(
			Loc::getMessage("RP_TURBO_OPTIONS_YA_PARAM"),
			array(
				"rp_auto_send",
				Loc::getMessage("RP_TURBO_OPTIONS_TAB_AUTO_SEND"),
				"Y",
				array("checkbox")
			),
			array(
				"rp_ya_auth",
				Loc::getMessage("RP_TURBO_OPTIONS_TAB_AUTH"),
				"",
				array("text", 50)
			),
		)
	),
	array(
		"DIV"		=> "edit2",
		"TAB"		=> "Доступ",
		"TITLE"		=> "Доступ",
	),
);

# Save
if($request->isPost() && $request["Update"] && check_bitrix_sessid()) {
	foreach($aTabs as $aTab) :
		foreach($aTab["OPTIONS"] as $arOption) :
			if(!is_array($arOption))
				continue;

			if($arOption["note"])
				continue;

			$optionName = $arOption[0];
			$optionValue = $request->getPost($optionName);

			Option::set($module_id, $optionName, is_array($optionValue) ? implode('','',$optionValue) : $optionValue);
			// if($request["apply"]) :
			// 	$optionValue = $request->getPost($arOption[0]);
			// 	if($arOption[0] == "switch_on") {
			// 		if($optionValue == "") {
			// 			$optionValue = "N";
			// 		}
			// 	}
			// 	Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
			// elseif ($request["default"]) :
			// 	Option::set($module_id, $arOption[0], $arOption[2]);
			// endif;
		endforeach;
	endforeach;
	LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANGUAGE_ID);
}

#Visual
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form action="<?= $APPLICATION->GetCurPage(); ?>?mid=<?= $module_id; ?>&lang=<?= LANGUAGE_ID; ?>" method="post">
	<?
	foreach($aTabs as $aTab) :
		if($aTab["OPTIONS"]) :
			$tabControl->BeginNextTab();
			__AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
		endif;
	endforeach;
	$tabControl->BeginNextTab();
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
	$tabControl->Buttons();
	?>
	<input type="submit" name="Update" value="<?= Loc::GetMessage("RP_TURBO_OPTIONS_INPUT_APPLY"); ?>" class="adm-btn-save">
	<input type="submit" name="reset" value="<?= Loc::GetMessage("RP_TURBO_OPTIONS_INPUT_DEFAULT"); ?>">
	<?= bitrix_sessid_post(); ?>
</form>
<? $tabControl->End(); ?>