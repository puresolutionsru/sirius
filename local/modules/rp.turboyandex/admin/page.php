<?
use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

Loader::includeModule("iblock");

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

$listinfoblock = array();
$res = CIBlock::GetList(
    Array('name' => 'ASC'),
    Array('ACTIVE' => 'Y', 'SITE_ID' => 's1')
);

$blockfalse = array(
    'https://sirius-aero.ru/about/smi/business-aviation-magazine/',
);


/***************************************************************************
							GET | POST processing
***************************************************************************/

$createFile = false;
if($_POST['Send']) :
    $rss = '<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" xmlns:turbo="http://turbo.yandex.ru" version="2.0">';
    $rss .= '<channel>';
    foreach($_POST["infoblocks"] as $item) :
        if($_POST['info-' . $item]) :
            $el = CIBlockElement::GetList(Array('name' => 'ASC'), Array('IBLOCK_ID' => $item, 'ACTIVE' => 'Y', 'SITE_ID' => 's1'));
            while($ar_el = $el->GetNext()) {
                $db_props = CIBlockElement::GetProperty($item, $ar_el["ID"], array("sort" => "asc"), array("CODE"=>"LANG"));
                if($ar_props = $db_props->Fetch()) :
                    $lang = $ar_props['VALUE_XML_ID'];
                endif;

                if($lang == 'ru') {
                    $ar_el["DETAIL_PAGE_URL"] = str_replace('/de/', '/', $ar_el["DETAIL_PAGE_URL"]);
                }
                else {
                    $ar_el["DETAIL_PAGE_URL"] = str_replace('/de/', '/'.$lang.'/', $ar_el["DETAIL_PAGE_URL"]);
                }

                $ar_el["DETAIL_TEXT"] = str_replace('src="/', 'src="' . $_SERVER["HTTP_ORIGIN"] . '/', $ar_el["DETAIL_TEXT"]);

                $linkturbo = $_SERVER["HTTP_ORIGIN"] . $ar_el["DETAIL_PAGE_URL"];
                if(in_array($linkturbo, $blockfalse)) {
                    $rss .= '<item turbo="false">';
                } else {
                    $rss .= '<item turbo="true">';
                }
                $rss .= '<link>' . $linkturbo . '</link>';
                $rss .= '<title>' . $ar_el["NAME"] . '</title>';
                $rss .= '<turbo:content>';
                $rss .= '<![CDATA[';
                $rss .= '<header><h1>' . $ar_el["NAME"] . '</h1></header>' . $ar_el["DETAIL_TEXT"];
                $rss .= ']]>';
                $rss .= '</turbo:content></item>';
            }
        endif;
    endforeach;

    $rss .= '</channel>';
    $rss .= '</rss>';
    $createFile = file_put_contents($_SERVER["DOCUMENT_ROOT"] . str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__)) . '/blocks.rss', $rss);
endif;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
/***************************************************************************
							HTML form
****************************************************************************/

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<form action="" method="post">
    <div class="adm-detail-block" id="tabControl_layout">
        <div class="adm-detail-content-wrap">
            <div class="adm-detail-content">
                <div class="adm-detail-title">Яндекс турбо</div>
                <? if($createFile !== FALSE) :
                    echo CAdminMessage::ShowNote('Файл успешно создан! '. $_SERVER["HTTP_ORIGIN"] . rp_turboyandex::GetPath(true).'/blocks.rss');
                endif; ?>
                <div class="adm-detail-content-item-block">
                    <table class="adm-detail-content-table edit-table" id="edit_edit_table" style="opacity: 1;">
                        <tbody>
                            <tr class="heading">
                                <td colspan="2">Выберите инфоблоки для импорта</td>
                            </tr>
                            <? while($ar_res = $res->Fetch()) {  ?>
                                <tr>
                                    <td width="50%" class="adm-detail-content-cell-l"><label for="<?= $ar_res['CODE'] . $ar_res['ID'] ?>"><?= $ar_res['NAME'] ?></label><a name="opt_rp_auto_send"></a></td>
                                    <td width="50%" class="adm-detail-content-cell-r"><input type="checkbox" id="<?= $ar_res['CODE'] . $ar_res['ID'] ?>" name="info-<?= $ar_res['ID'] ?>" value="N" class="adm-designed-checkbox">
                                        <label class="adm-designed-checkbox-label" for="<?= $ar_res['CODE'] . $ar_res['ID'] ?>" title=""></label>
                                        <input type="hidden" name="infoblocks[]" value="<?= $ar_res['ID'] ?>">
                                    </td>
                                </tr>
                            <? } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <? $tabControl->Buttons(); ?>
            <input type="submit" name="Send" value="Отправить" class="adm-btn-save">
            <?= bitrix_sessid_post(); ?>
        </div>
    </div>
</form>
<? $tabControl->End(); ?>

<? require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php"); ?>