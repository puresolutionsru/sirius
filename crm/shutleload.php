<?
require_once ('crest.php');
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;




$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
$firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
$database = $firebase->getDatabase();




$key = 12566;
$id = '"' . $key . '"';

$cost = 4500;
$costType = "seat";
$currency = "€";
$date = 1595674800;
$detailsPath = "Base";
$fromcity = "Pisa";
$fromCityCode = "LIRP";
$images = ["Shuttle_1_1.jpg", "Shuttle_1_2.jpg", "Shuttle_2_1.jpg", "Shuttle_2_2.jpg", "Shuttle_3_1.jpg", "Shuttle_3_2.jpg"];
$inFlightTime = 210;
$planeClass = "Business Jet";
$seatsAll = "14";
$seatsFree = 14;
$toCity = "Moscow";
$toCityCode = "UUWW";


$postData = [
    'Cost'              => (int) $cost,
    'CostType'          => $costType,
    'Currency'          => $currency,
    'Date'              => (int) $date,
    'DetailsPath'       => $detailsPath,
    'FromCity'          => $fromcity,
    'FromCityCode'      => $fromCityCode,
    'Images'            => $images,
    'InFlightTime'      => (int) $inFlightTime,
    'PlaneClass'        => $planeClass,
    'SeatsAll'          => $seatsAll,
    'SeatsFree'         => (int) $seatsFree,
    'ToCity'            => $toCity,
    'ToCityCode'        => $toCityCode,
];

$database->getReference('ShutleFull/'.$id)->set($postData);