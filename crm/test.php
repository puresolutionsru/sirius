<?

$codes = array();
$datafile = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/city.json'));

foreach($datafile as $data) {
    $dop = 0;

    $codes[$data->name] = str_replace(":00", "", $data->timezone);
    $codes[$data->name] = str_replace("+0", "+", $codes[$data->name]);
    $codes[$data->name] = str_replace("-0", "-", $codes[$data->name]);
    if(stripos($codes[$data->name], ":30")) {
        $codes[$data->name] = str_replace(":30", "", $codes[$data->name]);
        $dop = 1800;
    }
    if(stripos($codes[$data->name], ":45")) {
        $codes[$data->name] = str_replace(":45", "", $codes[$data->name]);
        $dop = 2700;
    }

    $codes[$data->name] = 3600 * $codes[$data->name] + $dop;
}

$codes = json_encode($codes);

echo $codes;

file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/test.txt', $codes);