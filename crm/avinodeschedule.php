<?
require_once ('crest.php');
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

// получаем из crm расписание
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/legs?auth_key=YXaH83pKK5U4w*HUwonbaCd@aB7WJ&date_from=1000000&date_to=15600000000");
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$schedules = curl_exec($ch);
$schedules = json_decode($schedules, true);
curl_close($ch);

$twoavi = array("9H-IDB", "LY-BGH", "9H-BSG");
$sendAr = array();

if($schedules) :

    foreach($schedules as $key=>$data) {
        $whattimeisit = time();
        if($whattimeisit > ((int)$data['arrivalDate'])) {
            continue;
        }

        // даннае самолета
        $cu = curl_init();
        curl_setopt($cu, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/aircrafts/id/".$data['aircraft_id']."?auth_key=".AUTH_KEY);
        curl_setopt($cu, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
        $aircraft = curl_exec($cu);
        $aircraft = json_decode($aircraft, true);
        curl_close($cu);

        date_default_timezone_set('UTC');


        $tail = $aircraft['tailNumber'];
        $number = 0;
        if(count($sendAr[$tail]["activities"]) > 0) {
            $number = count($sendAr[$tail]["activities"]);
        }

        $sendAr[$tail]["lift"]["aircraftTail"] = $aircraft['tailNumber'];
        $sendAr[$tail]["activities"][$number]["activityId"] = $data['id'];
        if($data['legType'] == "EmptyLeg") {
            $data['legType'] = "Empty Leg";
        }
        $sendAr[$tail]["activities"][$number]["activityType"] = $data['legType'];
        $sendAr[$tail]["activities"][$number]["startAirport"]["icao"] = $data['departureAirport'];
        $sendAr[$tail]["activities"][$number]["endAirport"]["icao"] = $data['arrivalAirport'];
        $sendAr[$tail]["activities"][$number]["startDateTime"] = $data['departureDate'];
        $sendAr[$tail]["activities"][$number]["startDateTime"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $sendAr[$tail]["activities"][$number]["startDateTime"]));
        $sendAr[$tail]["activities"][$number]["endDateTime"] = $data['arrivalDate'];
        $sendAr[$tail]["activities"][$number]["endDateTime"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $sendAr[$tail]["activities"][$number]["endDateTime"]));
        $sendAr[$tail]["activities"][$number]["paxCount"] = $data['numberOfSeats'];
        if(!$sendAr[$tail]["activities"][$number]["paxCount"]) {
            $sendAr[$tail]["activities"][$number]["paxCount"] = 0;
        }
        $sendAr[$tail]["activities"][$number]["price"] = $data['priceEmptyLegEUR'];
        if(!$sendAr[$tail]["activities"][$number]["price"]) {
            $sendAr[$tail]["activities"][$number]["price"] = 0;
        }
        $sendAr[$tail]["activities"][$number]["currency"] = "EUR";
        $sendAr[$tail]["activities"][$number]["active"] = true;
    }
    echo '<pre>';
    print_r($sendAr);
    echo '</pre>';

    foreach($sendAr as $k=>$v) {
        $request = json_encode($v);

        $now = str_replace("+00:00", ".001Z", date(DATE_ATOM, time()));

        if(in_array($k, $twoavi)) {
            $chPtwo = curl_init();
            curl_setopt($chPtwo, CURLOPT_URL, "https://services.avinode.com/api/schedules");
            curl_setopt($chPtwo, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chPtwo, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($chPtwo, CURLOPT_POSTFIELDS, $request);
            curl_setopt($chPtwo, CURLOPT_HTTPHEADER, array(
                'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
                'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI2OUM4QkY1MS1FMUNDLTQwNkMtOUM2Qi0zMjBBMjMxOEM4QUUiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjEyMjI5LCJpc3MiOiJhdmlub2RlIiwiYXZpdHlwZSI6MTUsImF2aW5vbmNlIjoiNzYwNDhhNTAtYjUxYy00MmFiLTk2YmMtMDEzZjM3MWRiMTc4In0.g-osAW9lPLzMw05oUT7HtnQU9beVw8wH_Xkg-7vrqQEQg6VyTubrhUXdDIx493WXdqEAkdZu2P4LcwO_XbFLUDi6TYEbKnNo30HJSztgyJaj9hV3aMFoeij43Ch0NrLpiC1Agwb9IBuLeH55ZG2ruTrcE1rSUuZNIhNpHDtCkrTiRE8CMJD11QkT8Bh2_Vyj7CoQdQrT8LXTfqEYcoFrKDipKMeHe9JvR21FSSoX6EzHOGDSBBoXlQAPKBfWsxHQumgLfRflOnNpOIdo9v6C3QXl3jgP16iq1oLfP78JpLpg1LCr9kl6ew9Qb-haJjsCHqPGk0wnYbhFk-sKIq9PVw',
                'Content-Type: application/json',
                'X-Avinode-SentTimestamp: '.$now
            ));
            $send = curl_exec($chPtwo);
            echo $k . '<br>';
            echo $send;
            echo '<br>';
            curl_close($chPtwo);
        } else {
            $chP = curl_init();
            curl_setopt($chP, CURLOPT_URL, "https://services.avinode.com/api/schedules");
            curl_setopt($chP, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chP, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($chP, CURLOPT_POSTFIELDS, $request);
            curl_setopt($chP, CURLOPT_HTTPHEADER, array(
                'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
                'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiIzQzhBRDEyOS0xNUIyLTRGN0YtQTdGNy01MzA0NUQ3MERCOTEiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjY1NjIsImlzcyI6ImF2aW5vZGUiLCJhdml0eXBlIjoxNSwiYXZpbm9uY2UiOiJmMTRmOTVhMC1lM2RhLTQwMzUtYjZiMS1hNDk1MzFjMjEzYTcifQ.SAv7z6DDay92uwBd6ec4K_12-ZwVIhcfCvUURBXNy6Rg-Gd3eXxqUcID4Pzw7zPofSq0qxBD5Iu3f6FeC1X_XQZXDt9iZooVrS0xzP4wku7Ji7RaDtAQ-nwn2gp6PLEt7uxLQePkLH-vWchaShzvNynLvuo35TmyV9YN-EFvhde2dPd3yjfLlRJ2cRT0zzIgkRYyYRdq4d4xOCl1kanDLDZLG-eztdQ-Rn-AMSVeH5IqRpP-jrTvAZG4-pfgfw5jXxh4AsuN2PBQlC8sUQQgo-QwMAYlRkx-JxVPjlP1MCIT1M91XEHUqEGXEsKUtB2JBSGTG_Bse-ggf6Jt2dxSvw',
                'Content-Type: application/json',
                'X-Avinode-SentTimestamp: '.$now
            ));
            $send = curl_exec($chP);
            echo $k . '<br>';
            echo $send;
            echo '<br>';
            curl_close($chP);
        }

    }

endif;