<?
require_once ('crest.php');
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
$firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
$database = $firebase->getDatabase();
$reference = $database->getReference('DealsFull');
$snapshot = $reference->getSnapshot();
$value = $snapshot->getValue();

$referenceShuttle = $database->getReference('ShutleFull');
$snapshotShuttle = $referenceShuttle->getSnapshot();
$valueShuttle = $snapshotShuttle->getValue();


foreach($value as $key=>$data) :
	$whattimeisit = time();
    if($whattimeisit > $data['EndDate']) :
        deleteRace($key);
	endif;
endforeach;

foreach($valueShuttle as $key=>$data) :
	$whattimeisit = time();
    if($whattimeisit > $data['Date']) :
        deleteShuttle($key);
	endif;
endforeach;


function deleteRace($key) {
	$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
    $database = $firebase->getDatabase();
    $database->getReference('DealsFull/'.$key)->remove();
}
function deleteShuttle($key) {
	$serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
    $database = $firebase->getDatabase();
    $database->getReference('ShutleFull/'.$key)->remove();
}

$info = $database->getReference('DealsShort')->getSnapshot()->getValue();
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/count.txt', count($info));
$infoShuttle = $database->getReference('ShutleShort')->getSnapshot()->getValue();
file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/shuttles/countAllow.txt', count($infoShuttle));

die();