<?
require_once ('crest.php');
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

// получаем из crm список emptylegs
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/emptylegs?auth_key=".AUTH_KEY."&date_to=1&date_from=9561765445");
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$emptylegs = curl_exec($ch);
$emptylegs = json_decode($emptylegs, true);
curl_close($ch);

echo '<pre>';
print_r($emptylegs);
echo '</pre>';

if($emptylegs) :
    // данные из firebase
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
    $database = $firebase->getDatabase();

    // данные deals из fb
    $reference = $database->getReference('DealsFull');
    $snapshot = $reference->getSnapshot();
    $arrayValuesDeals = $snapshot->getValue();
    // данные DealsShort из fb
    $Sreference = $database->getReference('DealsShort');
    $Ssnapshot = $Sreference->getSnapshot();
    $SarrayValuesDeals = $Ssnapshot->getValue();

    // данные shuttle из fb
    $referenceShuttle = $database->getReference('ShutleFull');
    $snapshotShuttle = $referenceShuttle->getSnapshot();
    $arrayValuesShuttles = $snapshotShuttle->getValue();
    // данные ShutleShort из fb
    $SreferenceShuttle = $database->getReference('ShutleShort');
    $SsnapshotShuttle = $SreferenceShuttle->getSnapshot();
    $SarrayValuesShuttles = $SsnapshotShuttle->getValue();




    // удаляем рейсы которые есть в фб но нет в crm и обновляем цены, тип заказа
    foreach($arrayValuesDeals as $dealKey=>$dealValue) {
        $dealsKeyInt = str_replace('"', '', $dealKey);
        $searchInDeals = findArray($emptylegs, $dealsKeyInt, array('id', 'aircraft_id', 'priceEmptyLegEUR', 'departureDate', 'arrivalDate', 'arrivalDateStart', 'arrivalDateEnd', 'legType', 'departureCity', 'arrivalCity', 'departureAirport', 'arrivalAirport'));
        if(!$searchInDeals[1] || $searchInDeals[7] == "Shuttle") {
            // удаляем рейс если его уже нет в crm
            $database->getReference('DealsFull/'.$dealKey)->remove();
        } else { // если рейс есть, то проверяем актуальность данных
            $startDate = 0;
            $endDate = 0;
            if($searchInDeals[5]) {
                $startDate = $searchInDeals[5];
            } else {
                $startDate = $searchInDeals[3];
            }
            
            if($searchInDeals[6]) {
                $endDate = $searchInDeals[6];
            } else {
                $endDate = $searchInDeals[4];
            }
            $fromCity = $searchInDeals[8];
            $fromCode = $searchInDeals[10];
            $toCity = $searchInDeals[9];
            $toCode = $searchInDeals[11];


            // меняем данные FULL если изменились
            if($arrayValuesDeals[$dealKey]['Cost'] != $searchInDeals[2]) {
                $database->getReference('DealsFull/'.$dealKey.'/Cost')->set((int) $searchInDeals[2]);
            }
            if($arrayValuesDeals[$dealKey]['StartDate'] != $startDate) {
                $database->getReference('DealsFull/'.$dealKey.'/StartDate')->set((int) $startDate);
            }
            if($arrayValuesDeals[$dealKey]['EndDate'] != $endDate) {
                $database->getReference('DealsFull/'.$dealKey.'/EndDate')->set((int) $endDate);
            }
            if($arrayValuesDeals[$dealKey]['FromCity'] != $fromCity) {
                $database->getReference('DealsFull/'.$dealKey.'/FromCity')->set($fromCity);
            }
            if($arrayValuesDeals[$dealKey]['FromCityCode'] != $fromCode) {
                $database->getReference('DealsFull/'.$dealKey.'/FromCityCode')->set($fromCode);
            }
            if($arrayValuesDeals[$dealKey]['ToCity'] != $toCity) {
                $database->getReference('DealsFull/'.$dealKey.'/ToCity')->set($toCity);
            }
            if($arrayValuesDeals[$dealKey]['ToCityCode'] != $toCode) {
                $database->getReference('DealsFull/'.$dealKey.'/ToCityCode')->set($toCode);
            }
            

            // меняем данные SHORT если изменились
            if($SarrayValuesDeals[$dealKey]['Cost'] != $searchInDeals[2]) {
                $database->getReference('DealsShort/'.$dealKey.'/Cost')->set((int) $searchInDeals[2]);
            }
            if($SarrayValuesDeals[$dealKey]['Date'] != $startDate) {
                $database->getReference('DealsShort/'.$dealKey.'/Date')->set((int) $startDate);
            }
            if($SarrayValuesDeals[$dealKey]['EndDate'] != $endDate) {
                $database->getReference('DealsShort/'.$dealKey.'/EndDate')->set((int) $endDate);
            }
            if($SarrayValuesDeals[$dealKey]['FromCity'] != $fromCity) {
                $database->getReference('DealsShort/'.$dealKey.'/FromCity')->set($fromCity);
            }
            if($SarrayValuesDeals[$dealKey]['ToCity'] != $toCity) {
                $database->getReference('DealsShort/'.$dealKey.'/ToCity')->set($toCity);
            }
        }
    }
    // удаляем шаттлы которые есть в фб но нет в crm и обновляем цены, тип заказа
    foreach($arrayValuesShuttles as $shuttleKey=>$shuttleValue) {
        $shuttleKeyInt = str_replace('"', '', $shuttleKey);
        $searchInShuttles = findArray($emptylegs, $shuttleKeyInt, array('id', 'aircraft_id', 'priceEmptyLegEUR', 'departureDate', 'arrivalDate', 'arrivalDateStart', 'arrivalDateEnd', 'legType', 'departureCity', 'arrivalCity', 'departureAirport', 'arrivalAirport', 'departureDateLocale', 'arrivalDateLocale'));
        if(!$searchInShuttles[1] || $searchInShuttles[7] == "EmptyLeg") {
            // удаляем рейс если его уже нет в crm
            $database->getReference('ShutleFull/'.$shuttleKey)->remove();
        } else {
            $startDate = $searchInShuttles[12];
            $flightTime = 0;
            if($searchInShuttles[4]) {
                $flightTime = ($searchInShuttles[4] - $searchInShuttles[3]) / 60;
            }
            $fromCity = $searchInShuttles[8];
            $fromCode = $searchInShuttles[10];
            $toCity = $searchInShuttles[9];
            $toCode = $searchInShuttles[11];


            // меняем цену если изменилась
            if($arrayValuesShuttles[$shuttleKey]['Cost'] != $searchInShuttles[2]) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/Cost')->set((int) $searchInShuttles[2]);
            }
            // меняем дату вылета если изменилась
            if($arrayValuesShuttles[$shuttleKey]['Date'] != $startDate) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/Date')->set((int) $startDate);
            }
            // меняем дату прилета если изменилась
            if($arrayValuesShuttles[$shuttleKey]['InFlightTime'] != $flightTime) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/InFlightTime')->set((int) $flightTime);
            }
            if($arrayValuesShuttles[$shuttleKey]['FromCity'] != $fromCity) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/FromCity')->set($fromCity);
            }
            if($arrayValuesShuttles[$shuttleKey]['FromCityCode'] != $fromCode) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/FromCityCode')->set($fromCode);
            }
            if($arrayValuesShuttles[$shuttleKey]['ToCity'] != $toCity) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/ToCity')->set($toCity);
            }
            if($arrayValuesShuttles[$shuttleKey]['ToCityCode'] != $toCode) {
                $database->getReference('ShutleFull/'.$shuttleKey.'/ToCityCode')->set($toCode);
            }






            // меняем цену если изменилась SHORT
            if($arrayValuesShuttles[$shuttleKey]['Cost'] != $searchInShuttles[2]) {
                $database->getReference('ShutleShort/'.$shuttleKey.'/Cost')->set((int) $searchInShuttles[2]);
            }
            if($SarrayValuesShuttles[$shuttleKey]['Date'] != $startDate) {
                $database->getReference('ShutleShort/'.$shuttleKey.'/Date')->set((int) $startDate);
            }
            if($SarrayValuesShuttles[$shuttleKey]['FromCity'] != $fromCity) {
                $database->getReference('ShutleShort/'.$shuttleKey.'/FromCity')->set($fromCity);
            }
            if($SarrayValuesShuttles[$shuttleKey]['ToCity'] != $toCity) {
                $database->getReference('ShutleShort/'.$shuttleKey.'/ToCity')->set($toCity);
            }
            if($SarrayValuesShuttles[$shuttleKey]['CostType'] != "seat") {
                $database->getReference('ShutleShort/'.$shuttleKey.'/CostType')->set("seat");
            }
            if($SarrayValuesShuttles[$shuttleKey]['Currency'] != "€") {
                $database->getReference('ShutleShort/'.$shuttleKey.'/Currency')->set("€");
            }
            if($SarrayValuesShuttles[$shuttleKey]['Image'] != "Shuttle_1_1.jpg") {
                $database->getReference('ShutleShort/'.$shuttleKey.'/Image')->set("Shuttle_1_1.jpg");
            }
            if($SarrayValuesShuttles[$shuttleKey]['PlaneSeats'] !== 14) {
                $database->getReference('ShutleShort/'.$shuttleKey.'/PlaneSeats')->set((int) 14);
            }
        }
    }




    // добавляем в firebase
    foreach($emptylegs as $key=>$data) {
        $whattimeisit = time();
        
        if($data['legType'] == 'EmptyLeg') {
            if(!$data['arrivalDateEnd']) {
                $data['arrivalDateEnd'] = $data['arrivalDate'];
            }
            if($whattimeisit > ((int)$data['arrivalDateEnd'])) {
                continue;
            }
            if($data['aircraft_id'] == "") {
                continue;
            }
            $id = $data['id'];
            $id = '"' . $id . '"';

            if(!array_key_exists($id, $arrayValuesDeals)) :
                $cu = curl_init();
                curl_setopt($cu, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/aircrafts/id/".$data['aircraft_id']."?auth_key=".AUTH_KEY);
                curl_setopt($cu, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
                $aircraft = curl_exec($cu);
                $aircraft = json_decode($aircraft, true);
                curl_close($cu);

                addRace($id, $data, $aircraft);
            endif;
        } else if ($data['legType'] == 'Shuttle') {
            if($whattimeisit > ((int)$data['departureDate'])) {
                continue;
            }
            $id = $data['id'];
            $id = '"' . $id . '"';

            if(!array_key_exists($id, $arrayValuesShuttles)) :
                addShuttle($id, $data);
            endif;
        }
    }


    // Обновляем количество рейсов в дилсах и шаттлах
    $info = $database->getReference('DealsShort')->getSnapshot()->getValue();
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/count.txt', count($info));

    $infoShuttle = $database->getReference('ShutleShort')->getSnapshot()->getValue();
    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/shuttles/countAllow.txt', count($infoShuttle));
endif;





function addRace($key, $data, $aircraft) {
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
    $database = $firebase->getDatabase();

    $air = array(
        "1365639" => array("Hawker 850 XP VP-BNW", "Midsize Jets", null, null, 26, null, "1.4", ["Hawker_850-XP-VP-BNW_1.jpg", "Hawker_850-XP-VP-BNW_5.jpg", "Hawker_850-XP-VP-BNW_6.jpg", "Hawker_850-XP-VP-BNW_7.jpg", "Hawker_850-XP-VP-BNW_8.jpg", "Hawker_850-XP-VP-BNW_9.jpg"]),
        "1365645" => array("Hawker 750 VQ-BBS", "Midsize Jets", null, null, 99, null, "2.28", ["HAWKER-750-VQ-BBS_5.jpg", "HAWKER-750-VQ-BBS_15.jpg", "HAWKER-750-VQ-BBS_16.jpg", "HAWKER-750-VQ-BBS_17.jpg", "HAWKER-750-VQ-BBS_18.jpg"]),
        "1369342" => array("Hawker 750 VQ-BBR", "Midsize Jets", null, null, 154, null, "2.28", ["HAWKER-750-VQ-BBR_19.jpg", "HAWKER-750-VQ-BBS_15.jpg", "HAWKER-750-VQ-BBS_16.jpg", "HAWKER-750-VQ-BBS_17.jpg", "HAWKER-750-VQ-BBS_18.jpg"]),
        "1369343" => array("Hawker 750 VQ-BAM", "Midsize Jets", null, null, 422, null, "2.28", ["HAWKER-750-VQ-BAM_19.jpg", "HAWKER-750-VQ-BBS_15.jpg", "HAWKER-750-VQ-BBS_16.jpg", "HAWKER-750-VQ-BBS_17.jpg", "HAWKER-750-VQ-BBS_18.jpg"]),
        "1365638" => array("Hawker 1000 VP-BMY", "Midsize Jets", null, null, 28, null, "2.04", ["Hawker-1000-VP-BMY_1.jpg", "Hawker-1000-VP-BMY_5.jpg", "Hawker-1000-VP-BMY_6.jpg", "Hawker-1000-VP-BMY_7.jpg", "Hawker-1000-VP-BMY_8.jpg"]),
        "1399129" => array("Hawker 850 XP LY-BGD", "Midsize Jets", null, null, 442, null, "1.4", ["Hawker-850-XP-LY-BGD_1.jpg", "Hawker-850-XP-LY-BGD_8.jpg", "Hawker-850-XP-LY-BGD_9.jpg", "Hawker-850-XP-LY-BGD_10.jpg", "Hawker-850-XP-LY-BGD_11.jpg"]),
        "1365642" => array("Challenger 850 LY-BGK", "Heavy Jets", null, null, 443, null, "6,52", ["CHALLENGER-850-LY-BGK_1.jpg", "CHALLENGER-850-LY-BGK_5.jpg", "CHALLENGER-850-LY-BGK_6.jpg", "CHALLENGER-850-LY-BGK_7.jpg", "CHALLENGER-850-LY-BGK_8.jpg"]),
        "1399264" => array("Challenger 850 LY-BGK", "Heavy Jets", null, null, 443, null, "6,52", ["CHALLENGER-850-LY-BGK_1.jpg", "CHALLENGER-850-LY-BGK_5.jpg", "CHALLENGER-850-LY-BGK_6.jpg", "CHALLENGER-850-LY-BGK_7.jpg", "CHALLENGER-850-LY-BGK_8.jpg"]),
        "1419675" => array("Challenger 850 9H-BSG", "Heavy Jets", null, null, 564, null, "6,52", ["CHALLENGER-850-9H-BSG_1.jpg", "CHALLENGER-850-9H-BSG_2.jpg", "CHALLENGER-850-9H-BSG_3.jpg", "CHALLENGER-850-9H-BSG_4.jpg", "CHALLENGER-850-9H-BSG_5.jpg", "CHALLENGER-850-9H-BSG_6.jpg"]),
        "1386130" => array("Legacy 600 VP-BGP", "Heavy Jets", null, null, 432, null, "6.8", ["LEGACY-600-VP-BGP_10.jpg", "LEGACY-600-VP-BGP_5.jpg", "LEGACY-600-VP-BGP_6.jpg", "LEGACY-600-VP-BGP_7.jpg", "LEGACY-600-VP-BGP_8.jpg", "LEGACY-600-VP-BGP_9.jpg"]),
        "1394659" => array("Hawker 750 LY-BGH", "Midsize Jets", null, null, 431, null, "2.28", ["HAWKER-750-LY-BGH_1.jpg", "HAWKER-750-LY-BGH_10.jpg", "HAWKER-750-LY-BGH_11.jpg", "HAWKER-750-LY-BGH_12.jpg", "HAWKER-750-LY-BGH_13.jpg", "HAWKER-750-LY-BGH_14.jpg"]),
        "1365635" => array("Legacy 600 VP-BGL", "Heavy Jets", null, null, 29, null, "6.8", ["LEGACY-600-VP-BGL_1.jpg", "LEGACY-600-VP-BGL_5.jpg", "LEGACY-600-VP-BGL_6.jpg", "LEGACY-600-VP-BGL_7.jpg", "LEGACY-600-VP-BGL_8.jpg", "LEGACY-600-VP-BGL_9.jpg"]),
        "1365641" => array("Hawker 750 VQ-BBK", "Midsize Jets", null, null, 78, null, "2.28", ["HAWKER-750-VQ-BBK_1.jpg", "HAWKER-750-VQ-BBK_13.jpg", "HAWKER-750-VQ-BBK_14.jpg", "HAWKER-750-VQ-BBK_15.jpg", "HAWKER-750-VQ-BBK_16.jpg"]),
        "1365636" => array("Legacy 600 VP-BGT", "Heavy Jets", null, null, 31, null, "6.8", ["Legacy-600-VP-BGT_1.jpg", "Legacy-600-VP-BGT_14.jpg", "Legacy-600-VP-BGT_15.jpg", "Legacy-600-VP-BGT_16.jpg", "Legacy-600-VP-BGT_17.jpg", "Legacy-600-VP-BGT_18.jpg"]),
        "1365637" => array("Legacy 600 VP-BGV", "Heavy Jets", null, null, 30, null, "6.8", ["Legacy-600-VP-BGV_1.jpg", "Legacy-600-VP-BGV_15.jpg", "Legacy-600-VP-BGV_16.jpg", "Legacy-600-VP-BGV_17.jpg", "Legacy-600-VP-BGV_18.jpg", "Legacy-600-VP-BGV_19.jpg"]),
        "1365633" => array("Legacy 600 9H-IDB", "Heavy Jets", null, null, 77, null, "6,8", ["Legacy-600-9H-IDB_1.jpg", "Legacy-600-9H-IDB_5.jpg", "Legacy-600-9H-IDB_6.jpg", "Legacy-600-9H-IDB_7.jpg", "Legacy-600-9H-IDB_8.jpg", "Legacy-600-9H-IDB_9.jpg"]),
        "2836205" => array("Legacy 600 9H-AGZ", "Heavy Jets", null, null, 77, null, "6,8", ["Legacy-600-9H-AGZ_10.jpg", "Legacy-600-9H-IDB_5.jpg", "Legacy-600-9H-IDB_6.jpg", "Legacy-600-9H-IDB_7.jpg", "Legacy-600-9H-IDB_8.jpg", "Legacy-600-9H-IDB_9.jpg"]),
        "1396179" => array("Yak 42 RA-42445", "Bizliner", null, null, 25, null, "11,8", ["Yak-42-RA-42445_7.jpg", "Yak-42-RA-42445_2.jpg", "Yak-42-RA-42445_3.jpg", "Yak-42-RA-42445_4.jpg", "Yak-42-RA-42445_5.jpg", "Yak-42-RA-42445_6.jpg"]),


        // "1365643" => array("Hawker 750 VQ-BBQ", "Midsize Jets", 8, 8, 24, 3600, "2.28", ["HAWKER-750-VQ-BBQ_1.jpg", "HAWKER-750-VQ-BBQ_2.jpg", "HAWKER-750-VQ-BBQ_3.jpg", "HAWKER-750-VQ-BBQ_4.jpg"]),
        // "1365642" => array("Challenger 850 VQ-BOV", "Heavy Jets", null, null, 33, null, "6,52", ["CHALLENGER-850-VQ-BOV_1.jpg", "CHALLENGER-850-VQ-BOV_2.jpg", "CHALLENGER-850-VQ-BOV_3.jpg", "CHALLENGER-850-VQ-BOV_4.jpg", "CHALLENGER-850-VQ-BOV_5.jpg", "CHALLENGER-850-VQ-BOV_6.jpg", "CHALLENGER-850-VQ-BOV_7.jpg", "CHALLENGER-850-VQ-BOV_8.jpg"]),
        "1365640" => array("Hawker 850 XP VQ-BVA", "Midsize Jets", null, null, 25, null, "1.4", ["Hawker-850-XP-VQ-BVA_1.jpg", "Hawker-850-XP-VQ-BVA_2.jpg", "Hawker-850-XP-VQ-BVA_3.jpg", "Hawker-850-XP-VQ-BVA_4.jpg"]),
    );

    

    if(!$data['price']) {
        $cost = 0;
        $costType = "request";
        $currency = "$";
    } else {
        $cost = $data['price'];
        $costType = "request";
        $currency = "$";
    }
    if($data['priceEmptyLegEUR']) {
        $cost = $data['priceEmptyLegEUR'];
        $costType = "plane";
        $currency = "€";
    }
    $startDate = 0;
    $endDate = 0;
    if($data['arrivalDateStart']) {
        $startDate = $data['arrivalDateStart'];
    } else {
        $startDate = $data['departureDate'];
    }
    
    if($data['arrivalDateEnd']) {
        $endDate = $data['arrivalDateEnd'];
    } else {
        $endDate = $data['arrivalDate'];
    }

    $postData = [
        'BagVol'            => (double) $air[$data['aircraft_id']][6],
        'Cost'              => (int) $cost,
        'CostType'          => $costType,
        'Currency'          => $currency,
        'EndDate'           => (int) $endDate,
        'FlyghtDistance'    => (int) $aircraft['maxDistance'],
        'FromCity'          => $data['departureCity'],
        'FromCityCode'      => $data['departureAirport'],
        'IdSite'            => (int) $air[$data['aircraft_id']][4],
        'Images'            => $air[$data['aircraft_id']][7],
        'MaxPlaneSeats'     => (int) $aircraft['numberOfSeats'],
        'Plane'             => $air[$data['aircraft_id']][0],
        'PlaneClass'        => $air[$data['aircraft_id']][1],
        'PlaneSeats'        => (int) $aircraft['numberOfSeats'],
        'StartDate'         => (int) $startDate,
        'ToCity'            => $data['arrivalCity'],
        'ToCityCode'        => $data['arrivalAirport'],
    ];

    if(!$postData['FromCityCode'] || !$postData['ToCityCode'] || !$postData['IdSite']) {
        // ошибка в данных, отправляем на почту оповещение
        $file = $_SERVER['DOCUMENT_ROOT'] . '/crm/ordererror.txt';
        $errorPosts = json_decode(file_get_contents($file));

        // если оповещение мы уже отправляли, то опять не нужно оповещать об этом
        if(!$errorPosts->$key) {
            $headers =  "MIME-Version: 1.0" . "\r\n" . "Content-type: text/html; charset=UTF-8" . "\r\n";
            $mess = 'Произошла ошибка при добавлении нового рейса из CRM в Firebase. Проверьте данные рейса №'.$key.'<br><br><br>';
            $mess .= 'Данные из CRM:<br>';
            foreach($data as $key2=>$value2) {
                $mess .= $key2 . " - " . $value2 . "<br>";
            }
            $mess .= '<br><br>Данные самолета:<br>';
            foreach($aircraft as $key1=>$value1) {
                $mess .= $key1 . " - " . $value1 . "<br>";
            }
            mail(
                'v.kolosov@rusline.aero',
                'Error in SiriusApp!',
                $mess,
                $headers
            );
            $errorPosts->$key = true;

            $errorPosts = json_encode($errorPosts);
            file_put_contents($file, $errorPosts);
        }
    } else {
        // если все нормально с данными, то добавляем новый дилс
        $database
            ->getReference('DealsFull/'.$key)
            ->set($postData);
    }
}





function addShuttle($key, $data) {
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__ . '/google-services.json');
    $firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
    $database = $firebase->getDatabase();

    $costType = "seat";

    if(!$data['price']) {
        $cost = 0;
        $currency = "$";
    } else {
        $cost = $data['price'];
        $currency = "$";
    }
    if($data['priceEmptyLegEUR']) {
        $cost = $data['priceEmptyLegEUR'];
        $currency = "€";
    } else {
        return false;
    }
    $startDate = $data['departureDateLocale'];
    $endDate = 0;
    if($data['arrivalDate']) {
        $endDate = ($data['arrivalDate'] - $data['departureDate']) / 60;
    }
    
    $images = ["Shuttle_1_1.jpg", "Shuttle_1_2.jpg", "Shuttle_2_1.jpg", "Shuttle_2_2.jpg", "Shuttle_3_1.jpg", "Shuttle_3_2.jpg"];
    $seatsAll = "14";
    $seatsFree = 14;
    if($data['numberOfSeats']) {
        $seatsFree = $data['numberOfSeats'];
    }

    $timezone = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/timezones.txt'));
    $timezones = array();
    foreach($timezone as $keyC=>$value) {
        $timezones[$keyC] = $value;
    }


    if($timezones[mb_strtoupper(getCodes($data['departureAirport'], 'city'))]) {
        $startDate += $timezones[mb_strtoupper(getCodes($data['departureAirport'], 'city'))];
    }

    $postData = [
        'Cost'              => (int) $cost,
        'CostType'          => $costType,
        'Currency'          => $currency,
        'Date'              => (int) $startDate,
        'DetailsPath'       => "Base",
        'FromCity'          => $data['departureCity'],
        'FromCityCode'      => $data['departureAirport'],
        'Images'            => $images,
        'InFlightTime'      => (int) $endDate,
        'PlaneClass'        => "Business Jet",
        'SeatsAll'          => $seatsAll,
        'SeatsFree'         => (int) $seatsFree,
        'ToCity'            => $data['arrivalCity'],
        'ToCityCode'        => $data['arrivalAirport'],
    ];

    if(!$postData['FromCityCode'] || !$postData['ToCityCode']) {
    //     // ошибка в данных, отправляем на почту оповещение
    //     $file = $_SERVER['DOCUMENT_ROOT'] . '/crm/ordererror.txt';
    //     $errorPosts = json_decode(file_get_contents($file));

    //     // если оповещение мы уже отправляли, то опять не нужно оповещать об этом
    //     if(!$errorPosts->$key) {
    //         $headers =  "MIME-Version: 1.0" . "\r\n" . "Content-type: text/html; charset=UTF-8" . "\r\n";
    //         $mess = 'Произошла ошибка при добавлении нового рейса из CRM в Firebase. Проверьте данные рейса №'.$key.'<br><br><br>';
    //         $mess .= 'Данные из CRM:<br>';
    //         foreach($data as $key2=>$value2) {
    //             $mess .= $key2 . " - " . $value2 . "<br>";
    //         }
    //         $mess .= '<br><br>Данные самолета:<br>';
    //         foreach($aircraft as $key1=>$value1) {
    //             $mess .= $key1 . " - " . $value1 . "<br>";
    //         }
    //         mail(
    //             'v.kolosov@rusline.aero',
    //             'Error in SiriusApp!',
    //             $mess,
    //             $headers
    //         );
    //         $errorPosts->$key = true;

    //         $errorPosts = json_encode($errorPosts);
    //         file_put_contents($file, $errorPosts);
    //     }
    } else {
        // если все нормально с данными, то добавляем новый дилс
        $database
            ->getReference('ShutleFull/'.$key)
            ->set($postData);
    }
}






















function getCodes($code, $flag) {
    $codes = array();
    $datafile = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/crm/cities.txt'));
    foreach($datafile as $key=>$value) {
        $codes[$key] = $value;
    }
    $codes["URFF"] = array("Simferopol","URFF");
    $codes["UUDL"] = array("Ярославль","UUDL");
    $codes["UCFM"] = array("Bishkek","UCFM");
    $codes["OMAD"] = array("Abu Dhabi","OMAD");
    $codes["OMFJ"] = array("Al Fujayrah","OMFJ");

    $answer = '';
    if($flag == 'city') :
        $answer = $codes[$code][0];
    else :
        $answer = $code;
    endif;
    
    return $answer;
}
function findArray ($ar, $findValue, $executeKeys){
    $result = array();

    foreach ($ar as $k => $v) {
      if (is_array($ar[$k])) {
        $second_result = findArray ($ar[$k], $findValue, $executeKeys);
        $result = array_merge($result, $second_result);
        continue;
      }
      if ($v === $findValue) {
        foreach ($executeKeys as $val){
          $result[] = $ar[$val];
        }
        
      }
    }
    return $result;
}

die();