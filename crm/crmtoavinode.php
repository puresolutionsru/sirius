<?
require_once ('crest.php');
require __DIR__.'/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

// получаем из crm список emptylegs
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/emptylegs?auth_key=".AUTH_KEY."&date_to=1&date_from=9561765445");
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$emptylegs = curl_exec($ch);
$emptylegs = json_decode($emptylegs, true);
curl_close($ch);

$existAir = array(
    "VP-BGL" => "Legacy 600",
    "VP-BGP" => "Legacy 600",
    "9H-IDB" => "Legacy 600",
    "9H-AGZ" => "Legacy 600",
    "VP-BGT" => "Legacy 600",
    "VP-BGV" => "Legacy 600",
    "LY-BGH" => "Hawker 750",
    "VQ-BBR" => "Hawker 750",
    "VQ-BBS" => "Hawker 750",
    "VQ-BAM" => "Hawker 750",
    "VQ-BBK" => "Hawker 750",
    "VQ-BOV" => "Challenger 850",
    "LY-BGD" => "Hawker 850 XP",
    "VQ-BVA" => "Hawker 850 XP",
    "VP-BNW" => "Hawker 850 XP",
    "VP-BMY" => "Hawker 1000",
);

$twoavi = array("9H-IDB", "9H-AGZ", "LY-BGH", "9H-BSG");

if($emptylegs) :

    foreach($emptylegs as $key=>$data) {
        $whattimeisit = time();
        if($data['legType'] == "Shuttle") {
            continue;
        }
        if(!$data['arrivalDateEnd']) {
            $data['arrivalDateEnd'] = $data['arrivalDate'];
        }
        if($whattimeisit > ((int)$data['arrivalDateEnd'])) {
            continue;
        }

        $file = $_SERVER['DOCUMENT_ROOT'].'/crm/avinode.txt';
        $dt = file_get_contents($file, true);
        $ar = json_decode($dt, true);
        if($ar[$data['id']]) {
            continue;
        }

        // даннае самолета
        $cu = curl_init();
        curl_setopt($cu, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/aircrafts/id/".$data['aircraft_id']."?auth_key=".AUTH_KEY);
        curl_setopt($cu, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
        $aircraft = curl_exec($cu);
        $aircraft = json_decode($aircraft, true);
        curl_close($cu);

        date_default_timezone_set('UTC');
        // получаем данные самолета в авиноде по tailNumber
        // $chA = curl_init();
        // $now = str_replace("+00:00", ".001Z", date(DATE_ATOM, time()));
        // curl_setopt($chA, CURLOPT_URL, "https://services.avinode.com/api/aircraft/search?tail=".$aircraft['tailNumber']);
        // curl_setopt($chA, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($chA, CURLOPT_CUSTOMREQUEST, "GET");
        // curl_setopt($chA, CURLOPT_HTTPHEADER, array(
        //     'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
        //     'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiIzQzhBRDEyOS0xNUIyLTRGN0YtQTdGNy01MzA0NUQ3MERCOTEiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjY1NjIsImlzcyI6ImF2aW5vZGUiLCJhdml0eXBlIjoxNSwiYXZpbm9uY2UiOiJmMTRmOTVhMC1lM2RhLTQwMzUtYjZiMS1hNDk1MzFjMjEzYTcifQ.SAv7z6DDay92uwBd6ec4K_12-ZwVIhcfCvUURBXNy6Rg-Gd3eXxqUcID4Pzw7zPofSq0qxBD5Iu3f6FeC1X_XQZXDt9iZooVrS0xzP4wku7Ji7RaDtAQ-nwn2gp6PLEt7uxLQePkLH-vWchaShzvNynLvuo35TmyV9YN-EFvhde2dPd3yjfLlRJ2cRT0zzIgkRYyYRdq4d4xOCl1kanDLDZLG-eztdQ-Rn-AMSVeH5IqRpP-jrTvAZG4-pfgfw5jXxh4AsuN2PBQlC8sUQQgo-QwMAYlRkx-JxVPjlP1MCIT1M91XEHUqEGXEsKUtB2JBSGTG_Bse-ggf6Jt2dxSvw',
        //     'Content-Type: application/json',
        //     'X-Avinode-SentTimestamp: '.$now
        // ));
        // $send = curl_exec($chA);
        // curl_close($chA);

        // собираем данные для авинода
        $request = array();
        // $request["lift"]["aircraftTail"] = $existAir[$aircraft['tailNumber']];
        $request["lift"]["aircraftTail"] = $aircraft['tailNumber'];
        // $request["lift"]["emptyLegId"] = $data["id"];
        // $request["activities"][0]["externalTripId"] = "";
        // $request["activities"][0]["tripId"] = "";
        $request["type"] = "EmptyLeg";
        $request["published"] = true;
        $request["sellerVerified"] = true;
        $request["startAirport"]['icao'] = $data["departureAirport"];
        $request["endAirport"]['icao'] = $data["arrivalAirport"];
        if($data['arrivalDateStart']) {
            $request["startDate"] = $data['arrivalDateStart'];
        } else {
            $request["startDate"] = $data['departureDate'];
        }
        if($data['arrivalDateEnd']) {
            $request["endDate"] = $data['arrivalDateEnd'];
        } else {
            $request["endDate"] = $data['arrivalDate'];
        }
        $request["startDate"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $request["startDate"]));
        $request["endDate"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $request["endDate"]));
        if($data['price']) {
            // $request["sellerPrice"] = $data['price'];
            // $request["sellerPrice"] = "";
            // $request["sellerPriceCurrency"] = "USD";
        }
        if($data['priceEmptyLegEUR']) {
            // $request["sellerPrice"] = $data["priceEmptyLegEUR"];
            // $request["sellerPrice"] = "";
            // $request["sellerPriceCurrency"] = "EUR";
        }

        $request = json_encode($request);
        $now = str_replace("+00:00", ".001Z", date(DATE_ATOM, time()));

        if(in_array($aircraft['tailNumber'], $twoavi)) {
            $chPtwo = curl_init();
            curl_setopt($chPtwo, CURLOPT_URL, "https://services.avinode.com/api/emptylegs");
            curl_setopt($chPtwo, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chPtwo, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($chPtwo, CURLOPT_POSTFIELDS, $request);
            curl_setopt($chPtwo, CURLOPT_HTTPHEADER, array(
                'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
                'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI2OUM4QkY1MS1FMUNDLTQwNkMtOUM2Qi0zMjBBMjMxOEM4QUUiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjEyMjI5LCJpc3MiOiJhdmlub2RlIiwiYXZpdHlwZSI6MTUsImF2aW5vbmNlIjoiNzYwNDhhNTAtYjUxYy00MmFiLTk2YmMtMDEzZjM3MWRiMTc4In0.g-osAW9lPLzMw05oUT7HtnQU9beVw8wH_Xkg-7vrqQEQg6VyTubrhUXdDIx493WXdqEAkdZu2P4LcwO_XbFLUDi6TYEbKnNo30HJSztgyJaj9hV3aMFoeij43Ch0NrLpiC1Agwb9IBuLeH55ZG2ruTrcE1rSUuZNIhNpHDtCkrTiRE8CMJD11QkT8Bh2_Vyj7CoQdQrT8LXTfqEYcoFrKDipKMeHe9JvR21FSSoX6EzHOGDSBBoXlQAPKBfWsxHQumgLfRflOnNpOIdo9v6C3QXl3jgP16iq1oLfP78JpLpg1LCr9kl6ew9Qb-haJjsCHqPGk0wnYbhFk-sKIq9PVw',
                'Content-Type: application/json',
                'X-Avinode-SentTimestamp: '.$now
            ));
            $send = curl_exec($chPtwo);
            curl_close($chPtwo);
        } else {
            $chP = curl_init();
            curl_setopt($chP, CURLOPT_URL, "https://services.avinode.com/api/emptylegs");
            curl_setopt($chP, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chP, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($chP, CURLOPT_POSTFIELDS, $request);
            curl_setopt($chP, CURLOPT_HTTPHEADER, array(
                'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
                'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiIzQzhBRDEyOS0xNUIyLTRGN0YtQTdGNy01MzA0NUQ3MERCOTEiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjY1NjIsImlzcyI6ImF2aW5vZGUiLCJhdml0eXBlIjoxNSwiYXZpbm9uY2UiOiJmMTRmOTVhMC1lM2RhLTQwMzUtYjZiMS1hNDk1MzFjMjEzYTcifQ.SAv7z6DDay92uwBd6ec4K_12-ZwVIhcfCvUURBXNy6Rg-Gd3eXxqUcID4Pzw7zPofSq0qxBD5Iu3f6FeC1X_XQZXDt9iZooVrS0xzP4wku7Ji7RaDtAQ-nwn2gp6PLEt7uxLQePkLH-vWchaShzvNynLvuo35TmyV9YN-EFvhde2dPd3yjfLlRJ2cRT0zzIgkRYyYRdq4d4xOCl1kanDLDZLG-eztdQ-Rn-AMSVeH5IqRpP-jrTvAZG4-pfgfw5jXxh4AsuN2PBQlC8sUQQgo-QwMAYlRkx-JxVPjlP1MCIT1M91XEHUqEGXEsKUtB2JBSGTG_Bse-ggf6Jt2dxSvw',
                'Content-Type: application/json',
                'X-Avinode-SentTimestamp: '.$now
            ));
            $send = curl_exec($chP);
            curl_close($chP);
        }




        // schedule
        // $request = array();
        // $request["lift"]["aircraftTail"] = $aircraft['tailNumber'];
        // $request["activities"][0]["activityId"] = $data['id'];
        // $request["activities"][0]["activityType"] = "EmptyLeg";
        // if($data['arrivalDateStart']) {
        //     $request["activities"][0]["startDateTime"] = $data['arrivalDateStart'];
        // } else {
        //     $request["activities"][0]["startDateTime"] = $data['departureDate'];
        // }
        // if($data['arrivalDateEnd']) {
        //     $request["activities"][0]["endDateTime"] = $data['arrivalDateEnd'];
        // } else {
        //     $request["activities"][0]["endDateTime"] = $data['arrivalDate'];
        // }
        // $request["activities"][0]["startDateTime"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $request["activities"][0]["startDateTime"]));
        // $request["activities"][0]["endDateTime"] = str_replace("+00:00", ".001Z", date(DATE_ATOM, $request["activities"][0]["endDateTime"]));
        // $request["activities"][0]["startAirport"]['icao'] = $data["departureAirport"];
        // $request["activities"][0]["endAirport"]['icao'] = $data["arrivalAirport"];
        // $request["activities"][0]["paxCount"] = $aircraft['numberOfSeats'];
        // $request["activities"][0]["active"] = true;
        // $request["activities"][0]["marketingVerified"] = true;
        // if($data['price']) {
        //     $request["activities"][0]["price"] = $data['price'];
        //     $request["activities"][0]["currency"] = "USD";
        // }
        // if($data['priceEmptyLegEUR']) {
        //     $request["activities"][0]["price"] = $data["priceEmptyLegEUR"];
        //     $request["activities"][0]["currency"] = "EUR";
        // }
        // $request["activities"] = $request["activities"][0];
        // print_r($request);
        // $request = json_encode($request);
        
        // $chP1 = curl_init();
        // curl_setopt($chP1, CURLOPT_URL, "https://services.avinode.com/api/schedules");
        // curl_setopt($chP1, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($chP1, CURLOPT_CUSTOMREQUEST, "PUT");
        // curl_setopt($chP1, CURLOPT_POSTFIELDS, $request);
        // curl_setopt($chP1, CURLOPT_HTTPHEADER, array(
        //     'X-Avinode-ApiToken: f45d0c43-0e4d-422d-9a20-fbef8d2a26a6',
        //     'Authorization: Bearer eyJraWQiOiIxNkVBQkQ5RS1BRjYyLTQ4NTEtODk5Qi1BM0UwMThGRjYxNDciLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiJ9.eyJzdWIiOiIzQzhBRDEyOS0xNUIyLTRGN0YtQTdGNy01MzA0NUQ3MERCOTEiLCJhdmlkb21haW4iOiIuYXZpbm9kZS5jb20iLCJhdml0ZW5hbnQiOjY1NjIsImlzcyI6ImF2aW5vZGUiLCJhdml0eXBlIjoxNSwiYXZpbm9uY2UiOiJmMTRmOTVhMC1lM2RhLTQwMzUtYjZiMS1hNDk1MzFjMjEzYTcifQ.SAv7z6DDay92uwBd6ec4K_12-ZwVIhcfCvUURBXNy6Rg-Gd3eXxqUcID4Pzw7zPofSq0qxBD5Iu3f6FeC1X_XQZXDt9iZooVrS0xzP4wku7Ji7RaDtAQ-nwn2gp6PLEt7uxLQePkLH-vWchaShzvNynLvuo35TmyV9YN-EFvhde2dPd3yjfLlRJ2cRT0zzIgkRYyYRdq4d4xOCl1kanDLDZLG-eztdQ-Rn-AMSVeH5IqRpP-jrTvAZG4-pfgfw5jXxh4AsuN2PBQlC8sUQQgo-QwMAYlRkx-JxVPjlP1MCIT1M91XEHUqEGXEsKUtB2JBSGTG_Bse-ggf6Jt2dxSvw',
        //     'Content-Type: application/json',
        //     'X-Avinode-SentTimestamp: '.$now
        // ));
        // $send = curl_exec($chP1);
        // print_r($send);
        // curl_close($chP1);
        // break;





        $ar[$data["id"]] = true;
        $write = json_encode($ar);
        file_put_contents($file, $write);
    }

endif;