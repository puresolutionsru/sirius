<?
ini_set("max_execution_time", "6000");


$json = file_get_contents("airports.json");
$jsonAr = json_decode($json);
$cities = file_get_contents("cities.txt");
$citiesAr = json_decode($cities);


$files = array();

foreach($jsonAr as $key => $value) {
    $icao = '';
    foreach($citiesAr as $k=>$v) {
        if($v[1] == $key) {
            $icao = $k;
            break;
        }
    }
    $files[$key][0] = $icao;
    if(!$icao) {
        $files[$key][0] = $key;
    }
    $files[$key][1] = $value->cityName->ru;
    $files[$key][2] = $value->cityName->en;
    $files[$key][3] = $value->area;
    $files[$key][4] = $value->country;
    $files[$key][5] = $value->timezone;
}



$finish;

foreach($files as $a=>$c) {
    // $finish += (object) array($c[0] => (object) 'foo');
    // $finish[] = array(
        $finish[$c[0]] = array(
            'cityName' => array(
                'ru' => $c[1],
                'en' => $c[2],
            ),
            'area' => $c[3],
            'country' => $c[4],
            'timezone' => $c[5],
        );
    // );
    $finish[$c[0]]['cityName'] = (object)$finish[$c[0]]['cityName'];
    $finish[$c[0]]['timezone'] = str_replace("\\", "", $finish[$c[0]]['timezone']);
    $finish[$c[0]] = (object)$finish[$c[0]];
}

$object = (object)$finish;

$object = json_encode($object, JSON_UNESCAPED_UNICODE);

// $final;

// foreach($object as $r=>$t) {

// }

// echo '<pre>';
// print_r($object);
// echo '</pre>';

$file = 'new.json';
file_put_contents($file, $object);


$fp = fopen('new.csv', 'w');
foreach($finish as $j=>$k) {
    $tx[0] = $j;
    $tx[1] = $k->cityName->ru;
    $tx[2] = $k->cityName->en;
    $tx[3] = $k->area;
    $tx[4] = $k->country;
    $tx[5] = $k->timezone;

    fputcsv($fp, $tx, ';');
}