<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Аренда самолетов бизнес авиации Sirius Aero. Закажите частный или корпоративный чартерный рейс VIP класса в авиакомпании Сириус Аэро.");
$APPLICATION->SetPageProperty("keywords", "заказ самолета, заказ рейса, бизнес авиация, бизнес-авиация, деловая авиация, заказ чартера, ВИП чартер, заказной рейс, бизнес рейс, ВИП перевозки, ВИП рейс, срочный рейс, бизнес самолет");
$APPLICATION->SetPageProperty("title", "Бизнес авиация в Москве - заказать частный самолет Sirius Aero");
$APPLICATION->SetTitle("Главная");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.dotdotdot.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/dot.js');
echo '<div class="main-info home-main-ttl cl">
        <img class="main-info__bg" src="'.SITE_TEMPLATE_PATH.'/img/img.jpg" alt="" title="">
        <div class="main-info__col-1 col-xs-6">
            <h1 class="ttl">';
                $APPLICATION->IncludeFile(
                    SITE_DIR.'/include/indexttl.php',
                    array(),
                    array(
                        "MODE"=>"text",
                    )
                );
echo '      </h1>
        </div>
        <div class="main-info__col-2 col-xs-6">
    ';
?><br>
<p>
	 Sirius Aero – международная компания деловой авиации и самый крупный коммерческий оператор в Восточной Европе.
</p>
<p>
	 Основные направления деятельности - организация премиальных чартерных перелётов и менеджмент воздушных судов. Подробнее в разделе <a href="/services/">услуги</a> и <a href="/menedzhment-vs/">менеджмент воздушных судов</a>. Там же информация о том, чем ещё мы можем быть вам полезны.
</p>
<p>
</p>
<p>
	 Мы выполняем внутренние и международные чартерные авиаперевозки практически во все страны мира. Базовые аэропорты располагаются в&nbsp;России и Европе.
</p>
<p>
	 Парк воздушных судов Sirius Aero – это современные иностранные бизнес джеты и российские бизнес лайнеры с премиальной конфигурацией салона. Всего в нашем флоте 6 типов самолётов:
</p>
<p>
 <a href="/fleet/#flot-list1">Midsize jets</a>: Hawker 750,&nbsp;Hawker 850&nbsp;XP и Hawker 1000
</p>
<p>
 <a href="/fleet/#flot-list2">Heavy jets</a>: Legacy 600, <!--Challenger 601, -->Challenger 850
</p>
<p>
 <a href="/fleet/#flot-list3">Biznes liners</a>: ЯК-42
</p>
<p>
	 Все самолёты отвечают международным стандартам безопасности полётов и высочайшим требованиям комфорта.
</p>
<div class="details">
 <a href="/about/company/">Подробнее о компании</a>
</div>
 <br><?
echo '  </div>
    </div>';

require($_SERVER['DOCUMENT_ROOT'].'/index-'.LANGUAGE_ID.'.php');

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>