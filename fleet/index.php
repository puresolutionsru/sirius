<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
//$APPLICATION->SetPageProperty("description", "Флот Sirius Aero | Hawker-750 | VQ-BBQ");
$APPLICATION->SetPageProperty("TITLE", "Бизнес джеты - каталог частных самолетов бизнес авиации Сириус Аэро");
$APPLICATION->SetTitle("Бизнес джеты");
if($_SERVER["REQUEST_URI"] == "/fleet/"):
	echo '
		<div class="the-content main-w">
			<h1>' . $APPLICATION->GetTitle(false) . '</h1>
		</div>
	';
endif;
?>
<?php
if($_SERVER['REQUEST_URI']=='/fleet/bizliner/') {
	$APPLICATION->SetPageProperty("description", "Каталог самолетов класса Bizliner доступных для аренды. Виртуальный тур по салонам. Закажите ваш VIP перелет в авиакомпании Sirius Aero.");
	$APPLICATION->SetPageProperty("TITLE", "Самолеты класса Bizliner в Москве - аренда, цены, характеристики, фото");
}
if($_SERVER['REQUEST_URI']=='/fleet/yak-42d/') {
	$APPLICATION->SetPageProperty("description", "Каталог самолетов Як-42Д доступных для аренды. Виртуальный тур по салонам. Закажите ваш VIP перелет в авиакомпании Sirius Aero.");
	$APPLICATION->SetPageProperty("TITLE", "Самолеты Як-42Д: аренда, цены, характеристики, фото");
}
if($_SERVER['REQUEST_URI']=='/fleet/yak-42d/?ra-42445') {
	$APPLICATION->SetPageProperty("description", 'Арендовать самолет Як-42Д RA-42445 в авиакомпании Sirius Aero. Собственные самолеты. Организация VIP перелетов "под ключ". Полеты в Европу и по всей России.');
	$APPLICATION->SetPageProperty("TITLE", "Арендовать самолет Як-42Д RA-42445 - Sirius Aero");
}
?>
<?$APPLICATION->IncludeComponent(
	"bitrix:news",
	"fleet",
	Array(
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "N",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "fleet",
		"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_FIELD_CODE" => array(0=>"",1=>"",),
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PROPERTY_CODE" => array(0=>"",1=>"",),
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "fleet",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
		"LIST_FIELD_CODE" => array(0=>"ELEMENT_META_KEYWORDS",1=>"",),
		"LIST_PROPERTY_CODE" => array(0=>"ELEMENT_META_KEYWORDS",1=>"",),
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"SET_META_DESCRIPTION" => "Y",
		"SET_BROWSER_TITLE" => "Y",
		"META_KEYWORDS" => "-",
		"NEWS_COUNT" => "-1",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Флот",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SEF_FOLDER" => "/fleet/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array("news"=>"","section"=>"#SECTION_CODE#/","detail"=>"section/#SECTION_CODE#/?#ELEMENT_CODE#",),
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_RATING" => "N",
		"USE_RSS" => "N",
		"USE_SEARCH" => "N",
		"USE_SHARE" => "N"
	)
);?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
<!-- <script src="js/preloader-control.js"></script>
<script src="js/preloader.js"></script> -->