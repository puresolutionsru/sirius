<?
use CMain;

class MainConstruct {

    public $bodyClass;
    public $bIsMainPage;
    public $isPhone = false;
    public $isIphone = false;
    public $isAndroid = false;
    public $isBot = array(
        'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
        'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
        'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
        'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
        'dataparksearch','google-sitemaps','appEngine-google','feedfetcher-google',
        'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
        'googlealert.com','seo-rus.com','yaDirectBot','yandeG','yandex',
        'yandexSomething','Copyscape.com','AdsBot-Google','domaintools.com',
        'Nigma.ru','bing.com','dotnetdotcom'
    );

    function __construct() {
        $app = new CMain;
        $this->bIsMainPage = $app->GetCurPage(false) == SITE_DIR;
        if($app->GetCurPage(false) == '/en/') {
            $this->bIsMainPage = true;
        }
        $this->bodyClass();
    }

    // public function isBot() {
    //     $bots = 
    // }

    public function bodyClass() {
        $bodyClass = '';
        $app = new CMain;
        if($this->bIsMainPage)
        	$bodyClass .= 'home';
        if($app->GetCurPage(false) == '/about/company/')
            $bodyClass .= ' p-company';
        if(stripos($app->GetCurPage(false), '/en/') !== false)
            $bodyClass .= ' ruen';
        if(stripos($_SERVER['SERVER_NAME'], 'sirius-aero.com') !== false)
            $bodyClass .= ' ruen comversion';
        if(strstr($app->GetCurPage(false), '/about/'))
            $bodyClass .= ' p-active-menu';

        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $mobile = strpos($_SERVER['HTTP_USER_AGENT'],"Mobile");
        $symb = strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");
        $operam = strpos($_SERVER['HTTP_USER_AGENT'],"Opera M");
        $htc = strpos($_SERVER['HTTP_USER_AGENT'],"HTC_");
        $fennec = strpos($_SERVER['HTTP_USER_AGENT'],"Fennec/");
        $winphone = strpos($_SERVER['HTTP_USER_AGENT'],"WindowsPhone");
        $wp7 = strpos($_SERVER['HTTP_USER_AGENT'],"WP7");
        $wp8 = strpos($_SERVER['HTTP_USER_AGENT'],"WP8");
        if($iphone) {
            $this->isIphone = true;
        }
        if($android) {
            $this->isAndroid = true;
        }
        if ($ipad || $iphone || $android || $palmpre || $ipod || $berry || $mobile || $symb || $operam || $htc || $fennec || $winphone || $wp7 || $wp8 === true) {
            $bodyClass .= ' mobile';
            $this->isPhone = true;
        }
        $this->bodyClass = $bodyClass;
    }
}

?>
