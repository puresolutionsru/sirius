<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<!--[if IE 8]><html lang="<?= LANGUAGE_ID ?>" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="<?= LANGUAGE_ID ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="<?= LANGUAGE_ID ?>"><!--<![endif]-->
<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139312863-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-139312863-1');
		</script>

  	<title><? $APPLICATION->ShowTitle(); ?></title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<? $APPLICATION->ShowHead(); ?>
  	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,700&amp;subset=cyrillic" rel="stylesheet">
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/grid.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slider.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/fancy/jquery.fancybox-1.3.4.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.all.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.base.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.core.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.accordion.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.datepicker.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.dialog.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.progressbar.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.resizable.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.slider.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.tabs.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.theme.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.datepicker.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/jquery.jgrowl.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/animate.min.css'); ?>
		<!-- <link rel="stylesheet" href="animate.min.css"> -->
  	<!--[if lt IE 9]>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/html5shiv.min.js'); ?>"></script>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/selectivizr-min.js'); ?>"></script>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/respond.min.js'); ?>"></script>
  	<![endif]-->

</head>
<body class="<?= $MainConstruct->bodyClass; ?>">
	<?if($_COOKIE['siriuscookie'] != 'off') : ?>
		<?
		$botname = '';
		foreach($MainConstruct->isBot as $bot) :
			if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false){
				$botname = $bot;
				break;
			}
		endforeach;
		if(!$botname) : ?>
			<div class="b-cook">
				<div class="b-cook_under"></div>
				<div class="b-cook_window">
					<div class="b-cook_window_tx">
						<?= GetMessage('PRIVACY') ?>
					</div>
					<button type="button" class="b-cook_but" role="button"><?= GetMessage('PRIVACY_BUT') ?></button>
				</div>
			</div>
		<? endif; ?>
	<? endif; ?>
	<? $APPLICATION->ShowPanel(); ?>
  	<!-- begin header-->
  	<header class="header cl">
    	<div class="col-lg-4 col-md-4 col-sm-6 hidden-xs col-sm-push-3 col-md-push-0 col-lg-push-0 header__col1">
      		<div class="b-info">
				<span class="b-info__phone">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/headerphone.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</span>
        		<div class="b-info__img pull-left">
					<img src="<?= SITE_TEMPLATE_PATH ?>/img/24_7.png" alt="">
				</div>
				<a class="b-info__lk pull-left fancymodal" href="#form-order"><?= GetMessage('ZAKAZ') ?></a>
      		</div>
    	</div>
    	<div class="col-lg-4 col-md-4 col-sm-3 col-xs-6 col-sm-pull-6 col-md-pull-0 col-lg-pull-0 header__col2">
      		<div class="logo">
				<a href="<?= SITE_DIR ?>">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/headerlogo.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</a>
			</div>
    	</div>
    	<div class="col-lg-4 col-md-4 col-sm-3 col-xs-6 header__col3">
      		<div class="col-xs-12 visible-md visible-lg">
				<?$APPLICATION->IncludeComponent(
					"bitrix:main.site.selector",
					"sitelang",
					Array(
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"SITE_LIST" => array(0=>"*all*",)
					)
				);?>
      		</div>
      		<div class="col-xs-12 visible-xs visible-sm">
        		<div class="hamburger-menu">
          			<div class="hamburger-menu__in"></div>
        		</div>
				<div class="mobile-lang">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.site.selector",
						"sitelang",
						Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"SITE_LIST" => array(0=>"*all*",)
						)
					);?>
	      		</div>
      		</div>
      		<div class="b-social pull-right visible-md visible-lg">
				<?$APPLICATION->IncludeFile(
					SITE_DIR.'/include/headersoc.php',
					array(),
					array(
						"MODE"=>"html",
					)
				);?>
			</div>
      		<div class="b-search pull-right visible-md visible-lg">
				<?$APPLICATION->IncludeComponent(
					"bitrix:search.form",
					"search",
					Array(
						"PAGE" => "#SITE_DIR#search/index.php",
						"USE_SUGGEST" => "N"
					)
				);?>
      		</div>
    	</div>
  	</header>
	<!-- end header-->
	<div class="hamburger-inner">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"menu",
			Array(
				"ALLOW_MULTI_SELECT" => "Y",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => array(0=>"",),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"CLASS_MENU" => '_mod3 visible-xs visible-sm',
			)
		);?>
	    <div class="b-search">
			<?$APPLICATION->IncludeComponent(
				"bitrix:search.form",
				"search",
				Array(
					"PAGE" => "#SITE_DIR#search/index.php",
					"USE_SUGGEST" => "N"
				)
			);?>
	    </div>
	    <div class="align-center">
	      <div class="b-info">
			<span class="b-info__phone">
	      		<?$APPLICATION->IncludeFile(
					SITE_DIR.'/include/headerphone.php',
					array(),
					array(
						"MODE"=>"text",
					)
				);?>
	      	</span>
	        <div class="b-info__img pull-left"><img src="<?= SITE_TEMPLATE_PATH ?>/img/24_7.png" alt=""></div><a class="b-info__lk pull-left" href="#form-order"><?= GetMessage('ZAKAZ') ?></a>
	      </div>
	    </div>
	    <div class="b-social">
	    	<?$APPLICATION->IncludeFile(
				SITE_DIR.'/include/headersoc.php',
				array(),
				array(
					"MODE"=>"html",
				)
			);?>
	    </div>
	</div>
		<?if($MainConstruct->bIsMainPage):?>

			<div class="just-photo">
				<script>
					var justphoto = document.getElementsByClassName('just-photo')[0];
					var clientWidth = document.body.clientWidth;
					var percentWidth = clientWidth/1920;
					var percentHeight = Math.floor(630*percentWidth);
					justphoto.style.minHeight = percentHeight+'px';
				</script>
				<div class="main-order-block">
					<div class="main-order-block__slogan">Ваш личный ассистент в мире деловой авиации 24/7</div>
					<div class="main-order-block__form">
						<div class="mof-input departure-inp">
							<label class="input-label" for="order_from">Откуда</label>
							<div>
								<input type ="text" class="mof-input-item city required" id="order_from" name="order_from" autocomplete="off">
								<div class="modal-city-from modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input arrival-inp">
							<label class="input-label" for="order_to">Куда</label>
							<div>
								<input class="mof-input-item city required" id="order_to" autocomplete="off">
								<div class="modal-city-to modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input">
							<label class="input-label " for="order_attrebute_date_0" >Дата</label>
							<div><input class="mof-input-item datepicker datepicker2 required" data-dp-class="custom" id="order_attrebute_date_0" autocomplete="off"/></div>
						</div>
						<div class="mof-input leg-pass">
							<div class="mof-input_add-pass">
								<div class="mof-input_add-pass-btn"></div>
								<div class="mof-input_add-pass-btn"></div>
							</div>
							<label class="input-label" for="add_pass">Пассажиры</label>
							<div><input class="mof-input-item pass required" id="add_pass" autocomplete="off"></div>
						</div>
						
					</div>



					<div class="main-order-block__form display-none">
						<div class="mof-input departure-inp">
							<label class="input-label" for="order_from2">Откуда</label>
							<div>
								<input type ="text" class="mof-input-item required" id="order_from2" autocomplete="off">
								<div class="modal-city-from2 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input arrival-inp">
							<label class="input-label" for="order_to2">Куда</label>
							<div>
								<input class="mof-input-item required" id="order_to2" autocomplete="off">
								<div class="modal-city-to2 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input">
							<label class="input-label" for="order_attrebute_date_2">Дата</label>
							<div><input class="mof-input-item datepicker required" data-dp-class="custom"id="order_attrebute_date_2" autocomplete="off"/></div>
						</div>
						<div class="mof-input leg-pass">
							<div class="mof-input_add-pass">
								<div class="mof-input_add-pass-btn"></div>
								<div class="mof-input_add-pass-btn"></div>
							</div>
							<label class="input-label" for="add_pass2">Пассажиры</label>
							<div><input class="mof-input-item pass required" id="add_pass2" autocomplete="off"></div>
						</div>
						<div class="del-block"></div>
					</div>

					
					<div class="main-order-block__form display-none">
						<div class="mof-input departure-inp">
							<label class="input-label" for="order_from3">Откуда</label>
							<div>
								<input type ="text" class="mof-input-item required" id="order_from3" autocomplete="off">
								<div class="modal-city-from3 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input arrival-inp">
							<label class="input-label" for="order_to3">Куда</label>
							<div>
								<input class="mof-input-item required" id="order_to3" autocomplete="off">
								<div class="modal-city-to3 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input">
							<label class="input-label" for="order_attrebute_date_3">Дата</label>
							<div><input class="mof-input-item datepicker required" id="order_attrebute_date_3" autocomplete="off"/></div>
						</div>
						<div class="mof-input leg-pass">
							<div class="mof-input_add-pass">
								<div class="mof-input_add-pass-btn"></div>
								<div class="mof-input_add-pass-btn"></div>
							</div>
							<label class="input-label" for="add_pass3">Пассажиры</label>
							<div><input class="mof-input-item pass required" id="add_pass3" autocomplete="off"></div>
						</div>
						<div class="del-block"></div>
					</div>


					<a href="#main-form-order" class="mof-btn">Далее</a>
					<div class="main-order-block__manage-btns">
						<div class="manage-btn comeback">Обратный перелет</div>
						<a href="#formMainOrderPreferences" class="manage-btn add-preferences">Предпочтения</a>
						<div class="manage-btn main-order-block__add-transfer">Добавить пересадку</div>

						<div class="social-btn social-btn-telegram">Telegram</div>
						<div class="social-btn social-btn-whats">Whats App</div>
						<div class="social-btn social-btn-viber">Viber</div>
					</div>
				</div>
				<div class="shadow"></div>


				<script>
			
					var mofInputs = document.getElementsByClassName('mof-input-item');
					for (var i = 0; i < mofInputs.length; i++){
						mofInputs[i].value = "";
						mofInputs[i].onfocus = (function(){
							var x = i;
							return function(){
								mofInputs[x].parentElement.previousElementSibling.classList.add("label-active");
							}
						})();
						mofInputs[i].onblur = (function(){
							var x = i;
							return function(){

								setTimeout(function(){
									if (mofInputs[x].value==""){
										mofInputs[x].parentElement.previousElementSibling.classList.remove("label-active");
										mofInputs[x].parentElement.previousElementSibling.classList.remove("input-label__error");
									}
								}, 300);
								// if (mofInputs[x].value==""){
								// 		mofInputs[x].parentElement.previousElementSibling.classList.remove("label-active");
								// 		mofInputs[x].parentElement.previousElementSibling.classList.remove("input-label__error");
								// }
							}
						})();
					}
					
				
					//добавляем пассажиров (как смог)
					let legPass = document.getElementsByClassName('leg-pass'); // дивы с пассажирами
					let passangers = document.getElementsByClassName('pass'); // инпуты с пассажирами
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.firstElementChild.onmousedown = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = (PasAmount.value == "") ? 1 : ++passValue;


								legPass[formNumber].children[1].classList.add("label-active");
							}
						} )();
					}

					//убавляем пассажиров (как смог)
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.lastElementChild.onclick = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = passValue > 1 ? --passValue : 1;
							}
						} )();
					}


					let mainOrderBlock = document.getElementsByClassName('main-order-block')[0];// блок со всеми формами
					let forms = document.getElementsByClassName('main-order-block__form'); // формы 
					let dalee = document.getElementsByClassName('mof-btn')[0]; // кнопка далее
					let comeback = document.getElementsByClassName('comeback')[0]; //кнопка добавить обратный рейс
					let addTransfer = document.getElementsByClassName('main-order-block__add-transfer')[0];//кнопка добавить пересадку
					let addPreferences = document.getElementById('prefencesSubmit');//кнопка добавить предпочтения


					// добавляем пересадочку
					var formCount = 1;
					addTransfer.onclick = function(){
						comeback.classList.add('display-none');
						if (formCount < 3) {
							formCount++;
						}
						switch(formCount){
							case 2: 
								forms[1].classList.remove('display-none');
								mainOrderBlock.classList.add('mob-whith-transfer');
								break;
							case 3:
								forms[2].classList.remove('display-none');
								break;
						}
					}

					// добавляем предпочтения
					// addPreferences.onclick = function(){
					// 	e.preventDefault();
					// }

					// удаляем пересадочку
					let delBlock = document.getElementsByClassName('del-block');
					for (var i = 0; i < delBlock.length; i++){
						delBlock[i].onclick = (function(){
							let x = i;
							return function(){
									formCount--;
								if (formCount == 1)	{
									mainOrderBlock.classList.remove('mob-whith-transfer');
									comeback.classList.remove('display-none');
									addTransfer.classList.remove('display-none');
								}
								forms[x+1].classList.add('display-none');
								let inpts = forms[x+1].getElementsByTagName("input");
								for (let i = 0; i < inpts.length; i++ ){
									inpts[i].value = "";
									inpts[i].parentElement.previousElementSibling.classList.remove("label-active");
								}
							}

						})();
					}

					//добавляем обратный рейс
					comeback.onclick = function(){
						addTransfer.classList.add('display-none');
						mainOrderBlock.classList.add('mob-whith-transfer');
						forms[1].classList.remove('display-none');
						formCount = 2;
						let departure = forms[0].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item')[0];
						let arrival = forms[0].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0];
						let passagir = forms[0].getElementsByClassName('leg-pass')[0].getElementsByClassName('mof-input-item')[0];

						let comebackDeparture = forms[1].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item')[0];
						let comebackArrival = forms[1].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0];
						let comebackPassagir = forms[1].getElementsByClassName('leg-pass')[0].getElementsByClassName('mof-input-item')[0];

						comebackDeparture.value = arrival.value;
						if (arrival.value != "") addActiveLabel(comebackDeparture);
						
						comebackArrival.value = departure.value;
						if (departure.value != "") addActiveLabel(comebackArrival);
						
						comebackPassagir.value = passagir.value;
						if (passagir.value != "") addActiveLabel(comebackPassagir);
					}

					//делаем активный лейбл меньше
					let addActiveLabel = function(input){
						input.parentElement.previousElementSibling.classList.add("label-active");
					}

					
					$('body').on('click', '.mof-btn', function(e){
						e.preventDefault();
						var check = 0;
						$('.main-order-block .main-order-block__form:not(.display-none) .required').each(function(){
							if(!$(this).val()) {
								$(this).focus();
								$(this).parents('.mof-input').find('.input-label').addClass('input-label__error');
								check = 1;
								return false;
							}
						});
						if(!check) {
							$('#formMainOrder').addClass('form-orderMtl_active');
						}
					});

					$('body').on('click', '.add-preferences', function(e){
						e.preventDefault();
						$('#formMainOrderPreferences').addClass('form-orderMtl_active');
					});

					function closeModal() {
						$('.form-orderMtl_active').removeClass('form-orderMtl_active');
					}
					// close modal window
					$('body').on('click', '.form-orderMtl-below', function(e){
						closeModal();
					});
						// keyboard event
					document.onkeyup = function(e){
						if (e.keyCode === 27){//esc
							closeModal();
						}
					}

				</script>


				<div class="just-photo_text">
					<div class="company-slogan">Serious Business. Serious Approach</div>
					<div class="company-name">Sirius Aero</div>
				</div>
				<img src="<?= SITE_TEMPLATE_PATH ?>/img/main-img.jpg" alt="" class="goanime" id="imgmainsl" style="width:100%;">
			</div>
			
			<script>
					var itisimg = document.getElementById('imgmainsl');
					itisimg.onload = function() {
						setTimeout(function(){
							console.log('asd');
							justphoto.style.minHeight = '0px';
						}, 1000);
  				}
					setTimeout(function(){
							document.getElementsByClassName('company-name')[0].classList.add("company-name_active");
					}, 1000);
					setTimeout(function(){
							document.getElementsByClassName('company-slogan')[0].classList.add("company-slogan_active");
					}, 500);
					setTimeout(function(){
							document.getElementsByClassName('main-order-block')[0].classList.add("main-order-block_active");
					}, 300);
				</script>
		<?endif;?>
	<?
	if($MainConstruct->bIsMainPage):
		$menu = '_mod1';
		$lvl = "2";
	else:
		$menu = '_mod2';
		$lvl = "2";
	endif;
	$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"menu",
		Array(
			"ALLOW_MULTI_SELECT" => "Y",
			"CHILD_MENU_TYPE" => "left",
			"DELAY" => "N",
			"MAX_LEVEL" => $lvl,
			"MENU_CACHE_GET_VARS" => array(0=>"",),
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"ROOT_MENU_TYPE" => "top",
			"USE_EXT" => "N",
			"CLASS_MENU" => $menu,
		)
	);
	$menu = '_mod4';
	$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"menumobile",
		Array(
			"ALLOW_MULTI_SELECT" => "Y",
			"CHILD_MENU_TYPE" => "left",
			"DELAY" => "N",
			"MAX_LEVEL" => $lvl,
			"MENU_CACHE_GET_VARS" => array(0=>"",),
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"ROOT_MENU_TYPE" => "top",
			"USE_EXT" => "N",
			"CLASS_MENU" => $menu,
		)
	);?>
	<?if(!$MainConstruct->bIsMainPage):
		if(ERROR_404 != 'Y') :?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumbs",
			Array(
				"PATH" => "",
				"SITE_ID" => "ru, en",
				"START_FROM" => "0"
			)
		);?>
	<?  endif;
	endif;?>
	<!-- begin content-->
