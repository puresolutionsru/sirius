<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="b-advantage">
  	<div class="b-advantage__t"><?= $arParams['PAGER_TITLE']; ?></div>
	<div class="b-advantage__items">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="b-advantage__i">
				<div class="b-advantage__i__img">
	                <img src="<?=$arItem["PREVIEW_PICTURE"]['SRC']?>" alt="">
	            </div>
				<?if($arParams["DISPLAY_NAME"]!="N"):?>
	                <div class="b-advantage__i__tx"><?=$arItem["NAME"]?></div>
				<?endif;?>
	            <div class="b-advantage__i__line"></div>
			</div>
		<?endforeach;?>
	</div>

	<div class="b-advantage__order"><i></i><?= $arParams['TEXT_PHONE']; ?>
	   <a href="tel:<?= $arParams['PHONE']; ?>"><span><?= $arParams['PHONE']; ?></span></a>
	</div>
	<div class="b-advantage__btn">
	  <div class="b-advantage__btn__in">
		<div class="cl-effect-2"><a href="#form-order" data-hover="<?= $arParams['BUTTON_TEXT']; ?>"><span><?= $arParams['BUTTON_TEXT']; ?></span></a></div>
	  </div>
	</div>
</div>
