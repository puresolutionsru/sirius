<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="lang-tabs">


<?$link = substr($_SERVER['REQUEST_URI'], 1);?>
<?foreach ($arResult["SITES"] as $key => $arSite):?>

	<?
	$lk = $arSite["DIR"].$link;
	if(LANGUAGE_ID != 'ru') {
		$lk = strpos($link, '/') + 1;
		$lk = substr($link, $lk);
		$lk = $arSite["DIR"].$lk;
	}
	?>

	<?if ($arSite["CURRENT"] == "Y"):?>
		<div class="lang-tabs__i"><span class="lang-tabs__link _active"><?=$arSite["NAME"]?></span></div>
	<?else:?>
		<div class="lang-tabs__i">
			<a class="lang-tabs__link" href="<?if(is_array($arSite['DOMAINS']) && strlen($arSite['DOMAINS'][0]) > 0 || strlen($arSite['DOMAINS']) > 0):?>http://<?endif?><?=(is_array($arSite["DOMAINS"]) ? $arSite["DOMAINS"][0] : $arSite["DOMAINS"])?><?=$lk?>"><?=$arSite["NAME"]?></a>
		</div>
	<?endif?>

<?endforeach;?>

</div>
