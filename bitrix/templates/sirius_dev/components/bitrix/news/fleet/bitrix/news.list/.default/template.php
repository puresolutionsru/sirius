<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="t-1 t-1__main"><?= $APPLICATION->GetTitle(); ?></div>

<div class="flot-list-m _on">
		<?
		$m = 1;
		$infoblock = $arParams["IBLOCK_ID"];
		$rs_Section = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => 0, 'ACTIVE' => 'Y'), false, Array("UF_*"));
		while($ar_Section = $rs_Section->GetNext()) {
			$ar_Resu[] = array(
		        'ID' => $ar_Section['ID'],
		        'NAME' => $ar_Section['NAME'],
		        'IBLOCK_SECTION_ID' => $ar_Section['IBLOCK_SECTION_ID'],
				'IMG' => CFile::GetPath($ar_Section['PICTURE']),
		    );
		}
		?>

		<?foreach($ar_Resu as $arItem):
			$i = 1;
			?>
			<div class="flot-list" id="flot-list<?= $arItem['ID'] ?>">
				<div class="flot-list__i">
					<div class="flot-list__i__img"><img src="<?= $arItem['IMG'] ?>" alt=""></div>
					<div class="flot-list__param">
				  		<div class="flot-list__t flot-list__row">
				  		  	<div class="flot-list__i__t"><?= $arItem['NAME'] ?></div>
				  		  	<div class="flot-list__t_pic sirius1"><div class='flot-list__t_pic-title'><?= GetMessage('VMEST') ?></div></div>
				  		  	<div class="flot-list__t_pic sirius2 _mod1"><div class='flot-list__t_pic-title'><?= GetMessage('DAL') ?></div></div>
				  		  	<div class="flot-list__t_pic sirius3"><div class='flot-list__t_pic-title'><?= GetMessage('OB') ?></div></div>
				  		  	<div class="flot-list__t_pic sirius4"><div class='flot-list__t_pic-title'><?= GetMessage('DLINA') ?></div></div>
				  		  	<div class="flot-list__t_pic sirius5"><div class='flot-list__t_pic-title'><?= GetMessage('SHIRINA') ?></div></div>
				  		  	<div class="flot-list__t_pic sirius6"><div class='flot-list__t_pic-title'><?= GetMessage('VYSOTA') ?></div></div>
				  		</div>
						<?
						$section_id = $arItem['ID'];
						$sct_el = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $section_id, 'ACTIVE' => 'Y'), false, array("UF_*"));
						while($ar_sct_el = $sct_el->GetNext()) {
							$link = $ar_sct_el['SECTION_PAGE_URL'];
							?>
							<div class="flot-list__row flot-list__l flot-list__l<?= $i ?>" data-row="<?=$m?>">
						  		  <div><span><?= $ar_sct_el['NAME'] ?></span></div>
						  		  <div><?= $ar_sct_el['UF_VMEST'] ?></div>
						  		  <div><?= $ar_sct_el['UF_DAL'] ?></div>
						  		  <div><?= $ar_sct_el['UF_OB'] ?></div>
						  		  <div><?= $ar_sct_el['UF_DLINA'] ?></div>
						  		  <div><?= $ar_sct_el['UF_SHIRINA'] ?></div>
						  		  <div><?= $ar_sct_el['UF_VYSOTA'] ?></div>
					  		</div>
							<?
							$el_i = CIBlockElement::GetList(array("SORT"=>"ASC"), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $ar_sct_el['ID'], 'ACTIVE' => 'Y'));
							while($item = $el_i->GetNextElement()) {
								$arFields = $item->GetFields();
								$arProps = $item->GetProperties();
								?>
								<a href="<?= $link . '?' . $arFields['CODE'] ?>" class="flot-list__row flot-list__l flot-list__l<?= $i ?> flot-list__child flot-list__child<?= $m ?>">
									<div><span><?= $arFields['NAME'] ?></span></div>
									<div><?= $arProps['VMEST']['VALUE'] ?></div>
									<div><?= $arProps['DAL']['VALUE'] ?></div>
									<div><?= $arProps['OB']['VALUE'] ?></div>
									<div><?= $arProps['DLINA']['VALUE'] ?></div>
									<div><?= $arProps['SHIRINA']['VALUE'] ?></div>
									<div><?= $arProps['VYSOTA']['VALUE'] ?></div>
	            				</a>
								<? if(true) : ?>
								<div class="flot-list__imgm">
									<? if($arProps['PIC_RAM']['VALUE']) : ?>
										<?
										$img = $arProps['PIC_RAM']['VALUE'];
										$img = CFile::GetPath($img);
										?>
										<img src="<?= $img ?>" alt="">
									<? endif; ?>
								</div>
								<? endif; ?>
								<?
							}

							$m++;
							$i = ($i == 1 ? '2' : '1');
						} ?>
				  	</div>
				</div>
			</div>
		<?endforeach;?>
</div>



<? if(false) : ?>
<div class="flot-list-m">
		<?
		$infoblock = $arParams["IBLOCK_ID"];
		$rs_Section = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => 0, 'ACTIVE' => 'Y'), false, Array("UF_*"));
		$ar_Resu = array();
		while($ar_Section = $rs_Section->Fetch()) {
			$ar_Resu[] = array(
		        'ID' => $ar_Section['ID'],
		        'NAME' => $ar_Section['NAME'],
		        'IBLOCK_SECTION_ID' => $ar_Section['IBLOCK_SECTION_ID'],
				'IMG' => CFile::GetPath($ar_Section['PICTURE']),
		    );
		}
		?>


		<?foreach($ar_Resu as $arItem):
			$i = 1;
			?>
			<div class="flot-list">
				<div class="flot-list__i">
					<div class="flot-list__i__img"><img src="<?= $arItem['IMG'] ?>" alt=""></div>
					<div class="flot-list__param">
				  		<div class="flot-list__t flot-list__row">
				  		  	<div class="flot-list__i__t"><?= $arItem['NAME'] ?></div>
				  		  	<div class="flot-list__t_pic sirius1"><?= GetMessage('VMEST') ?></div>
				  		  	<div class="flot-list__t_pic sirius2 _mod1"><?= GetMessage('DAL') ?></div>
				  		  	<div class="flot-list__t_pic sirius3"><?= GetMessage('OB') ?></div>
				  		  	<div class="flot-list__t_pic sirius4"><?= GetMessage('DLINA') ?></div>
				  		  	<div class="flot-list__t_pic sirius5"><?= GetMessage('SHIRINA') ?></div>
				  		  	<div class="flot-list__t_pic sirius6"><?= GetMessage('VYSOTA') ?></div>
				  		</div>
						<?
						$section_id = $arItem['ID'];
						$sct_el = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $section_id), false, array("UF_VMEST", "UF_DAL", "UF_OB", "UF_DLINA", "UF_SHIRINA", "UF_VYSOTA"));
						while($ar_sct_el = $sct_el->GetNext()) {
							$link = $ar_sct_el['SECTION_PAGE_URL'];
							?>
							<div class="flot-list__row flot-list__l flot-list__l<?= $i ?>" data-row="<?=$m?>">
						  		  <div>
									  <span><?= $ar_sct_el['NAME'] ?></span>
								  </div>
						  		  <div><?= $ar_sct_el['UF_VMEST'] ?></div>
						  		  <div><?= $ar_sct_el['UF_DAL'] ?></div>
						  		  <div><?= $ar_sct_el['UF_OB'] ?></div>
						  		  <div><?= $ar_sct_el['UF_DLINA'] ?></div>
						  		  <div><?= $ar_sct_el['UF_SHIRINA'] ?></div>
						  		  <div><?= $ar_sct_el['UF_VYSOTA'] ?></div>
					  		</div>
							<?
							$el_i = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $ar_sct_el['ID']));
							while($item = $el_i->GetNextElement()) {
								$arFields = $item->GetFields();
								$arProps = $item->GetProperties();
								?>
								<a href="<?= $link . '?' . $arFields['CODE'] ?>" class="flot-list__row flot-list__l flot-list__l<?= $i ?> flot-list__child flot-list__child<?= $m ?>">
									<div>
										<span><?= $arFields['NAME'] ?></span>
									</div>
									<div><?= $arProps['VMEST']['VALUE'] ?></div>
									<div><?= $arProps['DAL']['VALUE'] ?></div>
									<div><?= $arProps['OB']['VALUE'] ?></div>
									<div><?= $arProps['DLINA']['VALUE'] ?></div>
									<div><?= $arProps['SHIRINA']['VALUE'] ?></div>
									<div><?= $arProps['VYSOTA']['VALUE'] ?></div>
	            				</a>
								<div class="flot-list__imgm">
									<div style="display:none;">
										<? print_r($arFields); ?>
									</div>
									<?
									$img = $arFields['FIELDS']['DETAIL_PICTURE'];
									$img = CFile::GetPath($img);
									?>
									<img src="<?= $img ?>" alt="">
								</div>
								<?
							}

							$m++;
							$i = ($i == 1 ? '2' : '1');
						} ?>
				  	</div>
				</div>
			</div>
		<?endforeach;?>
</div>
<? endif; ?>