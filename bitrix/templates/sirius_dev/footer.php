<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
	<!-- begin footer-->
	<footer class="footer">
	<?if(!$MainConstruct->bIsMainPage):
		if(ERROR_404 != 'Y') :?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumbs",
			Array(
				"PATH" => "",
				"SITE_ID" => "ru, en",
				"START_FROM" => "0"
			)
		);?>
	<?  endif;
	endif;?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"menu",
			Array(
				"ALLOW_MULTI_SELECT" => "Y",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => array(0=>"",),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"CLASS_MENU" => '',
			)
		);?>
		<div class="footer__in cl">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-push-4 footer-col1">
				<div class="b-partners">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footerpartners.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-sm-push-6 col-md-push-4 footer-col">
				<div class="b-info-order">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footerright.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-md-pull-8 col-sm-pull-6 footer-col2">
				<a class="footer__logo hidden-xs" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logofoot.png" alt=""></a>
				<div class="copyrights">2007-<?= date(Y) ?> ®
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footercopy.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
				<a href="<?= SITE_DIR ?>sitemap/" class="link-sitemap"><?= GetMessage('SITEMAP') ?></a>
				<a href="<?= SITE_DIR ?>privacy-policy/" class="link-sitemap" style="margin-left:20px;"><?= GetMessage('PRIVACYLK') ?></a>
			</div>
		</div>
	</footer>
	<!-- end footer-->

	<a href="#form-order" class="btn-order fancymodal"><?= GetMessage('order_flight') ?></a>

	<div style="display: none; padding: 0 !important;" id="form-order" class="form-order">
		<div id="forms-order-wrap">
			<div id="tabs">
				<ul class="nav-tabs">
					<li class="nav">
						<a href="#tabs-1"><span class="glyphicon glyphicon-arrow-right"></span><?= GetMessage('form1') ?></a>
					</li>
					<li class="nav">
						<a href="#tabs-2"><span class="glyphicon glyphicon-transfer"></span><?= GetMessage('form2') ?></a>
					</li>
					<li class="nav">
						<a href="#tabs-3"><span class="glyphicon glyphicon-retweet"></span><?= GetMessage('form3') ?></a>
					</li>
				</ul>

				<div class="tab-content">
					<!-- Tab panes -->
					<div class="tab-pane" id="tabs-1">
						<form method="POST" class="ajax_order_big4 custom-validate">
							<input name="title" value="Заявка на подбор самолета в одну сторону" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form8') ?></label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form9') ?></label>
												<input type="text" name="person_phone" class="input_phone form-control"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label><?= GetMessage('form10') ?></label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl2"/>
										<label for="sogl2" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="tabs-2">
						<form method="POST" class="ajax_order_big5 custom-validate">
							<input name="title" value="Заявка на подбор самолета туда-обратно" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="h4"><?= GetMessage('form4') ?></div>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="h4"><?= GetMessage('form5') ?></div>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form8') ?></label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form9') ?> </label>
												<input type="text" name="person_phone" class="input_phone form-control"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> <?= GetMessage('form10') ?> </label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl3"/>
										<label for="sogl3" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="tabs-3">
						<form method="POST" class="ajax_order_big6 custom-validate">
							<input name="title" value="Заявка на подбор самолета с несколькими пересадками" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row copyrow">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn btn-addMore" id="copyrow">
								<?= GetMessage('form13') ?>
							</div>
							<br/>
							<br/>


							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form8') ?> </label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form9') ?> </label>
												<input type="text" name="person_phone" class="input_phone form-control"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> <?= GetMessage('form10') ?> </label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl4"/>
										<label for="sogl4" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="forms-order-info">
				<div class="info-content">
					<div class="form-row">
						<div class="col-8">
							<p class="screen-wis"><?= GetMessage('form14') ?></p>
							<p class="phone-wis"><?= GetMessage('form15') ?><br/><?= GetMessage('form16') ?></p>
							<a href="tel:+79295610357">+7(929)561-03-57</a>
							<!-- <a href="tel:+74959896191">+7(495)989-61-91</a> -->
							<!-- <a href="mailto:sales@sirius-aero.ru" class="email">sales@sirius-aero.ru</a> -->
						</div>
						<div class="col-4">
							<img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" class="img-responsive"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-orderMtl" id="form-orderMtl">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<div class="modal-form__t"><?= GetMessage('FB_DEALS') ?></div>
			<div class="modal-form__cities"><span id="modal-form__from"></span> - <span id="modal-form__to"></span></div>
			<div class="modal-form__date"><span id="modal-form__datefrom"></span></div>
			<input type="hidden" name="raceid" id="raceid" value="">
			<div class="modal-form__filds">
				<div class="form__item">
					<p><?= GetMessage('FB_NAME') ?></p>
					<input type="text" name="name" class="it">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_PHONE') ?></p>
					<input type="text" name="phone" class="it phone-mask">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_EMAIL') ?></p>
					<input type="text" name="email" class="it">
				</div>
				<div class="form__item form__item-select">
					<?= GetMessage('FB_COUNT') ?>
					<select id="modal-form__select-pass"></select>
				</div>
				<div class="form__item">
					<input type="submit" name="" class="form-orderMtl__submit it-s" value="<?= GetMessage('FB_ORDER') ?>">
				</div>
			</div>
		</div>
		<div class="modal-form__success"><?= GetMessage('FB_SEND') ?></div>
	</div>

<!-- для формы на первом экране -->
<div class="form-orderMtl" id="formMainOrderPreferences">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<form method="POST" class=".ajax_order_main custom-validate">
				<div class="formMainOrder-title">Предпочтения</div>
				<!-- <div class="formMainOrder-ask"></span></div> -->
				<br>
				<div class="modal-form__filds">
					<div class="form__item form__item-preferences ">
						<textarea name="preferences" class="it"></textarea>
							<!-- <input type="text" name="preferences" class="it"> -->
					</div>
					<div class="form__item">
						<input type="submit" name="" class="form-orderMtl__submit it-s" id="prefencesSubmit" value="Добавить">
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="form-orderMtl" id="formMainOrder">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<form method="POST" class=".ajax_order_main custom-validate">
				<div class="formMainOrder-title">Заказ рейса</div>
				<div class="formMainOrder-ask">Укажите пожалуйста ваш телефон и мы подберем лучшие варианты и сразу же свяжемся с вами!</span></div>
				<br>
				<input type="hidden" name="raceid" id="raceid" value="">
				<div class="modal-form__filds">
					<div class="form__item">
						<p>Ваше имя</p>
						<input type="text" name="name" class="it">
					</div>
					<div class="form__item">
						<p>Телефон</p>
						<input type="text" name="phone" class="it">
					</div>
					<div class="form__item">
						<p>Email</p>
						<input type="text" name="email" class="it">
					</div>
					<div class="form__item">
						<input type="submit" name="" class="form-orderMtl__submit it-s" value="Заказать">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-form__success">Ваше сообщение отправлено.</div>
	</div>

	<div id="gotop" class="fa fa-angle-up"></div>
	
	<?
	$APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick/slick.min.js');
	?>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery-ui.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/fancy/jquery.fancybox.pack.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.cookie.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.functions.js'></script>
	<? if(LANG == 's1') : ?>
		<script src='//www.google.com/recaptcha/api.js'></script>
	<? elseif(LANG == 'en') : ?>
		<script src='//www.google.com/recaptcha/api.js?hl=en'></script>
	<? endif; ?>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter28830840 = new Ya.Metrika2({
						id:28830840,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true,
						trackHash:true
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/tag.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks2");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/28830840" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<script>
        // (function(w,d,u){
        //         var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        //         var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        // })(window,document,'https://corp.rusline.aero/upload/crm/site_button/loader_1_0f02yn.js');
	</script>
</body>
</html>
