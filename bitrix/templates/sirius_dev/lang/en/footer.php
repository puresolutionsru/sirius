<?
$MESS['SITEMAP'] = 'Sitemap';
$MESS['order_flight'] = 'Order flight now';
$MESS['form1'] = 'One way';
$MESS['form2'] = 'Round trip';
$MESS['form3'] = 'Several destinations';
$MESS['form4'] = 'Location';
$MESS['form5'] = 'Where';
$MESS['form6'] = 'Departure date';
$MESS['form7'] = 'Passengers';
$MESS['form8'] = 'Full name';
$MESS['form9'] = 'Telephone';
$MESS['form10'] = 'E-mail';
$MESS['form11'] = 'Comment';
$MESS['form12'] = 'Send';
$MESS['form13'] = 'Add flight';
$MESS['form14'] = 'Order a private jet daily 24/7';
$MESS['form15'] = 'Order a private jet';
$MESS['form16'] = 'daily 24/7';
$MESS['form17'] = 'I consent to the processing of <a href="/en/privacy-policy/" target="_blank">my personal data</a>';
$MESS['PRIVACYLK'] = 'Privacy policy';

$MESS['FB_DEALS'] = 'Deal';
$MESS['FB_NAME'] = 'Name';
$MESS['FB_PHONE'] = 'Phone';
$MESS['FB_EMAIL'] = 'Email';
$MESS['FB_COUNT'] = 'Number of passengers';
$MESS['FB_ORDER'] = 'Order';
$MESS['FB_SEND'] = 'Your message is send.';
?>
