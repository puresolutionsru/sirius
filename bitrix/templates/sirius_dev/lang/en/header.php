<?
$MESS['ZAKAZ'] = 'Flight order';
$MESS ['FLEET'] = "Fleet Sirius Aero";
$MESS ['FLEET_PARTNERS'] = "Fleet partners";
$MESS ['PRIVACY'] = "<div class='bold t-3'>Cookie Notice</div><div>We use cookies to offer you a better browsing experience.</div><div>View our <a class='b-cookies-link' target='_blank' href='/en/privacy-policy/'>Privacy Policy</a> for more information</div>";
$MESS ['PRIVACY_BUT'] = 'I accept';
?>
