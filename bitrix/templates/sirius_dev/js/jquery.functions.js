$( document ).ready( function() {
    
        $('#gotop').click(function(){
            $('html, body').animate( { scrollTop: 0 }, 500 );
        });
        $(window).scroll(function(){
            if($(window).scrollTop() > 100){
                $('#gotop').addClass('visible');
            }else{
                $('#gotop').removeClass('visible');
            }
        });

    var obj,
        timerId,
        timerText,
        defOptions,
        rekursTime;

    function sliderRP(objcon){
        defOptions = {
            timeSlide: 7000,
            effect: 'fade'
        };
        obj = $(objcon);
        obj.addClass('kipslider');
        obj.find('img:first-child').addClass('sliderm_img__active');
        var htmlobj = obj.html();
        obj.html('<div class="kipslider-arrow"><div class="kipslider-arrow_next"></div><div class="kipslider-arrow_prev"></div></div><div class="kipslider-content">' + htmlobj + '</div><div class="kipslider-pag"></div>')
        // смена слайда8
        timerId = setInterval(function(){
            rekurs();
        }, defOptions.timeSlide, true);
        rekurs();

        $('body').on('click', '.kipslider-arrow_next', function(){
            clearInterval(timerId);
            clearTimeout(rekursTime);
            nextSlide();
        });
        $('body').on('click', '.kipslider-arrow_prev', function(){
            clearInterval(timerId);
            clearTimeout(rekursTime);
            prevSlide();
        });
    }
    function rekurs(){
        rekursTime = setTimeout(function(){
            nextSlide();
        }, defOptions.timeSlide - 500);
    }
    function nextSlide(){console.log('face');
        $('.sliderm_img__active').fadeOut(500, function() {
            var next = $('.sliderm_img__active').next();
            $('.sliderm_img__active').removeClass('sliderm_img__active');
            if(!next.length){
                obj.find('img:first-child').addClass('sliderm_img__active');
            }
            else {
                next.addClass('sliderm_img__active');
            }
            $('.sliderm_img__active').fadeIn(500);
        });
    }
    function prevSlide(){
        $('.sliderm_img__active').fadeOut(500, function() {
            var prev = $('.sliderm_img__active').prev();
            $('.sliderm_img__active').removeClass('sliderm_img__active');
            if(!prev.length){
                obj.find('img:last-child').addClass('sliderm_img__active');
            }
            else {
                prev.addClass('sliderm_img__active');
            }
            $('.sliderm_img__active').fadeIn(500);
        });
    }

    function changeText() {
        var tru = setTimeout(function(){
            $('.b-video-content_i').removeClass('fadeout');
            clearTimeout(tru);
        }, 1500);

        var act  = $('.b-video-content_i.active');
        if(!act.length) {
            $('.b-video-content_i:first-child').addClass('active');
        }
        else {
            var actn = act.next();
            if(!actn.length){
                clearInterval(timerText);
                return false;
            }

            act.removeClass('active');
            act.addClass('fadeout');
            actn.addClass('active');
        }

        if(!$('.b-video-content_i:last-child').hasClass('active')) {
            var clrt = setTimeout(function(){
                $('.b-video-content_i.active').addClass('fadeout');
                clearTimeout(clrt);
            }, 2200);
        }
    }

    // setTimeout(function(){
    //     changeText();
    //     timerText = setInterval(function(){
    //         changeText();
    //     }, 2800);
    // }, 2000);

    $(window).load(function() {
        // $('#imgmainsl').addClass('goanime');
        // $('#imgmainsl').removeClass('goanimestart');
        changeText();
    });

    $(window).resize(function(){
        slidecx();
    });
    function slidecx(){
        var hkip = $('.sliderm1 img').height();
        $('.sliderm1').height(hkip);
    }
    $('#imgmainsl').load(function(){
        slidecx();
     });

    $('.list-ttl-down').click(function(e){
        e.preventDefault();
        if ($(this).parent().next().css('display') == 'none' ) {
            $(this).addClass('list-ttl-down_active');
            $(this).parent().next().animate({ height: 'show' }, 500);;
        }
        else {
            $(this).removeClass('list-ttl-down_active');
            $(this).parent().next().animate({ height: 'hide' }, 500);;
        }
    });




    $('.b-cook_but').click(function(e){
        e.preventDefault();
        $('.b-cook').remove();
        // $('.b-cookies').animate({
        //     height: "0px",
        //     padding: '0px',
        // }, 200);
        $.cookie('siriuscookie', 'off', { path: '/', expires: 30 });
    });

  $('.fancybox').fancybox();
  $('a[href="#form-order"]').fancybox();

  $('.form-close').click(function(){
      $(this).parent().addClass('form__hide');
  });


  $('.language_2').change(function(){
      var uri = $('.language_2 option:selected').val();
      window.location.href = uri;
  });

    // FORM
    $("#tabs").tabs();
    $("#formtabfleet .tabs").tabs();
    $('input').on("keydown keypress", function(e){
		var e = e || event, k = e.which || e.button;
		if(e.ctrlKey && k == 86) return false;
	});
    $('.input_phone').on("change keyup input click", function() {
	    if (this.value.match(/[^0-9\s\-\(\)\+]/g)) {
	        this.value = this.value.replace(/[^0-9\s\-\(\)\+]/g, '');
	    }
	});
    $('.form-control[name="person_name"]').on("change keyup input click", function() {
	    if (this.value.match(/[^а-яА-Яa-zA-Z\s]/g)) {
	        this.value = this.value.replace(/[^а-яА-Яa-zA-Z\s]/g, '');
	    }
	});
    $(".datepicker").datepicker({
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		firstDay: 1,
		dateFormat: "dd/mm/yy",
		beforeShow: function(input, inst) {
	      inst.dpDiv
	        .removeClass('custom')
	        .addClass($(input).data('dp-class'));
	    }
	});
    $('.glyphicon-date-ico').on('click', function(){
		$(this).siblings(".datepicker").datepicker("show");
	});
    $('#copyrow').on('click',function(){
		var copy = $('.copyrow:last').clone(false).insertAfter('.copyrow:last');

		$('body').find('.datepicker').removeClass("hasDatepicker").attr('id','').datepicker('destroy');

		var i=0;
		$('body').find('.datepicker').each(function (i,el) {
			$(el).attr("id",'date' + i).datepicker(
				{
					monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					firstDay: 1,
					dateFormat: "dd/mm/yy",
				}
			);
			i++;
		});
	})
    $('.copyrowas').on('click',function(){
		var copy = $('.copyrowasb:last').clone(false).insertAfter('.copyrowasb:last');

		$('body').find('.datepicker').removeClass("hasDatepicker").attr('id','').datepicker('destroy');

		var i=0;
		$('body').find('.datepicker').each(function (i,el) {
			$(el).attr("id",'date' + i).datepicker(
				{
					monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					firstDay: 1,
					dateFormat: "dd/mm/yy",
				}
			);
			i++;
		});
	})
    $('#tabs-2 [name*="city_to"]').eq(0).on('change',function(){
		$('#tabs-2 [name*="city_from"]').eq(1).val($(this).val())
	});
    $('#tabs-2 [name*="city_from"]').eq(0).on('change',function(){
		$('#tabs-2 [name*="city_to"]').eq(1).val($(this).val())
	});
    $('#tabs-5 [name*="city_to"]').eq(0).on('change',function(){
		$('#tabs-5 [name*="city_from"]').eq(1).val($(this).val())
	});
    $('#tabs-5 [name*="city_from"]').eq(0).on('change',function(){
		$('#tabs-5 [name*="city_to"]').eq(1).val($(this).val())
	});
    $("form").submit(function(e) {

	    var ref = $(this).find("[required]");

	    $(ref).each(function(){
	        if ( $(this).val() == '' )
	        {
	            alert("Пожалуйста, заполните корректно поля формы");

	            $(this).focus();

	            e.preventDefault();
	            return false;
	        }
	    });  return true;
    });
    

    function removeFormClass() {
        $('#form-order').removeClass('js-send-head js-send-right js-send-foot');
    }
    $('.b-info__lk').on('click', function(){
        // yaCounter28830840.reachGoal('button_head');
        removeFormClass();
        $('#form-order').addClass('js-send-head');
    });
    $('.btn-order').on('click', function(){
        // yaCounter28830840.reachGoal('button_right');
        removeFormClass();
        $('#form-order').addClass('js-send-right');
    });
    $('.b-advantage__btn a').on('click', function(){
        // yaCounter28830840.reachGoal('butto_footer');
        removeFormClass();
        $('#form-order').addClass('js-send-foot');
    });
    $('.item_mini.calc').on('click', function(){
        // yaCounter28830840.reachGoal('button_price');
    });


    $(document).on('submit', '.ajax_order_big1, .ajax_order_big2, .ajax_order_big3, .ajax_order_big4, .ajax_order_big5, .ajax_order_big6', function(e){
		e.preventDefault();
		var form = $(this);
        var send_data = form.serializeArray();

		$.ajax({
			type: "POST",
			data: send_data,
			dataType: 'json',
			url: '/webforms/ajax_order_big/',
			beforeSend: function() {

			},
			success: function(data) {
				if(data.success){
                    console.log(data.success);
                    if($('#form-order').hasClass('js-send-head')) {
                        yaCounter28830840.reachGoal('order_head');
                    }
                    else if($('#form-order').hasClass('js-send-right')) {
                        yaCounter28830840.reachGoal('order_right');
                    }
                    else if($('#form-order').hasClass('js-send-foot')) {
                        yaCounter28830840.reachGoal('order_footer');
                    }
                    else if($('#formtabfleet').hasClass('form-order')) {
                        yaCounter28830840.reachGoal('order_price');
                    }
					$.fancybox.close();
					$.fancybox.open(
						'<div style="text-align: center;"><h1>Спасибо</h1><p>Ваша заявка принята.<br>В ближайшее время мы свяжемся с Вами.</p></div>'
                    );
                    removeFormClass();
				}
			}
		});
	});

  $(window).on('resize', function() {
    // setHeight();
    setHeightNews();
    hamh();
  });


  var menum = $('.b-menu._mod4');
  $('.the-content.main-w > .t-1').after(menum);

  var mmenu = $('.b-menu._mod4 li._active ul');
  if(mmenu[0]) {
      $('.the-content.main-w > .t-1').addClass('_menu-yes');
  }

  $('.the-content.main-w > .t-1._menu-yes').click(function(){
      $(this).toggleClass('_active');
      $('.the-content.main-w > .t-1 + .b-menu._mod4').toggleClass('_active');
  });


  /* Form capcha
    ****************************************************************/
    function formobj(check, capcha){
        this.check = check;
        this.capcha = capcha;
    }
    var form1 = new formobj(false, false);
    var form2 = new formobj(false, false);
    var form3 = new formobj(false, false);
    var form4 = new formobj(false, false);
    var form5 = new formobj(false, false);
    var form6 = new formobj(false, false);

    function captchapar(el, form) {
        if($(el)){
            var grecaptcha = $(el).val();
            if (grecaptcha == ''){
                form.capcha = false;
            } else {
                form.capcha = true;
            }
        }
    }
    function inputcheck(el, form, formobj, param) {
        if(form.hasClass(el)) {
            formobj.check = param;
        }
    }

    setInterval(function(){
        captchapar('.ajax_order_big1 .g-recaptcha-response', form1);
        captchapar('.ajax_order_big2 .g-recaptcha-response', form2);
        captchapar('.ajax_order_big3 .g-recaptcha-response', form3);
        captchapar('.ajax_order_big4 .g-recaptcha-response', form4);
        captchapar('.ajax_order_big5 .g-recaptcha-response', form5);
        captchapar('.ajax_order_big6 .g-recaptcha-response', form6);
        changeAttrButton();
    }, 1000);
    $('input[name="sogl"]').change(function(){
        var frm = $(this).parents('form');
        if($(this).prop('checked')) {
            inputcheck('ajax_order_big1', frm, form1, true);
            inputcheck('ajax_order_big2', frm, form2, true);
            inputcheck('ajax_order_big3', frm, form3, true);
            inputcheck('ajax_order_big4', frm, form4, true);
            inputcheck('ajax_order_big5', frm, form5, true);
            inputcheck('ajax_order_big6', frm, form6, true);
        } else {
            inputcheck('ajax_order_big1', frm, form1, false);
            inputcheck('ajax_order_big2', frm, form2, false);
            inputcheck('ajax_order_big3', frm, form3, false);
            inputcheck('ajax_order_big4', frm, form4, false);
            inputcheck('ajax_order_big5', frm, form5, false);
            inputcheck('ajax_order_big6', frm, form6, false);
        }
        changeAttrButton();
    });

    function setChangeAttrButton(form, el) {
        if(form.check && form.capcha) {
            $(el).removeAttr('disabled');
        } else {
            $(el).attr('disabled', 'disabled');
        }
    }
    function changeAttrButton() {
        setChangeAttrButton(form1, '.ajax_order_big1 button.btn-send');
        setChangeAttrButton(form2, '.ajax_order_big2 button.btn-send');
        setChangeAttrButton(form3, '.ajax_order_big3 button.btn-send');
        setChangeAttrButton(form4, '.ajax_order_big4 button.btn-send');
        setChangeAttrButton(form5, '.ajax_order_big5 button.btn-send');
        setChangeAttrButton(form6, '.ajax_order_big6 button.btn-send');
    }

  /* Panorams
    ****************************************************************/
    function panorams(item) {
        
    }

  /* Video slide
    ****************************************************************/
  function setHeight(){
    if($('body').is('.mobile')) {

    }
    else {
        var screenHeight = $(window).height();
        var topHeight = $('.header').outerHeight(true) + 90;
        var menuHeight = $('.b-menu._mod1').outerHeight(true);
        var h = screenHeight - (topHeight + menuHeight);
        var videoh = $('.b-video__show').outerHeight(true);
        if(videoh < h) {
          $('.b-video').height(videoh);
        } else {
    		  $('.b-video').height(h);
        }
    }
  };

  if($('.params-model._on .aircraft-3d')) {
      var framesrc = $('.params-model._on .aircraft-3d__link').attr('data-link');
      $('.params-model._on .frame-par').attr('src', framesrc);
  }

  $('.air-mod a').click(function(e){
      e.preventDefault();

      var lk = window.location.href;
      var mod = $(this).attr('href');
      mod = mod.replace('#','');

      if(lk.indexOf("?") == "-1") {
          lk = lk + '?' + mod;
      }
      else {
          var change = lk.split('?');
          lk = change[0] + '?' + mod;
      }
      history.pushState(null, null, lk);

      if($(window).width() <= 768) {
          var airmod = $(this).parents('.air-mod');
          if(!airmod.hasClass('air-mod_active')) {
              $('.air-mod').addClass('air-mod_active');
          }
          else {
              $('.air-mod a').removeClass('_on');
              $(this).addClass('_on');
              var id = $(this).attr('href');
              $('.params-model').removeClass('_on');
              $(id).addClass('_on');
              $('.single-flot__det-img').removeClass('_on');
              $('img[data-tab="'+id+'"]').addClass('_on');
              $('.air-mod').removeClass('air-mod_active');

              var th = $(this);
              $('.air-mod__in').prepend(th);
          }
      }
      else {
          $('.air-mod a').removeClass('_on');
          $(this).addClass('_on');
          var id = $(this).attr('href');
          $('.params-model').removeClass('_on');
          $(id).addClass('_on');
          $('.single-flot__det-img').removeClass('_on');
          $('img[data-tab="'+id+'"]').addClass('_on');
      }

      $('.params-model._on .b-gallery._mod1').slick('refresh');

      $('.params-model iframe').attr('src', '');
      var pansrc = $('.params-model._on .aircraft-3d__link').attr('data-link');
      $('.params-model._on iframe').attr('src', pansrc);

      var ttlpage = $('.params-model._on input[name="hidtitle"]').val();
      document.title = ttlpage;
  });

  /* News text
    ****************************************************************/
  function setHeightNews(){
		var w = $(window).width();
		if(w <= 679){
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 200
			});
		}else{
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 50
			});
		}
    }
if($('div').is('.b-news__i__tx')) {
  setHeightNews();
}

  // hamburger
	$('.hamburger-menu').on('click', function() {
		$('.hamburger-menu__in').toggleClass('_animate');
		$('.header').toggleClass('_on');
		$('.hamburger-inner').toggleClass('_on');
		$('body').toggleClass('_menu');
    hamh();
	});

  function hamh() {
    var h = $('.header').outerHeight(true);
		$('.hamburger-inner').css('top', h);
  }
  hamh();

  // Flot
  $('.flot-tabs span').click(function(){
    if(!$(this).is('._on')) {
      $(this).parent().find('span').toggleClass('_on');
      if($(this).parent().is('.aircraft-slider__tabs')) {
        $('.aircraft-slider__items').toggleClass('_on');
      }
      else {
        $('.flot-list-m').toggleClass('_on');
      }
    }
  });

  function flot() {
    $('.flot-list__l').on('click', function(e) {
      if($(this).attr('data-row')) {
        e.preventDefault();
        var row = $(this).attr('data-row');
        $(this).toggleClass('_on');
        $('.flot-list__child'+row).toggleClass('_on');
      }
    });
  }
  flot();

  $('.aircraft-config__i__t').on('click', function(){
    if(!$(this).is('._on')) {
      $('.aircraft-config__i__t').toggleClass('_on');
      $('.aircraft-config__m .aircraft-config__i').toggleClass('_on');
    }
  });


	/* Slider
	 ****************************************************************/
  $('.slider_fleet').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details',
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1600,
        settings: {
            slidesToShow: 4,
    		slidesToScroll: 4,
        }
    },
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 3,
    		slidesToScroll: 3,
        }
    },
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
    		slidesToScroll: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
    		slidesToScroll: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items1').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		dots: false,
		// centerMode:s true,
		// focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1500,
        settings: {
            slidesToShow: 4,
    		slidesToScroll: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
    		slidesToScroll: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
    		slidesToScroll: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
    		slidesToScroll: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items2').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		dots: false,
		// centerMode: true,
		// focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 4,
    		slidesToScroll: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
    		slidesToScroll: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
    		slidesToScroll: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
    		slidesToScroll: 1,
        }
    }
    ]
	});

	$('.slider_details').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet'
	});

	$('.slider_fleet_partners').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		dots: false,
		asNavFor: '.slider_details_partners',
		// centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1500,
        settings: {
            slidesToShow: 4,
    		slidesToScroll: 4,
        }
    },
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 3,
    		slidesToScroll: 3,
        }
    },
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
    		slidesToScroll: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
    		slidesToScroll: 1,
        }
    }
    ]
	});

	$('.slider_details_partners').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet_partners'
	});

    $('.b-gallery._mod1').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: false,
		arrows: true,
        autoplay: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
            		slidesToScroll: 2,
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1,
            		slidesToScroll: 1,
                }
            }
        ]
	});

	// переключение главных табов
	function switchTabsMain(){
		$('#big_stuff .title_stuff .main span').on('click', function(){
			if($(this).is('.others')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.aero').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_partners').addClass('on');
				}
			}
			if($(this).is('.aero')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.others').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_aero').addClass('on');
				}
			}
		});
	};
  switchTabsMain();


  $(document).click(function(e){
      if(e.target.className != 'item_tab') {
          $('.tabs-in-bg_active').removeClass('tabs-in-bg_active');
      }
  });
	// переключение внутренних табов
	function switchTabsMini(){
		$('#big_stuff .tab_wr').on('click', function(e){
            if($(window).width() <= 768) {

                if(e.target.className == 'item_tab') {
                    if($('.tabs-in-bg').hasClass('tabs-in-bg_active')) {
                        $(this).parents('.tabs').find('.tab_wr').removeClass('on');
                        $(this).parents('.tabs').removeClass('tab_active_one tab_active_two');
                        $(this).addClass('on');

                        var listItem = $(this).parents('.tabs').find('.tab_wr.on');
                        var num = $(this).parents('.tabs').find('.tab_wr').index(listItem);

                        var it = $(this).parents('.item');
                        it.find('.bg, section, .lkforair a').removeClass('on');
                        it.find('.lkforair a').eq(num).addClass('on');
                        it.find('.bg').eq(num).addClass('on');
                        it.find('section').eq(num).addClass('on');
                    }
                }
                $(this).parents('.tabs-in-bg').toggleClass('tabs-in-bg_active');
            }
            else {
                $(this).parents('.tabs').find('.tab_wr').removeClass('on');
                $(this).parents('.tabs').removeClass('tab_active_one tab_active_two');
                $(this).addClass('on');

                var listItem = $(this).parents('.tabs').find('.tab_wr.on');
                var num = $(this).parents('.tabs').find('.tab_wr').index(listItem);

                var it = $(this).parents('.item');
                it.find('.bg, section, .lkforair a').removeClass('on');
                it.find('.lkforair a').eq(num).addClass('on');
                it.find('.bg').eq(num).addClass('on');
                it.find('section').eq(num).addClass('on');
            }

		});
	};
  switchTabsMini();

  // слайдер преимущества
  $('.b-advantage__items').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      adaptiveHeight: true,
      dots: true,
      responsive: [
      {
          breakpoint: 1300,
          settings: {
              slidesToShow: 4,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 829,
          settings: {
              slidesToShow: 2,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 489,
          settings: {
              slidesToShow: 1,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      }
      ]
  });

//   $('select').styler();


  $('a.flot-list__l').mouseover(function(e){
      var container = $(this).next().html();
      if(container) {
          var pos = mousepos(e);
          $(this).next().css('left', pos[0]);
          $(this).next().css('top', pos[1]);
      }
      $(this).next().addClass('flot-list__imgm_block');
  });
  $('a.flot-list__l').mousemove(function(e){
      var container = $(this).next().html();
      if(container) {
          var pos = mousepos(e);
          $(this).next().css('left', pos[0]);
          $(this).next().css('top', pos[1]);
      }
      $(this).next().addClass('flot-list__imgm_block');
  });
  $('a.flot-list__l').mouseout(function(){
      $(this).next().removeClass('flot-list__imgm_block');
  });


  function mousepos(e){
      var top = e.pageY - $(window).scrollTop() + 30;
      var left = e.pageX;
      var pos = [left, top];
      return pos;
  }
  
  
  
  // слайдеры siriusaerocard
  $(function(){
      let plainSlides = $('.flight-slider__slide');
      plainSlides.hide();
      $('.flight-slider > :first-child').next().show();
  });
  let nextPlane = function(){
    let parent = $(this).parent();
    let current = parent.find('.flight-slider__slide.active');
    let next = current.next();
    let first = parent.find('.flight-slider__slide:first');
    
    current.removeClass('active').fadeOut(); 
    next.hasClass('flight-slider__arrow') ? first.addClass('active').fadeIn() : next.addClass('active').fadeIn();
  }
  let prevPlane = function(){
    let parent = $(this).parent();
    let current = parent.find('.flight-slider__slide.active');
    let prev = current.prev();
    let last = parent.find('.flight-slider__slide:last');

    current.removeClass('active').fadeOut(); ; 
    prev.hasClass('flight-slider__arrow') ? last.addClass('active').fadeIn() : prev.addClass('active').fadeIn();
  }
//   let nextPlane = function(){
//     let parent = $(this).parent();
//     let current = parent.find('.flight-slider__slide.active');
//     let next = current.next();
//     let first = parent.find('.flight-slider__slide:first');
    
//     current.removeClass('active'); 
//     next.hasClass('flight-slider__arrow') ? first.addClass('active') : next.addClass('active');
//   }
//   let prevPlane = function(){
//     let parent = $(this).parent();
//     let prev = parent.find('.flight-slider__slide.active').prev();
//     let last = parent.find('.flight-slider__slide:last');

//     parent.find('.flight-slider__slide.active').removeClass('active'); 
//     prev.hasClass('flight-slider__arrow') ? last.addClass('active') : prev.addClass('active');
//   }
  $('.flight-slider__arrow:last-of-type').on('click', nextPlane);
  $('.flight-slider__arrow:first-of-type').on('click', prevPlane);





























































  
  let cities = {};
  let finishList1 = [];
  let finishList = {};
  let changeCountry = {
      "Российская Федерация": "Россия",
      "Russian Federation": "Russia",
      "Соединенные Штаты Америки": "США",
  };
  
  function createFormList(value, parent) {
      var block = $(parent).find('.modal-city-it');
      $(block).html('');
      var active = 0;
  
      $.each(finishList, function(key, val) {
          if(key.toUpperCase().indexOf(value.toUpperCase()) > -1) {
              var append = '<div class="autocom autocom-city">';
              append += key.toLowerCase().replace(new RegExp("\"", "g"), "").split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ') + ', ';
              append += '<span>' + val.country + '</span>';
              append += '</div>';
              $.each(val.airports, function(keyc, valc) {
                  append += '<div class="autocom autocom-airport">';
                  append += valc[0].charAt(0).toUpperCase() + valc[0].toLowerCase().substr(1) + '<span style="display: none;">, </span><span>' + valc[1] + '</span>';
                  append += '</div>';
              });
              $(block).append(append);
          }
      });
  }
  
  $('#order_from, #order_from2, #order_from3').focus(function(){
      $('.modal-city-to_active').removeClass('modal-city-to_active');
  });
  $('#order_to, #order_to2, #order_to3').focus(function(){
      $('.modal-city-from_active').removeClass('modal-city-from_active');
  });
  $(document).click(function(e){
      if($(e.target).attr("id") == 'order_from') {
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
      else if($(e.target).attr("id") == 'order_to') {
          $('.modal-city-from_active').removeClass('modal-city-from_active');
      }
      else {
          $('.modal-city-from_active').removeClass('modal-city-from_active');
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
  });
  // keyboard events
  $(document).keyup(function(e) {
      if (e.keyCode === 27) { // esc
          $('.modal-city-from_active').removeClass('modal-city-from_active');
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
      if (e.keyCode === 40) { // down arrow
          var block;
          if($('.modal-city-from').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from');
          } else if($('.modal-city-to').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to');
          } else if($('.modal-city-from2').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from2');
          } else if($('.modal-city-to2').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to2');
          } else if($('.modal-city-from3').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from3');
          } else if($('.modal-city-to3').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to3');
          }
          if(block) {
              var active = block.find('.autocom_active');
              var next = active.nextAll('.autocom:first');
              if(next.length == 0) {
                  next = block.find('.autocom:first');
              }
              active.removeClass('autocom_active');
              next.addClass('autocom_active');
          }
      }
      if (e.keyCode === 38) { // up arrow
          var block;
          if($('.modal-city-from').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from');
          } else if($('.modal-city-to').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to');
          } else if($('.modal-city-from2').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from2');
          } else if($('.modal-city-to2').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to2');
          } else if($('.modal-city-from3').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from3');
          } else if($('.modal-city-to3').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to3');
          }
          if(block) {
              var active = block.find('.autocom_active');
              var prev = active.prevAll('.autocom:first');
              if(prev.length == 0) {
                  prev = block.find('.autocom:last');
              }
              active.removeClass('autocom_active');
              prev.addClass('autocom_active');
          }
      }
      if (e.keyCode === 13) { // enter
          if($('.modal-city-from').hasClass('modal-city-from_active')){
              $('#order_from').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to').hasClass('modal-city-to_active')){
              $('#order_to').val($('.modal-city-to_active .autocom_active').text());
          }
          if($('.modal-city-from2').hasClass('modal-city-from_active')){
              $('#order_from2').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to2').hasClass('modal-city-to_active')){
              $('#order_to2').val($('.modal-city-to_active .autocom_active').text());
          }
          if($('.modal-city-from3').hasClass('modal-city-from_active')){
              $('#order_from3').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to3').hasClass('modal-city-to_active')){
              $('#order_to3').val($('.modal-city-to_active .autocom_active').text());
          }
          $('.modal-city-from_active').removeClass('modal-city-from_active');
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
  });
  
  $('body').on('click', '.modal-city-from .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from').val(air);
  });
  $('body').on('click', '.modal-city-to .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to').val(air);
  });
  $('body').on('click', '.modal-city-from2 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from2').val(air);
  });
  $('body').on('click', '.modal-city-to2 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to2').val(air);
  });
  $('body').on('click', '.modal-city-from3 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from3').val(air);
  });
  $('body').on('click', '.modal-city-to3 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to3').val(air);
  });
  
  
  
  
  
  
  
  
  $(document).on("input", "#order_from", function(ev){
      $(".modal-city-from").html('');
      $(".modal-city-from").addClass('modal-city-from_active');
      $("#order_from").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  $(document).on("input", "#order_to", function(ev){
      $(".modal-city-to").html('');
      $(".modal-city-to").addClass('modal-city-to_active');
      $("#order_to").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  $(document).on("input", "#order_from2", function(ev){
      $(".modal-city-from2").html('');
      $(".modal-city-from2").addClass('modal-city-from_active');
      $("#order_from2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  $(document).on("input", "#order_to2", function(ev){
      $(".modal-city-to2").html('');
      $(".modal-city-to2").addClass('modal-city-to_active');
      $("#order_to2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  $(document).on("input", "#order_from3", function(ev){
      $(".modal-city-from3").html('');
      $(".modal-city-from3").addClass('modal-city-from_active');
      $("#order_from3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  $(document).on("input", "#order_to3", function(ev){
      $(".modal-city-to3").html('');
      $(".modal-city-to3").addClass('modal-city-to_active');
      $("#order_to3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent());
      }
  });
  
  
  
  
  
  
  
  
  
  
  
$( function() {

    let airportsList = [];
    let citiesList = {};
    let countryList = {};

    $.getJSON("https://www.rusline.aero/connectsirena/files/airports.json", function( data ) {
        $.each(data, function(key, val) {
            airportsList.push(val);
        });


        $.getJSON("https://www.rusline.aero/connectsirena/files/city.json", function(city) {
            $.each(city, function(key, val) {
                citiesList['"'+key+'"'] = val;
            });

            $.getJSON("https://www.rusline.aero/connectsirena/files/country.json", function(country) {
                $.each(country, function(key, val) {
                    countryList[key] = val;
                });


                $.each(airportsList, function(key, val) {
                    if(!cities['"'+val.city[0]+'"']) {
                        cities['"'+val.city[0]+'"'] = [];
                    }
                    let arr = [val.code[0], val.name[0]];
                    cities['"'+val.city[0]+'"'].push(arr);
                });

                $.each(cities, function(key, val) {
                    var cityCode = key;
                    if(citiesList[cityCode]) {
                        var cit = citiesList[cityCode].name[0];
                        var cou = countryList[citiesList[cityCode].country[0]].toLowerCase();
                        cou = cou.split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ');
                        if(changeCountry[cou]) {
                            cou = changeCountry[cou];
                        }
                        finishList1.push(cit + ", " + cou);
                        finishList['"'+cit+'"'] = [];
                        finishList['"'+cit+'"']["country"] = cou;
                        finishList['"'+cit+'"']["airports"] = Array();
                        
                        $.each(val, function(keyi, vali) {
                            finishList1.push(vali[1] + ", " + vali[0]);
                            let arrs = [vali[1], vali[0]];
                            finishList['"'+cit+'"']["airports"].push(arrs);
                        });
                    }
                });
                $.each(citiesList, function(key, val) {
                    if(!finishList['"'+val.name[0]+'"']) {
                        finishList['"'+val.name[0]+'"'] = [];
                        let countr = countryList[val.country[0]].toLowerCase().split(/\s+/).map(word => word[0].toUpperCase() + word.substring(1)).join(' ');
                        if(changeCountry[countr]) {
                            countr = changeCountry[countr];
                        }
                        finishList['"'+val.name[0]+'"']["country"] = countr;
                        finishList['"'+val.name[0]+'"']["airports"] = Array();
                    }
                });
            });
        });
    });

// $( "#order_from" ).autocomplete({source: finishList});
// $( "#order_to" ).autocomplete({source: finishList});
} );




$('#formMainOrder form').submit(function(e){
    e.preventDefault();
    var email = $('#formMainOrder form input[name="email"]');

    email.removeClass('it_error');

    if(email.val() == '') {
        email.addClass('it_error');
        return false;
    } else {
        var preferences = $('#formMainOrderPreferences textarea[name="preferences"]').val();
        var name = $('#formMainOrder form input[name="name"]').val();
        var phone = $('#formMainOrder form input[name="phone"]').val();
        email = email.val();

        var from1 = $('.main-order-block input#order_from').val();
        var from2 = $('.main-order-block input#order_from2').val();
        var from3 = $('.main-order-block input#order_from3').val();

        var to1 = $('.main-order-block input#order_to').val();
        var to2 = $('.main-order-block input#order_to2').val();
        var to3 = $('.main-order-block input#order_to3').val();

        var date1 = $('.main-order-block input#order_attrebute_date_0').val();
        var date2 = $('.main-order-block input#order_attrebute_date_2').val();
        var date3 = $('.main-order-block input#order_attrebute_date_3').val();

        var pass1 = $('.main-order-block input#add_pass').val();
        var pass2 = $('.main-order-block input#add_pass2').val();
        var pass3 = $('.main-order-block input#add_pass3').val();

        var send_data = "preferences="+preferences+"&name="+name+"&phone="+phone+"&email="+email+"&from1="+from1+"&from2="+from2+"&from3="+from3+"&to1="+to1+"&to2="+to2+"&to3="+to3+"&date1="+date1+"&date2="+date2+"&date3="+date3+"&pass1="+pass1+"&pass2="+pass2+"&pass3="+pass3;

        console.log(send_data);

        $.ajax({
            type: "POST",
            data: send_data,
            dataType: 'html',
            url: '/webforms/ajax_order_main/',
            beforeSend: function() {

            },
            success: function(data) {
                var data = JSON.parse(data);
				if(data.success == 'succes-send_message'){
					$.fancybox.open(
						'<div style="text-align: center;"><h1>Спасибо</h1><p>Ваша заявка принята.<br>В ближайшее время мы свяжемся с Вами.</p></div>'
					);
				}
            }
        });
    }
});

$('body').on('click', '#prefencesSubmit', function(e){
    e.preventDefault();
    $('#formMainOrderPreferences').removeClass('form-orderMtl_active');
})



});
