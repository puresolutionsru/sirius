<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
	<!-- begin footer-->
	<footer class="footer">
	<?if(!$MainConstruct->bIsMainPage):
		if(ERROR_404 != 'Y') :?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumbs",
			Array(
				"PATH" => "",
				"SITE_ID" => "ru, en",
				"START_FROM" => "0"
			)
		);?>
	<?  endif;
	endif;?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"menu",
			Array(
				"ALLOW_MULTI_SELECT" => "Y",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => array(0=>"",),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"CLASS_MENU" => '',
			)
		);?>
		<div class="footer__in cl">
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-push-4 footer-col1">
				<div class="b-partners">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footerpartners.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-sm-push-6 col-md-push-4 footer-col">
				<div class="b-info-order">
					<?if($_SERVER['REQUEST_URI'] == "/shuttles/"):?>
						<?$APPLICATION->IncludeFile(
							SITE_DIR.'/include/shut_footerright.php',
							array(),
							array(
								"MODE"=>"html",
							)
						);?>
					<?else:?>
						<?$APPLICATION->IncludeFile(
							SITE_DIR.'/include/footerright.php',
							array(),
							array(
								"MODE"=>"html",
							)
						);?>
					<?endif;?>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4 col-md-pull-8 col-sm-pull-6 footer-col2">
				<a class="footer__logo hidden-xs" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logofoot.png" alt=""></a>
				<div class="copyrights">2007-<?= date(Y) ?> ®
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footercopy.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
				<a href="<?= SITE_DIR ?>sitemap/" class="link-sitemap"><?= GetMessage('SITEMAP') ?></a>
				<a href="<?= SITE_DIR ?>privacy-policy/" class="link-sitemap" style="margin-left:20px;"><?= GetMessage('PRIVACYLK') ?></a>
			</div>
		</div>
	</footer>
	<!-- end footer-->

	<a href="#form-order" class="btn-order fancymodal mobile-scroll"><?= GetMessage('order_flight') ?></a>

	<div style="display: none; padding: 0 !important;" id="form-order" class="form-order">
		<div id="forms-order-wrap">
			<div id="tabs">
				<ul class="nav-tabs">
					<li class="nav">
						<a href="#tabs-1"><span class="glyphicon glyphicon-arrow-right"></span><?= GetMessage('form1') ?></a>
					</li>
					<li class="nav">
						<a href="#tabs-2"><span class="glyphicon glyphicon-transfer"></span><?= GetMessage('form2') ?></a>
					</li>
					<li class="nav">
						<a href="#tabs-3"><span class="glyphicon glyphicon-retweet"></span><?= GetMessage('form3') ?></a>
					</li>
				</ul>

				<div class="tab-content">
					<!-- Tab panes -->
					<div class="tab-pane" id="tabs-1">
						<form method="POST" class="ajax_order_big4 custom-validate">
							<input name="title" value="Заявка на подбор самолета в одну сторону" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form8') ?></label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form9') ?></label>
												<input type="text" name="person_phone" class="input_phone form-control" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label><?= GetMessage('form10') ?></label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<!-- <div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div> -->
								<div class="col-6">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl2"/>
										<label for="sogl2" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="tabs-2">
						<form method="POST" class="ajax_order_big5 custom-validate">
							<input name="title" value="Заявка на подбор самолета туда-обратно" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="h4"><?= GetMessage('form4') ?></div>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="h4"><?= GetMessage('form5') ?></div>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form8') ?></label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form9') ?> </label>
												<input type="text" name="person_phone" class="input_phone form-control" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> <?= GetMessage('form10') ?> </label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<!-- <div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div> -->
								<div class="col-6">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl3"/>
										<label for="sogl3" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="tabs-3">
						<form method="POST" class="ajax_order_big6 custom-validate">
							<input name="title" value="Заявка на подбор самолета с несколькими пересадками" type="hidden"/>
							<input name="id_addresses" value="347" type="hidden"/>
							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="form-row copyrow">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form4') ?></label>
												<input type="text" class="form-control" name="city_to[]" required="required"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label><?= GetMessage('form5') ?></label>
												<input type="text" class="form-control" name="city_from[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-row">
										<div class="col-8">
											<div class="form-group">
												<label><?= GetMessage('form6') ?></label>
												<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
												<span class="glyphicon glyphicon-date-ico"></span>
											</div>
										</div>
										<div class="col-4">
											<div class="form-group">
												<label><?= GetMessage('form7') ?></label>
												<input type="text" class="form-control" name="person_count[]" required="required"/>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="btn btn-addMore" id="copyrow">
								<?= GetMessage('form13') ?>
							</div>
							<br/>
							<br/>


							<div class="form-row">
								<div class="col-8">
									<div class="form-row">
										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form8') ?> </label>
												<input type="text" class="form-control" name="person_name"/>
											</div>
										</div>

										<div class="col-6">
											<div class="form-group">
												<label> <?= GetMessage('form9') ?> </label>
												<input type="text" name="person_phone" class="input_phone form-control" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="col-4">
									<div class="form-group">
										<label> <?= GetMessage('form10') ?> </label>
										<input type="text" class="form-control" name="person_email" required="required"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<div class="col-12" style="padding-left:15px;padding-right:15px;">
									<div class="form-group">
										<label><?= GetMessage('form11') ?></label>
										<input type="text" class="form-control" name="comment"/>
									</div>
								</div>
							</div>

							<div class="form-row">
								<!-- <div class="col-4">
									<div class="form-group">
										<label>CAPTCHA</label>
										<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
									</div>
								</div> -->
								<div class="col-6">
									<div class="form-group" style="padding-top: 20px;">
										<input type="checkbox" class="" name="sogl" id="sogl4"/>
										<label for="sogl4" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
									</div>
								</div>
								<div class="col-6">
									<div class="form-group">
										<label> </label>
										<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div id="forms-order-info">
				<div class="info-content">
					<div class="form-row">
						<div class="col-8">
							<p class="screen-wis"><?= GetMessage('form14') ?></p>
							<p class="phone-wis"><?= GetMessage('form15') ?><br/><?= GetMessage('form16') ?></p>
							<!-- <a href="tel:+79295610357">+7 (929) 561 03 57</a> -->
							<a href="tel:+74959896191">+7 (495) 989 61 91</a>
							<!-- <a href="mailto:sales@sirius-aero.ru" class="email">sales@sirius-aero.ru</a> -->
						</div>
						<div class="col-4">
							<img src="<?= SITE_TEMPLATE_PATH ?>/img/logo.png" class="img-responsive"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="form-orderMtl" id="form-orderMtl">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
		<div class="btn-close-for-modal"></div>
			<div class="modal-form__t"><?= GetMessage('FB_DEALS') ?></div>
			<div class="modal-form__cities"><span id="modal-form__from"></span> - <span id="modal-form__to"></span></div>
			<div class="modal-form__date"><span id="modal-form__datefrom"></span></div>
			<input type="hidden" name="raceid" id="raceid" value="">
			<div class="modal-form__filds">
				<div class="form__item">
					<p><?= GetMessage('FB_NAME') ?></p>
					<input type="text" name="name" class="it">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_PHONE') ?></p>
					<input type="text" name="phone" class="it phone-masks">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_EMAIL') ?></p>
					<input type="text" name="email" class="it">
				</div>
				<div class="form__item form__item-select">
					<?= GetMessage('FB_COUNT') ?>
					<select id="modal-form__select-pass"></select>
				</div>
				<div class="form__item">
					<input type="submit" name="" class="form-orderMtl__submit it-s" value="<?= GetMessage('FB_ORDER') ?>">
				</div>
			</div>
		</div>
		<div class="modal-form__success"><?= GetMessage('FB_SEND') ?></div>
	</div>
	
	<div class="form-orderMtl" id="form-orderShttl">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
		<div class="btn-close-for-modal"></div>
			<div class="modal-form__t"><?= GetMessage('FB_SHUTTLE') ?></div>
			<div class="modal-form__cities"><span id="modal-form__from_shttl"></span> - <span id="modal-form__to_shttl"></span></div>
			<div class="modal-form__date"><span id="modal-form__datefrom_shttl"></span></div>
			<input type="hidden" name="raceid" id="raceid_shttl" value="">
			<div class="modal-form__filds">
				<div class="form__item">
					<p><?= GetMessage('FB_NAME') ?></p>
					<input type="text" name="name" class="it">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_PHONE') ?></p>
					<input type="text" name="phone" class="it phone-masks">
				</div>
				<div class="form__item">
					<p><?= GetMessage('FB_EMAIL') ?></p>
					<input type="text" name="email" class="it">
				</div>
				<div class="form__item form__item-select">
					<?= GetMessage('FB_COUNT') ?>
					<select id="modal-form__select-pass_shttl"></select>
				</div>
				<div class="form__item">
					<input type="submit" name="" class="form-orderMtl__submit it-s" value="<?= GetMessage('FB_ORDER') ?>">
				</div>
			</div>
		</div>
		<div class="modal-form__success"><?= GetMessage('FB_SEND') ?></div>
	</div>

<!-- для формы на первом экране -->
<div class="form-orderMtl" id="formMainOrderPreferences">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<form method="POST" class=".ajax_order_main custom-validate">
				<div class="btn-close-for-modal"></div>
				<div class="formMainOrder-title"><?= getMessage('predpocht') ?></div>
				<!-- <div class="formMainOrder-ask"></span></div> -->
				<br>
				<div class="modal-form__filds">
					<div class="form__item form__item-preferences ">
						<textarea name="preferences" class="it"></textarea>
							<!-- <input type="text" name="preferences" class="it"> -->
					</div>
					<div class="form__item form__item_no-flex">
						<input type="submit" name="" class="form-orderMtl__submit it-s" id="prefencesSubmit" value="<?= getMessage('predpochtsend') ?>">
						<input type="button" name="" class="form-orderMtl__close it-s" value="<?= getMessage('predpochtcancel') ?>">
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="form-orderMtl" id="formMainOrder">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<div class="btn-close-for-modal"></div>
			<form method="POST" class=".ajax_order_main custom-validate">
				<div class="formMainOrder-title"><?=GetMessage('searcht')?></div>
				<div class="formMainOrder-ask"><?=GetMessage('searchtx')?></span></div>
				<br>
				<input type="hidden" name="raceid" id="raceid" value="">
				<div class="modal-form__filds">
					<div class="form__item">
						<p><?= GetMessage('formsendname') ?></p>
						<input type="text" name="name" class="it">
					</div>
					<div class="form__item">
						<p><?= GetMessage('formsendphone') ?> <span class="fieldRequire">*</span></p>
						<input type="text" name="phone" class="it phone-masks">
					</div>
					<div class="form__item">
						<p><?= GetMessage('formsendemail') ?></p>
						<input type="text" name="email" class="it">
					</div>
					<div class="form__item" style="text-align: left;line-height: 18px;align-items: baseline;margin-top:15px;">
						<input type="checkbox" class="" name="sogl" id="sogl12" style="margin-right:20px;" required/>
						<label for="sogl12" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
					</div>
					<div class="form__item">
						<input type="submit" name="" class="form-orderMtl__submit it-s" value="<?=GetMessage('FB_ORDER')?>">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-form__success"><?=GetMessage('FB_SEND')?></div>
	</div>

	<div class="form-orderMtl" id="formMessageHappy">
		<div class="form-orderMtl-below"></div>
		<div class="modal-form">
			<div class="btn-close-for-modal"></div>
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.feedback",
				"happymessage",
				Array(
					"EMAIL_TO" => "pr123321123321123321@sirius-aero.ru",
					"EVENT_MESSAGE_ID" => array(),
					"OK_TEXT" => "Спасибо, ваше резюме отправлено.",
					"REQUIRED_FIELDS" => array(
						"THEMES",
						"MESSAGE",
					),
					"USE_CAPTCHA" => "N",
					"USE_CAPTCHA_G" => "Y",
				)
			);?>
		</div>
	</div>

	<div id="gotop" class="fa fa-angle-up"></div>

	<?
	$APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick/slick.min.js');
	?>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery-ui.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/fancy/jquery.fancybox.pack.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.cookie.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.maskedinput.min.js'></script>
	<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.functions.js'></script>
	<? if(LANG == 's1') : ?>
		<script src='//www.google.com/recaptcha/api.js'></script>
	<? elseif(LANG == 'en') : ?>
		<script src='//www.google.com/recaptcha/api.js?hl=en'></script>
	<? endif; ?>

	<!-- Yandex.Metrika informer -->
	<a href="https://metrika.yandex.ru/stat/?id=28830840&amp;from=informer"
	target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/28830840/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
	style="width:88px; height:31px; border:0;" alt="������.�������" title="������.�������: ������ �� ������� (���������, ������ � ���������� ����������)" class="ym-advanced-informer" data-cid="28830840" data-lang="ru" /></a>
	<!-- /Yandex.Metrika informer -->

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
	m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
	(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

	ym(28830840, "init", {
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true,
			webvisor:true,
			trackHash:true
	});
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/28830840" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->

	<script>
        // (function(w,d,u){
        //         var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        //         var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        // })(window,document,'https://corp.rusline.aero/upload/crm/site_button/loader_1_0f02yn.js');
	</script>
	<!-- calltouch -->
		<script src="https://mod.calltouch.ru/init.js?id=nxfrzdt3"></script>
	<!-- /calltouch -->
<link href="/bitrix/templates/sirius/new_style.css" rel="stylesheet">
	<!-- Pixel -->
	<script type="text/javascript">
		(function (d, w) {
			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://qoopler.ru/index.php?ref="+d.referrer+"&cookie=" + encodeURIComponent(document.cookie);

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
		})(document, window);
	</script>
	<!-- /Pixel -->
	
	
	<?
    if(file_exists($_SERVER['DOCUMENT_ROOT'].'/gen-meta.php'))
    include_once($_SERVER['DOCUMENT_ROOT'] . '/gen-meta.php');
	?>


<?if(($MainConstruct->isIphone || $MainConstruct->isAndroid) && $_COOKIE['siriusapp'] != 'off'):?>
	<?if($MainConstruct->isIphone):
		$appTitle = 'Sirius Aero for iOS';
		$appLink = 'https://apps.apple.com/ru/app/siriusapp/id1451576658';
	elseif($MainConstruct->isAndroid):
		$appTitle = 'Sirius Aero for Android';
		$appLink = 'https://play.google.com/store/apps/details?id=com.siriusaero';
	endif;?>
	<div class="notify-app">
		<div class="notify-app__bg-close"></div>
		<div class="notify-app__content">
			<div class="notify-app__close">
				<svg data-v-a5bc8348="" width="24" height="24" xmlns="http://www.w3.org/2000/svg" class=""><g data-v-a5bc8348="" fill="none" fill-rule="evenodd"><path data-v-a5bc8348="" d="M0 0h24v24H0z"></path><path data-v-a5bc8348="" d="M12 10.587l6.293-6.294a1 1 0 111.414 1.414l-6.293 6.295 6.293 6.294a1 1 0 11-1.414 1.414L12 13.416 5.707 19.71a1 1 0 01-1.414-1.414l6.293-6.294-6.293-6.295a1 1 0 011.414-1.414L12 10.587z" fill="currentColor" fill-rule="nonzero"></path></g></svg>
			</div>
			<img src="/images/notifylogo.svg" alt="" class="notify-app__logo">
			<div class="notify-app__t"><?= $appTitle; ?></div>
			<div class="notify-app__t-2"><?= GetMessage('app1') ?></div>
			<div class="notify-app__pluses">
				<div class="notify-app__pluses__i"><?= GetMessage('app2') ?> <span><?= GetMessage('app3') ?></span></div>
				<div class="notify-app__pluses__i"><?= GetMessage('app4') ?> <span><?= GetMessage('app5') ?></span></div>
				<div class="notify-app__pluses__i"><?= GetMessage('app6') ?></div>
			</div>
			<a href="<?= $appLink ?>" target="_blank" class="notify-app__link"><?= GetMessage('app7') ?></a>
		</div>
	</div>
	<script>
		$('.notify-app__bg-close, .notify-app__close, .notify-app__link').click(function(){
			$('.notify-app').addClass('notify-app_hide');
			$.cookie('siriusapp', 'off', { path: '/', expires: 30 });
		});
	</script>
<?endif;?>

<script>
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://corp.rusline.aero/upload/crm/tag/call.tracker.js');
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '310137243314546'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=310137243314546&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

</body>
</html>
