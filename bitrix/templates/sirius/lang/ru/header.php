<?
$MESS['ZAKAZ'] = 'Заказ рейса';
$MESS ['FLEET'] = "Флот Sirius Aero";
$MESS ['FLEETUPR'] = "Флот в управлении";
$MESS ['FLEET_PARTNERS'] = "Флот партнеров";
$MESS ['PRIVACY'] = "<div class='bold t-3'>Cookie Notice</div><div>Мы используем файлы cookies для корректной работы сайта.</div><div>Оставаясь на этом сайте, вы соглашаетесь с <a class='b-cookies-link' target='_blank' href='/privacy-policy/'>условиями использования</a> файлов cookies</div>";
$MESS ['PRIVACY_BUT'] = 'Я согласен';

// форма поиска
$MESS ['textfrom'] = 'Ваш личный ассистент в мире деловой авиации 24/7';
$MESS ['from'] = 'Откуда';
$MESS ['to'] = 'Куда';
$MESS ['passengers'] = 'Пассажиры';
$MESS ['date'] = 'Дата';
$MESS ['comebackfly'] = 'Обратный перелет';
$MESS ['predpocht'] = 'Предпочтения';
$MESS ['transfer'] = 'Добавить пересадку';
$MESS ['next'] = 'Далее';
?>
