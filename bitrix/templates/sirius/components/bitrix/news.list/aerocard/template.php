<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$count = 0;
if($arParams["INFO_FLOAT"] == "LEFT") {
	$position = 'm-size-jet';
} else {
	$position = 'h-size-jet';
}
?>
<div class="<?= $position ?> wow fadeIn">
				<? if($arParams["TITLE_H2"]) : ?>
					<h2><?= $arParams["TITLE_H2"] ?></h2>
				<? endif; ?>
        <div class="flight-slider">
            <div class="flight-slider__arrow wow fadeInLeft" data-wow-delay="0.5s" ></div>

						<?foreach($arResult["ITEMS"] as $arItem):?>
							<?
							$photogallery = CFile::GetPath($arItem['PROPERTIES']['GALLERY']['VALUE'][0]);
							$planephoto = CFile::GetPath($arItem['PROPERTIES']['PIC_AEROCARD']['VALUE']);
							$res = CIBlockSection::GetByID($arItem["IBLOCK_SECTION_ID"]);
							if($ar_res = $res->GetNext()) {
								$namePar = $ar_res['NAME'];
							}
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
							?>
							<div class="flight-slider__slide <?if($count==0) {echo 'active'; $count++;}?>">
									<div class="flight-slider__small-part">
											<div class="flight-slider__small-img" style="background: url('<?= $planephoto ?>') no-repeat; background-size: cover;"></div>
											<div class="flight-slider__parameters">
													<div class="sirius1 f-sirius">Вместимость <span><?= $arItem['PROPERTIES']['VMEST']['VALUE'] ?> пасс.</span></div>
													<div class="sirius2 f-sirius">Дальность перелета <span><?= $arItem['PROPERTIES']['DAL']['VALUE'] ?> км</span></div>
													<div class="sirius3 f-sirius">Объем багажного отделния <span><?= $arItem['PROPERTIES']['OB']['VALUE'] ?> м3</span></div>
													<div class="sirius4 f-sirius">Длина салона <span><?= $arItem['PROPERTIES']['DLINA']['VALUE'] ?> м</span></div>
													<div class="sirius5 f-sirius">Ширина салона <span><?= $arItem['PROPERTIES']['SHIRINA']['VALUE'] ?> м</span></div>
													<div class="sirius6 f-sirius">Высота салона <span><?= $arItem['PROPERTIES']['VYSOTA']['VALUE'] ?> м</span></div>
											</div>
									</div>
									<div class="flight-slider__big-img" style="background: url('<?= $photogallery ?>') no-repeat; background-size: cover;">
											<a class="flight-slider__planes-text" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
													<div class="flight-slider__planes-name"><?= $namePar ?> <?= $arItem["NAME"] ?></div>
													<div class="flight-slider__planes-type">тип <?= $arItem['PROPERTIES']['TYPE_PLANE']['VALUE'] ?></div>
											</a>
									</div>
							</div>
						<? endforeach; ?>

            <div class="flight-slider__arrow wow fadeInRight" data-wow-delay="0.5s"></div>
        </div>
</div>
