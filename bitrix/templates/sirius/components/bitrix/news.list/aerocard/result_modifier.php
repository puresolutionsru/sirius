<?
$parent = $arParams["SUB_CATEGORY"];
$newElements = array();

foreach($arResult["ITEMS"] as $arItem) :
	$nav = CIBlockSection::GetNavChain(false, $arItem['IBLOCK_SECTION_ID'], Array("ID", "IBLOCK_ID", "SECTION_ID", "NAME", "UF_PINCLUDE"));
	while ($sub = $nav->GetNext()){
		if($sub['ID'] == $parent) {
			$newElements[] = $arItem;
		} 
	}
endforeach;

$arResult["ITEMS"] = $newElements;

?>