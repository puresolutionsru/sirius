<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"DISPLAY_DATE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_DATE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_NAME" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_NAME"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PICTURE" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_PICTURE"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"DISPLAY_PREVIEW_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_DESC_NEWS_TEXT"),
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"BUTTON_TEXT" => Array(
		"NAME" => GetMessage("T_IBLOCK_BUTTON"),
		"TYPE" => "TEXT",
		"DEFAULT" => "Button",
	),
	"TEXT_PHONE" => Array(
		"NAME" => GetMessage("T_IBLOCK_TX_PHONE"),
		"TYPE" => "TEXT",
		"DEFAULT" => "Text phone",
	),
	"PHONE" => Array(
		"NAME" => GetMessage("T_IBLOCK_PHONE"),
		"TYPE" => "TEXT",
		"DEFAULT" => "8900",
	),
);
?>
