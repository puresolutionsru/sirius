<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$pageurl = "";
if((stripos($_SERVER["REQUEST_URI"], "/sitenews/") !== FALSE || stripos($_SERVER["REQUEST_URI"], "/smi/") !== FALSE) && ($_SERVER["REQUEST_URI"] != "/sitenews/" && $_SERVER["REQUEST_URI"] != "/en/sitenews/" && $_SERVER["REQUEST_URI"] != "/about/smi/" && $_SERVER["REQUEST_URI"] != "/en/about/smi/")) {
	
} else {
	if(LANGUAGE_ID == 'ru'):
		$pageurl = $_SERVER["REQUEST_URI"];
	else:
		$pageurl = str_replace("/en/", "", $_SERVER["REQUEST_URI"]);
	endif;
}

if(LANGUAGE_ID == 'ru' && !$pageurl):
	$pageurl = "/";
endif;
?>

<div class="lang-tabs">
<?php if(stripos($_SERVER['REQUEST_URI'], '/en/') !== false) {?>
	<div class="lang-tabs__i"><span class="lang-tabs__link _active">EN</span></div>
	<div class="lang-tabs__i">
		<a class="lang-tabs__link" href="/<?=$pageurl?>">RU</a>
	</div>
<?} else {?>
	<div class="lang-tabs__i">
		<a class="lang-tabs__link" href="/en<?=$pageurl?>">EN</a>
	</div>
	<div class="lang-tabs__i"><span class="lang-tabs__link _active">RU</span></div>
<?}?>
</div>
