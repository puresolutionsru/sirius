<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

	<menu class="b-menu <?= $arParams['CLASS_MENU'] ?>">

	<?
	$emptylegs = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/crm/count.txt');
	$previousLevel = 0;
	foreach($arResult as $arItem):
	?>
		<?if($arItem["LINK"]=="/about/"){
			$arItem["LINK"] = "/about/company/";
		}?>
		<?if($arItem["LINK"]=="/en/about/"){
			$arItem["LINK"] = "/en/about/company/";
		}?>
		<?if($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
			<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
		<?endif?>

		<?if($arItem["IS_PARENT"]):?>
				<li<?if ($arItem["SELECTED"]):?> class="_active"<?endif?><?if($arItem["CHILD_SELECTED"] !== true):?> class="menu-close"<?endif?>>
					<a href="<?=$arItem["LINK"]?>" <?if($arItem['PARAMS']['class']){echo 'class="'.$arItem['PARAMS']['class'].'"';}?>><span><?=$arItem["TEXT"]?></span></a>
					<ul>
		<?else:?>
			<?if($arItem["PERMISSION"] > "D"):?>
				<li<?if ($arItem["SELECTED"]):?> class="_active"<?endif?> <?if($arItem["LINK"]=='/emptylegs/' || $arItem["LINK"]=='/en/emptylegs/'): echo "data-count='".$emptylegs."'"; endif;?>>
					<a href="<?=$arItem["LINK"]?>" <?if($arItem['PARAMS']['class']){echo 'class="'.$arItem['PARAMS']['class'].'"';}?>><span><?=$arItem["TEXT"]?></span></a>
				</li>
			<?endif?>
		<?endif?>

		<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

	<?endforeach?>

	<?if ($previousLevel > 1)://close last item tags?>
		<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
	<?endif?>

	</menu>
<?endif?>
