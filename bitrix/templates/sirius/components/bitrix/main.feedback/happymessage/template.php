<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mform">

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="form-send-happy">
	<?=bitrix_sessid_post()?>
	<h3>Сообщение</h3>
	<div class="mf-item mf-vacancy">
		<input type="text" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("THEMES", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="theme" value="<?=$arResult["VACANCY"]?>" placeholder="<?=GetMessage("MFT_THEMES")?>">
	</div>
	<div class="mf-item mf-message">
		<textarea name="message" rows="20" cols="40" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea>
	</div>
	
	
	
	
	
	<div class="mf-item mf-captcha">
		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
			<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
			<input type="text" name="captcha_word" size="30" maxlength="50" value="">
		<?endif;?>

		<?if($arParams["USE_CAPTCHA_G"] == "Y"):?>
			<div class="mf-item g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq" style="display: flex; justify-content: center;"></div>
		<?endif;?>

		<div class="mf-item" style="text-align: center;">
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
			<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
		</div>
	</div>
</form>
</div>

<script>
	$(".form-send-happy").submit(function(e){
		e.preventDefault();
		var error = false;
		$('.form-send-happy .it__error').removeClass('it__error');
		$('.form-send-happy .required').each(function(){
			if($(this).val() == '') {
				error = true;
				$(this).addClass('it__error');
			}
		});
		if(error) {
			return false;
		}

			var formData = $(".form-send-happy").serialize();
			$.ajax({
				type: "POST",
				data: formData,
				dataType: "html",
				url: '/webforms/happymessage/',
				beforeSend: function() {

				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success == 'succes-send_message'){
						$.fancybox.open(
							'<div style="text-align: center;"><p>Ваше сообщение отправлено.</p></div>'
						);
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
							
				}
			});
	});
</script>