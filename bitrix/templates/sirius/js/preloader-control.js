$(document).ready(function () {
    $(".dws-progress-bar").circularProgress({
        color: "#1a325a",
        line_width: 1,
        height: "200px",
        width: "200px",
        percent: 0,
        counter_clockwise: false,
        starting_position: 25
    }).circularProgress('animate', 100, 2000);
});  
$(window).on('load', function () {
    var $preloader = $('#preloader');
    $preloader.delay(1800).fadeOut('slow');
});    