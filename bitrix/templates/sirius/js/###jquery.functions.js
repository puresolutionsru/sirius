$( document ).ready( function() {

  $('.fancybox').fancybox();
  $('a[href="#form-order"]').fancybox();

  $('.form-close').click(function(){
      $(this).parent().addClass('form__hide');
  });


  $('.language_2').change(function(){
      var uri = $('.language_2 option:selected').val();
      window.location.href = uri;
  });

    // FORM
    $("#tabs").tabs();
    $('input').on("keydown keypress", function(e){
		var e = e || event, k = e.which || e.button;
		if(e.ctrlKey && k == 86) return false;
	});
    $('.input_phone').on("change keyup input click", function() {
	    if (this.value.match(/[^0-9\s\-\(\)\+]/g)) {
	        this.value = this.value.replace(/[^0-9\s\-\(\)\+]/g, '');
	    }
	});
    $('.form-control[name="person_name"]').on("change keyup input click", function() {
	    if (this.value.match(/[^а-яА-Яa-zA-Z\s]/g)) {
	        this.value = this.value.replace(/[^а-яА-Яa-zA-Z\s]/g, '');
	    }
	});
    $(".datepicker").datepicker({
		monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		firstDay: 1,
		dateFormat: "dd/mm/yy",
		beforeShow: function(input, inst) {
	      inst.dpDiv
	        .removeClass('custom')
	        .addClass($(input).data('dp-class'));
	    }
	});
    $('.glyphicon-date-ico').on('click', function(){
		$(this).siblings(".datepicker").datepicker("show");
	});
    $('#copyrow').on('click',function(){
		var copy = $('.copyrow:last').clone(false).insertAfter('.copyrow:last');

		$('body').find('.datepicker').removeClass("hasDatepicker").attr('id','').datepicker('destroy');

		var i=0;
		$('body').find('.datepicker').each(function (i,el) {
			$(el).attr("id",'date' + i).datepicker(
				{
					monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					firstDay: 1,
					dateFormat: "dd/mm/yy",
				}
			);
			i++;
		});
	})
    $('#tabs-2 [name*="city_to"]').eq(0).on('change',function(){
		console.log(123);
		$('#tabs-2 [name*="city_from"]').eq(1).val($(this).val())
	});
    $('#tabs-2 [name*="city_from"]').eq(0).on('change',function(){
		$('#tabs-2 [name*="city_to"]').eq(1).val($(this).val())
	});
    $("form").submit(function(e) {

	    var ref = $(this).find("[required]");

	    $(ref).each(function(){
	        if ( $(this).val() == '' )
	        {
	            alert("Пожалуйста, заполните корректно поля формы");

	            $(this).focus();

	            e.preventDefault();
	            return false;
	        }
	    });  return true;
	});
    $(document).on('submit', '.ajax_order_big1, .ajax_order_big2, .ajax_order_big3', function(e){
		e.preventDefault();
		var form = $(this);
		var send_data = form.serializeArray();

		$.ajax({
			type: "POST",
			data: send_data,
			dataType: 'json',
			url: '/webforms/ajax_order_big/',
			beforeSend: function() {

			},
			success: function(data) {
				console.log(data.success);
				if(data.success){
					$.fancybox.close();
					$.fancybox.open(
						'<div style="text-align: center;"><h1>Спасибо</h1><p>Ваша заявка принята.<br>В ближайшее время мы свяжемся с Вами.</p></div>'
					);
				}
			}
		});
	});

  $(window).on('resize', function() {
    // setHeight();
    setHeightNews();
    hamh();
  });


  var menum = $('.b-menu._mod4');
  $('.the-content.main-w > .t-1').after(menum);

  var mmenu = $('.b-menu._mod4 li._active ul');
  if(mmenu[0]) {
      $('.the-content.main-w > .t-1').addClass('_menu-yes');
  }

  $('.the-content.main-w > .t-1._menu-yes').click(function(){
      $(this).toggleClass('_active');
      $('.the-content.main-w > .t-1 + .b-menu._mod4').toggleClass('_active');
  });

  /* Panorams
    ****************************************************************/
    function panorams($item) {
        pano=new pano2vrPlayer("container_"+$item);
        window.addEventListener("load", function() {
            pano.readConfigUrlAsync("http://siriusjet.com/panorams/panorama1/pano.xml");
        });
    }

  /* Video slide
    ****************************************************************/
  function setHeight(){
    if($('body').is('.mobile')) {

    }
    else {
        var screenHeight = $(window).height();
        var topHeight = $('.header').outerHeight(true) + 90;
        var menuHeight = $('.b-menu._mod1').outerHeight(true);
        var h = screenHeight - (topHeight + menuHeight);
        var videoh = $('.b-video__show').outerHeight(true);
        if(videoh < h) {
          $('.b-video').height(videoh);
        } else {
    		  $('.b-video').height(h);
        }
    }
  };
  // setHeight();

  $('.air-mod a').click(function(e){
      e.preventDefault();

      if($(window).width() <= 768) {
          var airmod = $(this).parents('.air-mod');
          if(!airmod.hasClass('air-mod_active')) {
              $('.air-mod').addClass('air-mod_active');
          }
          else {
              $('.air-mod a').removeClass('_on');
              $(this).addClass('_on');
              var id = $(this).attr('href');
              $('.params-model').removeClass('_on');
              $(id).addClass('_on');
              $('.single-flot__det-img').removeClass('_on');
              $('img[data-tab="'+id+'"]').addClass('_on');
              $('.air-mod').removeClass('air-mod_active');

              var th = $(this);
              $('.air-mod__in').prepend(th);
          }
      }
      else {
          $('.air-mod a').removeClass('_on');
          $(this).addClass('_on');
          var id = $(this).attr('href');
          $('.params-model').removeClass('_on');
          $(id).addClass('_on');
          $('.single-flot__det-img').removeClass('_on');
          $('img[data-tab="'+id+'"]').addClass('_on');
      }

      // var xml = $('.params-model._on .typeload').attr('value');
      // if(xml) {
      //     var numbercont = $('.params-model._on .numberload').attr('value');
      //
      //     var pano = new pano2vrPlayer("container_"+numbercont);
      //     var skin = new pano2vrSkin(pano);
      //     pano.readConfigUrlAsync(xml);
      // }
  });

  /* News text
    ****************************************************************/
  function setHeightNews(){
		var w = $(window).width();
		if(w <= 679){
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 200
			});
		}else{
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 50
			});
		}
	}
  setHeightNews();

  // hamburger
	$('.hamburger-menu').on('click', function() {
		$('.hamburger-menu__in').toggleClass('_animate');
		$('.header').toggleClass('_on');
		$('.hamburger-inner').toggleClass('_on');
		$('body').toggleClass('_menu');
    hamh();
	});

  function hamh() {
    var h = $('.header').outerHeight(true);
		$('.hamburger-inner').css('top', h);
  }
  hamh();

  // Flot
  $('.flot-tabs span').click(function(){
    if(!$(this).is('._on')) {
      $(this).parent().find('span').toggleClass('_on');
      if($(this).parent().is('.aircraft-slider__tabs')) {
        $('.aircraft-slider__items').toggleClass('_on');
      }
      else {
        $('.flot-list-m').toggleClass('_on');
      }
    }
  });

  function flot() {
    $('.flot-list__l').on('click', function(e) {
      if($(this).attr('data-row')) {
        e.preventDefault();
        var row = $(this).attr('data-row');
        $(this).toggleClass('_on');
        $('.flot-list__child'+row).toggleClass('_on');
      }
    });
  }
  flot();

  $('.aircraft-config__i__t').on('click', function(){
    if(!$(this).is('._on')) {
      $('.aircraft-config__i__t').toggleClass('_on');
      $('.aircraft-config__m .aircraft-config__i').toggleClass('_on');
    }
  });


	/* Slider
	 ****************************************************************/
  $('.slider_fleet').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details',
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1600,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items1').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1500,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items2').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});

	$('.slider_details').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet'
	});

	$('.slider_fleet_partners').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details_partners',
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1500,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});

	$('.slider_details_partners').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet_partners'
	});

    $('.b-gallery._mod1').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
        autoplay: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
	});

	// переключение главных табов
	function switchTabsMain(){
		$('#big_stuff .title_stuff .main span').on('click', function(){
			if($(this).is('.others')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.aero').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_partners').addClass('on');
				}
			}
			if($(this).is('.aero')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.others').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_aero').addClass('on');
				}
			}
		});
	};
  switchTabsMain();


  $(document).click(function(e){
      if(e.target.className != 'item_tab') {
          $('.tabs-in-bg_active').removeClass('tabs-in-bg_active');
      }
  });
	// переключение внутренних табов
	function switchTabsMini(){
		$('#big_stuff .tab_wr').on('click', function(e){
            if($(window).width() <= 768) {

                if(e.target.className == 'item_tab') {
                    if($('.tabs-in-bg').hasClass('tabs-in-bg_active')) {
                        $(this).parents('.tabs').find('.tab_wr').removeClass('on');
                        $(this).parents('.tabs').removeClass('tab_active_one tab_active_two');
                        $(this).addClass('on');

                        var listItem = $(this).parents('.tabs').find('.tab_wr.on');
                        var num = $(this).parents('.tabs').find('.tab_wr').index(listItem);

                        var it = $(this).parents('.item');
                        it.find('.bg, section, .lkforair a').removeClass('on');
                        it.find('.lkforair a').eq(num).addClass('on');
                        it.find('.bg').eq(num).addClass('on');
                        it.find('section').eq(num).addClass('on');
                    }
                }
                $(this).parents('.tabs-in-bg').toggleClass('tabs-in-bg_active');
            }
            else {
                $(this).parents('.tabs').find('.tab_wr').removeClass('on');
                $(this).parents('.tabs').removeClass('tab_active_one tab_active_two');
                $(this).addClass('on');

                var listItem = $(this).parents('.tabs').find('.tab_wr.on');
                var num = $(this).parents('.tabs').find('.tab_wr').index(listItem);

                var it = $(this).parents('.item');
                it.find('.bg, section, .lkforair a').removeClass('on');
                it.find('.lkforair a').eq(num).addClass('on');
                it.find('.bg').eq(num).addClass('on');
                it.find('section').eq(num).addClass('on');
            }

		});
	};
  switchTabsMini();

  // слайдер преимущества
  $('.b-advantage__items').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      adaptiveHeight: true,
      dots: true,
      responsive: [
      {
          breakpoint: 1300,
          settings: {
              slidesToShow: 4,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 829,
          settings: {
              slidesToShow: 2,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 489,
          settings: {
              slidesToShow: 1,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      }
      ]
  });

  $('select').styler();


	/* Placeholder
	 ****************************************************************/
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
			$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
				}
			})
		});
	}

});
