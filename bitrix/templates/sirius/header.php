<?
if($_SERVER['REMOTE_ADDR'] == '31.31.196.176') {
	die();
}
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?
    if(file_exists($_SERVER['DOCUMENT_ROOT'].'/redirects.php'))
    include_once($_SERVER['DOCUMENT_ROOT'] . '/redirects.php');
?>
<?
$pageurl = "";
$alternative = "";

$canonical = $_SERVER["HTTP_X_FORWARDED_PROTO"] . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
$canonical = explode('?', $canonical);
$canonicalLink = $canonical[0];

$mlk = $_SERVER["HTTP_X_FORWARDED_PROTO"] . "://" . $_SERVER["HTTP_HOST"];

if((stripos($_SERVER["REQUEST_URI"], "/sitenews/") !== FALSE || stripos($_SERVER["REQUEST_URI"], "/smi/") !== FALSE) && ($_SERVER["REQUEST_URI"] != "/sitenews/" && $_SERVER["REQUEST_URI"] != "/en/sitenews/" && $_SERVER["REQUEST_URI"] != "/about/smi/" && $_SERVER["REQUEST_URI"] != "/en/about/smi/")) {
	
} else {
	$pageurl = $mlk . $_SERVER["REQUEST_URI"];
	if(LANGUAGE_ID=="ru") {
		$alternative = $mlk . "/en" . $_SERVER["REQUEST_URI"];
	} else {
		$alternative = $mlk . "/" . substr($_SERVER["REQUEST_URI"], 4);
	}
}
?>
<!DOCTYPE html>
<!--[if IE 8]><html lang="<?= LANGUAGE_ID ?>" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="<?= LANGUAGE_ID ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="<?= LANGUAGE_ID ?>"><!--<![endif]-->
<head>
	<link rel="canonical" href="<?=$canonicalLink?>">
<?if($_SERVER["HTTP_HOST"] == "sirius-aero.ru" && $pageurl && $alternative && !$canonical[1]):?>
	<?if(LANGUAGE_ID=="ru") {?>
	<link rel="alternate" hreflang="ru" href="<?=$pageurl?>">
	<link rel="alternate" hreflang="en" href="<?=$alternative?>">
	<?} else {?>
	<link rel="alternate" hreflang="en" href="<?=$pageurl?>">
	<link rel="alternate" hreflang="ru" href="<?=$alternative?>">
	<?}?>
<?endif;?>

	
	<meta name="yandex-verification" content="41c049edd37c4bb9" />
	<meta name="yandex-verification" content="754bd0af613ede34" />
	<meta name="yandex-verification" content="a8e8ee1ab8e52324" />

	<meta name="authoring-tool" content="Adobe_Animate_CC">

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139312863-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-139312863-1');
		</script>

  	<title><? $APPLICATION->ShowTitle(); ?></title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<? $APPLICATION->ShowHead(); ?>
  	<link rel="shortcut icon" href="/favicon.svg" type="image/x-icon">
		<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,700&amp;subset=cyrillic" rel="stylesheet">
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/grid.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slider.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/fancy/jquery.fancybox-1.3.4.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.all.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.base.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.core.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.accordion.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.datepicker.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.dialog.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.progressbar.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.resizable.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.slider.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.tabs.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.theme.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.datepicker.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/jquery.jgrowl.css'); ?>
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.css'); ?>
		
		<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/animate.min.css'); ?>
		<!-- <link rel="stylesheet" href="animate.min.css"> -->
  	<!--[if lt IE 9]>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/html5shiv.min.js'); ?>"></script>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/selectivizr-min.js'); ?>"></script>
  	<script src="<?= CUtil::GetAdditionalFileURL(SITE_TEMPLATE_PATH.'js/respond.min.js'); ?>"></script>
  	<![endif]-->
		<?
			if($_SERVER['REMOTE_ADDR']=='94.141.60.142'){
				// $animation_gogo = "menu-emptylegs-btn1";
			}
		?>
		<? 
			$thisDir = $_SERVER['REQUEST_URI'];
			// echo "<script>console.log(' $thisDir ')</script>";
			if($thisDir != "/emptylegs/" & $thisDir != "/en/emptylegs/" ){
				$btnClass = !empty($_GET['btnClass']) ? $_GET['btnClass'] : 'menu-emptylegs-btn2';  	
			}

			$glasses = '';
			if($_COOKIE['glasseson'] == 'on'):
				$glasses = 'glasses-on';
			endif;
		?>
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '2132889060188220');
	fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=2132889060188220&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</head>
<body class="<?= $MainConstruct->bodyClass; ?> <?=$animation_gogo;?> <?=$btnClass;?> <?=$glasses;?>">
	<?if($_COOKIE['siriuscookie'] != 'off') : ?>
		<?
		$botname = '';
		foreach($MainConstruct->isBot as $bot) :
			if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false){
				$botname = $bot;
				break;
			}
		endforeach;
		if(!$botname) : ?>
			<div class="b-cook">
				<div class="b-cook_under"></div>
				<div class="b-cook_window">
					<div class="b-cook_window_tx">
						<?= GetMessage('PRIVACY') ?>
					</div>
					<button type="button" class="b-cook_but" role="button"><?= GetMessage('PRIVACY_BUT') ?></button>
				</div>
			</div>
		<? endif; ?>
		<script>
			$('.b-cook_but').click(function(e){
				e.preventDefault();
				$('.b-cook').remove();
				$.cookie('siriuscookie', 'off', { path: '/', expires: 30 });
			});
		</script>
	<? endif; ?>
	<? $APPLICATION->ShowPanel(); ?>
		<!-- begin header-->

  	<header class="header cl  ">
	  	<?if(LANGUAGE_ID == 'ru'):?>
			<a href="#formMessageHappy" class="message-happy addMessageHappy">Добровольные сообщения</a>
		  <?endif;?>
	  	<div class="glasses">
			<svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M33.4333 15.8667H32.198C31.6591 12.6559 28.883 10.2 25.5408 10.2C22.2417 10.2 19.4797 12.5341 18.84 15.6281C18.2897 15.1668 17.6619 14.922 17 14.922C16.3387 14.922 15.7114 15.1668 15.1612 15.6275C14.5288 12.5335 11.8008 10.2 8.5408 10.2C4.96173 10.2 2.02017 12.9489 1.7289 16.4333H0.566667C0.2533 16.4333 0 16.6872 0 17C0 17.3128 0.2533 17.5667 0.566667 17.5667H1.7289C2.02017 21.0511 4.96173 23.8 8.5408 23.8C12.2054 23.8 15.1912 20.8494 15.2904 17.1864C15.7737 16.4583 16.3755 16.0554 17 16.0554C17.6245 16.0554 18.2263 16.4588 18.7096 17.1864C18.8105 20.8494 21.8325 23.8 25.5408 23.8C29.2678 23.8 32.3 20.7496 32.3 17H33.4333C33.7467 17 34 16.7461 34 16.4333C34 16.1205 33.7467 15.8667 33.4333 15.8667ZM8.5408 22.6667C5.3941 22.6667 2.83333 20.1246 2.83333 17C2.83333 13.8754 5.3941 11.3333 8.5408 11.3333C11.696 11.3333 14.1667 13.8227 14.1667 17C14.1667 20.1773 11.696 22.6667 8.5408 22.6667ZM25.5408 22.6667C22.3941 22.6667 19.8333 20.1246 19.8333 17C19.8333 13.8754 22.3941 11.3333 25.5408 11.3333C28.696 11.3333 31.1667 13.8227 31.1667 17C31.1667 20.1773 28.696 22.6667 25.5408 22.6667Z" fill="#888888"/>
			</svg>
			<svg width="34" height="34" viewBox="0 0 34 34" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M33.4333 15.8667H31.7333C31.6971 15.8667 31.6653 15.8808 31.6308 15.8871C31.1162 12.9767 28.6235 10.7667 25.5408 10.7667C22.5125 10.7667 19.9863 12.8979 19.397 15.7329C18.6382 15.0784 17.8506 14.7396 17.0436 14.7333C17.0363 14.7333 17.0289 14.7333 17.0215 14.7333C16.2027 14.7333 15.3952 15.0756 14.6092 15.7448C14.0403 12.9036 11.5736 10.7667 8.5408 10.7667C5.2666 10.7667 2.58117 13.26 2.295 16.439C2.28537 16.4384 2.27687 16.4333 2.26667 16.4333H0.566667C0.2533 16.4333 0 16.6872 0 17C0 17.3128 0.2533 17.5667 0.566667 17.5667H2.26667C2.27687 17.5667 2.28537 17.5616 2.295 17.561C2.58117 20.74 5.2666 23.2333 8.5408 23.2333C11.9289 23.2333 14.6036 20.5638 14.722 17.2278C15.5102 16.3279 16.2832 15.8667 17.0215 15.8667C17.0261 15.8667 17.0312 15.8667 17.0351 15.8667C17.7656 15.8723 18.5198 16.3319 19.278 17.2238C19.3976 20.5621 22.1521 23.2333 25.5408 23.2333C29.0065 23.2333 31.7333 20.4425 31.7333 17H33.4333C33.7467 17 34 16.7461 34 16.4333C34 16.1205 33.7467 15.8667 33.4333 15.8667Z" fill="black"/>
			</svg>
		</div>
		<script>
			$('.glasses').click(function(e){
				e.preventDefault();
				$('body').toggleClass('glasses-on');
				if($.cookie('glasseson')=='on') {
					$.cookie('glasseson', null, { path: '/' });
				} else {
					console.log('add glasseson');
					$.cookie('glasseson', 'on', { path: '/', expires: 30 });
				}
			});
		</script>
    	<div class="col-lg-4 col-md-4 col-sm-6 hidden-xs col-sm-push-3 col-md-push-0 col-lg-push-0 header__col1">
      		<div class="b-info">
				<span class="b-info__phone">
					<?if($_SERVER['REQUEST_URI'] == "/shuttles/"):?>
						<?$APPLICATION->IncludeFile(
							SITE_DIR.'/include/shut_headerphone.php',
							array(),
							array(
								"MODE"=>"html",
							)
						);?>
					<?else:?>
						<?$APPLICATION->IncludeFile(
							SITE_DIR.'/include/headerphone.php',
							array(),
							array(
								"MODE"=>"html",
							)
						);?>
					<?endif;?>
				</span>
        		<div class="b-info__img pull-left">
					<img src="<?= SITE_TEMPLATE_PATH ?>/img/24_7.png" alt="">
				</div>
				<a class="b-info__lk pull-left fancymodal" href="#form-order"><?= GetMessage('ZAKAZ') ?></a>
      		</div>
    	</div>
    	<div class="col-lg-4 col-md-4 col-sm-3 col-xs-6 col-sm-pull-6 col-md-pull-0 col-lg-pull-0 header__col2">
      		<div class="logo">
				<a href="<?= SITE_DIR ?>">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/headerlogo.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</a>
			</div>
    	</div>
    	<div class="col-lg-4 col-md-4 col-sm-3 col-xs-6 header__col3">
      		<div class="col-xs-12 visible-md visible-lg">
				<?
				$APPLICATION->IncludeComponent(
					"bitrix:main.site.selector",
					"sitelang",
					Array(
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"SITE_LIST" => array(0=>"*all*",)
					)
				);
				?>

      		</div>
      		<div class="col-xs-12 visible-xs visible-sm">
        		<div class="hamburger-menu">
          			<div class="hamburger-menu__in"></div>
        		</div>
				<div class="mobile-lang">
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.site.selector",
						"sitelang",
						Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"SITE_LIST" => array(0=>"*all*",)
						)
					);?>
	      		</div>
      		</div>
      		<div class="b-social pull-right visible-md visible-lg">
				<?$APPLICATION->IncludeFile(
					SITE_DIR.'/include/headersoc.php',
					array(),
					array(
						"MODE"=>"html",
					)
				);?>
			</div>
      		<div class="b-search pull-right visible-md visible-lg">
				<?$APPLICATION->IncludeComponent(
					"bitrix:search.form",
					"search",
					Array(
						"PAGE" => "#SITE_DIR#search/index.php",
						"USE_SUGGEST" => "N"
					)
				);?>
      		</div>
    	</div>
  	</header>
	<!-- end header-->
	<div class="hamburger-inner">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"menu",
			Array(
				"ALLOW_MULTI_SELECT" => "Y",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "2",
				"MENU_CACHE_GET_VARS" => array(0=>"",),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"CLASS_MENU" => '_mod3 visible-xs visible-sm',
			)
		);?>
		<!-- <a href="#formMessageHappy" class="message-happy addMessageHappy">Добровольные сообщения</a> -->
		<script>
			$('body').on('click', '.addMessageHappy', function(e){
				e.preventDefault();
				$('#formMessageHappy').addClass('form-orderMtl_active');
			});
			
			function closeModal() {
				$('.form-orderMtl_active').removeClass('form-orderMtl_active');
			}
			// close modal window
			$('body').on('click', '.form-orderMtl-below', function(e){
				closeModal();
			});
				// keyboard event
			document.onkeyup = function(e){
				if (e.keyCode === 27){//esc
					closeModal();
				}
			}
		</script>
	    <div class="b-search">
			<?$APPLICATION->IncludeComponent(
				"bitrix:search.form",
				"search",
				Array(
					"PAGE" => "#SITE_DIR#search/index.php",
					"USE_SUGGEST" => "N"
				)
			);?>
	    </div>
	    <div class="align-center">
	      <div class="b-info">
			<span class="b-info__phone">
	      		<?$APPLICATION->IncludeFile(
					SITE_DIR.'/include/headerphone.php',
					array(),
					array(
						"MODE"=>"text",
					)
				);?>
	      	</span>
	        <div class="b-info__img pull-left"><img src="<?= SITE_TEMPLATE_PATH ?>/img/24_7.png" alt=""></div><a class="b-info__lk pull-left" href="#form-order"><?= GetMessage('ZAKAZ') ?></a>
	      </div>
	    </div>
	    <div class="b-social">
	    	<?$APPLICATION->IncludeFile(
				SITE_DIR.'/include/headersoc.php',
				array(),
				array(
					"MODE"=>"html",
				)
			);?>
	    </div>
	</div>
	
		<?if($MainConstruct->bIsMainPage):?>

			<? //if($_SERVER['REMOTE_ADDR'] == '94.141.60.142' or $_SERVER['REMOTE_ADDR'] == '193.106.41.54' or $_SERVER['REMOTE_ADDR'] == '195.225.109.81') : ?>
			
			<div class="photo-for-adaptive just-photo">
				<div class="just-photo_text">
					<div class="company-slogan">Serious Business. Serious Approach</div>
					<div class="company-name">Sirius Aero</div>
				</div>
				<div class="jusp-app-on-slide">
					<div class="b-load__link-on-app">
						<div class="b-load__link-on-app__i">
							<a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios'); return true;" class="appstore" target="_blank"></a>
						</div>
						<div class="b-load__link-on-app__i">
							<a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android'); return true;" class="gplay" target="_blank"></a>
						</div>
					</div>
				</div>
				<img src="<?= SITE_TEMPLATE_PATH ?>/img/main-img.jpg" alt="" class="goanime" id="imgmainsl2" style="width:100%;">
			</div>


			<div class="just-photo decstop-img just-adaptive">
				<script>
					var justphoto = document.getElementsByClassName('just-photo')[0];
					var justphotoAdaptive = document.getElementsByClassName('photo-for-adaptive')[0];
					var clientWidth = document.body.clientWidth;
					var percentWidth = clientWidth/1920;
					var percentHeight = Math.floor(630*percentWidth);
					// justphoto.style.minHeight = percentHeight+'px';
					// justphotoAdaptive.style.minHeight = percentHeight+'px';
				</script>
				<div class="main-order-block">
					<div class="main-order-block__slogan"><?= GetMessage('textfrom') ?></div>
					<div class="main-order-block__form">
						<!-- <div class="for-cryak"></div> -->
						<!-- <div class="for-cryak-hidden"> -->
							<div class="mof-input departure-inp">
								<label class="input-label" for="order_from"><?= GetMessage('from') ?></label>
								<div>
									<input type ="text" class="mof-input-item city required" id="order_from" name="order_from" autocomplete="off">
									<div class="modal-city-from modal-city-it"></div>
								</div>
							</div>
							<div class="mof-input arrival-inp">
								<label class="input-label" for="order_to"><?= GetMessage('to') ?></label>
								<div>
									<input type="text" class="mof-input-item city required" id="order_to" autocomplete="off">
									<div class="modal-city-to modal-city-it"></div>
								</div>
							</div>
							<div class="mof-input">
								<label class="input-label "  for="order_attrebute_date_0" ><?= GetMessage('date') ?></label>
								<div>
									<? if($MainConstruct->isPhone) : ?>
										<input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_0" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
									<? else : ?>
										<input class="mof-input-item mof-input-item_date datepicker datepicker2 required" data-dp-class="custom" id="order_attrebute_date_0" autocomplete="off"/>
									<? endif; ?>
								</div>
							</div>
							<div class="mof-input leg-pass">
								<div class="mof-input_add-pass">
									<div class="mof-input_add-pass-btn"></div>
									<div class="mof-input_add-pass-btn"></div>
								</div>
								<label class="input-label" for="add_pass"><?= GetMessage('passengers') ?></label>
								<div><input class="mof-input-item pass required" id="add_pass" autocomplete="off" type="number"></div>
							</div>
							<? if($MainConstruct->isPhone) : ?>
								<div class="for-cryak">
									<div class="for-cryak__routes"></div>
									<div class="for-cryak__date"></div>
								</div>
							<? endif; ?>
						<!-- </div> -->
						
					</div>



					<div class="main-order-block__form display-none">
						<div class="mof-input departure-inp">
							<label class="input-label" for="order_from2"><?= GetMessage('from') ?></label>
							<div>
								<input type ="text" class="mof-input-item city required" id="order_from2" autocomplete="off">
								<div class="modal-city-from2 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input arrival-inp">
							<label class="input-label" for="order_to2"><?= GetMessage('to') ?></label>
							<div>
								<input type="text" class="mof-input-item city required" id="order_to2" autocomplete="off">
								<div class="modal-city-to2 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input">
							<label class="input-label" for="order_attrebute_date_2"><?= GetMessage('date') ?></label>
							<div>
								<? if($MainConstruct->isPhone) : ?>
									<input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_2" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
								<? else : ?>
									<input class="mof-input-item mof-input-item_date datepicker required" data-dp-class="custom" id="order_attrebute_date_2" autocomplete="off"/>
								<? endif; ?>
							</div>
						</div>
						<div class="mof-input leg-pass">
							<div class="mof-input_add-pass">
								<div class="mof-input_add-pass-btn"></div>
								<div class="mof-input_add-pass-btn"></div>
							</div>
							<label class="input-label" for="add_pass2"><?= GetMessage('passengers') ?></label>
							<div><input class="mof-input-item pass required" id="add_pass2" autocomplete="off"></div>
						</div>
						<? if($MainConstruct->isPhone) : ?>
							<div class="for-cryak">
								<div class="for-cryak__routes"></div>
								<div class="for-cryak__date"></div>
							</div>
						<?endif;?>
						<div class="del-block"></div>
					</div>

					
					<div class="main-order-block__form display-none">
						<div class="mof-input departure-inp">
							<label class="input-label" for="order_from3"><?= GetMessage('from') ?></label>
							<div>
								<input type ="text" class="mof-input-item city required" id="order_from3" autocomplete="off">
								<div class="modal-city-from3 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input arrival-inp">
							<label class="input-label" for="order_to3"><?= GetMessage('to') ?></label>
							<div>
								<input type="text" class="mof-input-item city required" id="order_to3" autocomplete="off">
								<div class="modal-city-to3 modal-city-it"></div>
							</div>
						</div>
						<div class="mof-input">
							<label class="input-label" for="order_attrebute_date_3"><?= GetMessage('date') ?></label>
							<div>
								<? if($MainConstruct->isPhone) : ?>
									<input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_3" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
								<? else : ?>
									<input class="mof-input-item mof-input-item_date datepicker required" data-dp-class="custom" id="order_attrebute_date_3" autocomplete="off"/>
								<? endif; ?>
							</div>
						</div>
						<div class="mof-input leg-pass">
							<div class="mof-input_add-pass">
								<div class="mof-input_add-pass-btn"></div>
								<div class="mof-input_add-pass-btn"></div>
							</div>
							<label class="input-label" for="add_pass3"><?= GetMessage('passengers') ?></label>
							<div><input class="mof-input-item pass required" id="add_pass3" type="number" autocomplete="off"></div>
						</div>
						<? if($MainConstruct->isPhone) : ?>
							<div class="for-cryak">
								<div class="for-cryak__routes"></div>
								<div class="for-cryak__date"></div>
							</div>
						<?endif;?>
						<div class="del-block"></div>
					</div>


					<a href="#main-form-order" class="mof-btn"><?= GetMessage('next') ?></a>
					<div class="main-order-block__manage-btns">
						<div class="manage-btn comeback"><?= GetMessage('comebackfly') ?></div>
						<a href="#formMainOrderPreferences" class="manage-btn add-preferences"><?= GetMessage('predpocht') ?></a>
						<div class="manage-btn main-order-block__add-transfer"><?= GetMessage('transfer') ?></div>

						<div class="social-btn social-btn-telegram">Telegram</div>
						<div class="social-btn social-btn-whats">Whats App</div>
						<div class="social-btn social-btn-viber">Viber</div>
					</div>
				</div>
				<div class="adaptive-socials">
						<div class="social-btn social-btn-telegram">Telegram</div>
						<div class="social-btn social-btn-whats">Whats App</div>
						<div class="social-btn social-btn-viber">Viber</div>
				</div>
				<div class="shadow"></div>


				<script>
					//добавляем пассажиров (как смог)
					let legPass = document.getElementsByClassName('leg-pass'); // дивы с пассажирами
					let passangers = document.getElementsByClassName('pass'); // инпуты с пассажирами
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.firstElementChild.onmousedown = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = (PasAmount.value == "") ? 1 : ++passValue;


								legPass[formNumber].children[1].classList.add("label-active");
							}
						} )();
					}

					//убавляем пассажиров (как смог)
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.lastElementChild.onclick = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = passValue > 1 ? --passValue : "";
							}
						} )();
					}


					let mainOrderBlock = document.getElementsByClassName('main-order-block')[0];// блок со всеми формами
					let forms = document.getElementsByClassName('main-order-block__form'); // формы 
					let dalee = document.getElementsByClassName('mof-btn')[0]; // кнопка далее
					let comeback = document.getElementsByClassName('comeback')[0]; //кнопка добавить обратный рейс
					let addTransfer = document.getElementsByClassName('main-order-block__add-transfer')[0];//кнопка добавить пересадку
					let addPreferences = document.getElementById('prefencesSubmit');//кнопка добавить предпочтения


					// добавляем пересадочку
					var formCount = 1;
					var centerBlockHid = 0;
					addTransfer.onclick = function(){
						comeback.classList.add('display-none');
						if (formCount < 3) {
							formCount++;
						}
						if (formCount == 3) {
							addTransfer.classList.add('manage-btn_hidden');
						}
						switch(formCount){
							case 2: 
								if(document.getElementById('order_from').value != '' && document.getElementById('order_to').value != '' && document.getElementById('order_attrebute_date_0').value != '' && document.getElementById('add_pass').value != '') {
									if(document.body.classList.contains('mobile')) {
										forms[0].classList.add('main-order-block__form_cryak');
									}
									let from = forms[0].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item city')[0].value;
									let to = forms[0].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0].value;
									let date = forms[0].getElementsByClassName('mof-input-item_date')[0].value;
									if(document.body.classList.contains('mobile')) {
										forms[0].getElementsByClassName('for-cryak__routes')[0].innerHTML = from+' - '+to;
										forms[0].getElementsByClassName('for-cryak__date')[0].innerHTML = date;
									}
									forms[1].classList.remove('display-none');
									mainOrderBlock.classList.add('mob-whith-transfer');
								} else {
									comeback.classList.remove('display-none');
									setTimeout(() => {
										if(document.getElementById('order_from').value == '') {
											document.getElementById('order_from').focus();
											document.getElementsByClassName('departure-inp')[0].getElementsByClassName('input-label')[0].classList.add('label-active');
										} else if(document.getElementById('order_to').value == '') {
											document.getElementById('order_to').focus();
											document.getElementsByClassName('arrival-inp')[0].getElementsByClassName('input-label')[0].classList.add('label-active');
										} else if(document.getElementById('order_attrebute_date_0').value == '') {
											document.getElementById('order_attrebute_date_0').focus();
											document.getElementsByClassName('mof-input')[2].getElementsByClassName('input-label')[0].classList.add('label-active');
										} else if(document.getElementById('add_pass').value == '') {
											document.getElementById('add_pass').focus();
											document.getElementsByClassName('leg-pass')[0].getElementsByClassName('input-label')[0].classList.add('label-active');
										}
									}, 100);
									formCount--;
								}
								break;
							case 3:
								if(centerBlockHid == 1) {
									forms[1].classList.remove('display-none');
									centerBlockHid = 0;
								} else {
									// if(document.getElementById('order_from2').value != '' && document.getElementById('order_to2').value != '' && document.getElementById('order_attrebute_date_2').value != '' && document.getElementById('add_pass2').value != '') {
										if(document.body.classList.contains('mobile')) {
											forms[0].classList.add('main-order-block__form_cryak');
										}
										let from1 = forms[0].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item city')[0].value;
										let to1 = forms[0].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0].value;
										let date1 = forms[0].getElementsByClassName('mof-input-item_date')[0].value;
										if(document.body.classList.contains('mobile')) {
											forms[0].getElementsByClassName('for-cryak__routes')[0].innerHTML = from1+' - '+to1;
											forms[0].getElementsByClassName('for-cryak__date')[0].innerHTML = date1;
										}
										
										if(document.body.classList.contains('mobile')) {
											forms[1].classList.add('main-order-block__form_cryak');
										}
										let from2 = forms[1].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item city')[0].value;
										let to2 = forms[1].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0].value;
										let date2 = forms[1].getElementsByClassName('mof-input-item_date')[0].value;
										if(document.body.classList.contains('mobile')) {
											forms[1].getElementsByClassName('for-cryak__routes')[0].innerHTML = from2+' - '+to2;
											forms[1].getElementsByClassName('for-cryak__date')[0].innerHTML = date2;
										}
									// } else {
									// 	comeback.classList.remove('display-none');
									// 	formCount--;
									// }
								}
								
								
								forms[2].classList.remove('display-none');
								break;
						}
					}

					// удаляем пересадочку
					let delBlock = document.getElementsByClassName('del-block');
					for (var i = 0; i < delBlock.length; i++){
						delBlock[i].onclick = (function(){
							let x = i;
							return function(){
								if(formCount == 3 && x == 0) {
									centerBlockHid = 1;
								} else {
									centerBlockHid = 0;
								}
								addTransfer.classList.remove('manage-btn_hidden');
								formCount--;
								if (formCount == 1)	{
									forms[0].classList.remove('main-order-block__form_cryak');
									mainOrderBlock.classList.remove('mob-whith-transfer');
									comeback.classList.remove('display-none');
									addTransfer.classList.remove('display-none');
								}
								forms[1].classList.remove('main-order-block__form_cryak');
								forms[x+1].classList.add('display-none');
								let inpts = forms[x+1].getElementsByTagName("input");
								for (let i = 0; i < inpts.length; i++ ){
									inpts[i].value = "";
									inpts[i].parentElement.previousElementSibling.classList.remove("label-active");
								}
							}

						})();
					}

					//добавляем обратный рейс
					comeback.onclick = function(){
						addTransfer.classList.add('display-none');
						mainOrderBlock.classList.add('mob-whith-transfer');
						forms[1].classList.remove('display-none');
						formCount = 2;
						let departure = forms[0].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item')[0];
						let arrival = forms[0].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0];
						let passagir = forms[0].getElementsByClassName('leg-pass')[0].getElementsByClassName('mof-input-item')[0];

						let comebackDeparture = forms[1].getElementsByClassName('departure-inp')[0].getElementsByClassName('mof-input-item')[0];
						let comebackArrival = forms[1].getElementsByClassName('arrival-inp')[0].getElementsByClassName('mof-input-item')[0];
						let comebackPassagir = forms[1].getElementsByClassName('leg-pass')[0].getElementsByClassName('mof-input-item')[0];

						comebackDeparture.value = arrival.value;
						if (arrival.value != "") addActiveLabel(comebackDeparture);
						
						comebackArrival.value = departure.value;
						if (departure.value != "") addActiveLabel(comebackArrival);
						
						comebackPassagir.value = passagir.value;
						if (passagir.value != "") addActiveLabel(comebackPassagir);
					}

					//делаем активный лейбл меньше
					let addActiveLabel = function(input){
						input.parentElement.previousElementSibling.classList.add("label-active");
					}

					
					$('body').on('click', '.mof-btn', function(e){
						e.preventDefault();
						var check = 0;
						$('.main-order-block .main-order-block__form:not(.display-none) .required').each(function(){
							if(!$(this).val()) {
								$(this).focus();
								$(this).parents('.mof-input').find('.input-label').addClass('input-label__error ');
								check = 1;
								return false;
							}
						});
						if(!check) {
							yaCounter28830840.reachGoal('order_main_next');
							$('#formMainOrder').addClass('form-orderMtl_active');
						}
					});

					$('body').on('click', '.add-preferences', function(e){
						e.preventDefault();
						$('#formMainOrderPreferences').addClass('form-orderMtl_active');
					});

					var mofInputs = document.getElementsByClassName('mof-input-item');
					for (var i = 0; i < mofInputs.length; i++){
						mofInputs[i].value = "";
						mofInputs[i].onfocus = (function(){
							var x = i;
							return function(){
								mofInputs[x].parentElement.previousElementSibling.classList.add("label-active");
							}
						})();
						// mofInputs[i].onblur = (function(){
						// 	var x = i;
						// 	return function(){

						// 		// setTimeout(function(){
						// 		// 	if (mofInputs[x].value==""){
						// 		// 		mofInputs[x].parentElement.previousElementSibling.classList.remove("label-active");
						// 		// 		mofInputs[x].parentElement.previousElementSibling.classList.remove("input-label__error");
						// 		// 	}
						// 		// }, 300);
						// 		if (mofInputs[x].value==""){
						// 				mofInputs[x].parentElement.previousElementSibling.classList.remove("label-active");
						// 				mofInputs[x].parentElement.previousElementSibling.classList.remove("input-label__error");
						// 		}
						// 	}

						// })();
					}
					let checkLebels = function(){
						for (var i = 0; i < mofInputs.length; i++){
										if (mofInputs[i].value==""){
											mofInputs[i].parentElement.previousElementSibling.classList.remove("label-active");
											mofInputs[i].parentElement.previousElementSibling.classList.remove("input-label__error");
										}	
									}
					}
					var isDatepicker = function(element, className){
						while(element){
							if(element.matches(className)){
								return true;
								break;
							}else{
								element = element.parentElement;
							}
						}
						
						return false;

					}
					let doImputs = function(e){
							if(isDatepicker(e.target,'.ui-datepicker')||isDatepicker(e.target,'.ui-datepicker-header')||e.target.classList.contains('mof-btn')){
								}else if(e.target.matches('.mof-input-item')){
								checkLebels();
								e.target.parentElement.previousElementSibling.classList.add("label-active");
							}else{
								checkLebels();
							}
					}
					let doImputsOnPress = function(e){
						if(e.key == "Tab"){
							checkLebels();
						}
					}
					window.addEventListener('click', doImputs);
					dalee.addEventListener('click', checkLebels);
					window.addEventListener('keydown', doImputsOnPress);

				</script>


				<div class="just-photo_text">
					<div class="company-slogan">Serious Business. Serious Approach</div>
					<div class="company-name">Sirius Aero</div>
				</div>
				<div class="jusp-app-on-slide">
					<div class="b-load__link-on-app">
						<div class="b-load__link-on-app__i">
							<a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios'); return true;" class="appstore" target="_blank"></a>
						</div>
						<div class="b-load__link-on-app__i">
							<a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android'); return true;" class="gplay" target="_blank"></a>
						</div>
					</div>
				</div>
				<img src="<?= SITE_TEMPLATE_PATH ?>/img/main-img.jpg" alt="" class="goanime" id="imgmainsl" style="width:100%;">
			</div>



		<? if(false) : ?>
			<div class="just-photo">
				<div class="just-photo_text">
					<div class="company-slogan">Serious Business. Serious Approach</div>
					<div class="company-name">Sirius Aero</div>
				</div>
				<img src="<?= SITE_TEMPLATE_PATH ?>/img/main-img.jpg" alt="" class="goanime" id="imgmainsl" style="width:100%;">
			</div>
			<?endif;?>
			<script>
					var itisimg = document.getElementById('imgmainsl');
					itisimg.onload = function() {
						setTimeout(function(){
							justphoto.style.minHeight = '0px';
						}, 1000);
  					}
					setTimeout(function(){
							document.getElementsByClassName('company-name')[0].classList.add("company-name_active");
					}, 1000);
					setTimeout(function(){
							document.getElementsByClassName('company-slogan')[0].classList.add("company-slogan_active");
					}, 500);
					setTimeout(function(){
							document.getElementsByClassName('main-order-block')[0].classList.add("main-order-block_active");
					}, 300);
					// setTimeout(function(){
					// 		document.getElementsByClassName('jusp-app-on-slide')[0].classList.add("jusp-app-on-slide_active");
					// }, 1300);


					var normalLoad = function(id){
						let itisimg = document.getElementById(id);
						let photo = itisimg.parentNode;
						itisimg.onload = function() {
							setTimeout(function(){
								// photo.style.minHeight = percentHeight+'px';
							}, 1000);
  						}
						setTimeout(function(){
							photo.getElementsByClassName('company-name')[0].classList.add("company-name_active");
						}, 1000);
						setTimeout(function(){
							photo.getElementsByClassName('company-slogan')[0].classList.add("company-slogan_active");
						}, 500);
						setTimeout(function(){
							photo.getElementsByClassName('main-order-block')[0].classList.add("main-order-block_active");
						}, 300);
						setTimeout(function(){
							photo.getElementsByClassName('jusp-app-on-slide')[0].classList.add("jusp-app-on-slide_active");
						}, 1300);
					}
					normalLoad('imgmainsl');
					normalLoad('imgmainsl2');
				</script>
		<?endif;?>
	
	
	<?
	if($MainConstruct->bIsMainPage):
		$menu = '_mod1';
		$lvl = "2";
	else:
		$menu = '_mod2';
		$lvl = "2";
	endif;
	$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"menu",
		Array(
			"ALLOW_MULTI_SELECT" => "Y",
			"CHILD_MENU_TYPE" => "left",
			"DELAY" => "N",
			"MAX_LEVEL" => $lvl,
			"MENU_CACHE_GET_VARS" => array(0=>"",),
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"ROOT_MENU_TYPE" => "top",
			"USE_EXT" => "N",
			"CLASS_MENU" => $menu,
		)
	);
	$menu = '_mod4';
	$APPLICATION->IncludeComponent(
		"bitrix:menu",
		"menumobile",
		Array(
			"ALLOW_MULTI_SELECT" => "Y",
			"CHILD_MENU_TYPE" => "left",
			"DELAY" => "N",
			"MAX_LEVEL" => $lvl,
			"MENU_CACHE_GET_VARS" => array(0=>"",),
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"ROOT_MENU_TYPE" => "top",
			"USE_EXT" => "N",
			"CLASS_MENU" => $menu,
		)
	);?>
	<?if(!$MainConstruct->bIsMainPage && false):
		if(ERROR_404 != 'Y') :?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"breadcrumbs",
			Array(
				"PATH" => "",
				"SITE_ID" => "ru, en",
				"START_FROM" => "0"
			)
		);?>
	<?  endif;
	endif;?>
	<!-- begin content-->
