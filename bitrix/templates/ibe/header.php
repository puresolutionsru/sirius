<?
if($_SERVER['REMOTE_ADDR'] == '31.31.196.176') {
	die();
}
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<!DOCTYPE html>
<!--[if IE 8]><html lang="<?= LANGUAGE_ID ?>" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="<?= LANGUAGE_ID ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="<?= LANGUAGE_ID ?>"><!--<![endif]-->
<head>
	<title><? $APPLICATION->ShowTitle(); ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<? $APPLICATION->ShowHead(); ?>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,700&amp;subset=cyrillic" rel="stylesheet">

	<? //$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/grid.css'); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.core.css'); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.theme.css'); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/js/ui.datepicker.css'); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style.css'); ?>
	<? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/style1.css'); ?>

	<? $APPLICATION->SetAdditionalCSS('/js/fancy/jquery.fancybox.min.css'); ?>
	<? $APPLICATION->SetAdditionalCSS('/css/slick/slick.css'); ?>
	<? $APPLICATION->SetAdditionalCSS('/css/slick/slick-theme.css'); ?>
	<? $APPLICATION->SetAdditionalCSS('/results/style.css'); ?>
</head>
<body class="<?=$MainConstruct->bodyClass?>">
	<main class="main main-part">
	<?if(!$MainConstruct->bIsMainPage):?>
		<header class="header cl">
			<div class="logo">
				<a href="/">
					<img class="logo-norm" src="/bitrix/templates/sirius/img/logo.jpg" alt="">
				</a>
			</div>
		</header>
	<?endif;?>
