$( document ).ready( function() {

    /* News text
    ****************************************************************/
    function setHeightNews(){
        var w = $(window).width();
        if(w <= 679){
            $('.b-news__i__tx').dotdotdot({
                ellipsis: "...",
                wrap: "letter",
                after: null,
                watch: false,
                height: 200
            });
        }else{
            $('.b-news__i__tx').dotdotdot({
                ellipsis: "...",
                wrap: "letter",
                after: null,
                watch: false,
                height: 50
            });
        }
    }
    if($('div').is('.b-news__i__tx')) {
        setHeightNews();
    }
    $(window).on('resize', function() {
        setHeightNews();
    });

});