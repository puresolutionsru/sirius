$( document ).ready( function() {

  let companyCode = "7R";
  let routes = '';
  let citiesM = '';
  let countriesM = '';

  
  $(".data-passenger__add").click(function(){
    var count = Number($(".data-passenger__items").attr("data-count"));
    count += 1;
    $(".data-passenger__items").append('<div class="data-passenger__t data-passenger__tm">Пассажир '+count+'</div><div class="data-passenger__row"><input type="text" class="form-it" placeholder="Имя"><input type="text" class="form-it" placeholder="Отчество"><input type="text" class="form-it" placeholder="Фамилия"><input type="radio" name="sex-'+count+'" id="sex1-'+count+'" class="form-it" value="Муж"><label for="sex1-'+count+'">Муж</label><input type="radio" name="sex-'+count+'" id="sex2-'+count+'" class="form-it" value="Жен"><label for="sex2-'+count+'">Жен</label><input type="text" class="form-it" placeholder="Дата рождения"></div><div class="data-passenger__row data-passenger__for"><input type="text" class="form-it" placeholder="Страна"><input type="text" class="form-it" placeholder="Документ"><input type="text" class="form-it" placeholder="Номер"><input type="text" class="form-it" placeholder="Годен до"></div>');
    $(".data-passenger__items").attr("data-count", count);
  });
  
  function getRoutes() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/get_company_routes/?company='+companyCode, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {} 
        else {
          routes = JSON.parse(xhr.responseText);
        }
    }
  }
  function cities() {
    var xhrCity = new XMLHttpRequest();
    xhrCity.open('GET', 'https://sirius-aero.ru/enginetais/Requests/get_cities/', true);
    xhrCity.send();
    xhrCity.onreadystatechange = function() {
        if (xhrCity.readyState != 4) return;
        if (xhrCity.status != 200) {} 
        else {
          citiesM = JSON.parse(xhrCity.responseText);
        }
    }
  }
  function countries() {
    var xhrCountry = new XMLHttpRequest();
    xhrCountry.open('GET', 'https://sirius-aero.ru/enginetais/Requests/get_countries/', true);
    xhrCountry.send();
    xhrCountry.onreadystatechange = function() {
        if (xhrCountry.readyState != 4) return;
        if (xhrCountry.status != 200) {} 
        else {
          countriesM = JSON.parse(xhrCountry.responseText);
        }
    }
  }
  getRoutes();
  cities();
  countries();



  function string(string) {
    var splits = string.split(" ");
    var stringItog = "";

    for (let i = 0; i < splits.length; i++) {
      let Name = splits[i];
      let First = Name.substring(0, 1).toUpperCase();
      let Leftovers = Name.substring(1, Name.length)
      stringItog += First + Leftovers + " ";
    }
  
    return stringItog;
  }











  let input = document.getElementById("order_attrebute_date_0");
  let options = {
    "routeFrom": null, // храним код аэропорта откуда летим
    "routeTo": null, // храним код аэропорта куда летим
    "dateFrom": {value: null},
    "dateTo": {value: null},
    "countPass": 1, // устанавливаем количество пассажиров
  };
  if(typeof from !== "undefined") {
    options.routeFrom = from;
    options.routeTo = to;
  }
  let calendar = {};
  let translate = {
      "ru": {
          "wordsPass": ["пассажир", "пассажира", "пассажиров"],
          "monthName": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
          "monthNameWN": ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"],
          "dayName": ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"],
          "transfer": "с пересадкой",
      },
      "en": {
          "wordsPass": ["passenger", "passengers", "passengers"],
          "monthName": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          "monthNameWN" : ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"],
          "dayName": ["MO", "TU", "WE", "TH", "FR", "SA", "SU"],
          "transfer": "transfer",
      }
  };
  calendar.selectedDate = {
    'Day': new Date().getDate(),
    'Month': parseInt(new Date().getMonth()) + 1,
    'Year': new Date().getFullYear()
  };
  let schedule = null;
  let lang = "ru"
  $("#order_attrebute_date_0").click(function(){
    let time = new Date();
    $("#search-calendar").addClass("search-calendar_active");
    drawCalendar((time.getMonth()+1), time.getFullYear());
  });
  // загрузка расписания полетов
  function getSchedule() {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/get_schedule/?company='+companyCode, true);
      xhr.send();
      xhr.onreadystatechange = function() {
          if (xhr.readyState != 4) return;
          if (xhr.status != 200) {} 
          else {schedule = JSON.parse(xhr.responseText);}
      }
  };
  getSchedule();
  
  // события клавиатуры
  document.addEventListener('keyup', function(e) {
      if (e.keyCode === 27) { // esc
          closeAllWindows();
      }
  }, false);
  $(document).click(function(e){
    if(!$(e.target).hasClass("mof-input-item_date")) {
      console.log($(e.target).parents("#search-calendar").hasClass("search-calendar_active"));
      if(!$(e.target).parents("#search-calendar").hasClass("search-calendar_active")) {
        closeAllWindows();
      }
    } else {

    }
  });
  function drawCalendar(month, year) {
      let container = document.getElementById("search-calendar");
      let navLeft = "ruswidget__calendar__navigation_no-active";
      if((month>1?(+month-1):12) >= calendar.selectedDate.Month || (month>1?year:(+year-1)) > calendar.selectedDate.Year) {
          navLeft = "";
      }


      var calendarHTML = '';
      calendarHTML += '<div class="ruswidget__calendar__top">';
          calendarHTML += '<div class="ruswidget__calendar__navigation ruswidget__calendar__navigation_left '+navLeft+'" month="' + (month>1?(+month-1):12) + '" year="' + (month>1?year:(+year-1)) + '"></div>';
          calendarHTML += '<div class="ruswidget__calendar__t ruswidget__calendar__navigation-to-this-day" month="' + calendar.selectedDate.Month + '" year="' + calendar.selectedDate.Year + '">' + translate[lang].monthName[(month - 1)] + ',&nbsp;' + year + '</div>';
          calendarHTML += '<div class="ruswidget__calendar__navigation ruswidget__calendar__navigation_right" month="' + (month<12?(+month+1):1) + '" year="' + (month<12?year:(+year+1)) + '"></div>';
      calendarHTML += '</div>';
      calendarHTML += '<div class="ruswidget__calendar__body">';
          calendarHTML += '<div class="ruswidget__calendar__dayWeek">';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day">' + translate[lang].dayName[0] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day">' + translate[lang].dayName[1] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day">' + translate[lang].dayName[2] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day">' + translate[lang].dayName[3] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day">' + translate[lang].dayName[4] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day ruswidget__calendar__dayWeek__day_holiday">' + translate[lang].dayName[5] + '</div>';
              calendarHTML += '<div class="ruswidget__calendar__dayWeek__day ruswidget__calendar__dayWeek__day_holiday">' + translate[lang].dayName[6] + '</div>';
          calendarHTML += '</div>';

          // Количество дней в месяце
          var total_days = 32 - new Date(year, (month-1), 32).getDate();
          // Начальный день месяца
          var start_day = new Date(year, (month-1), 1).getDay();
          if (start_day == 0) { start_day = 7; }
          start_day--;
          // Количество ячеек в таблице
          var final_index = Math.ceil((total_days+start_day)/7)*7;

          var day = 1;
          var index = 0;
          calendarHTML += '<div class="ruswidget__calendar__list-days">';
          do {
              // Начало строки таблицы
              if(index % 7 == 0) {
                  calendarHTML += '<div class="ruswidget__calendar__list-days__row">';
              }
              // Пустые ячейки до начала месяца или после окончания
              if((index < start_day) || (index >= (total_days + start_day))) {
                  calendarHTML += '<div class="ruswidget__calendar__list-days__cell ruswidget__calendar__list-days__cell_empty ruswidget__calendar__list-days__cell_nochooise">&nbsp;</div>';
              } else {
                  var class_name = '';
                  // Выбранный день
                  if (calendar.selectedDate.Day == day &&
                      calendar.selectedDate.Month == month &&
                      calendar.selectedDate.Year == year) {
                      class_name = 'ruswidget__calendar__list-days__cell_selected';
                  } else if(
                      calendar.selectedDate.Day > day &&
                      calendar.selectedDate.Month == month &&
                      calendar.selectedDate.Year == year) {
                      class_name = 'ruswidget__calendar__list-days__cell_past ruswidget__calendar__list-days__cell_nochooise';
                  } else if (index % 7 == 6 || index % 7 == 5) {
                      class_name = 'holiday';
                  }
                  if(options.routeFrom &&
                    options.routeTo) {
                          if(schedule[options.routeFrom+options.routeTo]) {
                              var dc = day;
                              if(dc<10) {
                                  dc = "0" + day;
                              }
                              var mc = month;
                              if(mc<10) {
                                  mc = "0" + month;
                              }
                              var yc = String(year);
                              yc = yc.replace(/^.{2}/, '');
                              var dateformatc = dc + "." + mc + "." + yc;

                              if(schedule[options.routeFrom+options.routeTo]['calendar'][dateformatc]) {
                                  if(schedule[options.routeFrom+options.routeTo]['calendar'][dateformatc]['flight']) {
                                      class_name += ' ruswidget__calendar__list-days__cell_flight';
                                      class_name += ' ruswidget__calendar__list-days__cell_direct';
                                  }
                              }
                          }
                  } 
                  // Ячейка с датой
                  calendarHTML += '<div class="ruswidget__calendar__list-days__cell ' + class_name + '" day="' + day + '" month="' + month + '" year="' + year + '">' + day + '</div>';
                  day++;
              }
              // Конец строки таблицы
              if(index % 7 == 6) {
                  calendarHTML += '</div>';
              }
              index++;
          }
          while (index < final_index);
          calendarHTML += '</div>';

      calendarHTML += '</div>';

      container.innerHTML = calendarHTML;

      /**
       * Клик по навигации и дням
       */
      container.querySelector('.ruswidget__calendar__navigation_left').addEventListener('click', function (e) {
          drawCalendar(this.getAttribute('month'), this.getAttribute('year'));
          e.stopPropagation();
      }, false);
      container.querySelector('.ruswidget__calendar__navigation-to-this-day').addEventListener('click', function (e) {

      }, false);
      container.querySelector('.ruswidget__calendar__navigation_right').addEventListener('click', function (e) {
          drawCalendar(this.getAttribute('month'), this.getAttribute('year'));
          e.stopPropagation();
      }, false);



      var all_td_day_active = document.querySelectorAll('.ruswidget__calendar__list-days__cell:not(.ruswidget__calendar__list-days__cell_nochooise)'),
          ind_td_day_active, btn_td_day_active;

      for(ind_td_day_active = 0; ind_td_day_active < all_td_day_active.length; ind_td_day_active++) {
          btn_td_day_active = all_td_day_active[ind_td_day_active];
          btn_td_day_active.addEventListener('click', function (e) {
              selectDate(e.target.getAttribute('day'), e.target.getAttribute('month'), e.target.getAttribute('year'));

              // rusWidget.getParent("#"+container.getAttribute("id"), ".ruswidget__i").classList.remove("ruswidget__i_active");
              // if(container === rusWidget.calendarFromBox && rusWidget.options.dateTo.value) {
              //     if(rusWidget.options.dateFrom.value > rusWidget.options.dateTo.value)
              //         rusWidget.selectDate(e.target.getAttribute('day'), e.target.getAttribute('month'), e.target.getAttribute('year'), rusWidget.calendarTo);
              // }
              // if(container === rusWidget.calendarToBox && rusWidget.options.dateFrom.value) {
              //     if(rusWidget.options.dateFrom.value > rusWidget.options.dateTo.value)
              //         rusWidget.selectDate(e.target.getAttribute('day'), e.target.getAttribute('month'), e.target.getAttribute('year'), rusWidget.calendarFrom);
              // }
              
              e.stopPropagation();
          }, false);
      }
  }
  // выбор даты в календаре
  function selectDate(day, month, year) {
      if(day<10) day = '0' + day;
      if(month<10) month = '0' + month;
      let d = new Date(year, (month-1), day);

      let val = day+""+month+""+year;
      options.dateFrom.value = val;

      let dayShort = d.getDay()-1;
      if(d.getDay()==0) {
          dayShort = 6;
      }
      input.value = day+"/"+month+"/"+year;

      $("label[for='order_attrebute_date_0']").addClass("label-active");

      closeAllWindows();
  };


  function closeAllWindows() {
    $("#search-calendar").removeClass("search-calendar_active");
  }





    var DatePickerMonth;
    var DatePickerDay;
    if($('html').attr('lang')=='ru') {
        DatePickerMonth = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        DatePickerDay = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
    } else {
        DatePickerMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        DatePickerDay = ['Su','Mo','Tu','We','Th','Fr','Sa'];
    }
    $(".datepicker").datepicker({
		monthNames: DatePickerMonth,
		dayNamesMin: DatePickerDay,
		firstDay: 1,
        dateFormat: "dd/mm/yy",
		beforeShow: function(input, inst) {
	      inst.dpDiv.removeClass('custom').addClass($(input).data('dp-class'));
	    }
	});
    $('.glyphicon-date-ico').on('click', function(){
		$(this).siblings(".datepicker").datepicker("show");
	});
    $('#copyrow').on('click',function(){
		var copy = $('.copyrow:last').clone(false).insertAfter('.copyrow:last');
		$('body').find('.datepicker').removeClass("hasDatepicker").attr('id','').datepicker('destroy');
		var i=0;
		$('body').find('.datepicker').each(function (i,el) {
			$(el).attr("id",'date' + i).datepicker(
				{
					monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					firstDay: 1,
					dateFormat: "dd/mm/yy",
				}
			);
			i++;
		});
	})
    $('.copyrowas').on('click',function(){
		var copy = $('.copyrowasb:last').clone(false).insertAfter('.copyrowasb:last');
		$('body').find('.datepicker').removeClass("hasDatepicker").attr('id','').datepicker('destroy');
		var i=0;
		$('body').find('.datepicker').each(function (i,el) {
			$(el).attr("id",'date' + i).datepicker(
				{
					monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
					dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
					firstDay: 1,
					dateFormat: "dd/mm/yy",
				}
			);
			i++;
		});
	})
    $('#tabs-2 [name*="city_to"]').eq(0).on('change',function(){
		$('#tabs-2 [name*="city_from"]').eq(1).val($(this).val())
	});
    $('#tabs-2 [name*="city_from"]').eq(0).on('change',function(){
		$('#tabs-2 [name*="city_to"]').eq(1).val($(this).val())
	});
    $('#tabs-5 [name*="city_to"]').eq(0).on('change',function(){
		$('#tabs-5 [name*="city_from"]').eq(1).val($(this).val())
	});
    $('#tabs-5 [name*="city_from"]').eq(0).on('change',function(){
		$('#tabs-5 [name*="city_to"]').eq(1).val($(this).val())
	});












  let fromCode = null;
  let toCode = null;
  let cit = {};
  let mainCity;
  if(window.location.pathname!='/'){
    if(typeof from !== undefined) {if(from) {
      fromCode = from;
    }}
    if(typeof to !== undefined) {if(to) {
      toCode = to;
    }}
  }
  if($('html').attr('lang')=='ru') {
    mainCity = {
        "Москва": "Москва",
        "Санкт-Петербург": "Санкт-петербург",
        "Ницца": "Ницца",
        "Лондон": "Лондон",
        "Париж": "Париж",
        "Рига": "Рига",
        "Цюрих": "Цюрих",
        "Женева": "Женева",
        "Симферополь": "Симферополь",
        "Сочи": "Сочи",
        "Ламеция Терме": "Ламеция Терме",
        "Неаполь": "Неаполь",
        "Минск": "Минск",
        "Вена": "Вена",
        "Будапешт": "Будапешт",
        "Ларнака": "Ларнака",
        "Мюнхен": "Мюнхен",
        "Салоники": "Салоники",
        "Геленджик": "Геленджик",
        "Нур-султан": "Нур-султан",
        "Дубай": "Дубай",
        "Ереван": "Ереван",
        "Баку": "Баку",
        "Милан": "Милан",
        "Рим": "Рим",
        "Тбилиси": "Тбилиси",
        "Мадрид": "Мадрид",
    };
  } else {
    mainCity = {
        "Moscow": "Moscow",
        "St Petersburg": "St Petersburg",
        "Nice": "Nice",
        "London": "London",
        "Paris": "Paris",
        "Riga": "Riga",
        "Zurich": "Zurich",
        "Geneva": "Geneva",
        "Simferopol": "Simferopol",
        "Sochi": "Sochi",
        "Lamezia Terme": "Lamezia Terme",
        "Naples": "Naples",
        "Minsk": "Minsk",
        "Madrid": "Madrid",
        "Budapest": "Budapest",
        "Larnaca": "Larnaca",
        "Munich": "Munich",
        "Thessaloniki": "Thessaloniki",
        "Gelendzik": "Gelendzik",
        "Nur-sultan": "Nur-sultan",
        "Dubai": "Dubai",
        "Yerevan": "Yerevan",
        "Baku": "Baku",
        "Milan": "Milan",
        "Rome": "Rome",
        "Tbilisi": "Tbilisi",
        "Vienna": "Vienna",
    };
  }
  
  function createFormList(value, parent, tester) {
        var block = $(parent).find('.modal-city-it');
        $(block).html('');
        var active = 0;

        if(!routes) {
          routes = getRoutes();
        }

        $.each(routes, function(key, val) {
          if(parent.context.id == 'order_from') {
            let country = '';
            var append = '<div class="autocom autocom-airport">';
            for(let item in citiesM) {
              if(citiesM[item].codeen == key) {
                let city = citiesM[item].name.toLowerCase();
                city = city[0].toUpperCase() + city.slice(1);
                append += city + ', ';
                country = citiesM[item].country;
                break;
              }
            }
            contry = '';
            if(typeof countriesM[country] !== "undefined") {
              country = countriesM[country].name;
            }
            country = country.toLowerCase();
            country = string(country);
            append += country + '<span style="display: none;">, </span><span class="autocom__code">' + key + '</span>';
            append += '</div>';
            $(block).append(append);
          } else {
            if(fromCode) {
              if(fromCode == key) {
                for(let item in val) {
                  let country = '';
                  var append = '<div class="autocom autocom-airport">';
                  for(let el in citiesM) {
                    if(citiesM[el].codeen == item) {
                      let city = citiesM[el].name.toLowerCase();
                      city = city[0].toUpperCase() + city.slice(1);
                      append += city + ', ';
                      country = citiesM[el].country;
                      break;
                    }
                  }
                  contry = '';
                  if(typeof countriesM[country] !== "undefined") {
                    country = countriesM[country].name;
                  }
                  country = country.toLowerCase();
                  country = string(country);
                  append += country + '<span style="display: none;">, </span><span class="autocom__code">' + item + '</span>';
                  append += '</div>';
                  $(block).append(append);
                }
              }
            } else {
              let country = '';
              var append = '<div class="autocom autocom-airport">';
              for(let item in citiesM) {
                if(citiesM[item].codeen == key) {
                  let city = citiesM[item].name.toLowerCase();
                  city = city[0].toUpperCase() + city.slice(1);
                  append += city + ', ';
                  country = citiesM[item].country;
                  break;
                }
              }
              country = countriesM[country].name;
              country = country.toLowerCase();
              country = string(country);
              append += country + '<span style="display: none;">, </span><span class="autocom__code">' + key + '</span>';
              append += '</div>';
              $(block).append(append);
            }
          }
        });
  }


  var mobileElementInput;
  var typeField;
  if($('body').hasClass('mobile')) {
        $('.mobile .departure-inp input[type="text"], .mobile .arrival-inp input[type="text"]').each(function(){
            $(this).attr('disabled', 'disabled');
        });
  }
  $('.mobile .departure-inp, .mobile .arrival-inp').click(function(){
        mobileElementInput = $(this);
        typeField = $(this).find('.input-label').text();
        $('input[type="text"]').blur();
        openChooseCity();
  });
  $('body').on('click', '.mobile-block-city-close', function(){
      removeFixedWindow();
      $('.mobile-block-city').remove();
  });
  $(document).on("input", "#input-mobile-list", function(ev){
        $(".mobile-block-city .modal-city-it").html('');
        createFormList($(this).val(), $(".mobile-block-city-list"), false);
  });
  $('body').on('click', '.mobile-block-city .autocom', function(e){
      if($(e.target).hasClass('autocom')) {
        var text = $(e.target).text();
      } else {
        var text = $(e.target).parents('.autocom').text();
      }
      if(text) {
        mobileElementInput.find('.mof-input-item.city').val(text);
        mobileElementInput.find('.input-label').addClass('label-active');
      }
      removeFixedWindow();
      $('.mobile-block-city').remove();
      if(typeField=='Откуда') {
        mobileElementInput = mobileElementInput.next();
        typeField = mobileElementInput.find('.input-label').text();
        openChooseCity();
      } else {
        mobileElementInput = mobileElementInput.next();
        mobileElementInput.find('.mof-input-item').focus().click();
      }
  })
  $('.mof-input-item.mof-input-item_date').focus(function(){
      var lab = $(this).parents('.mof-input').find('.input-label');
      setTimeout(function(){
        lab.addClass('label-active');
      }, 50);
  });
  
  function openChooseCity() {
        $('body').append('<div class="mobile-block-city"><div class="mobile-block-city-close"></div><div class="mobile-block-city-search"><input type="text" class="mof-input-item" id="input-mobile-list" placeholder="'+typeField+'" autocomplete="off"></div><div class="mobile-block-city-list"><div class="modal-city-it"></div></div></div>');
        setTimeout(function() {
            $('.mobile-block-city').addClass('mobile-block-city__active');
        }, 100);
        createFormList("", $(".mobile-block-city-list"), false);
        fixedWindow();
  }
  function fixedWindow() {
      $('body').addClass('fancybox-active compensate-for-scrollbar');
  }
  function removeFixedWindow() {
      $('body').removeClass('fancybox-active compensate-for-scrollbar');
  }
  function mobileScrollToForm() {
    // if($('body').hasClass('mobile')) {
    //     var formPosition = $('.main-order-block').offset().top;
    //     $('html, body').animate({scrollTop: formPosition}, 500);
    // }
  }

  $('.mof-input-item.pass, .mof-input-item.datepicker').focus(function(){
    mobileScrollToForm();
  });

  $('.city').blur(function() {
    var cit = $(this);
    setTimeout(function() {
        cit.next().removeClass('modal-city-to_active');
        cit.next().removeClass('modal-city-from_active');
    }, 100);
  });
  $("body").click(function(e){
      if($(e.target).hasClass('city')) {
        $('.modal-city-to_active').removeClass('modal-city-to_active');
        $('.modal-city-from_active').removeClass('modal-city-from_active');
        var id = $(e.target).attr('id');
        if(id == 'order_to' || id == 'order_to2' || id == 'order_to3') {
            $(e.target).parents('.mof-input').find('.modal-city-it').addClass('modal-city-to_active');
        } else {
            $(e.target).parents('.mof-input').find('.modal-city-it').addClass('modal-city-from_active');
        }
        mobileScrollToForm();
      } else if($(e.target).hasClass('arrival-inp') || $(e.target).hasClass('departure-inp')) {
        $('.modal-city-to_active').removeClass('modal-city-to_active');
        $('.modal-city-from_active').removeClass('modal-city-from_active');
        var id = $(e.target).find('.city').attr('id');
        if(id == 'order_to' || id == 'order_to2' || id == 'order_to3') {
            $(e.target).find('.modal-city-it').addClass('modal-city-to_active');
        } else {
            $(e.target).find('.modal-city-it').addClass('modal-city-from_active');
        }
        mobileScrollToForm();
      } else if($(e.target).hasClass('autocom') || $(e.target).hasClass('modal-city-it')) {
      
      } else {
        $('.modal-city-to_active').removeClass('modal-city-to_active');
        $('.modal-city-from_active').removeClass('modal-city-from_active');
      }
  });
  // keyboard events
  $(document).keyup(function(e) {
      if (e.keyCode === 27) { // esc
          $('.modal-city-from_active').removeClass('modal-city-from_active');
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
      if (e.keyCode === 40) { // down arrow
          var block;
          if($('.modal-city-from').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from');
          } else if($('.modal-city-to').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to');
          } else if($('.modal-city-from2').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from2');
          } else if($('.modal-city-to2').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to2');
          } else if($('.modal-city-from3').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from3');
          } else if($('.modal-city-to3').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to3');
          }
          if(block) {
              var active = block.find('.autocom_active');
              var next = active.nextAll('.autocom:first');
              if(next.length == 0) {
                  next = block.find('.autocom:first');
              }
              active.removeClass('autocom_active');
              next.addClass('autocom_active');
          }
      }
      if (e.keyCode === 38) { // up arrow
          var block;
          if($('.modal-city-from').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from');
          } else if($('.modal-city-to').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to');
          } else if($('.modal-city-from2').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from2');
          } else if($('.modal-city-to2').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to2');
          } else if($('.modal-city-from3').hasClass('modal-city-from_active')) {
              block = $('.modal-city-from3');
          } else if($('.modal-city-to3').hasClass('modal-city-to_active')) {
              block = $('.modal-city-to3');
          }
          if(block) {
              var active = block.find('.autocom_active');
              var prev = active.prevAll('.autocom:first');
              if(prev.length == 0) {
                  prev = block.find('.autocom:last');
              }
              active.removeClass('autocom_active');
              prev.addClass('autocom_active');
          }
      }
      if (e.keyCode === 13) { // enter
          if($('.modal-city-from').hasClass('modal-city-from_active')){
              $('#order_from').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to').hasClass('modal-city-to_active')){
              $('#order_to').val($('.modal-city-to_active .autocom_active').text());
          }
          if($('.modal-city-from2').hasClass('modal-city-from_active')){
              $('#order_from2').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to2').hasClass('modal-city-to_active')){
              $('#order_to2').val($('.modal-city-to_active .autocom_active').text());
          }
          if($('.modal-city-from3').hasClass('modal-city-from_active')){
              $('#order_from3').val($('.modal-city-from_active .autocom_active').text());
          }
          if($('.modal-city-to3').hasClass('modal-city-to_active')){
              $('#order_to3').val($('.modal-city-to_active .autocom_active').text());
          }
          $('.modal-city-from_active').removeClass('modal-city-from_active');
          $('.modal-city-to_active').removeClass('modal-city-to_active');
      }
  });
  
  $('body').on('mousedown', '.modal-city-from .autocom', function(){
      var air = $(this).text();
      fromCode = $(this).find(".autocom__code").text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from').val(air);
      options.routeFrom = fromCode;
  });
  $('body').on('mousedown', '.modal-city-to .autocom', function(){
      var air = $(this).text();
      toCode = $(this).find(".autocom__code").text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to').val(air);
      options.routeTo = toCode;
  });
  $('body').on('mousedown', '.modal-city-from2 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from2').val(air);
  });
  $('body').on('mousedown', '.modal-city-to2 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to2').val(air);
  });
  $('body').on('mousedown', '.modal-city-from3 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-from_active').removeClass('modal-city-from_active');
      $('#order_from3').val(air);
  });
  $('body').on('mousedown', '.modal-city-to3 .autocom', function(){
      var air = $(this).text();
      $(this).parents('.modal-city-to_active').removeClass('modal-city-to_active');
      $('#order_to3').val(air);
  });

  $('body').on('click', '.mof-btn', function(e){
    e.preventDefault();
    var check = 0;
    $('.main-order-block .main-order-block__form:not(.display-none) .required').each(function(){
        if(!$(this).val()) {
            $(this).focus();
            $(this).parents('.mof-input').find('.input-label').addClass('input-label__error ');
            check = 1;
            return false;
        }
    });
    if(!check) {
        let date = $("#order_attrebute_date_0").val();
        date = date.split("/");
        let pass = $("#add_pass").val();
        let link = window.location.protocol + "//" + window.location.hostname + "/results/a"+fromCode+"a"+toCode+"a"+date[0]+date[1]+date[2]+"a"+pass+"/";
        document.location.href = link;
    }
  });
  
  
  
  
  
  
  $(document).on("focus", "#order_from", function(ev){
    $(".modal-city-from").html('');
    $(".modal-city-from").addClass('modal-city-from_active');
    $("#order_from").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_from", function(ev){
      $(".modal-city-from").html('');
      $(".modal-city-from").addClass('modal-city-from_active');
      $("#order_from").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });

  $(document).on("focus", "#order_to", function(ev){
    $(".modal-city-to").html('');
    $(".modal-city-to").addClass('modal-city-to_active');
    $("#order_to").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_to", function(ev){
      $(".modal-city-to").html('');
      $(".modal-city-to").addClass('modal-city-to_active');
      $("#order_to").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });

  $(document).on("focus", "#order_from2", function(ev){
      $(".modal-city-from2").html('');
      $(".modal-city-from2").addClass('modal-city-from_active');
      $("#order_from2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
      createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_from2", function(ev){
      $(".modal-city-from2").html('');
      $(".modal-city-from2").addClass('modal-city-from_active');
      $("#order_from2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });

  $(document).on("focus", "#order_to2", function(ev){
    $(".modal-city-to2").html('');
    $(".modal-city-to2").addClass('modal-city-to_active');
    $("#order_to2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_to2", function(ev){
      $(".modal-city-to2").html('');
      $(".modal-city-to2").addClass('modal-city-to_active');
      $("#order_to2").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });

  $(document).on("focus", "#order_from3", function(ev){
    $(".modal-city-from3").html('');
    $(".modal-city-from3").addClass('modal-city-from_active');
    $("#order_from3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_from3", function(ev){
      $(".modal-city-from3").html('');
      $(".modal-city-from3").addClass('modal-city-from_active');
      $("#order_from3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });

  $(document).on("focus", "#order_to3", function(ev){
    $(".modal-city-to3").html('');
      $(".modal-city-to3").addClass('modal-city-to_active');
      $("#order_to3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    createFormList($(this).val(), $(this).parent(), true);
  });
  $(document).on("input", "#order_to3", function(ev){
      $(".modal-city-to3").html('');
      $(".modal-city-to3").addClass('modal-city-to_active');
      $("#order_to3").parents('.mof-input').find('.input-label').removeClass('input-label__error');
    //   if($(this).val().length > 1) {
          createFormList($(this).val(), $(this).parent(), false);
    //   }
  });
  
  
  
  
  
  var mr = true;
  var kr = true;
  var cr = true;
  $(".list-var__i").click(function() {
    $(this).toggleClass("list-var__i_active");
    if($(".list-var__i_e").hasClass("list-var__i_active")) {
      mr = true;
    } else {
      mr = false;
    }
    if($(".list-var__i_r").hasClass("list-var__i_active")) {
      kr = true;
    } else {
      kr = false;
    }
    if($(".list-var__i_t").hasClass("list-var__i_active")) {
      cr = true;
    } else {
      cr = false;
    }

    $(".vydacha-items__i").each(function(){
      if(!mr && !kr && !cr) {
        $(this).removeClass("vydacha-items__i_dis");
      } else {
        if($(this).attr("data-type")=="Shuttle") {
          if(!mr) {
            $(this).addClass("vydacha-items__i_dis");
          } else {
            $(this).removeClass("vydacha-items__i_dis");
          }
        } else if($(this).attr("data-type")=="EmptyLeg") {
          if(!kr) {
            $(this).addClass("vydacha-items__i_dis");
          } else {
            $(this).removeClass("vydacha-items__i_dis");
          }
        } else if($(this).attr("data-type")=="Pax charter") {
          if(!cr) {
            $(this).addClass("vydacha-items__i_dis");
          } else {
            $(this).removeClass("vydacha-items__i_dis");
          }
        }
      }
    });
  });
  
  
  
  
    $(function() {
        $.getJSON("https://ibe.sirius-aero.ru/results/search.txt", function( data ) {
            $.each(data, function(key, val) {
                cit['"'+key+'"'] = val;
            });
        });
    });
});