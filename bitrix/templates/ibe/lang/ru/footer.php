<?
$MESS['SITEMAP'] = 'Карта сайта';
$MESS['order_flight'] = 'Заказать рейс';
$MESS['form1'] = 'В одну сторону';
$MESS['form2'] = 'Туда-Обратно';
$MESS['form3'] = 'Несколько пересадок';
$MESS['form4'] = 'Откуда';
$MESS['form5'] = 'Куда';
$MESS['form6'] = 'Дата вылета';
$MESS['form7'] = 'Пассажиры';
$MESS['form8'] = 'ФИО';
$MESS['form9'] = 'Номер телефона';
$MESS['form10'] = 'E-mail';
$MESS['form11'] = 'Комментарий';
$MESS['form12'] = 'Отправить';
$MESS['form13'] = 'Добавить перелет';
$MESS['form14'] = 'Заказ частного самолета ежедневно 24/7';
$MESS['form15'] = 'Заказ частного самолета';
$MESS['form16'] = 'ежедневно 24/7';
$MESS['form17'] = 'Я даю свое согласие на Обработку <a href="/privacy-policy/" target="_blank">персональных данных</a>';
$MESS['PRIVACYLK'] = 'Политика конфиденциальности';

$MESS['FB_DEALS'] = 'Возвратный рейс';
$MESS['FB_SHUTTLE'] = 'Shuttle';
$MESS['FB_NAME'] = 'Ваше имя';
$MESS['FB_PHONE'] = 'Телефон';
$MESS['FB_EMAIL'] = 'Email';
$MESS['FB_COUNT'] = 'Количество пассажиров';
$MESS['FB_ORDER'] = 'Заказать';
$MESS['FB_SEND'] = 'Ваше сообщение отправлено.';


$MESS ['predpocht'] = 'Предпочтения';
$MESS ['predpochtsend'] = 'Добавить';
$MESS ['predpochtcancel'] = 'Отмена';


$MESS ['formsendname'] = 'Ваше имя';
$MESS ['formsendphone'] = 'Телефон';
$MESS ['formsendemail'] = 'Email';

$MESS ['formmessagethemes'] = 'Тема сообщения';
$MESS ['formmessagetext'] = 'Текст сообщения';


$MESS ['app1'] = 'Услуги Sirius Aero всегда под рукой';
$MESS ['app2'] = 'Возвратные рейсы.';
$MESS ['app3'] = 'Экономьте до 75% от стоимости обычного чартера.';
$MESS ['app4'] = 'Заказ чартера.';
$MESS ['app5'] = 'Чартерные авиаперевозки практически во все страны мира.';
$MESS ['app6'] = 'Уведомления о возвратных рейсах.';
$MESS ['app7'] = 'Открыть';
?>
