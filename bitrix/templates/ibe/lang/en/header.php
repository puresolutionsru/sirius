<?
$MESS['ZAKAZ'] = 'Flight order';
$MESS ['FLEET'] = "Fleet Sirius Aero";
$MESS ['FLEETUPR'] = "Managed by Sirius Aero";
$MESS ['FLEET_PARTNERS'] = "Fleet partners";
$MESS ['PRIVACY'] = "<div class='bold t-3'>Cookie Notice</div><div>We use cookies to offer you a better browsing experience.</div><div>View our <a class='b-cookies-link' target='_blank' href='/en/privacy-policy/'>Privacy Policy</a> for more information</div>";
$MESS ['PRIVACY_BUT'] = 'I accept';

// форма поиска
$MESS ['textfrom'] = 'Your personal assistant in the world of business aviation 24/7';
$MESS ['from'] = 'From';
$MESS ['to'] = 'To';
$MESS ['passengers'] = 'Passengers';
$MESS ['date'] = 'Date';
$MESS ['comebackfly'] = 'Round trip';
$MESS ['predpocht'] = 'Preferences';
$MESS ['transfer'] = 'Add a stop-over';
$MESS ['next'] = 'Next';
?>
