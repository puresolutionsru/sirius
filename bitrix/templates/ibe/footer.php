<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>

	</main>
	<!-- begin footer-->
	<footer class="footer">
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu",
			"menu",
			Array(
				"ALLOW_MULTI_SELECT" => "Y",
				"CHILD_MENU_TYPE" => "left",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => array(0=>"",),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "N",
				"CLASS_MENU" => '',
			)
		);?>
		<div class="footer__in cl">
			<div class="footer-cols footer-col2">
				<a class="footer__logo hidden-xs" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/img/logofoot.png" alt=""></a>
				<div class="copyrights">2007-<?= date(Y) ?> ®
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footercopy.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
				<a href="<?= SITE_DIR ?>sitemap/" class="link-sitemap"><?= GetMessage('SITEMAP') ?></a>
				<a href="<?= SITE_DIR ?>privacy-policy/" class="link-sitemap" style="margin-left:20px;"><?= GetMessage('PRIVACYLK') ?></a>
			</div>
			<div class="footer-cols footer-col1">
				<div class="b-partners">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footerpartners.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
			</div>
			<div class="footer-cols footer-col">
				<div class="b-info-order">
					<?$APPLICATION->IncludeFile(
						SITE_DIR.'/include/footerright.php',
						array(),
						array(
							"MODE"=>"html",
						)
					);?>
				</div>
			</div>
		</div>
	</footer>
	<!-- end footer-->
		<?
		$APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');
		?>
		<script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery-ui.js'></script>
	<?
    if($APPLICATION->GetCurPage() !== '/results_new/form/'){?>


        <script src='<?= SITE_TEMPLATE_PATH ?>/js/jquery.functions.js'></script><?
    }
    ?>

	<link href="/bitrix/templates/sirius/new_style.css" rel="stylesheet">

</body>
</html>
