<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<div class="aligncenter">
			<img
				src="<?= $arResult["DETAIL_PICTURE"]["SRC"]?>"
				width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"]?>"
				height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
				alt="<?= $arResult["DETAIL_PICTURE"]["ALT"]?>"
				title="<?= $arResult["DETAIL_PICTURE"]["TITLE"]?>"
				/>
		</div>
	<?endif?>
	<div class="main-w _mod10">
		<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
			<h1><span itemprop="name"><?=$arResult["NAME"]?></span></h1>
		<?endif;?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
			<div class="b-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
		<?endif;?>
		<span itemprop="articleBody"><?= $arResult["DETAIL_TEXT"];?></span>
    </div>
