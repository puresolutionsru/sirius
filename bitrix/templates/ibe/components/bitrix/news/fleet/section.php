<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);


$section = $arResult['VARIABLES']['SECTION_CODE'];
$section = CIBlockSection::GetList(array(), array('CODE' => $section))->GetNext();

if(!$section['IBLOCK_ID']) :
	global $APPLICATION;
        $APPLICATION->RestartBuffer();
        CHTTP::SetStatus("404 Not Found");
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/header.php");
        include($_SERVER["DOCUMENT_ROOT"]."/404.php");
        include($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/footer.php");
endif;

$elements = CIBlockElement::GetList(array("SORT"=>"ASC"), array('IBLOCK_ID' => $section['IBLOCK_ID'], 'SECTION_ID' => $section['ID'], 'ACTIVE' => 'Y'));
$elem = array();
$i = 0;
while($el = $elements->GetNextElement()) {
	$elem[$i]['FIELDS'] = $el->GetFields();
	$elem[$i]['PROP'] = $el->GetProperties();
	$i++;
}

$count = 0;
$i = 0;

// выбран ли модуль
$mod = false;
foreach($elem as $item) :
	if(isset($_GET[$item['FIELDS']["CODE"]])) :
		$mod = true;
	endif;
endforeach;

$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/slick/slick.min.js');
$APPLICATION->AddChainItem($section['NAME']);
?>

<div class="b-content single-flot">
	<!-- <h1><?= $section['NAME'] ?></h1> -->
	<div class="single-flot_t"><h1><?= $section['NAME'] ?></h1></div>
	<!-- <div class="single-flot_t"><p>.</p></div> -->
	<div class="single-flot__mod">
		<?
		foreach($elem as $item) :
			// Основная картинка самолета
			$img = $item['FIELDS']['DETAIL_PICTURE'];
			$img = CFile::GetPath($img);
			if($img) :
				if(isset($_GET[$item['FIELDS']["CODE"]]) or (!$mod and $i==0)) :
					echo '<img src="' . $img . '" alt="" class="single-flot__det-img _on" data-tab="#'.$item['FIELDS']["CODE"].'">';
					$count = $i;
				else :
					echo '<img src="' . $img . '" alt="" class="single-flot__det-img" data-tab="#'.$item['FIELDS']["CODE"].'">';
				endif;
			endif;
			$i++;
		endforeach;
		?>
		<div class="air-mod">
			<div class="air-mod__in">
				<?
				$i=0;
				// Табы
				foreach($elem as $item) :
					if(isset($_GET[$item['FIELDS']["CODE"]]) or (!$mod and $i==0)) :
						echo '<a href="#'.$item['FIELDS']["CODE"].'" class="_on"><div class="air-lk-d"><i></i>'.$item['FIELDS']["NAME"].'</div></a>';
					else :
						echo '<a href="#'.$item['FIELDS']["CODE"].'"><div class="air-lk-d"><i></i>'.$item['FIELDS']["NAME"].'</div></a>';
					endif;
					$i++;
				endforeach;
				?>
			</div>
		</div>
    </div>

	<? //INFO
	$i = 0;
	foreach($elem as $item) :
		$classmodel = '';
		CModule::IncludeModule("iblock");
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(
			$item['FIELDS']["IBLOCK_ID"], // ID инфоблока
			$item['FIELDS']["ID"] // ID элемента
		);
		$arElMetaProp = $ipropValues->getValues();
		if(isset($_GET[$item['FIELDS']["CODE"]]) or (!$mod and $i==0)) :
			$classmodel = '_on';
			$APPLICATION->SetPageProperty("title", $arElMetaProp['ELEMENT_META_TITLE']);
		endif;

		?>
		<div class="params-model <?=$classmodel?>" id="<?= $item['FIELDS']['CODE'] ?>">
			<div class="paramseo" style="display:none;";>
				<input type="hidden" name="hidtitle" value="<?= $arElMetaProp['ELEMENT_META_TITLE'] ?>">
			</div>
			<div class="aircraft-param">
		      <div class="aircraft-data cl">
		        <div class="aircraft-data__i">
		          <div class="sirius1 f-sirius aircraft-data__i__t"><?= GetMessage('VMEST') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['VMEST']['VALUE'] ?></div>
		        </div>
		        <div class="aircraft-data__i">
		          <div class="sirius2 f-sirius aircraft-data__i__t"><?= GetMessage('DAL') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['DAL']['VALUE'] ?></div>
		        </div>
		        <div class="aircraft-data__i">
		          <div class="sirius3 f-sirius aircraft-data__i__t"><?= GetMessage('OB') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['OB']['VALUE'] ?></div>
		        </div>
		        <div class="aircraft-data__i">
		          <div class="sirius4 f-sirius aircraft-data__i__t"><?= GetMessage('DLINA') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['DLINA']['VALUE'] ?></div>
		        </div>
		        <div class="aircraft-data__i">
		          <div class="sirius5 f-sirius aircraft-data__i__t"><?= GetMessage('SHIRINA') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['SHIRINA']['VALUE'] ?></div>
		        </div>
		        <div class="aircraft-data__i">
		          <div class="sirius6 f-sirius aircraft-data__i__t"><?= GetMessage('VYSOTA') ?></div>
		          <div class="aircraft-data__i__tx"><?= $item['PROP']['VYSOTA']['VALUE'] ?></div>
		        </div>
		      </div>
		    </div>

			<? if($item['PROP']['UPRAVLENIE']['VALUE']) : ?>
				<div class="b-info-upravlenie">
					<span><?= GetMessage('UPR') ?></span>
				</div>
			<? endif; ?>
				<? if($item['PROP']['PANORAMS']['VALUE']) : ?>
					<div class="aircraft-3d" id="aircraft3d<?= $item['FIELDS']['CODE'] ?>">
					<div class="aircraft-3d__link" data-link="/panorams/<?= $item['PROP']['PANORAMS']['VALUE'] ?>"></div>
					<iframe class="frame-par" src="">
						Ваш браузер не поддерживает плавающие фреймы!
					</iframe>
					</div>
				<? endif; ?>

				<? if(!$item['PROP']['PANORAMS']['VALUE']) : ?>
					<div class="aircraft-config <? if(!$item['PROP']['VMEST_N']['VALUE']) : echo 'aircraft-config_min'; endif; ?>">
						<div class="aircraft-config__row aircraft-config__t cl">
							<? if($item['PROP']['VMEST_N']['VALUE']) : ?>
								<div class="aircraft-config__i"><span class="aircraft-config__i__t _on"><?= GetMessage('D_CONFIG_S') ?></span></div>
								<div class="aircraft-config__i"><span class="aircraft-config__i__t"><?= GetMessage('N_CONFIG_S') ?></span></div>
							<? else : ?>
								<div class="aircraft-config__i aircraft-config__i_one"><span class="aircraft-config__i__t _on"><?= GetMessage('M_CONFIG_S') ?></span></div>
							<? endif ?>
						</div>
						<div class="aircraft-config__row aircraft-config__m">
							<?
							$img = $item['PROP']['VMEST_D_PIC']['VALUE'];
							$img = CFile::GetPath($img);
							?>
							<div class="aircraft-config__i _on <? if(!$item['PROP']['VMEST_N']['VALUE']) : echo 'aircraft-config__i_full-line'; endif; ?>">
							<div class="aircraft-config__i_imgw"><img class="aircraft-config__i_img" src="<?= $img ?>" alt=""></div>
							<div class="aircraft-config__i__info"> <span class="f-sirius sirius1"><?= GetMessage('VMEST_CONFIG') ?></span>
								<div class="aircraft-config__i__info__number"><?= $item['PROP']['VMEST_D']['VALUE'] ?></div>
							</div>
							</div>
							<? if($item['PROP']['VMEST_N']['VALUE']) : ?>
								<?
								$img = $item['PROP']['VMEST_N_PIC']['VALUE'];
								$img = CFile::GetPath($img);
								?>
								<div class="aircraft-config__i">
								<img class="aircraft-config__i_img" src="<?= $img ?>" alt="">
								<div class="aircraft-config__i__info _mod1"><span class="f-sirius sirius1"><?= GetMessage('VMEST_CONFIG') ?></span>
									<div class="aircraft-config__i__info__number"><?= $item['PROP']['VMEST_N']['VALUE'] ?></div>
								</div>
								</div>
							<? endif; ?>
						</div>
					</div>
				<? endif; ?>
				<?
				// Begin Gallery
				$gallery = $item['PROP']['GALLERY'];
				if($gallery['VALUE']) : ?>
					<div class="b-gallery _mod1">
						<?foreach($gallery['VALUE'] as $img_id) :
							$img = CFile::GetPath($img_id);
							$imgmini = CFile::ResizeImageGet($img_id, Array("width" => '627', "height" => '425'), BX_RESIZE_IMAGE_EXACT);
							?>
							<a href="<?= $img ?>" class="fancybox" data-fancybox="group-gal"><img src="<?= $imgmini['src'] ?>" alt=""></a>
						<?endforeach;?>
					</div>
				<?endif;
				// End Gallery
			// endif;

		$i++;
		echo '</div>';
	endforeach; ?>

















			<div class="aircraft-calc" id="aircraftcalc">
			  	<div class="aircraft-calc__t t-1"><?= GetMessage('FORM_TITLE') ?></div>
				<div style="padding: 0 !important;" class="form-order" id="formtabfleet">
					<div class="forms-order-wrap">
						<div class="tabs">
							<ul class="nav-tabs">
								<li class="nav">
									<a href="#tabs-4"><span class="glyphicon glyphicon-arrow-right"></span><?= GetMessage('form1') ?></a>
								</li>
								<li class="nav">
									<a href="#tabs-5"><span class="glyphicon glyphicon-transfer"></span><?= GetMessage('form2') ?></a>
								</li>
								<li class="nav">
									<a href="#tabs-6"><span class="glyphicon glyphicon-retweet"></span><?= GetMessage('form3') ?></a>
								</li>
							</ul>

							<div class="tab-content">
								<!-- Tab panes -->
								<div class="tab-pane" id="tabs-4">
									<form method="POST" class="ajax_order_big1 custom-validate">
										<input name="title" value="Заявка на подбор самолета в одну сторону" type="hidden"/>
										<input name="id_addresses" value="347" type="hidden"/>
										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form4') ?></label>
															<input type="text" class="form-control" name="city_to" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form5') ?></label>
															<input type="text" class="form-control" name="city_from" required="required"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-row">
													<div class="col-8">
														<div class="form-group">
															<label><?= GetMessage('form6') ?></label>
															<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date"/>
															<span class="glyphicon glyphicon-date-ico"></span>
														</div>
													</div>
													<div class="col-4">
														<div class="form-group">
															<label><?= GetMessage('form7') ?></label>
															<input type="text" class="form-control" name="person_count" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form8') ?></label>
															<input type="text" class="form-control" name="person_name" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form9') ?></label>
															<input type="text" name="person_phone" required="required" class="input_phone form-control"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label><?= GetMessage('form10') ?></label>
													<input type="text" class="form-control" name="person_email"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-12" style="padding-left:15px;padding-right:15px;">
												<div class="form-group">
													<label><?= GetMessage('form11') ?></label>
													<input type="text" class="form-control" name="comment"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-4">
												<div class="form-group">
													<label>CAPTCHA</label>
													<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group" style="padding-top: 20px;">
													<input type="checkbox" class="" name="sogl" id="sogl3"/>
													<label for="sogl3" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label> </label>
													<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="tab-pane" id="tabs-5">
									<form method="POST" class="ajax_order_big2 custom-validate">
										<input name="title" value="Заявка на подбор самолета туда-обратно" type="hidden"/>
										<input name="id_addresses" value="347" type="hidden"/>
										<div class="h4"><?= GetMessage('form4') ?></div>
										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form4') ?></label>
															<input type="text" class="form-control" name="city_to[]" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form5') ?></label>
															<input type="text" class="form-control" name="city_from[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-row">
													<div class="col-8">
														<div class="form-group">
															<label><?= GetMessage('form6') ?></label>
															<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
															<span class="glyphicon glyphicon-date-ico"></span>
														</div>
													</div>
													<div class="col-4">
														<div class="form-group">
															<label><?= GetMessage('form7') ?></label>
															<input type="text" class="form-control" name="person_count[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="h4"><?= GetMessage('form5') ?></div>
										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form4') ?></label>
															<input type="text" class="form-control" name="city_to[]" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form5') ?></label>
															<input type="text" class="form-control" name="city_from[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-row">
													<div class="col-8">
														<div class="form-group">
															<label><?= GetMessage('form6') ?></label>
															<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
															<span class="glyphicon glyphicon-date-ico"></span>
														</div>
													</div>
													<div class="col-4">
														<div class="form-group">
															<label><?= GetMessage('form7') ?></label>
															<input type="text" class="form-control" name="person_count[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form8') ?></label>
															<input type="text" class="form-control" name="person_name" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label> <?= GetMessage('form9') ?> </label>
															<input type="text" name="person_phone" required="required" class="input_phone form-control"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label> <?= GetMessage('form10') ?> </label>
													<input type="text" class="form-control" name="person_email"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-12" style="padding-left:15px;padding-right:15px;">
												<div class="form-group">
													<label><?= GetMessage('form11') ?></label>
													<input type="text" class="form-control" name="comment"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-4">
												<div class="form-group">
													<label>CAPTCHA</label>
													<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group" style="padding-top: 20px;">
													<input type="checkbox" class="" name="sogl" id="sogl3"/>
													<label for="sogl3" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label> </label>
													<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
												</div>
											</div>
										</div>
									</form>
								</div>

								<div class="tab-pane" id="tabs-6">
									<form method="POST" class="ajax_order_big3 custom-validate">
										<input name="title" value="Заявка на подбор самолета с несколькими пересадками" type="hidden"/>
										<input name="id_addresses" value="347" type="hidden"/>
										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form4') ?></label>
															<input type="text" class="form-control" name="city_to[]" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form5') ?></label>
															<input type="text" class="form-control" name="city_from[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-row">
													<div class="col-8">
														<div class="form-group">
															<label><?= GetMessage('form6') ?></label>
															<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
															<span class="glyphicon glyphicon-date-ico"></span>
														</div>
													</div>
													<div class="col-4">
														<div class="form-group">
															<label><?= GetMessage('form7') ?></label>
															<input type="text" class="form-control" name="person_count[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="form-row copyrowasb">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form4') ?></label>
															<input type="text" class="form-control" name="city_to[]" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label><?= GetMessage('form5') ?></label>
															<input type="text" class="form-control" name="city_from[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-row">
													<div class="col-8">
														<div class="form-group">
															<label><?= GetMessage('form6') ?></label>
															<input type="text" placeholder="<?= date('d/m/Y') ?>" class="datepicker form-control date" data-dp-class="custom" name="date[]"/>
															<span class="glyphicon glyphicon-date-ico"></span>
														</div>
													</div>
													<div class="col-4">
														<div class="form-group">
															<label><?= GetMessage('form7') ?></label>
															<input type="text" class="form-control" name="person_count[]" required="required"/>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="btn btn-addMore copyrowas">
											<?= GetMessage('form13') ?>
										</div>
										<br/>
										<br/>


										<div class="form-row">
											<div class="col-8">
												<div class="form-row">
													<div class="col-6">
														<div class="form-group">
															<label> <?= GetMessage('form8') ?> </label>
															<input type="text" class="form-control" name="person_name" required="required"/>
														</div>
													</div>

													<div class="col-6">
														<div class="form-group">
															<label> <?= GetMessage('form9') ?> </label>
															<input type="text" name="person_phone" required="required" class="input_phone form-control"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label> <?= GetMessage('form10') ?> </label>
													<input type="text" class="form-control" name="person_email"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-12" style="padding-left:15px;padding-right:15px;">
												<div class="form-group">
													<label><?= GetMessage('form11') ?></label>
													<input type="text" class="form-control" name="comment"/>
												</div>
											</div>
										</div>

										<div class="form-row">
											<div class="col-4">
												<div class="form-group">
													<label>CAPTCHA</label>
													<div class="g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group" style="padding-top: 20px;">
													<input type="checkbox" class="" name="sogl" id="sogl3"/>
													<label for="sogl3" style="display: inline; white-space: normal;"><?= GetMessage('form17') ?></label>
												</div>
											</div>
											<div class="col-4">
												<div class="form-group">
													<label> </label>
													<button class="btn btn-send" disabled="disabled"><?= GetMessage('form12') ?></button>
												</div>
											</div>
										</div>
									</form>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="aircraft-calc" id="aircraftcalc">
		      <div class="aircraft-calc__t t-1"><?= GetMessage('FORM_TITLE') ?></div>
		      <form class="form cl">
		        <div class="aircraft-calc__radio">
		          <input id="onestr" type="radio" name="str" value="<?= GetMessage('FORM_TAB1') ?>" checked>
		          <label for="onestr"><?= GetMessage('FORM_TAB1') ?></label>
		          <input id="twostr" type="radio" name="str" value="<?= GetMessage('FORM_TAB2') ?>">
		          <label for="twostr"><?= GetMessage('FORM_TAB2') ?></label>
		          <input id="thstr" type="radio" name="str" value="<?= GetMessage('FORM_TAB3') ?>">
		          <label for="thstr"><?= GetMessage('FORM_TAB3') ?></label>
		        </div>
		        <input class="it" type="text" placeholder="<?= GetMessage('FORM_IN1') ?>">
		        <input class="it" type="text" placeholder="<?= GetMessage('FORM_IN2') ?>">
		        <input class="it _mod1" type="text" placeholder="<?= GetMessage('FORM_IN3') ?>">
		        <input class="it _mod1 _mod2" type="text" placeholder="<?= GetMessage('FORM_IN4') ?>">
		        <input class="it" type="text" placeholder="<?= GetMessage('FORM_IN5') ?>">
		        <input class="it" type="text" placeholder="<?= GetMessage('FORM_IN6') ?>">
		        <input class="is" type="submit" value="<?= GetMessage('FORM_INB') ?>">
		      </form>
		    </div> -->

			<div class="aircraft-slider">
		      <!-- <div class="aircraft-slider__t t-2">Флот</div> -->
		      <div class="aircraft-slider__tabs flot-tabs">
				  <!-- <span class="_on">Флот Sirius Aero</span>
				  <span>Флот партнеров</span> -->
				  <span class="_on _mod1 _mod2"><?= GetMessage('FLEET') ?></span><span class="_mod1"><?= GetMessage('FLEET_PARTNERS') ?></span>
			  </div>
			  <div class="aircraft-slider__items aircraft-slider__items1 _on">
				  <?
				  $infoblock = $arParams["IBLOCK_ID"];
				  $sct_el = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, '!=SECTION_ID' => '0', 'ACTIVE' => 'Y'));
				  while($ar_sct_el = $sct_el->GetNext()) {
					  	$img_src = CFile::GetPath($ar_sct_el['PICTURE']);
					  	?>
					  	<a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>">
							<img src="<?= $img_src ?>">
				          	<div class="aircraft-slider__items__t"><?= $ar_sct_el['NAME'] ?></div>
						</a>
				  <?}?>
			  </div>
		      <div class="aircraft-slider__items aircraft-slider__items2">
				  <?
				  $sct_el = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $infoblock, '!=SECTION_ID' => '0', 'ACTIVE' => 'Y'));
				  while($ar_sct_el = $sct_el->GetNext()) {
					  	$img_src = CFile::GetPath($ar_sct_el['PICTURE']);
					  	?>
					  	<a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>">
							<img src="<?= $img_src ?>">
				          	<div class="aircraft-slider__items__t"><?= $ar_sct_el['NAME'] ?></div>
						</a>
				  <?}?>
			  </div>
		    </div>
</div>
