<?
$MESS ['SEARCH_LABEL'] = "Search:";
$MESS ['D_CONFIG_S'] = "Daily interior configuration";
$MESS ['N_CONFIG_S'] = "Night cabin configuration";
$MESS ['M_CONFIG_S'] = "Cabin configuration";
$MESS ['VMEST_CONFIG'] = "Passenger capacity";

$MESS ['VMEST'] = "Passenger <br>capacity";
$MESS ['DAL'] = "Range<br>of flight (km)";
$MESS ['OB'] = "Luggage compartment<br>volume (m3)";
$MESS ['DLINA'] = "Length of salon<br>(m)";
$MESS ['SHIRINA'] = "Width of salon<br>(m)";
$MESS ['VYSOTA'] = "Height of salon<br>(m)";
$MESS ['UPR'] = "The aircraft is managed by Sirius Aero";

$MESS ['FORM_TITLE'] = "Calculate the estimated cost of the flight";
$MESS ['FORM_TAB1'] = "One way";
$MESS ['FORM_TAB2'] = "There and back again";
$MESS ['FORM_TAB3'] = "Several transplants";

$MESS ['FORM_IN1'] = "Location:";
$MESS ['FORM_IN2'] = "Where:";
$MESS ['FORM_IN3'] = "Date:";
$MESS ['FORM_IN4'] = "Places:";
$MESS ['FORM_IN5'] = "Your name:";
$MESS ['FORM_IN6'] = "Phone or e-mail:";
$MESS ['FORM_INB'] = "Calculate the cost";

$MESS ['FORM_360_NAME'] = "Your name:";
$MESS ['FORM_360_PHONE'] = "Phone or e-mail:";
$MESS ['FORM_360_BUT'] = "Reserve now";

$MESS['form1'] = 'One way';
$MESS['form2'] = 'Round trip';
$MESS['form3'] = 'Several destinations';
$MESS['form4'] = 'Location';
$MESS['form5'] = 'Where';
$MESS['form6'] = 'Departure date';
$MESS['form7'] = 'Passengers';
$MESS['form8'] = 'Full name';
$MESS['form9'] = 'Telephone';
$MESS['form10'] = 'E-mail';
$MESS['form11'] = 'Comment';
$MESS['form12'] = 'Send';
$MESS['form13'] = 'Add flight';
$MESS['form14'] = 'Order a private jet daily 24/7';
$MESS['form15'] = 'Order a private jet';
$MESS['form16'] = 'daily 24/7';
$MESS['form17'] = 'I consent to the processing of <a href="/en/privacy-policy/" target="_blank">my personal data</a>';
?>
