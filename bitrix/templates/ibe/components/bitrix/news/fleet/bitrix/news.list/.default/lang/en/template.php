<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["VMEST"] = "Passenger <br>capacity";
$MESS["DAL"] = "Range <br>of flight (km)";
$MESS["OB"] = "Luggage compartment <br>volume (m3)";
$MESS["DLINA"] = "Length of salon <br>(m)";
$MESS["SHIRINA"] = "Width of salon <br>(m)";
$MESS["VYSOTA"] = "Height of salon <br>(m)";
?>
