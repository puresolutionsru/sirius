<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="lang-tabs">
<?php if(stripos($_SERVER['REQUEST_URI'], '/en/') !== false) {?>
	<div class="lang-tabs__i"><span class="lang-tabs__link _active">EN</span></div>
	<div class="lang-tabs__i">
		<a class="lang-tabs__link" href="/">RU</a>
	</div>
<?} else {?>
	<div class="lang-tabs__i">
		<a class="lang-tabs__link" href="/en/">EN</a>
	</div>
	<div class="lang-tabs__i"><span class="lang-tabs__link _active">RU</span></div>
<?}?>
</div>
