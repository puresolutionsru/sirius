<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$frame = $this->createFrame()->begin("");
$all_link = '';
?>
<div class="b-smi">
  <div class="b-smi__t b-articles__t"><?= $arParams['PAGER_TITLE'] ?></div>
  <div class="b-smi__items">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<div class="b-smi__i">
			<?if($arItem["PREVIEW_PICTURE"]["SRC"]):?>
				<div class="b-smi__i__img b-articles__i__img">
					<img
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						>
				</div>
			<?endif;?>

			<div class="b-smi__i_cont">
				<? $arItem["DETAIL_PAGE_URL"] = str_replace('/en/en/', '/en/', $arItem["DETAIL_PAGE_URL"]);
      		$blank = false;
      		if($arItem["PROPERTIES"]["LINK_SITE"]["VALUE"]) {
      		    $blank = true;
							$arItem["DETAIL_PAGE_URL"] = $arItem["PROPERTIES"]["LINK_SITE"]["VALUE"];
				}?>

				<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N"):?>
					<div class="b-smi__i__tx"><a href="<?= $arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
				<?endif;?>
				
				<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
					<div class="b-smi__i__date b-articles__date"><?= $arItem["DISPLAY_ACTIVE_FROM"]?></div>
				<?endif;?>

				<div class="b-smi__i__lk"><a href="<?= $arItem["DETAIL_PAGE_URL"]?>" <?if($blank){echo "target='_blank'";}?>><?= GetMessage("LIST_MORE"); ?></a></div>
			</div>
		</div>
        <? $all_link = $arItem["LIST_PAGE_URL"]; ?>
        <? $all_link = str_replace('/en/en/', '/en/', $all_link); ?>
	<?endforeach;?>
  </div>
  <div class="b-smi__lk b-articles__lk"><a href="<?= $all_link ?>"><?= $arParams['PAGER_TITLE_ALL'] ?></a></div>
</div>
<? $frame->end(); ?>