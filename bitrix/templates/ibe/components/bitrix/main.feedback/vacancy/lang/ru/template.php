<?
$MESS ['MFT_VACANCY'] = "Должность";
$MESS ['MFT_NAME'] = "Имя, Фамилия";
$MESS ['MFT_PHONE'] = "Контактный номер телефона";
$MESS ['MFT_EMAIL'] = "Адрес эл.почты";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_FILE'] = "добавить файл с резюме";


$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
?>