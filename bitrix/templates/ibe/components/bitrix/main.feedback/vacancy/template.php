<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<div class="mform">
<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="form-send">
<?=bitrix_sessid_post()?>
	<h3>Оставьте вашу заявку</h3>
	<div class="mf-item mf-vacancy">
		<input type="text" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("VACANCY", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="user_vacancy" value="<?=$arResult["VACANCY"]?>" placeholder="<?=GetMessage("MFT_VACANCY")?>">
	</div>
	<div class="mf-item mf-name">
		<input type="text" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("NAME", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="user_name" value="<?=$arResult["AUTHOR_NAME"]?>" placeholder="<?=GetMessage("MFT_NAME")?>">
	</div>
	<div class="mf-item mf-phone">
		<input type="text" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("PHONE", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="user_phone" value="<?=$arResult["AUTHOR_PHONE"]?>" placeholder="<?=GetMessage("MFT_PHONE")?>">
	</div>
	<div class="mf-item mf-email">
		<input type="text" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("EMAIL", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="user_email" value="<?=$arResult["AUTHOR_EMAIL"]?>" placeholder="<?=GetMessage("MFT_EMAIL")?>">
	</div>
	<div class="mf-item mf-message">
		<textarea name="user_message" rows="20" cols="40" class="it <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("MESSAGE", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" placeholder="<?=GetMessage("MFT_MESSAGE")?>"><?=$arResult["MESSAGE"]?></textarea>
	</div>
	<div class="mf-item mf-file">
		Резюме: <input type="file" class=" <?if(empty($arParams["REQUIRED_FIELDS"]) || in_array("FILE", $arParams["REQUIRED_FIELDS"])):?>required<?endif?>" name="user_file" style="width:300px;">
	</div>
	
	
	
	
	
	<div class="mf-item mf-captcha mf-two-col">
		<?if($arParams["USE_CAPTCHA"] == "Y"):?>
			<div class="mf-text"><?=GetMessage("MFT_CAPTCHA")?></div>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["capCode"]?>">
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["capCode"]?>" width="180" height="40" alt="CAPTCHA">
			<div class="mf-text"><?=GetMessage("MFT_CAPTCHA_CODE")?><span class="mf-req">*</span></div>
			<input type="text" name="captcha_word" size="30" maxlength="50" value="">
		<?endif;?>

		<?if($arParams["USE_CAPTCHA_G"] == "Y"):?>
			<div class="mf-two-col_i g-recaptcha" data-sitekey="6LeWrHUUAAAAAH18xqBOAjwoSN1WmZhdgazG0sTq"></div>
		<?endif;?>

		<div class="mf-two-col_i mf-right-flex">
			<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
			<input type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>">
		</div>
	</div>
</form>
</div>

<script>
	var files;

	$('input[type=file]').change(function(){
		files = this.files;
	});




	$(".form-send").submit(function(e){
		e.preventDefault();
		var error = false;
		$('.form-send .it__error').removeClass('it__error');
		$('.form-send .required').each(function(){
			if($(this).val() == '') {
				error = true;
				$(this).addClass('it__error');
			}
		});
		if(error) {
			return false;
		}

		var data = new FormData();
		$.each(files, function( key, value ){
			data.append( key, value );
		});
		$.ajax({
			url: '/webforms/vakansii/upload.php?uploadfiles',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function( respond, textStatus, jqXHR ){
				// if(respond.error === 'undefined'){
					// Файлы успешно загружены, делаем что нибудь здесь
					// выведем пути к загруженным файлам в блок '.ajax-respond'
					var files_path = respond.files;
					// var html = '';
					// $.each( files_path, function( key, val ) { 
					// 	html += val; 
					// });

					var formData = $(".form-send").serialize();
					formData = formData + '&file=' + files_path;
					$.ajax({
						type: "POST",
						data: formData,
						dataType: "html",
						url: '/webforms/vakansii/',
						beforeSend: function() {

						},
						success: function(data) {
							var data = JSON.parse(data);
							if(data.success == 'succes-send_message'){
								$.fancybox.open(
									'<div style="text-align: center;"><p>Ваше резюме отправлено.</p></div>'
								);
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							
						}
					});
				// }
			},
			error: function( jqXHR, textStatus, errorThrown ){
			}
		});
	});
</script>