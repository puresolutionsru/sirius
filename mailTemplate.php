<?
$messageHeader = '';
$messageFooter = '';

$messageHeader .= '<table id="m_-4050985802777539881bodyTable" style="border-collapse: collapse; height: 100%; margin: 0; padding: 0; width: 100%; background-color: #fafafa;" cellspacing="0" cellpadding="0" border="0" align="center">';
	$messageHeader .= '<tbody>';
		$messageHeader .= '<tr>';
			$messageHeader .= '<td id="m_-4050985802777539881bodyCell" style="height: 100%; margin: 0; padding: 10px; width: 100%; border-top: 0;" valign="top" align="center">';
				$messageHeader .= '<table style="background: white; width: 600px;" cellspacing="0" cellpadding="10" border="0">';
					$messageHeader .= '<tbody>';
						$messageHeader .= '<tr>';
							$messageHeader .= '<td style="background-color: #fafafa;    font-family: arial; font-size: 12px; color: #1a325a; text-align: center;">Пожалуйста, не отвечайте на это письмо. Рассылка осуществляется автоматически.</td>';
						$messageHeader .= '</tr>';
						$messageHeader .= '<tr>';
							$messageHeader .= '<td style=" text-align: center;  border-top: 1px solid #20b3ef;background: white;"><span style="width: 264px; height: 60px; display: inline-block; background: url(https://sirius-aero.ru/bitrix/templates/sirius/img/logo.jpg) no-repeat;"></span></td>';
						$messageHeader .= '</tr>';
						$messageHeader .= '<tr style="background: url(https://sirius-aero.ru/for-mails/bg2.png) no-repeat;">';
							$messageHeader .= '<td style=" height: 214px; text-align: center;"></td>';
						$messageHeader .= '</tr>';
						$messageHeader .= '<tr>';
							$messageHeader .= '<td style=" text-align: center; padding: 15px 30px">';
								$messageHeader .= '<table style="width: 100%; height: 100%; text-align: center; font-family: arial;">';
									$messageHeader .= '<tbody>';
										$messageHeader .= '<tr style="width: 100%; text-align: center;">';
											$messageHeader .= '<td style="font-family: arial; font-size: 16px; text-align: center; color: #00A8EC; height: 30px;">Здравствуйте!</td>';
										$messageHeader .= '</tr>';
										$messageHeader .= '<tr style="width: 100%; ">';
											$messageHeader .= '<td style="border-bottom: solid 1px #E6E6E6; height: 30px; padding-bottom: 20px;"><span style="font-size: 16px; color: #1a325a;"> Вы сделали заказ перелета на сайте <a href="https://sirius-aero.ru" style="color: #00A8EC; text-decoration:none;">sirius-aero.ru</a></span></td>';
										$messageHeader .= '</tr>';
									$messageHeader .= '</tbody>';
								$messageHeader .= '</table>';
							$messageHeader .= '</td>';
						$messageHeader .= '</tr>';





						$messageFooter .= '<tr>';
							$messageFooter .= '<td>';
								$messageFooter .= '<table style="font-family: arial; text-align: center;color: #1a325a; width: 100%;">';
									$messageFooter .= '<tbody>';
										$messageFooter .= '<tr>';
											$messageFooter .= '<td style="width: 100%; height: 76px; background:url(https://sirius-aero.ru/for-mails/support.png) center no-repeat; "></td>';
										$messageFooter .= '</tr>';
										$messageFooter .= '<tr>';
											$messageFooter .= '<td style=" font-size: 22px;  ">В ближайшее время<br> с Вами свяжется наш менеджер<br><br><br></td>';
										$messageFooter .= '</tr>';
										$messageFooter .= '<tr>';
											$messageFooter .= '<td style="border-top: solid 1px #E6E6E6;"><br>Спасибо, что выбрали Sirius Aero!<br><br></td>';
										$messageFooter .= '</tr>';
									$messageFooter .= '</tbody>';
								$messageFooter .= '</table>';
							$messageFooter .= '</td>';
						$messageFooter .= '</tr>';

						$messageFooter .= '<tr>';
							$messageFooter .= '<td style="background: #1a325a;  color: white; font-family: arial; font-size: 16px; text-align: center;">';
								$messageFooter .= '<br>По любым вопросам Вы можете обратиться <br /> в службу информационной поддержки клиентов<br><br>';
								$messageFooter .= '<span style="font-family: arial; font-size: 22px; text-align: center;">+7 (495) 989 61 91</span><br><br>';
							$messageFooter .= '</td>';
						$messageFooter .= '</tr>';
					$messageFooter .= '</tbody>';
				$messageFooter .= '</table>';
			$messageFooter .= '</td>';
		$messageFooter .= '</tr>';
	$messageFooter .= '</tbody>';
$messageFooter .= '</table>';