<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Sirius Aero – одна из старейших российских компаний деловой авиации. Парк воздушных судов Sirius Aero – это современные иностранные бизнес джеты и российские бизнес лайнеры с премиальной конфигурацией салона");
$APPLICATION->SetPageProperty("title", "О компании Сириус-Аэро");
$APPLICATION->SetTitle("Компания");

echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <h1>' . $APPLICATION->GetTitle(false) . '</h1>
    ';
?><p>
	Sirius Aero – международная, крупнейшая в России компания деловой авиации. Мы выполняем внутренние и международные чартерные авиаперевозки практически во все страны мира.
</p>
<p>
 <span class="bold">Парк воздушных судов</span> Sirius Aero – это современные иностранные бизнес джеты и российские бизнес лайнеры с премиальной конфигурацией салона. Всего в нашем флоте 6 типов самолётов. <a href="/fleet/">Посмотреть флот</a>
</p>
<p>
	 Sirius Aero – <span class="bold">самый крупный коммерческий оператор</span> в Восточной Европе. Офисы Sirius Aero находятся в Москве и Вене. Базовые аэропорты располагаются в Москве и на Мальте. Деятельность нашей компании соответствует российскому и международному воздушному законодательству. Это подтверждено соответствующими сертификатами и лицензиями.
</p>
<div class="b-pic-gal">
 <a href="/about/company/img/cert1.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СЕРТИФИКАТ КОММЕРЧЕСКОГО ЭКСПЛУАТАНТА">
    <img src="/about/company/img/cert1.jpg" alt="">
 </a>
 <a href="/about/company/img/sert2.jpg" class="fancybox" data-fancybox="group-gal" data-caption="ЛИЦЕНЗИЯ НА ПЕРЕВОЗКУ ПАССАЖИРОВ">
    <img src="/about/company/img/sert2.jpg" alt="">
 </a>
 <a href="/about/company/img/sert3.jpg" class="fancybox" data-fancybox="group-gal" data-caption="ЛИЦЕНЗИЯ НА ПЕРЕВОЗКУ ГРУЗОВ">
    <img src="/about/company/img/sert3.jpg" alt="">
 </a>
 <!-- <a href="/upload/medialibrary/ae3/ae3c4fe1dac6e294274812e99b9ba70b.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СВИДЕТЕЛЬСТВО ЭКСПЛУАТАНТА АВИАЦИИ ОБЩЕГО НАЗНАЧЕНИЯ (АОН)">
    <img src="/upload/medialibrary/ae3/ae3c4fe1dac6e294274812e99b9ba70b.jpg" alt="">
 </a> -->
	<p>
		 Авиакомпания Sirius Aero неизменно заботится о своих пассажирах, поэтому мы придерживаемся самых строгих международных стандартов безопасности.
	</p>
 <a href="/images/IS-BAO-Certificate2019.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СЕРТИФИКАТ IS-BAO"><img src="/upload/medialibrary/939/939f2fec5cf87d35790e91c396dc1c29.jpg" alt=""></a> <a href="/upload/medialibrary/ce7/ce7e18f6918266366a8ccf7265ed7c81.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СЕРТИФИКАТ НА ПРАВО ПЛГ (EASA)"><img src="/upload/medialibrary/ce7/ce7e18f6918266366a8ccf7265ed7c81.jpg" alt=""></a> <a href="/upload/medialibrary/7c2/7c2bfd8b965f1f141ba86f3b31eea218.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СЕРТИФИКАТ НА ПРАВО ПЛГ РЕГИСТРАЦИИ О. БЕРМУДЫs"><img src="/upload/medialibrary/7c2/7c2bfd8b965f1f141ba86f3b31eea218.jpg" alt=""></a>
</div>
<p>
 <a href="/upload/Политика%20АК%20в%20области%20безопасности%20полетов.pdf" target="_blank">Политика ООО АК "Сириус-Аэро" в области безопасности полетов</a>
</p>
<p>
 <a href="Политика в области качества.pdf" target="_blank">Политика ООО АК "Сириус-Аэро" в области качества</a>
</p>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
