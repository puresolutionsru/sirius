<?php
/**
 * Traditional Chinese PHPMailer language file: refer to English translation for definitive list
 * @package PHPMailer
 * @author liqwei <liqwei@liqwei.com>
 * @author Peter Dave Hello <@PeterDaveHello/>
 * @author Jason Chiang <xcojad@gmail.com>
 */

$PHPMAILER_LANG['authenticate']         = 'SMTP йЊЇиЄ¤пјљз™»е…Ґе¤±ж•—гЂ‚';
$PHPMAILER_LANG['connect_host']         = 'SMTP йЊЇиЄ¤пјљз„Ўжі•йЂЈз·ље€° SMTP дё»ж©џгЂ‚';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP йЊЇиЄ¤пјљз„Ўжі•жЋҐеЏ—зљ„иі‡ж–™гЂ‚';
$PHPMAILER_LANG['empty_message']        = 'йѓµд»¶е…§е®№з‚єз©є';
$PHPMAILER_LANG['encoding']             = 'жњЄзџҐз·Ёзўј: ';
$PHPMAILER_LANG['execute']              = 'з„Ўжі•еџ·иЎЊпјљ';
$PHPMAILER_LANG['file_access']          = 'з„Ўжі•е­?еЏ–жЄ”жЎ€пјљ';
$PHPMAILER_LANG['file_open']            = 'жЄ”жЎ€йЊЇиЄ¤пјљз„Ўжі•й–‹е•џжЄ”жЎ€пјљ';
$PHPMAILER_LANG['from_failed']          = 'з™јйЂЃењ°еќЂйЊЇиЄ¤пјљ';
$PHPMAILER_LANG['instantiate']          = 'жњЄзџҐе‡Ѕж•ёе‘јеЏ«гЂ‚';
$PHPMAILER_LANG['invalid_address']      = 'е› з‚єй›»е­ђйѓµд»¶ењ°еќЂз„Ўж•€пјЊз„Ўжі•е‚ійЂЃ: ';
$PHPMAILER_LANG['mailer_not_supported'] = 'дёЌж”ЇжЏґзљ„з™јдїЎе®ўж€¶з«ЇгЂ‚';
$PHPMAILER_LANG['provide_address']      = 'еї…й €жЏђдѕ›и‡іе°‘дёЂеЂ‹ж”¶д»¶дєєењ°еќЂгЂ‚';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP йЊЇиЄ¤пјљд»Ґдё‹ж”¶д»¶дєєењ°еќЂйЊЇиЄ¤пјљ';
$PHPMAILER_LANG['signing']              = 'й›»е­ђз°Ѕз« йЊЇиЄ¤: ';
$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP йЂЈз·ље¤±ж•—';
$PHPMAILER_LANG['smtp_error']           = 'SMTP дјєжњЌе™ЁйЊЇиЄ¤: ';
$PHPMAILER_LANG['variable_set']         = 'з„Ўжі•иЁ­е®љж€–й‡ЌиЁ­и®Љж•ё: ';
$PHPMAILER_LANG['extension_missing']    = 'йЃєе¤±жЁЎзµ„ Extension: ';
