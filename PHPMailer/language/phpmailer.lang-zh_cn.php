<?php
/**
 * Simplified Chinese PHPMailer language file: refer to English translation for definitive list
 * @package PHPMailer
 * @author liqwei <liqwei@liqwei.com>
 * @author young <masxy@foxmail.com>
 */

$PHPMAILER_LANG['authenticate']         = 'SMTP й”™иЇЇпјљз™»еЅ•е¤±иґҐгЂ‚';
$PHPMAILER_LANG['connect_host']         = 'SMTP й”™иЇЇпјљж— жі•иїћжЋҐе€° SMTP дё»жњєгЂ‚';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP й”™иЇЇпјљж•°жЌ®дёЌиў«жЋҐеЏ—гЂ‚';
$PHPMAILER_LANG['empty_message']        = 'й‚®д»¶ж­Јж–‡дёєз©єгЂ‚';
$PHPMAILER_LANG['encoding']             = 'жњЄзџҐзј–з Ѓ: ';
$PHPMAILER_LANG['execute']              = 'ж— жі•ж‰§иЎЊпјљ';
$PHPMAILER_LANG['file_access']          = 'ж— жі•и®їй—®ж–‡д»¶пјљ';
$PHPMAILER_LANG['file_open']            = 'ж–‡д»¶й”™иЇЇпјљж— жі•ж‰“ејЂж–‡д»¶пјљ';
$PHPMAILER_LANG['from_failed']          = 'еЏ‘йЂЃењ°еќЂй”™иЇЇпјљ';
$PHPMAILER_LANG['instantiate']          = 'жњЄзџҐе‡Ѕж•°и°ѓз”ЁгЂ‚';
$PHPMAILER_LANG['invalid_address']      = 'еЏ‘йЂЃе¤±иґҐпјЊз”µе­ђй‚®з®±ењ°еќЂж?Їж— ж•€зљ„пјљ';
$PHPMAILER_LANG['mailer_not_supported'] = 'еЏ‘дїЎе®ўж€·з«ЇдёЌиў«ж”ЇжЊЃгЂ‚';
$PHPMAILER_LANG['provide_address']      = 'еї…йЎ»жЏђдѕ›и‡іе°‘дёЂдёЄж”¶д»¶дєєењ°еќЂгЂ‚';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP й”™иЇЇпјљж”¶д»¶дєєењ°еќЂй”™иЇЇпјљ';
$PHPMAILER_LANG['signing']              = 'з™»еЅ•е¤±иґҐпјљ';
$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTPжњЌеЉЎе™ЁиїћжЋҐе¤±иґҐгЂ‚';
$PHPMAILER_LANG['smtp_error']           = 'SMTPжњЌеЉЎе™Ёе‡єй”™: ';
$PHPMAILER_LANG['variable_set']         = 'ж— жі•и®ѕзЅ®ж€–й‡ЌзЅ®еЏ?й‡Џпјљ';
//$PHPMAILER_LANG['extension_missing']    = 'Extension missing: ';
