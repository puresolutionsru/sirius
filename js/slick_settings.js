// template
//
// <div class="your-class">
// 		<div style="background: red;">your content 1</div>
// 		<div style="background: green;">your content 2</div>
// 		<div style="background: blue;">your content 3</div>
// 		<div style="background: orange;">your content 4</div>
// 		<div style="background: brown;">your content 5</div>
// 		<div style="background: pink;">your content 6</div>
// 		<div style="background: yellow;">your content 7</div>
// </div>

$(document).ready(function(){

	$('.slider_fleet').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details',
		centerMode: true,
		focusOnSelect: true
	});

	$('.slider_details').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet'
	});

	$('.slider_fleet_partners').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details_partners',
		centerMode: true,
		focusOnSelect: true
	});

	$('.slider_details_partners').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet_partners'
	});

    $('#plus .items_slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        adaptiveHeight: true,
        dots: true,
        
        responsive: [
        {
            breakpoint: 829,
            settings: {
                slidesToShow: 2,
                autoplay: true,
                adaptiveHeight: true,
                dots: true,
            }
        },

        {
            breakpoint: 489,
            settings: {
                slidesToShow: 1,
                autoplay: true,
                adaptiveHeight: true,
                dots: true,
            }
        }
        ]  
         
    });

}); // jQuery