$( document ).ready( function() {

  $(window).on('resize', function() {
    setHeight();
    setHeightNews();
    hamh();
  });

  /* Video slide
    ****************************************************************/
  function setHeight(){
    var screenHeight = $(window).height();
    var topHeight = $('.header').outerHeight(true) + 60;
    var menuHeight = $('.b-menu._mod1').outerHeight(true);
    var h = screenHeight - (topHeight + menuHeight);
    var videoh = $('.b-video__show').outerHeight(true);
    if(videoh < h) {
      $('.b-video').height(videoh);
    } else {
		  $('.b-video').height(h);
    }
  };
  setHeight();

  /* News text
    ****************************************************************/
  function setHeightNews(){
		var w = $(window).width();
		if(w <= 679){
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 200
			});
		}else{
			$('.b-news__i__tx').dotdotdot({
				ellipsis: "...",
				wrap: "letter",
				after: null,
				watch: false,
				height: 50
			});
		}
	}
  setHeightNews();

  // hamburger
	$('.hamburger-menu').on('click', function() {
		$('.hamburger-menu__in').toggleClass('_animate');
		$('.header').toggleClass('_on');
		$('.hamburger-inner').toggleClass('_on');
		$('body').toggleClass('_menu');
    hamh();
	});

  function hamh() {
    var h = $('.header').outerHeight(true);
		$('.hamburger-inner').css('top', h);
  }
  hamh();

  // Flot
  $('.flot-tabs span').click(function(){
    if(!$(this).is('._on')) {
      $(this).parent().find('span').toggleClass('_on');
      if($(this).parent().is('.aircraft-slider__tabs')) {
        $('.aircraft-slider__items').toggleClass('_on');
      }
      else {
        $('.flot-list-m').toggleClass('_on');
      }
    }
  });

  function flot() {
    $('.flot-list__l a').on('click', function(e) {
      if($(this).attr('data-row')) {
        e.preventDefault();
        var row = $(this).attr('data-row');
        $(this).toggleClass('_on');
        $('.flot-list__child'+row).toggleClass('_on');
      }
    });
  }
  flot();

  $('.aircraft-config__i__t').on('click', function(){
    if(!$(this).is('._on')) {
      $('.aircraft-config__i__t').toggleClass('_on');
      $('.aircraft-config__m .aircraft-config__i').toggleClass('_on');
    }
  });


	/* Slider
	 ****************************************************************/
  $('.slider_fleet').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details',
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items1').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});
  $('.aircraft-slider__items2').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		dots: false,
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1400,
        settings: {
            slidesToShow: 4,
        }
    },
    {
        breakpoint: 1100,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 800,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 500,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});

	$('.slider_details').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet'
	});

	$('.slider_fleet_partners').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		asNavFor: '.slider_details_partners',
		centerMode: true,
		focusOnSelect: true,
    responsive: [
    {
        breakpoint: 1300,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 900,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});

	$('.slider_details_partners').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: false,
		asNavFor: '.slider_fleet_partners'
	});

  $('.b-gallery._mod1').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
    autoplay: true,
    adaptiveHeight: true,
    responsive: [
    {
        breakpoint: 350,
        settings: {
            slidesToShow: 1,
        }
    }
    ]
	});

	// переключение главных табов
	function switchTabsMain(){
		$('#big_stuff .title_stuff .main span').on('click', function(){
			if($(this).is('.others')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.aero').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_partners').addClass('on');
				}
			}
			if($(this).is('.aero')){
				if(!$(this).is('.on')){
					$(this).addClass('on');

					$('#big_stuff .title_stuff .main span.others').removeClass('on');

					$('#big_stuff .stuff').removeClass('on');
					$('#big_stuff #stuff_aero').addClass('on');
				}
			}
		});
	};
  switchTabsMain();

	// переключение внутренних табов
	function switchTabsMini(){
		$('#big_stuff .tab_wr').on('click', function(){
			$(this).parent('.tabs').children('.tab_wr').removeClass('on');
			$(this).parent('.tabs').removeClass('tab_active_one tab_active_two');
			$(this).addClass('on');

			if( $(this).parent('.tabs').children('.tab_wr').eq(0).is('.on') ){
				$(this).parent('.tabs').addClass('tab_active_one');

				$(this).parent('.tabs').parent('.item').children('section').removeClass('on');
				$(this).parent('.tabs').parent('.item').children('section').eq(0).addClass('on');

				$(this).parent('.tabs').parent('.item').children('img').removeClass('on');
				$(this).parent('.tabs').parent('.item').children('img').eq(0).addClass('on');
			}
			if( $(this).parent('.tabs').children('.tab_wr').eq(1).is('.on') ){
				$(this).parent('.tabs').addClass('tab_active_two');

				$(this).parent('.tabs').parent('.item').children('section').removeClass('on');
				$(this).parent('.tabs').parent('.item').children('section').eq(1).addClass('on');

				$(this).parent('.tabs').parent('.item').children('img').removeClass('on');
				$(this).parent('.tabs').parent('.item').children('img').eq(1).addClass('on');
			}
		});
	};
  switchTabsMini();

  // слайдер преимущества
  $('.b-advantage__items').slick({
      slidesToShow: 7,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      adaptiveHeight: true,
      dots: true,
      responsive: [
      {
          breakpoint: 1300,
          settings: {
              slidesToShow: 4,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 829,
          settings: {
              slidesToShow: 2,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      },
      {
          breakpoint: 489,
          settings: {
              slidesToShow: 1,
              autoplay: true,
              adaptiveHeight: true,
              dots: true,
          }
      }
      ]
  });

  $('select').styler();

	/* Fancybox for img
	 ****************************************************************/


	/* Placeholder
	 ****************************************************************/
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
			$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
				}
			})
		});
	}

});
