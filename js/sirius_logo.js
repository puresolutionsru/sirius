(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


(lib.AnMovieClip = function(){
	this.actionFrames = [];
	this.gotoAndPlay = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndPlay.call(this,positionOrLabel);
	}
	this.play = function(){
		cjs.MovieClip.prototype.play.call(this);
	}
	this.gotoAndStop = function(positionOrLabel){
		cjs.MovieClip.prototype.gotoAndStop.call(this,positionOrLabel);
	}
	this.stop = function(){
		cjs.MovieClip.prototype.stop.call(this);
	}
}).prototype = p = new cjs.MovieClip();
// symbols:



(lib.Анимация1 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Слой_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPAcInOCGIHBizIh9maICqGJIHNiJInBC6IB1Gdg");
	this.shape.setTransform(0.025,0);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	this._renderFirstFrame();

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-47.8,-42.8,95.69999999999999,85.6);


// stage content:
(lib.ВОССТАНОВИТЬ_Безымянный3_202010783444 = function(mode,startPosition,loop,reversed) {
if (loop == null) { loop = true; }
if (reversed == null) { reversed = false; }
	var props = new Object();
	props.mode = mode;
	props.startPosition = startPosition;
	props.labels = {};
	props.loop = loop;
	props.reversed = reversed;
	cjs.MovieClip.apply(this,[props]);

	// Слой_3
	this.instance = new lib.Анимация1("synched",0);
	this.instance.setTransform(62.3,50.35,0.9103,0.9103,-28.7781,0,0,0.1,0);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off:false},0).to({scaleX:0.916,scaleY:0.916,rotation:4.5251,x:62.4,y:50.4,alpha:0.4883},43,cjs.Ease.none).to({regX:1.1,regY:0.1,scaleX:0.8808,scaleY:0.8808,rotation:36.5014,x:62.7,y:50.95,alpha:0},33,cjs.Ease.none).to({regY:0,scaleX:0.5464,scaleY:0.5464,rotation:126.5032,y:51,alpha:0.25},27,cjs.Ease.none).to({regX:1,regY:0.1,rotation:186.5035,y:50.95,alpha:0},29).wait(69).to({regX:1.1,regY:0,rotation:126.5032,y:51},0).to({regX:1,regY:-0.1,scaleX:1.2414,scaleY:0.7734,rotation:156.5044,x:62.8,y:50.95,alpha:0.5},26).to({scaleX:0.5394,scaleY:0.5706,rotation:201.5032,y:50.9,alpha:0},26).wait(54).to({regX:1.1,regY:0.1,scaleX:0.8808,scaleY:0.8808,rotation:36.5014,x:62.7,y:50.95},0).to({regX:1,scaleX:0.5465,scaleY:0.5465,rotation:171.5022,x:62.65,y:51,alpha:0.25},34,cjs.Ease.none).to({regX:1.1,regY:0,scaleX:0.5464,scaleY:0.5464,rotation:126.5032,x:62.7,alpha:0},30).wait(106));

	// Слой_2
	this.instance_1 = new lib.Анимация1("synched",0);
	this.instance_1.setTransform(62.45,50.3,0.4556,0.4556,89.2381,0,0,0,-0.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({regX:0.1,scaleX:0.8171,scaleY:0.8171,rotation:48.7885,x:62.25,y:49.8,alpha:0},59,cjs.Ease.quadOut).wait(23).to({regX:0,regY:0,scaleX:0.6581,scaleY:0.6581,rotation:48.7892,x:61.85},0).to({regX:0.2,regY:-0.1,scaleX:0.8251,scaleY:0.8251,rotation:18.7891,x:62.2,y:49.85,alpha:0.6016},28).to({regX:0.4,regY:0.4,scaleX:0.549,scaleY:0.549,rotation:-19.9423,x:62.45,y:49.95,alpha:0},30).wait(76).to({regX:0.3,regY:0.1,scaleX:0.5491,scaleY:0.5491,rotation:-26.209,x:62.3,y:49.85},0).to({regY:0.2,scaleX:0.6411,scaleY:1.5485,rotation:-56.2094,x:62.4,alpha:0.4102},30).to({regX:0.4,scaleX:0.5999,scaleY:0.9663,rotation:-86.208,y:49.8,alpha:0},27).wait(48).to({regX:0,regY:0,scaleX:0.6581,scaleY:0.6581,rotation:48.7892,x:61.85},0).to({regX:0.3,regY:-0.3,scaleX:0.8251,scaleY:0.8251,rotation:108.7875,x:62.25,y:49.95,alpha:0.5195},26).to({regY:0.1,scaleX:0.5491,scaleY:0.5491,rotation:153.791,x:62.3,y:49.75,alpha:0},21).wait(112));

	// Слой_5
	this.instance_2 = new lib.Анимация1("synched",0);
	this.instance_2.setTransform(61.9,49.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(480));

	// Слой_4
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.384,0.725,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape.setTransform(62.225,50);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.388,0.725,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_1.setTransform(62.225,50);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.392,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_2.setTransform(62.225,50);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.396,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_3.setTransform(62.225,50);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.4,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_4.setTransform(62.225,50);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.404,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_5.setTransform(62.225,50);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.408,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_6.setTransform(62.225,50);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.412,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_7.setTransform(62.225,50);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.416,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_8.setTransform(62.225,50);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.416,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_9.setTransform(62.225,50);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.42,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_10.setTransform(62.225,50);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.424,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_11.setTransform(62.225,50);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.424,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_12.setTransform(62.225,50);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.427,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_13.setTransform(62.225,50);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.196,0.431,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_14.setTransform(62.225,50);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.196,0.435,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_15.setTransform(62.225,50);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.2,0.435,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_16.setTransform(62.225,50);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.2,0.439,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_17.setTransform(62.225,50);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.204,0.439,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_18.setTransform(62.225,50);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.204,0.443,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_19.setTransform(62.225,50);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.204,0.447,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_20.setTransform(62.225,50);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.208,0.447,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_21.setTransform(62.225,50);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.208,0.451,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_22.setTransform(62.225,50);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.212,0.451,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_23.setTransform(62.225,50);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.212,0.455,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_24.setTransform(62.225,50);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.212,0.455,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_25.setTransform(62.225,50);
	this.shape_25._off = true;

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.216,0.455,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_26.setTransform(62.225,50);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.216,0.459,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_27.setTransform(62.225,50);
	this.shape_27._off = true;

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.427,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_28.setTransform(62.225,50);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.408,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_29.setTransform(62.225,50);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.404,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_30.setTransform(62.225,50);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.392,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_31.setTransform(62.225,50);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.388,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_32.setTransform(62.225,50);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.388,0.725,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_33.setTransform(62.225,50);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.388,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_34.setTransform(62.225,50);
	this.shape_34._off = true;

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.396,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_35.setTransform(62.225,50);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.4,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_36.setTransform(62.225,50);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.404,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_37.setTransform(62.225,50);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.408,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_38.setTransform(62.225,50);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.412,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_39.setTransform(62.225,50);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.416,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_40.setTransform(62.225,50);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.42,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_41.setTransform(62.225,50);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.424,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_42.setTransform(62.225,50);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.427,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_43.setTransform(62.225,50);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.431,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_44.setTransform(62.225,50);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.435,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_45.setTransform(62.225,50);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.439,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_46.setTransform(62.225,50);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.443,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_47.setTransform(62.225,50);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.447,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_48.setTransform(62.225,50);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.451,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_49.setTransform(62.225,50);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.455,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_50.setTransform(62.225,50);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.459,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_51.setTransform(62.225,50);
	this.shape_51._off = true;

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.455,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_52.setTransform(62.225,50);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.416,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_53.setTransform(62.225,50);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.408,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_54.setTransform(62.225,50);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.392,0.729,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_55.setTransform(62.225,50);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.161,0.392,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_56.setTransform(62.225,50);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.396,0.733,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_57.setTransform(62.225,50);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.4,0.737,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_58.setTransform(62.225,50);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.404,0.741,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_59.setTransform(62.225,50);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.408,0.745,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_60.setTransform(62.225,50);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.412,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_61.setTransform(62.225,50);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.416,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_62.setTransform(62.225,50);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.42,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_63.setTransform(62.225,50);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.424,0.757,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_64.setTransform(62.225,50);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.424,0.761,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_65.setTransform(62.225,50);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.427,0.765,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_66.setTransform(62.225,50);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.431,0.765,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_67.setTransform(62.225,50);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.435,0.769,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_68.setTransform(62.225,50);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.439,0.773,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_69.setTransform(62.225,50);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.443,0.776,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_70.setTransform(62.225,50);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.447,0.78,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_71.setTransform(62.225,50);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.451,0.78,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_72.setTransform(62.225,50);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.455,0.784,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_73.setTransform(62.225,50);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.459,0.788,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_74.setTransform(62.225,50);
	this.shape_74._off = true;

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.463,0.792,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_75.setTransform(62.225,50);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.467,0.796,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_76.setTransform(62.225,50);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.471,0.796,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_77.setTransform(62.225,50);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.475,0.8,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_78.setTransform(62.225,50);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.478,0.804,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_79.setTransform(62.225,50);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.482,0.808,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_80.setTransform(62.225,50);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.486,0.812,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_81.setTransform(62.225,50);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.49,0.812,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_82.setTransform(62.225,50);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.494,0.816,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_83.setTransform(62.225,50);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.498,0.82,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_84.setTransform(62.225,50);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.498,0.824,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_85.setTransform(62.225,50);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.502,0.827,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_86.setTransform(62.225,50);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.506,0.827,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_87.setTransform(62.225,50);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.51,0.831,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_88.setTransform(62.225,50);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.514,0.835,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_89.setTransform(62.225,50);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.518,0.839,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_90.setTransform(62.225,50);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.522,0.843,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_91.setTransform(62.225,50);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.525,0.843,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_92.setTransform(62.225,50);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.529,0.847,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_93.setTransform(62.225,50);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.533,0.851,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_94.setTransform(62.225,50);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.502,0.824,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_95.setTransform(62.225,50);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.494,0.82,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_96.setTransform(62.225,50);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.49,0.816,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_97.setTransform(62.225,50);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.478,0.808,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_98.setTransform(62.225,50);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.475,0.804,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_99.setTransform(62.225,50);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.471,0.8,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_100.setTransform(62.225,50);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.459,0.788,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_101.setTransform(62.225,50);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.447,0.776,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_102.setTransform(62.225,50);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.443,0.773,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_103.setTransform(62.225,50);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.439,0.769,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_104.setTransform(62.225,50);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.427,0.761,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_105.setTransform(62.225,50);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.165,0.416,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_106.setTransform(62.225,50);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.416,0.749,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_107.setTransform(62.225,50);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.431,0.765,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_108.setTransform(62.225,50);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.173,0.435,0.769,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_109.setTransform(62.225,50);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.451,0.78,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_110.setTransform(62.225,50);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.176,0.455,0.784,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_111.setTransform(62.225,50);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.471,0.796,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_112.setTransform(62.225,50);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.475,0.8,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_113.setTransform(62.225,50);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.478,0.804,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_114.setTransform(62.225,50);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.49,0.812,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_115.setTransform(62.225,50);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.494,0.816,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_116.setTransform(62.225,50);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.498,0.82,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_117.setTransform(62.225,50);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.498,0.824,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_118.setTransform(62.225,50);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.506,0.827,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_119.setTransform(62.225,50);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.51,0.831,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_120.setTransform(62.225,50);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.514,0.835,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_121.setTransform(62.225,50);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.518,0.839,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_122.setTransform(62.225,50);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.188,0.522,0.843,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_123.setTransform(62.225,50);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.525,0.843,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_124.setTransform(62.225,50);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.529,0.847,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_125.setTransform(62.225,50);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.192,0.533,0.851,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_126.setTransform(62.225,50);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.494,0.82,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_127.setTransform(62.225,50);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.49,0.816,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_128.setTransform(62.225,50);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.184,0.486,0.812,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_129.setTransform(62.225,50);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.478,0.808,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_130.setTransform(62.225,50);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.475,0.804,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_131.setTransform(62.225,50);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.471,0.8,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_132.setTransform(62.225,50);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.18,0.467,0.796,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_133.setTransform(62.225,50);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.rf(["#EAF7FD","#009AE1","#004276","#002148"],[0.169,0.416,0.753,0.973],0,0,0,0,0,36.4).s().p("AkAECQhrhrAAiXQAAiWBrhrQBqhqCWAAQCXAABqBqQBrBrAACWQAACXhrBrQhqBqiXAAQiWAAhqhqg");
	this.shape_134.setTransform(62.225,50);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},2).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},73).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_77}]},1).to({state:[{t:this.shape_78}]},1).to({state:[{t:this.shape_79}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_82}]},1).to({state:[{t:this.shape_83}]},1).to({state:[{t:this.shape_84}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_94}]},1).to({state:[{t:this.shape_93}]},1).to({state:[{t:this.shape_92}]},1).to({state:[{t:this.shape_91}]},1).to({state:[{t:this.shape_90}]},1).to({state:[{t:this.shape_89}]},1).to({state:[{t:this.shape_88}]},1).to({state:[{t:this.shape_87}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_85}]},1).to({state:[{t:this.shape_96}]},1).to({state:[{t:this.shape_97}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_98}]},1).to({state:[{t:this.shape_99}]},1).to({state:[{t:this.shape_100}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_101}]},1).to({state:[{t:this.shape_73}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_106}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape}]},1).to({state:[{t:this.shape}]},28).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_107}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_108}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_70}]},1).to({state:[{t:this.shape_71}]},1).to({state:[{t:this.shape_110}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_76}]},1).to({state:[{t:this.shape_112}]},1).to({state:[{t:this.shape_113}]},1).to({state:[{t:this.shape_114}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_81}]},1).to({state:[{t:this.shape_115}]},1).to({state:[{t:this.shape_116}]},1).to({state:[{t:this.shape_117}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_86}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_126}]},1).to({state:[{t:this.shape_125}]},1).to({state:[{t:this.shape_124}]},1).to({state:[{t:this.shape_123}]},1).to({state:[{t:this.shape_122}]},1).to({state:[{t:this.shape_121}]},1).to({state:[{t:this.shape_120}]},1).to({state:[{t:this.shape_119}]},1).to({state:[{t:this.shape_95}]},1).to({state:[{t:this.shape_118}]},1).to({state:[{t:this.shape_127}]},1).to({state:[{t:this.shape_128}]},1).to({state:[{t:this.shape_129}]},1).to({state:[{t:this.shape_80}]},1).to({state:[{t:this.shape_130}]},1).to({state:[{t:this.shape_131}]},1).to({state:[{t:this.shape_132}]},1).to({state:[{t:this.shape_133}]},1).to({state:[{t:this.shape_75}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_74}]},1).to({state:[{t:this.shape_111}]},1).to({state:[{t:this.shape_72}]},1).to({state:[{t:this.shape_102}]},1).to({state:[{t:this.shape_103}]},1).to({state:[{t:this.shape_104}]},1).to({state:[{t:this.shape_109}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_105}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_134}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_34}]},1).to({state:[{t:this.shape}]},1).wait(86));
	this.timeline.addTween(cjs.Tween.get(this.shape).to({_off:true},1).wait(72).to({_off:false},0).wait(8).to({_off:true},1).wait(49).to({_off:false},0).wait(77).to({_off:true},1).wait(78).to({_off:false},0).wait(28).to({_off:true},1).wait(78).to({_off:false},0).wait(86));
	this.timeline.addTween(cjs.Tween.get(this.shape_25).wait(30).to({_off:false},0).wait(2).to({_off:true},1).wait(8).to({_off:false},0).to({_off:true},1).wait(438));
	this.timeline.addTween(cjs.Tween.get(this.shape_27).wait(34).to({_off:false},0).wait(6).to({_off:true},1).wait(439));
	this.timeline.addTween(cjs.Tween.get(this.shape_34).wait(82).to({_off:false},0).to({_off:true},1).wait(45).to({_off:false},0).to({_off:true},1).wait(80).to({_off:false},0).to({_off:true},1).wait(76).to({_off:false},0).to({_off:true},1).wait(29).to({_off:false},0).to({_off:true},1).wait(76).to({_off:false},0).to({_off:true},1).wait(86));
	this.timeline.addTween(cjs.Tween.get(this.shape_51).wait(105).to({_off:false},0).wait(4).to({_off:true},1).wait(370));
	this.timeline.addTween(cjs.Tween.get(this.shape_74).wait(228).to({_off:false},0).to({_off:true},1).wait(38).to({_off:false},0).to({_off:true},1).wait(67).to({_off:false},0).to({_off:true},1).wait(38).to({_off:false},0).wait(1).to({_off:true},1).wait(104));

	// Слой_1
	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#002148").s().p("AK4C6QglguAAhGQAAhPAngvQAqgyBOgBQBPAAAnA0QAiAsAABEIAAAcIjbAAQAAAgAQAXQASAZAeAAQAbAAAOgLQAKgHAHgRIBcAAQgNAqgdAaQgpAnhFAAQhMAAgpgzgAMCgYQgPASgCAfIB9AAQAAgegNgSQgQgVgfAAQgfAAgRAUgAjJDOQghgcgEgwIBXAAIAAgDQALAoA0AAQAsAAAAghQAAgOgNgJQgNgJgkgHQhLgUgcgaQgWgWAAgmQAAgqAdgbQAkgfBDAAQBEAAAkAgQAfAbACApIhXAAQAAgMgJgIQgNgNgcAAQgoAAAAAbQAAAOAMAGQANAIAoAJQBMARAbAeQAVAWAAArQAAAtgiAcQgkAghHAAQhHAAgngfgAUIC6QgsgwABhMQAAhNAsguQAtgwBNAAQBKAAArAvQArAuAABOQAABNgrAvQgtAwhMAAQhMAAgrgwgAU7A+QAAAvATAaQAUAbAhAAQAiAAASgbQASgaAAgvQAAhlhGAAQhIAEAABhgAonDMQgdgfAAg9IAAjVIBaAAIAADEQAABDAxAAQAhAAAOgUQAOgTgBgwIAAiuIBdAAIAAFGIhaAAQgCgKgBgcQgPAYgVALQgVAKggAAQg0ABgdgfgA3uDFQgsgjgIg/IBfAAQAOA/BOAAQBJAAAAg1QAAgbgUgNQgVgQg5gLQhVgTgigiQgfgdAAguQAAg7AqgiQArgjBLAAQBRAAAtAoQAlAgAHA4IhcAAQgFgbgOgPQgTgSgmAAQgcAAgRALQgRANAAAXQABAWARANQASAMA2ANQBZAVAkAkQAeAeAAAzQgBA/gsAjQguAlhWAAQhSAAguglgAQEDmIAAlLIBaAAIAAASIACAnQAMgeAZgRQAcgSAoAAIAABYQg3gDgXAVQgbAXAABBIAACRgAvaDmIAAlLIBaAAIAAASIACAnQAMgeAZgRQAcgSAoAAIAABYQg3gDgXAVQgbAXAABBIAACRgAIdDjIgihnIijAAIgiBnIhdAAICVm2IB5AAICaG2gAF2AOIAAABIgKAgIB4AAIg/i/QgHAigoB8gAroDjIAAlIIBcAAIAAFIgAyCDjIAAlIIBdAAIAAFIgAroiaIAAhSIBcAAIAABSgAyCiaIAAhSIBdAAIAABSg");
	this.shape_135.setTransform(278,50);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#002148").s().p("AoUGHIDxqpQAQgtAngbQAngcAvAAIKrAAIj5KrQgRAsgmAbQgnAbgvAAg");
	this.shape_136.setTransform(62.225,50);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_136},{t:this.shape_135}]}).wait(480));

	this._renderFirstFrame();

}).prototype = p = new lib.AnMovieClip();
p.nominalBounds = new cjs.Rectangle(226.5,57,208.60000000000002,35.599999999999994);
// library properties:
lib.properties = {
	id: 'B2E14D971C8FCD449EC654FD37E82D0B',
	width: 450,
	height: 100,
	fps: 45,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B2E14D971C8FCD449EC654FD37E82D0B'] = {
	getStage: function() { return exportRoot.stage; },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}


an.makeResponsive = function(isResp, respDim, isScale, scaleType, domContainers) {		
	var lastW, lastH, lastS=1;		
	window.addEventListener('resize', resizeCanvas);		
	resizeCanvas();		
	function resizeCanvas() {			
		var w = lib.properties.width, h = lib.properties.height;			
		var iw = window.innerWidth, ih=window.innerHeight;			
		var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;			
		if(isResp) {                
			if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {                    
				sRatio = lastS;                
			}				
			else if(!isScale) {					
				if(iw<w || ih<h)						
					sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==1) {					
				sRatio = Math.min(xRatio, yRatio);				
			}				
			else if(scaleType==2) {					
				sRatio = Math.max(xRatio, yRatio);				
			}			
		}
		domContainers[0].width = w * pRatio * sRatio;			
		domContainers[0].height = h * pRatio * sRatio;
		domContainers.forEach(function(container) {				
			container.style.width = w * sRatio + 'px';				
			container.style.height = h * sRatio + 'px';			
		});
		stage.scaleX = pRatio*sRatio;			
		stage.scaleY = pRatio*sRatio;
		lastW = iw; lastH = ih; lastS = sRatio;            
		stage.tickOnUpdate = false;            
		stage.update();            
		stage.tickOnUpdate = true;		
	}
}
an.handleSoundStreamOnTick = function(event) {
	if(!event.paused){
		var stageChild = stage.getChildAt(0);
		if(!stageChild.paused){
			stageChild.syncStreamSounds();
		}
	}
}


})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;