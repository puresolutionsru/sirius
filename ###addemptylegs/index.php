<html>
<head>
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>
<script src="https://code.jquery.com/jquery-1.8.3.js"></script>
</head>
<body>

<?
$filename = $_SERVER['DOCUMENT_ROOT'] . '/addemptylegs/id.txt';
$handler = fopen($filename, 'r');
$content = fread($handler, filesize($filename));
fclose($handler);
?>

<?php $btnClass = !empty($_GET['btnClass']) ? $_GET['btnClass'] : ''; 
echo $btnClass ?>

	<form action="">
		<div class="forflex <?= $btnClass ?>">
			<div class="forflex_column">
				<label>ID</label>
				<input type="text" class="s1" value="<?=$content?>">
				<br><br>
				<label>Самолет</label>
				<select class="s20">
					<option value="">Выберите самолет или введите данные</option>
					<option value="0">Legacy 600 VP-BGT</option>
					<option value="1">Legacy 600 VP-BGV</option>
					<option value="2">Legacy 600 VP-BGL</option>
					<option value="3">Legacy 600 9H-IDB</option>
					<option value="4">Hawker 750 VQ-BBQ</option>
					<option value="9">Hawker 750 VQ-BBS</option>
					<option value="5">Hawker 850 XP VP-BVA</option>
					<option value="6">Hawker 850 XP VP-BNW</option>
					<option value="7">Challenger 850 VQ-BOV</option>
					<option value="8">Hawker 1000 VP-BMY</option>
				</select>
				<!-- <br><br> -->

				<!-- <label>Город вылета</label>
				<select class="s21">
					<option value="">Выберите откуда или введите данные</option>
					<option value="VKO">VKO</option>
					<option value="LED">LED</option>
					<option value="AER">AER</option>
					<option value="BJV">BJV</option>
					<option value="NCE">NCE</option>
					<option value="PSA">PSA</option>
					<option value="URFF">URFF</option>
					<option value="AYT">AYT</option>
					<option value="LBG">LBG</option>
					<option value="QSR">QSR</option>
					<option value="REU">REU</option>
					<option value="JMK">JMK</option>
					<option value="MJT">MJT</option>
					<option value="VIE">VIE</option>
					<option value="LCA">LCA</option>
					<option value="GVA">GVA</option>
					<option value="RIX">RIX</option>
				</select>
				<br><br> -->
				<!-- <label>Город назначения</label>
				<select class="s22">
					<option value="">Выберите куда или введите данные</option>
					<option value="VKO">VKO</option>
					<option value="LED">LED</option>
					<option value="AER">AER</option>
					<option value="BJV">BJV</option>
					<option value="NCE">NCE</option>
					<option value="PSA">PSA</option>
					<option value="URFF">URFF</option>
					<option value="AYT">AYT</option>
					<option value="LBG">LBG</option>
					<option value="QSR">QSR</option>
					<option value="REU">REU</option>
					<option value="JMK">JMK</option>
					<option value="MJT">MJT</option>
					<option value="VIE">VIE</option>
					<option value="PMI">PMI</option>
					<option value="LCA">LCA</option>
					<option value="GVA">GVA</option>
					<option value="RIX">RIX</option>
				</select>
				 -->
				
				 <br><br>
				<br>
				<label>FromCity</label>
				<input type="text" class="s8">
				<br><br>
				<label>FromCityCode</label>
				<input type="text" class="s9">
				<br><br>
				<label>ToCity</label>
				<input type="text" class="s17">
				<br><br>
				<label>ToCityCode</label>
				<input type="text" class="s18">
				<br><br><br>
				<label>StartDate</label>
				<input type="text" class="s16">
				<br><br>
				<label>EndDate</label>
				<input type="text" class="s6">
				<br><br>
				<div class="addbutton">Добавить</div>
			</div>
			<div class="forflex_column">
				<label>Cost</label>
				<input type="text" class="s3" value="0">
				<br><br>
				<label>CostType</label>
				<input type="text" class="s4" value="request">
				<br><br>
				<label>Currency</label>
				<input type="text" class="s5" value="$">
				<br><br>
			</div>
			<div class="forflex_column">
				<label>BagVol</label>
				<input type="text" class="s2">
				<br><br>
				<label>FlyghtDistance</label>
				<input type="text" class="s7">
				<br><br>
				<label>IdSite</label>
				<input type="text" class="s10">
				<br><br>
				<label>Images</label>
				<input type="text" class="s11">
				<label style="width: 500px;">Формат: image1.jpg, image2.jpg</label>
				<br><br>
				<label>MaxPlaneSeats</label>
				<input type="text" class="s12">
				<br><br>
				<label>Plane</label>
				<input type="text" class="s13">
				<br><br>
				<label>PlaneClass</label>
				<input type="text" class="s14">
				<br><br>
				<label>PlaneSeats</label>
				<input type="text" class="s15">
				<br><br>
			</div>
		</div>
	</form>
	<style>
	.forflex {
		display: flex;
	}
	.forflex_column {
		width: 33%;
	}
	label {
		display: inline-block;
		width: 140px;
	}
	input,
	select {
		display: inline-block;
		width: 300px;
	}
	.addbutton {
		margin-top: 50px;
		cursor: pointer;
	}
	</style>



	<script>
		var cities = {
			"VKO": ["Moscow","VKO"],
			"LED": ["St Petersburg","LED"],
			"AER": ["Sochi","AER"],
			"URFF": ["Simferopol","URFF"],
			"CEK": ["CHELYABINSK","CEK"],
			"BJV": ["BODRUM","BJV"],
			"NCE": ["Nice","NCE"],
			"PSA": ["Pisa","PSA"],
			"AYT": ["ANTALYA","AYT"],
			"LBG": ["LE BOURGET","LBG"],
			"QSR": ["PONTECAGNANO","QSR"],
			"REU": ["REUS","REU"],
			"KLX": ["KALAMATA","KLX"],
			"JMK": ["MIKONOS","JMK"],
			"MJT": ["MITILINI","MJT"],
			"VIE": ["VIENNA","VIE"],
			"PMI": ["MALLORCA","PMI"],
			"LCA": ["LARNACA","LCA"],
			"GVA": ["GENEVA","GVA"],
			"RIX": ["RIGA","RIX"],
			"TZX": ["TRABZON","TZX"],
			"SKG": ["MAKEDONIA","SKG"],
			"OLB": ["OLBIA","OLB"],
		};

		
		let newSelect = function(className,placeHolder){
			let select = document.createElement('select');
				select.className = className;
				select.innerHTML = '<option value="">'+placeHolder+'</option>';

				for (key in cities)
        		select.innerHTML += '<option value="'+ key +'">'+ key +'</option>';
				
				return select;
		}
		let createSelect = function(afterElement,className, label, placeHolder){
			$(newSelect(className,placeHolder)).insertAfter($("." + afterElement));
			$("<br><br>").insertBefore($('.' + className));
			$("<label>"+ label +"</label>").insertBefore($('.' + className));
		}
		createSelect('s20' ,'s21', 'Город вылета','Откуда');
		createSelect('s21' ,'s22', 'Город назначения','Куда');




		var airplane = {
			"0": ["Legacy 600 VP-BGT", "Heavy Jets", 13, 13, 31, 6200, "6.8", "Legacy-600-VP-BGT_1.jpg, Legacy-600-VP-BGT_2.jpg, Legacy-600-VP-BGT_3.jpg, Legacy-600-VP-BGT_4.jpg"],
			"1": ["Legacy 600 VP-BGV", "Heavy Jets", 13, 13, 30, 6200, "6.8", "Legacy-600-VP-BGV_1.jpg, Legacy-600-VP-BGV_2.jpg, Legacy-600-VP-BGV_3.jpg, Legacy-600-VP-BGV_4.jpg"],
			"2": ["Legacy 600 VP-BGL", "Heavy Jets", 13, 13, 29, 6200, "6.8", "LEGACY-600-VP-BGL_1.jpg, LEGACY-600-VP-BGL_2.jpg, LEGACY-600-VP-BGL_3.jpg, LEGACY-600-VP-BGL_4.jpg"],
			"3": ["Legacy 600 9H-IDB", "Heavy Jets", 13, 13, 30, 6200, "6,8", "Legacy-600-9H-IDB_1.jpg, Legacy-600-9H-IDB_2.jpg, Legacy-600-9H-IDB_3.jpg, Legacy-600-9H-IDB_4.jpg"],
			"4": ["Hawker 750 VQ-BBQ", "Heavy Jets", 8, 8, 24, 3600, "2.28", "HAWKER-750-VQ-BBQ_1.jpg, HAWKER-750-VQ-BBQ_2.jpg, HAWKER-750-VQ-BBQ_3.jpg, HAWKER-750-VQ-BBQ_4.jpg"],
			"9": ["Hawker 750 VQ-BBS", "Heavy Jets", 8, 8, 99, 3600, "2.28", "HAWKER-750-VQ-BBS_1.jpg, HAWKER-750-VQ-BBS_2.jpg, HAWKER-750-VQ-BBS_3.jpg, HAWKER-750-VQ-BBS_4.jpg"],
			"5": ["Hawker 850 XP VQ-BVA", "Midsize Jets", 8, 8, 25, 4800, "1.4", "Hawker-850-XP-VQ-BVA_1.jpg, Hawker-850-XP-VQ-BVA_2.jpg, Hawker-850-XP-VQ-BVA_3.jpg, Hawker-850-XP-VQ-BVA_4.jpg"],
			"6": ["Hawker 850 XP VP-BNW", "Midsize Jets", 8, 8, 26, 4800, "1.4", "Hawker_850-XP-VP-BNW_1.jpg, Hawker_850-XP-VP-BNW_2.jpg, Hawker_850-XP-VP-BNW_3.jpg, Hawker_850-XP-VP-BNW_4.jpg"],
			"7": ["Challenger 850 VQ-BOV", "Heavy Jets", 14, 14, 33, 3900, "6,52", "CHALLENGER-850-VQ-BOV_1.jpg, CHALLENGER-850-VQ-BOV_2.jpg, CHALLENGER-850-VQ-BOV_3.jpg, CHALLENGER-850-VQ-BOV_4.jpg"],
			"8": ["Hawker 1000 VP-BMY", "Midsize Jets", 8, 8, 28, 5100, "2.04", "Hawker-1000-VP-BMY_1.jpg, Hawker-1000-VP-BMY_2.jpg, Hawker-1000-VP-BMY_3.jpg, Hawker-1000-VP-BMY_4.jpg"],
		};
		var config = {
			apiKey: "AIzaSyCFP1R4wHvhpU9t2BPFXI4LElbwIhfxD94",
			authDomain: "sirius-app-4021f.firebaseapp.com",
			databaseURL: "https://sirius-app-4021f.firebaseio.com",
			projectId: "sirius-app-4021f",
			storageBucket: "sirius-app-4021f.appspot.com",
			messagingSenderId: "91652656168"
		};
		firebase.initializeApp(config);

		function toTitleCase (str) {
    		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		}

		$('.addbutton').click(function(){
			let ref = firebase.database().ref('DealsFull');

			var images = $('.s11').val().split(', ');

			var startDate = Number(new Date($('.s16').val()+":00.000Z").getTime()/1000);
			var endDate   = Number(new Date($('.s6').val()+":00.000Z").getTime()/1000);
			var fromCity = toTitleCase($('.s8').val());
			var toCity = toTitleCase($('.s17').val());
			


			ref.child('"'+$('.s1').val()+'"').set({
				BagVol: parseFloat($('.s2').val()),
				Cost: Number($('.s3').val()),
				CostType: $('.s4').val(),
				Currency: $('.s5').val(),
				EndDate: endDate,
				FlyghtDistance: Number($('.s7').val()),
				// FromCity: $('.s8').val(),
				FromCity: fromCity,
				FromCityCode: $('.s9').val(),
				IdSite: Number($('.s10').val()),
				Images: images,
				MaxPlaneSeats: Number($('.s12').val()),
				Plane: $('.s13').val(),
				PlaneClass: $('.s14').val(),
				PlaneSeats: Number($('.s15').val()),
				StartDate: startDate,
				// ToCity: $('.s17').val(),
				ToCity: toCity,
				ToCityCode: $('.s18').val(),
			});

			let newid = Number($('.s1').val())+1;
			$('.s1').val(newid);

			$.ajax({
				type: "POST",
				url: '/addemptylegs/changeid.php?id=' + newid,
				beforeSend: function() {

				},
				success: function(data) {
				}
			});


			alert('Добавлен!');
		});
		$(".s21").change(function() {
			let fields = $(".s21").val();
			$(".s8").val(cities[fields][0]);
			$(".s9").val(cities[fields][1]);
		});
		$(".s22").change(function() {
			let fields = $(".s22").val();
			$(".s17").val(cities[fields][0]);
			$(".s18").val(cities[fields][1]);
		});
		$(".s20").change(function() {
			var active = $(".s20").val();
			$(".s13").val(airplane[active][0]);
			$(".s14").val(airplane[active][1]);
			$(".s12").val(airplane[active][2]);
			$(".s15").val(airplane[active][3]);
			$(".s10").val(airplane[active][4]);
			$(".s7").val(airplane[active][5]);
			$(".s2").val(airplane[active][6]);
			$(".s11").val(airplane[active][7]);
		});

	</script>
</body>
</html>