<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
\CModule::IncludeModule('iblock');

$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$_GET["id"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    echo $arFields["~DETAIL_TEXT"];
}
?>

</body>
</html>