<img src="video/cover.jpg" class="b-video__poster" alt="">
<video class="b-video__show" id="mainvideo" autoplay="autoplay" loop muted>
  <source src="/video/video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'/>
  <source src="/video/video.webm" type='video/webm; codecs="vp8, vorbis"'/>
  <source src="/video/video.ogv" type='video/ogg; codecs="theora, vorbis"'/>
</video>
<div class="b-video-content">
    <div class="b-video-content_i active"><span>Безопасность</span></div>
    <div class="b-video-content_i"><span>Комфорт</span></div>
    <div class="b-video-content_i"><span>Конфиденциальность</span></div>
    <div class="b-video-content_i"><span>Serious Business -  Serious approach - Sirius Aero</span></div>
</div>
