<?if(false):?>
	<img class='logo-norm' src="<?= SITE_TEMPLATE_PATH ?>/img/logo.jpg" alt="">
	<img class='logo-black' src="<?= SITE_TEMPLATE_PATH ?>/img/logo-black.jpg" alt="">
<?else:?>
	<div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:290px; height:64px">
		<canvas id="canvas" width="290" height="64" style="display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
		<div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:290; height:64px; position: absolute; left: 0px; top: 0px; display: block;"></div>
	</div>
	<script src="https://code.createjs.com/1.0.0/createjs.min.js"></script>
	<script src="/js/sirius_logo.js"></script>
	<script>
		var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
		function init() {
			canvas = document.getElementById("canvas");
			anim_container = document.getElementById("animation_container");
			dom_overlay_container = document.getElementById("dom_overlay_container");
			var comp=AdobeAn.getComposition("B2E14D971C8FCD449EC654FD37E82D0B");
			var lib=comp.getLibrary();
			handleComplete({},comp);
		}
		function handleComplete(evt,comp) {
			//This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
			var lib=comp.getLibrary();
			var ss=comp.getSpriteSheet();
			exportRoot = new lib.ВОССТАНОВИТЬ_Безымянный3_202010783444();
			stage = new lib.Stage(canvas);	
			//Registers the "tick" event listener.
			fnStartAnimation = function() {
				stage.addChild(exportRoot);
				createjs.Ticker.framerate = lib.properties.fps;
				createjs.Ticker.addEventListener("tick", stage);
			}	    
			//Code to support hidpi screens and responsive scaling.
			AdobeAn.makeResponsive(false,'both',false,1,[canvas,anim_container,dom_overlay_container]);	
			AdobeAn.compositionLoaded(lib.properties.id);
			fnStartAnimation();
		}
		init();
	</script>
    <style>
        #canvas {
            width: 290px !important;
            height: 64px !important;
            margin: 0 auto;
        }
        #canvas {
            width: 250px !important;
            height: 56px !important;
            margin: 0 auto;
        }
        #animation_container {
            width: 100% !important;
            height: 64px !important;
            padding-top: 5px !important;
        }
		@media screen and (max-width: 768px) {
			#canvas {
				width: 166px !important;
	            height: auto !important;
				margin: 0;
			}
			#animation_container {
				display: flex;
				align-items: center;
				height: 41px !important;
			}
			#dom_overlay_container {
				width: 100% !important;
			}
		}
		@media screen and (max-width: 374px) {
			.message-happy {
				right: 123px;
			}
		}
    </style>
<?endif;?>