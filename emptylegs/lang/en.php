<?
$LANG_PAGE = [
    'button_order'  => 'Order',
    'class'         => 'Class',
    'maxpas'        => 'Max. number of passengers',
    'obj'           => 'Volume of baggage compartment',
    'dal'           => 'Flight distance',
    'plane'         => 'Detailed specification of the aircraft',
    'button_pl'     => 'Book a flight',
];
?>
