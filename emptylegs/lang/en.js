var lang = {
    'lang'              : 'en/',
    'priceTypeOne'      : 'Price upon request',
    'priceTypeTwo'      : 'for plane',
    'priceTypeThree'    : 'за место',
    'order'             : 'Order',
    'orderReq'          : 'Learn more about the price',
    'orderBtn'          : 'Book a flight',
    'seats'             : 'seats',
    'class'             : 'Class',
    'maxpass'           : 'Max. number of passengers',
    'volume'            : 'Volume of baggage compartment',
    'longFly'           : 'Flight distance',
    'allParam'          : 'Detailed specification of the aircraft',
    'from'              : 'from'
};