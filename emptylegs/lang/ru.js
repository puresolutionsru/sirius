var lang = {
    'lang'              : '',
    'priceTypeOne'      : 'Цена по запросу',
    'priceTypeTwo'      : 'за самолет',
    'priceTypeThree'    : 'за место',
    'order'             : 'Заказать',
    'orderReq'          : 'Узнать стоимость',
    'orderBtn'          : 'Заказать рейс',
    'seats'             : 'мест',
    'class'             : 'Класс',
    'maxpass'           : 'Макс. число пассажиров',
    'volume'            : 'Объем багажного отделения',
    'longFly'           : 'Дальность перелета',
    'allParam'          : 'Полные характеристики ВС',
    'from'              : 'от'
};