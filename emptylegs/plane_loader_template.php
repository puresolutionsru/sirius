<? include_once __DIR__."/lang/".LANGUAGE_ID.".php"; ?>
<?
$mtlid = substr($_REQUEST['mtlid'], 2);
?>
<script>
    var alertid = <?=$mtlid?>;
    $("body").addClass("compensate-for-scrollbar");
</script>
<div class="alert alert-plane alert_success alert_no-animation">
    <div class="alert-background"></div>
    <div class="arrow-plane arrow-left-plane"></div>
    <div class="arrow-plane arrow-right-plane"></div>
    <div class="main-w main-w_plane" id="planePage">
        <div class="alert-close"></div>
        <div class="plane__period-w">
            <div class="plane__period"><span class="animation-js"><div class="line" style="width:250px; height:26px;"></div></span></div>
        </div>
        <div class="plane__t"><span class="animation-js animation-js-t"><div class="line" style="margin: 0 auto; height:44px;"></div></span></div>
        <div class="plane__route">
            <div class="plane__route__from plane__route__ar"><span><span class="animation-js"><div class="line" style="width:100%; height:26px;"></div></span></span><span class="animation-js"><div class="line" style="width:40%; height:18px;"></div></span></div>
            <div class="plane__route__center"></div>
            <div class="plane__route__to plane__route__ar"><span><span class="animation-js"><div class="line" style="width:100%; height:26px;"></div></span></span><span class="animation-js"><div class="line" style="width:50%; height:18px; float:right;"></div></span></div>
        </div>
        <div class="plane__info">
            <div class="plane__img plane__info__column">
                <span class="animation-js animation-js-photo"><div class="line"></div></span>
            </div>
            <div class="plane__info__column">
                <div class="plane__type"><span class="animation-js"><div class="line" style="width:60%; height:26px;"></div></span></div>
                <div class="plane__param">
                    <div class="plane__param__row">
                        <div class="plane__param__t"><?= $LANG_PAGE['class'] ?></div>
                        <div class="plane__param__value"><span class="animation-js" style="width: 90px; display:block;"><div class="line" style="width:60%; height:26px;"></div></span></div>
                    </div>
                    <div class="plane__param__row">
                        <div class="plane__param__t"><?= $LANG_PAGE['maxpas'] ?></div>
                        <div class="plane__param__value"><span class="animation-js" style="width: 90px; display:block;"><div class="line" style="width:60%; height:26px;"></div></span></div>
                    </div>
                    <div class="plane__param__row">
                        <div class="plane__param__t"><?= $LANG_PAGE['obj'] ?></div>
                        <div class="plane__param__value"><span class="animation-js" style="width: 90px; display:block;"><div class="line" style="width:60%; height:26px;"></div></span></div>
                    </div>
                    <div class="plane__param__row">
                        <div class="plane__param__t"><?= $LANG_PAGE['dal'] ?></div>
                        <div class="plane__param__value"><span class="animation-js" style="width: 90px; display:block;"><div class="line" style="width:60%; height:26px;"></div></span></div>
                    </div>
                </div>
                <a href="/fleet" class="plane__link-page" target="_blank"><?= $LANG_PAGE['plane'] ?></a>
                <div class="plane__bottom">
                    <div class="plane__price"><span class="animation-js" style="width: 150px; display:block;"><div class="line" style="width:60%; height:36px;"></div></span></div>
                    <div class="mlt-intem__buy-btn"><?= $LANG_PAGE['button_pl'] ?></div>
                </div>
            </div>
        </div>
    </div>
</div>