<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Empty Legs  - текущие предложения авиакомпании Sirius Aero. Программа позволяет съэкономить до 75% при перелетах на возвратных рейсах.");
$APPLICATION->SetPageProperty("title", "Empty Legs - актуальные возвратные рейсы авиакомпании Сириус-Аэро");
$APPLICATION->SetPageProperty("title_legs", "Что такое Empty Legs");
$APPLICATION->SetTitle("Empty Legs");
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <h1>' . $APPLICATION->GetTitle(false) . '</h1>
    ';

if($_REQUEST['mtlid']) {
    include_once "plane_loader_template.php";
}

echo '<div class="mlt-description">
        <div class="mlt-description__button">'.$APPLICATION->getProperty("title_legs").'</div>
        <div class="mlt-description__text">'
?><p>
	<strong>Empty Legs</strong> - это возвратный рейс без пассажиров. Это происходит, когда воздушное судно высаживает пассажиров в аэропорту назначения и возвращается на аэродром приписки или когда самолет летит, чтобы забрать пассажиров в другом аэропорту. Вы можете сэкономить до 75% от стоимости обычного чартера.
</p>
 <?
echo '  </div>
      </div>';

      ?>
<div class="b-app-info">
	<div class="b-app-info__t">
		Установите наше приложение и будьте в курсе о новых рейсах:
	</div>
	<div class="b-app-info__list">
		<div class="b-app-info__i">
 <a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios_emptyleg'); return true;" class="appstore" target="_blank"></a>
		</div>
		<div class="b-app-info__i">
 <a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android_emptyleg'); return true;" class="gplay" target="_blank"></a>
		</div>
	</div>
</div><?


include_once "loader_template.php";
echo '
    <script src="/emptylegs/lang/ru.js" defer></script>
    <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js" defer></script>
    <script src="/emptylegs/lib/cardTemplate.js" defer></script>
    <script src="/emptylegs/lib/planeTemplate.js" defer></script>
    <script src="/emptylegs/firebase.js" defer></script>
    </div>
</div>
';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>