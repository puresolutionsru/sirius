function templateCard(data, id) {
	let img = "";
	for(var i=0; i<data.Images.length; i++) {
		img += "<img src='https://firebasestorage.googleapis.com/v0/b/sirius-app-4021f.appspot.com/o/planeImages%2F"+ data.Images[i] +"?alt=media&token=6ac3caa1-39ed-4f33-b1f4-1bd25af5697a' alt=''>";
		break;
	}
	let startDate = formatDate(data.StartDate);
	let endDate = formatDate(data.EndDate);

	$.ajax({
		method: 'POST',
		url: '/ajax/linkplane.php?id='+data.IdSite,
		success: function(ans) {
			var ans = JSON.parse(ans);
			if(!ans['error']) {
				data.linkFull = ans['link'];
				$('.mlt-item[data-id="'+id+'"] .link-plane').attr('href', ans['link']);
			}
		}
	});

	if(data.CostType == "request") {
		var price = '<span class="price_request">'+lang.priceTypeOne+'</span>';
	} else if(data.CostType == "plane") {
		var price = lang.from+' '+data.Currency+' '+data.Cost+'<span>'+lang.priceTypeTwo+'</span>';
	} else {
		var price = lang.from+' '+data.Currency+' '+data.Cost+'<span>'+lang.priceTypeThree+'</span>';
	}

	id = id.replace(/"/g, '');
	let card = '<div class="mlt-item" data-id="'+id+'">'+
					'<div class="mlt-item__photo mlt-item__photo_on"><span class="animation-js animation-js-photo"><div class="line"></div></span>'+img+'</div>'+ 
					'<div class="mlt-item__info">'+
						'<div class="mlt-item__flight">'+
							'<div class="mlt-item__cities"><span class="departure">'+data.FromCity+', '+data.FromCityCode+'</span><span class="destination">'+data.ToCity+', '+data.ToCityCode+'</span></div>'+
							'<div class="mlt-item__time-period">'+
								'<span class="mlt-item__time-period__info"><span>'+startDate.date+'.'+startDate.month+'.'+startDate.year+'</span><span class="mlt-item__time-period__info__time">'+toHHMMSS(data.StartDate, 'hours')+':'+toHHMMSS(data.StartDate, 'minutes')+'</span></span>'+
								'<span class="mlt-item__time-period__info mlt-item__time-period__info_razd">–</span>'+
								'<span class="mlt-item__time-period__info"><span>'+endDate.date+'.'+endDate.month+'.'+endDate.year+'</span><span class="mlt-item__time-period__info__time">'+toHHMMSS(data.EndDate, 'hours')+':'+toHHMMSS(data.EndDate, 'minutes')+'</span></span>'+
							'</div>'+
						'</div>'+
						'<div class="mlt-item_plane"><a href="" target="_blank" class="link-plane">'+data.Plane+' <span>'+data.MaxPlaneSeats+'&nbsp;'+lang.seats+'</span></a></div>'+
						'<div class="mlt-item__buy-block">'+
							'<div class="mil-item__price">'+price+'</div>';
							if(data.CostType == "request") {
								card += '<a href="#order" class="mlt-intem__buy-btn">'+lang.orderReq+'</a>';
							} else {
								card += '<a href="#order" class="mlt-intem__buy-btn">'+lang.order+'</a>';
							}
				card += '</div>'+
					'</div>'+
				'</div>';
	let mlt = document.getElementById("mlt");
	mlt.innerHTML += card;
}

function toHHMMSS(value, type) {
	var sec_num = value; // don't forget the second param
	var day = Math.floor(sec_num/86400);
	var hours   = Math.floor((sec_num-day*86400)/3600);
    var minutes = Math.floor((sec_num - (day*86400 + hours*3600))/60);
	var seconds = sec_num - (hours * 3600) - (minutes * 60);
	
	hours += 3;
	if((hours-24)>=0) {
		hours = hours-24;
	}

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
	if (seconds < 10) {seconds = "0"+seconds;}
	if(type=="hours") {
		return hours;
	} else if(type=="minutes") {
		return minutes;
	} else {
		return seconds;
	}
}