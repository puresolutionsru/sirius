function templatePlane(id) {
	let startDate = formatDate(info[id].StartDate);
	let endDate = formatDate(info[id].EndDate);

	if(info[id].CostType == "request") {
		var price = '<span class="price_request">'+lang.priceTypeOne+'</span>';
	} else if(info[id].CostType == "plane") {
		var price = lang.from+' '+info[id].Currency+' '+info[id].Cost+'<span>'+lang.priceTypeTwo+'</span>';
	} else {
		var price = lang.from+' '+info[id].Currency+' '+info[id].Cost+'<span>'+lang.priceTypeThree+'</span>';
	}

	let img = "";
	for(let i=0; i<info[id].Images.length; i++) {
		img += "<a href='"+info[id].linkFull+"' target='_blank'><img src='https://firebasestorage.googleapis.com/v0/b/sirius-app-4021f.appspot.com/o/planeImages%2F"+ info[id].Images[i] +"?alt=media&token=6ac3caa1-39ed-4f33-b1f4-1bd25af5697a' alt=''></a>";
	}
	let html = '<div class="main-w main-w_plane">'+
					'<div class="alert-close"></div>'+
					'<div class="plane__period-w"><div class="plane__period">'+startDate.date+'.'+startDate.month+'.'+startDate.year+'<span class="mlt-item__time-period__info__time">'+toHHMMSS(info[id].StartDate, 'hours')+':'+toHHMMSS(info[id].StartDate, 'minutes')+'</span> - '+endDate.date+'.'+endDate.month+'.'+endDate.year+'<span class="mlt-item__time-period__info__time">'+toHHMMSS(info[id].EndDate, 'hours')+':'+toHHMMSS(info[id].EndDate, 'minutes')+'</span></div></div>'+
					'<div class="plane__t">'+info[id].FromCity+' - '+info[id].ToCity+'</div>'+
                    '<div class="plane__route"><div class="plane__route__from plane__route__ar"><span>'+info[id].FromCityCode+'</span>'+info[id].FromCity+'</div><div class="plane__route__center"></div><div class="plane__route__to plane__route__ar"><span>'+info[id].ToCityCode+'</span>'+info[id].ToCity+'</div></div>'+
                    '<div class="plane__info">'+
                        '<div class="plane__img plane__info__column">'+img+'</div>'+
                        '<div class="plane__info__column">'+
                            '<div class="plane__type">'+info[id].Plane+'</div>'+
                            '<div class="plane__param">'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.class+'</div>'+
                                    '<div class="plane__param__value">'+info[id].PlaneClass+'</div>'+
                                '</div>'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.maxpass+'</div>'+
                                    '<div class="plane__param__value">'+info[id].MaxPlaneSeats+'</div>'+
                                '</div>'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.volume+'</div>'+
                                    '<div class="plane__param__value">'+info[id].BagVol+' м<sup>3</sup></div>'+
                                '</div>'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.longFly+'</div>'+
                                    '<div class="plane__param__value">'+info[id].FlyghtDistance+'</div>'+
                                '</div>'+
							'</div>'+
							'<a href="'+info[id].linkFull+'" class="plane__link-page" target="_blank">'+lang.allParam+'</a>'+
                            '<div class="plane__bottom">'+
                                '<div class="plane__price">'+price+'</div>';
                                if(info[id].CostType == "request") {
                                    html += '<a href="#order" class="mlt-intem__buy-btn" data-id="'+id+'">'+lang.orderReq+'</a>';
                                } else {
                                    html += '<a href="#order" class="mlt-intem__buy-btn" data-id="'+id+'">'+lang.orderBtn+'</a>';
                                }
                        html += '</div>'+
                        '</div>'+
                    '</div>'+
				'</div>';
	return html;
}