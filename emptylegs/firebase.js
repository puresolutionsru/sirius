var mltDescriptionButton = document.getElementsByClassName("mlt-description__button")[0];
mltDescriptionButton.onclick = function(){
	this.parentElement.classList.toggle("hide-description");
}
// keyboard event
document.onkeyup = function(e){
	if (e.keyCode === 27){//esc
		closeModal();
	}
}
function formatDate(timestap) {
	let time = new Object();
	let date = new Date(timestap * 1000 - 10800);
	// date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
	time.year = date.getFullYear();
	time.date = date.getDate();
	time.month = date.getMonth()+1;
	if(time.month<10) {
		time.month = '0' + time.month;
	}

	return time;
}
function alertUpScroll() {
	$('body').addClass('compensate-for-scrollbar');
	$('body').css('padding-right', '17px');
}
function alertDownScroll() {
	$('body').removeClass('compensate-for-scrollbar');
	$('body').css('padding-right', '0px');
}







// Start firebase
var alert;
var info = Array();

var config = {
    apiKey: "AIzaSyCFP1R4wHvhpU9t2BPFXI4LElbwIhfxD94",
    authDomain: "sirius-app-4021f.firebaseapp.com",
    databaseURL: "https://sirius-app-4021f.firebaseio.com",
    projectId: "sirius-app-4021f",
    storageBucket: "sirius-app-4021f.appspot.com",
    messagingSenderId: "91652656168"
};
firebase.initializeApp(config);
if(window.alertid) {
	pushAlert(alertid);
}


var starCountRef = firebase.database().ref('DealsFull');
starCountRef.orderByChild('StartDate').once('value').then(function(snapshot) {
	document.getElementById('mlt').innerHTML = '';

	var values = {};
	snapshot.forEach(child => {
		values[child.key] = child.val();
	});

	// var values = snapshot.val();
	var length = Object.keys(values).length;
	var keys = Array();

	var m = 0;
	for(var key in values) {
		keys[m] = key;
		m++;
	}

	for(var i=0; i<length; i++) { 
		var key = keys[i].replace(/"/g, '');;
		var data = values[keys[i]];
		info[key] = values[keys[i]];
		templateCard(data, key);
	}
});

function templateModal(id) {
	$('.alert').remove();
	var alert = document.createElement('div');
	alert.className = "alert alert-plane";
	alert.innerHTML = "<div class='alert-background'></div><div class='arrow-plane arrow-left-plane'></div><div class='arrow-plane arrow-right-plane'></div>";
	alert.innerHTML += templatePlane(id);

	document.body.appendChild(alert);

	let a = new Date(info[id].StartDate * 1000);

	var lk = window.location.href;
    var mod = 'SA' + id + '/' + info[id].FromCityCode + '-' + info[id].ToCityCode + '-' + a.getDate() + '-' + a.getMonth() + '-' + a.getFullYear();
    mod = mod.replace('#','');

    if(lk.indexOf("?") == "-1") {
        lk = lk + '' + mod;
    } else {
        var change = lk.split('?');
        lk = change[0] + '' + mod;
    }
	history.pushState(null, null, lk);
	alert.className += " alert_success";
}

function templateModalOrder(id) {
	$('.modal-form').removeClass('modal-form_hide');
	$('.modal-form__success').removeClass('modal-form__success_active');

	$('#modal-form__select-pass').html('');
	$('#raceid').val(id);

	let startDate = formatDate(info[id].StartDate);
	let endDate = formatDate(info[id].EndDate);

	document.getElementById('modal-form__from').innerText = info[id].FromCity;
	document.getElementById('modal-form__to').innerText = info[id].ToCity;
	document.getElementById('modal-form__datefrom').innerText = startDate.date+'.'+startDate.month+'.'+startDate.year+' - '+endDate.date+'.'+endDate.month+'.'+endDate.year;
	if(info[id].CostType == "seat") {
		for(let i=1; i<=info[id].MaxPlaneSeats; i++) {
			$('.form__item-select').css('display', 'flex');
			let opt = $('#modal-form__select-pass');
			opt.html(opt.html() + '<option>'+i+'</option>');
		}
	} else {
		$('.form__item-select').css('display', 'none');
	}
	alertUpScroll();
	$('#form-orderMtl').addClass('form-orderMtl_active');
	yaCounter28830840.reachGoal('emptyleg_open_form');
}
// click on plane card
$('body').on('click', '.mlt-item', function(e){
	e.preventDefault();
	if(e.target.classList.contains('mlt-intem__buy-btn')) {
		templateModalOrder($(this).attr('data-id'));
	} else if(e.target.classList.contains('link-plane')) {

	} else {
		alertUpScroll();
		templateModal($(this).attr("data-id"));
	}
});


// send order
$('body').on('click', '.form-orderMtl__submit', function(e){
	e.preventDefault();
	let id = $('.form-orderMtl input[name="raceid"]').val();
	let name = $('.form-orderMtl input[name="name"]').val();
	let phone = $('.form-orderMtl input[name="phone"]').val();
	let pass = $('#modal-form__select-pass').val();
	let email = '';
	if($('.form-orderMtl input[name="email"]').val()) {
		email = $('.form-orderMtl input[name="email"]').val();
	}
	
	let countError = 0;

	if(!id) {
		
	}

	if(!phone) {
		$('.form-orderMtl input[name="phone"]').addClass('it_error');
		countError++;
	} else {
		$('.form-orderMtl input[name="phone"]').removeClass('it_error');
	}

	if(countError) {
		return;
	}

	var dealId = '"'+id+'"';
	let ref = firebase.database().ref('booking');
	ref.push().set({
		userName: name,
		userPhone: phone,
		userEmail: email,
		seats: pass,
		deal: dealId,
		type: info[id].CostType,
	});

	yaCounter28830840.reachGoal('emptyleg_order');
	// ym(28830840, 'reachGoal', 'ya_order');

	$('.modal-form').addClass('modal-form_hide');
	$('.modal-form__success').addClass('modal-form__success_active');
});
// click on order button
$('body').on('click', '.plane__bottom .mlt-intem__buy-btn', function(e){
	e.preventDefault();
	closeModal();
	alertUpScroll();
	templateModalOrder($(this).attr('data-id'));
});
// close modal window
$('body').on('click', '.alert-close, .alert-background, .form-orderMtl-below', function(e){
	closeModal();
});
function closeModal() {
	$('.alert').addClass('alert_close');
	$('.form-orderMtl_active').removeClass('form-orderMtl_active');
	alertDownScroll();
	history.pushState(null, null, '/'+lang.lang+'emptylegs/');
}
function pushAlert(id) {
	id = '"'+id+'"';
	var starCountRef = firebase.database().ref('DealsFull/'+id);
	starCountRef.orderByChild('StartDate').on('value', function(snapshot) {
		var values = snapshot.val();
		info[id] = values;
		var html = templatePlane(id);

		html = html.replace('<div class="main-w main-w_plane">', '');
		html = html.slice(0, -6);
		$("#planePage").html(html);

	});
}