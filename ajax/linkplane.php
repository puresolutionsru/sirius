<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
\CModule::IncludeModule('iblock');

if(empty($_GET['id'])) :
	$json['error'] = true;
	echo json_encode($json);
	exit();
endif;

$json['error'] = false;

$res = CIBlockElement::GetList(array(), array('ID'=>$_GET['id'], "ACTIVE"=>"Y", "IBLOCK_ID"=>7));
while($ob = $res->GetNextElement()) {
	$arFields = $ob->GetFields();
	$json['link'] = 'https://sirius-aero.ru'.$arFields['DETAIL_PAGE_URL'];

	echo json_encode($json);
}

