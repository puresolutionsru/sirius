<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if(LANGUAGE_ID == 'ru'){
    $APPLICATION->SetPageProperty("title", 'Страница не найдена');
}
else {
    $APPLICATION->SetPageProperty("title", 'Page Not Found');
}
if(LANGUAGE_ID != 'ru'){
echo '<a href="/" title="на главную"><div class="page404"></div></a>';
}
else {
 echo '<div class="b-content _page">
    <div class="the-content main-w"><h1 > 404 Страница не найдена</h1>


<p><span style="font-weight:bold;">Возможно, это случилось по одной из следующих причин:</span></p>

<ul>
<li>Вы ошиблись при наборе адреса страницы (URL);

</li>
<li>Вы перешли по «битой» (неработающей, неправильной) ссылке; запрашиваемой страницы никогда не было на сайте или она была удалена/перемещена.

</li>
</ul>

<p><span style="font-weight:bold;">Чтобы решить проблему, попробуйте следующее:</span></p>

<ul>
<li>вернуться назад при помощи кнопки браузера Back;

</li>
<li>проверить правильность написания адреса страницы (URL);

</li>
<li>перейти на <a href="https://sirius-aero.ru/">главную страницу</a>;

</li>
<li><a href="mailto:info@sirius-aero.ru">написать нам почту</a>;

</li>
<li>воспользуйтесь <a href="https://sirius-aero.ru/sitemap/">картой сайта</a>.

</li>
</ul>
</div>
</div>

 ';
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
