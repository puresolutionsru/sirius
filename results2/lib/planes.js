var planes = {
    "H28": {
        "name": "Hawker 850 XP",
        "number": "VP-BNW",
        "class": "Midsize Jets",
        "count": "8",
        "configuration": "/results/img/h850.jpg",
        "small": "/results/img/s-h850.jpg",
        "photo": "",
        "description": "<p>Рейс, на который уже собрано количество пассажиров, необходимое для выполнения рейса.</p><p>Гарантированный рейс. У перелета есть точная дата. Стоимость делится на всех пассажиров.</p>",
        "map": {
            "1A": {
                "top": 64,
                "left": 219,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 219,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 283,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 283,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 314,
                "route": "bottomleft",
            },
            "3B": {
                "top": 4,
                "left": 346,
                "route": "right",
            },
            "4A": {
                "top": 63,
                "left": 347,
                "route": "bottomcenter",
            },
            "5A": {
                "top": 63,
                "left": 380,
                "route": "bottomright",
            },
        }
    },
    "CRJ": {
        "name": "Hawker 850 XP",
        "number": "VP-BNW",
        "class": "Midsize Jets",
        "count": "8",
        "configuration": "/results/img/h850.jpg",
        "small": "/results/img/s-h850.jpg",
        "photo": "",
        "description": "<p>Рейс, на который уже собрано количество пассажиров, необходимое для выполнения рейса.</p><p>Гарантированный рейс. У перелета есть точная дата. Стоимость делится на всех пассажиров.</p>",
        "map": {
            "1A": {
                "top": 64,
                "left": 219,
                "route": "left",
            },
            "1B": {
                "top": 4,
                "left": 219,
                "route": "left",
            },
            "2A": {
                "top": 64,
                "left": 283,
                "route": "right",
            },
            "2B": {
                "top": 4,
                "left": 283,
                "route": "right",
            },
            "3A": {
                "top": 63,
                "left": 314,
                "route": "bottomleft",
            },
            "3B": {
                "top": 4,
                "left": 346,
                "route": "right",
            },
            "4A": {
                "top": 63,
                "left": 347,
                "route": "bottomcenter",
            },
            "5A": {
                "top": 63,
                "left": 380,
                "route": "bottomright",
            },
        }
    },
    "X75": {
        "name": "Hawker 750",
        "number": "VQ-BBK",
        "class": "Midsize Jets",
        "count": "8",
        "configuration": "/results/img/h750.jpg",
        "small": "/results/img/h750.jpg",
        "photo": "",
        "description": "<p>Рейс, на который уже собрано количество пассажиров, необходимое для выполнения рейса.</p><p>Гарантированный рейс. У перелета есть точная дата. Стоимость делится на всех пассажиров.</p>",
    },
    "H21": {
        "name": "Hawker 1000",
        "number": "VP-BMY",
        "class": "Midsize Jets",
        "count": "9",
        "configuration": "/results/img/h1000.jpg",
        "small": "/results/img/s-h1000.jpg",
        "photo": "",
        "description": "<p>Рейс, на который уже собрано количество пассажиров, необходимое для выполнения рейса.</p><p>Гарантированный рейс. У перелета есть точная дата. Стоимость делится на всех пассажиров.</p>",
    },
    "EM6": {
        "name": "Legacy 600",
        "number": "VP-BGT",
        "class": "Heavy Jets",
        "count": "13",
        "configuration": "/results/img/l600.jpg",
        "small": "/results/img/s-l600.jpg",
        "photo": "",
        "description": "<p>Рейс, на который уже собрано количество пассажиров, необходимое для выполнения рейса.</p><p>Гарантированный рейс. У перелета есть точная дата. Стоимость делится на всех пассажиров.</p>",
    }
}