<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');

$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);

if($_GET['orderId']) { ?>
    <div class="toptop"></div>
    <style>
        .toptop {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 10;
            background-color: rgba(0,0,0,.3);
        }
    </style>
    <script>
        var session = "<?=$data->session?>";
        var fare = <?=$data->fare?>;
        var token = "<?=$_GET['order']?>";
        var firstname = "<?=$data->firstname?>";
        var lastname = "<?=$data->lastname?>";
        var numberFlight = "<?=$data->numberFlight?>";
    </script>
    <script>
            // var xhr = new XMLHttpRequest();
            // xhr.open('GET', 'https://sirius-aero.ru/enginetais/Describes/set_flight/?flight='+numberFlight+'&fare='+fare+'&session='+session, true);
            // xhr.send();
            // xhr.onreadystatechange = function() {
            //     if (xhr.readyState != 4) return;
            //     if (xhr.status != 200) {} 
            //     else {
            //         var responseText = JSON.parse(xhr.responseText);
            //         if(responseText.Result === "SUCCESS") {
                        var addpass = new XMLHttpRequest();
                        addpass.open('GET', 'https://sirius-aero.ru/enginetais/Describes/booking_place/?flight=2&firstname='+firstname+'&lastname='+lastname+'&session='+session, true);
                        addpass.send();
                        addpass.onreadystatechange = function() {
                            if (addpass.readyState != 4) return;
                            if (addpass.status != 200) {}
                            else {
                                var dataPass = JSON.parse(addpass.responseText);
                                console.log(dataPass);
                                
                                var issue = new XMLHttpRequest();
                                let bookingreference = dataPass['GeneralInfo']['@attributes']['BookingReference'];
                                let leadpassenger = dataPass['Passengers']['Passenger']['@attributes']['LastName'];
                                let currency = dataPass['FareInfo']['Fares']['Fare']['Price']['@attributes']['Currency'];
                                let amount = dataPass['FareInfo']['Fares']['Fare']['Price']['@attributes']['Total'];
                                issue.open('GET', 'https://sirius-aero.ru/enginetais/Describes/issue_ticket/?bookingreference='+bookingreference+'&leadpassenger='+leadpassenger+'&currency='+currency+'&amount='+amount, true);
                                issue.send();
                                issue.onreadystatechange = function() {
                                    if (issue.readyState != 4) return;
                                    if (issue.status != 200) {}
                                    else {
                                        let tick = JSON.parse(issue.responseText);
                                        let ticket = tick['TicketInfo']['Ticket']['@attributes']['TicketNumber'];
                                        let rloc = tick['TicketInfo']['Ticket']['TicketData']['IssueData']['@attributes']['IssueRLOC'];

                                        let lk = 'https://ibe.sirius-aero.ru/order/success/?order=' + token + '&ticket=' + ticket + '&loc=' + rloc;
                                        document.location.href = lk;
                                    }
                                }
                            }
                        }
            //         }
            //     }
            // }
    </script>
<?}?>
<div class="breadcrumbs">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper order-payment">
    <!-- <div class="bank-card">
        <div class="bank-card__top">
            <div class="bank-card__types">
                <img src="/bitrix/templates/ibe/img/visa.svg" alt="">
                <img src="/bitrix/templates/ibe/img/master.svg" alt="">
                <img src="/bitrix/templates/ibe/img/f.svg" alt="">
                <img src="/bitrix/templates/ibe/img/jcb.svg" alt="">
                <img src="/bitrix/templates/ibe/img/maestro.svg" alt="">
                <img src="/bitrix/templates/ibe/img/mir.svg" alt="">
            </div>
            <div>
                <input type="text" placeholder="Номер карты">
            </div>
            <div class="bank-card__date">
                <input class="small" type="text" placeholder="мм"> / <input class="small" type="text" placeholder="гг">
            </div>
            <div>
                <input type="text" placeholder="ИМЯ И ФАМИЛИЯ, КАК НА КАРТЕ">
            </div>
        </div>
        <div class="bank-card__bottom">
            <div class="bank-card__line"></div>
            <div class="bank-card__cvc">CVV2 / CVC2</div>
            <div class="bank-card__cvc-code">
                <input type="password" placeholder="">
            </div>
        </div>
    </div>
    <div class="payment-info">
        <div class="payment-info__data">
            <div class="payment-info__data__i">
                получатель платежа:
                <span>ООО АК «Сириус-Аэро»</span>
            </div>
            <div class="payment-info__data__i">
                сумма к оплате:
                <span class="price"><?=$data->onlyPrice?> €</span>
            </div>
        </div>
        <div class="payment-info__btn">Оплатить</div>
    </div> -->
</div>

<script>
    var session = "<?=$data->session?>";
    var fare = <?=$data->fare?>;
    var token = "<?=$_GET['order']?>";
</script>
<script src="/results/order/payment/script.js"></script>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>