<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');

$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);

$dayofweek = array(
    '', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'
);

$_monthsList = array(
    "1"=>"января",
    "2"=>"февраля",
    "3"=>"марта",
    "4"=>"апреля",
    "5"=>"мая",
    "6"=>"июня",
    "7"=>"июля",
    "8"=>"августа",
    "9"=>"сентября",
    "10"=>"октября",
    "11"=>"ноября",
    "12"=>"декабря"
);

$date = $data->formatDate;
$month = $_monthsList[date("n", strtotime($date))];
$day = $dayofweek[date("N", strtotime($date))];
?>




<div class="breadcrumbs">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper">
    <div class="data-sender">
        <div class="data-sender__ticket">
            <div class="data-sender__ticket__info">
                <div class="data-sender__ticket__info__order">
                    <div class="data-sender__ticket__info__order__t">заказ:</div>
                    <div class="data-sender__ticket__info__order__tx">516478355</div>
                </div>
                <div class="data-sender__ticket__info__code">
                    <div class="data-sender__ticket__info__code__t">код доступа:</div>
                    <div class="data-sender__ticket__info__code__tx"><span>34516D</span></div>
                </div>
            </div>
            <div class="data-sender__ticket__price">
                <div class="data-sender__ticket__price__t">цена:</div>
                <div class="data-sender__ticket__price__tx"><?=$data->priceBackspace?> <?=$data->currency?></div>
            </div>
        </div>
        <div class="data-sender__flight">
            <div class="data-sender__flight__top">
                <div class="data-sender__flight__info">
                    <div class="data-sender__flight__info__t"><?=$data->fromCity?> &#8594; <?=$data->toCity?></div>
                    <div class="data-sender__flight__info__timeofflight">Прямой рейс. Время в пути <?=$data->timeFlight?></div>
                    <div class="data-sender__flight__info__time">
                        <div class="data-sender__flight__info__time__clock"><?=$data->timeDeparture?> – <?=$data->timeArrival?></div>
                        <div class="data-sender__flight__info__time__day"><?=date("d", strtotime($date))?> <?=$month?>, <?=$day?></div>
                    </div>
                </div>
                <img src="/results/img/sirius.jpg" alt="">
            </div>
            <div class="data-sender__flight__bottom">
                <div class="data-sender__flight__tarif">
                    тариф:
                    <span><?=$data->tarif?></span>
                </div>
                <div class="data-sender__flight__plane">
                    <div class="data-sender__flight__plane__params">
                        <?=$data->plane->class?><br>
                        <?=$data->plane->name?><br>
                        <?=$data->plane->number?>
                    </div>
                    <img src="<?=$data->plane->small?>" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="data-passenger">
        <div class="data-passenger__items" data-count="1">
            <div class="data-passenger__t">Пассажир 1</div>
            <div class="data-passenger__row">
                <input type="text" class="form-it data-firstname" placeholder="Имя">
                <input type="text" class="form-it" placeholder="Отчество">
                <input type="text" class="form-it data-lastname" placeholder="Фамилия">
                <input type="radio" name="sex" id="sex1" class="form-it" value="Муж">
                <label for="sex1">Муж</label>
                <input type="radio" name="sex" id="sex2" class="form-it" value="Жен">
                <label for="sex2">Жен</label>
                <input type="text" class="form-it" placeholder="Дата рождения">
            </div>
            <div class="data-passenger__row data-passenger__for">
                <input type="text" class="form-it" placeholder="Страна">
                <input type="text" class="form-it" placeholder="Документ">
                <input type="text" class="form-it" placeholder="Номер">
                <input type="text" class="form-it" placeholder="Годен до">
            </div>
        </div>
        <div class="data-passenger__add">Добавить пассажира</div>
    </div>
    <div class="data-buyer">
        <div class="data-buyer__t">Покупатель</div>
        <div class="data-buyer__tx">Оставьте свой e-mail и телефон, и мы будем сообщать вам обо всех изменениях в вашем бронировании или <br>статусе рейса</div>
        <div class="data-buyer__form">
            <div class="data-buyer__form__row">
                <input type="text" class="form-it" placeholder="Ваше имя">
                <input type="text" class="form-it" placeholder="Телефон">
            </div>
            <div class="data-buyer__form__row">
                <div class="form-it form-it-block">
                    Создать аккаунт
                </div>
                <input type="text" class="form-it" placeholder="E-mail">
            </div>
        </div>
    </div>
    <div class="next-step">
        <div class="next-step__w">
            <a href="/order/payment/?order=<?=$_GET['order']?>" class="next-step__lk next-step__lk_create next-step__lk_active">Забронировать</a>
            <div class="next-step__tx">Шаг 3. Опции</div>
        </div>
    </div>
</div>



<script>
    var session = "<?=$data->session?>";
    var fare = <?=$data->fare?>;
    var token = "<?=$_GET['order']?>";
</script>


<script src="/results/order/script.js"></script>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>