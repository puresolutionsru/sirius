<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Оформление");
$APPLICATION->SetTitle('Оформление');

$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/enginetais/cache/'.$_GET['order'].'.json');
$data = json_decode($data);
?>
<div class="breadcrumbs">
    <div class="wrapper breadcrumbs__w">
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">1</span><span class="breadcrumbs__i__tx">Выбор тарифа</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">2</span><span class="breadcrumbs__i__tx">Ввод данных</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">3</span><span class="breadcrumbs__i__tx">Опции</span></div>
        <div class="breadcrumbs__char"></div>
        <div class="breadcrumbs__i breadcrumbs__i_last"><span class="breadcrumbs__i__number">4</span><span class="breadcrumbs__i__tx">Оплата</span></div>
        <div class="breadcrumbs__char breadcrumbs__char_active"></div>
        <div class="breadcrumbs__i breadcrumbs__i_active"><span class="breadcrumbs__i__number">5</span><span class="breadcrumbs__i__tx">Оформление</span></div>
    </div>
</div>

<div class="wrapper order-success">
    <div class="order-success__t">Благодарим за покупку.</div>
    <div class="order-success__tx">Ваш заказ № <?=$_GET['ticket']?> успешно оплачен.</div>
    <div class="data-sender order-success__data-sender">
        <div class="data-sender__ticket order-success__data-sender__ticket">
            <div class="data-sender__ticket__info order-success__data-sender__ticket__info">
                <div class="data-sender__ticket__info__order order-success__data-sender__ticket__info__order">
                    <div class="data-sender__ticket__info__order__t order-success__data-sender__ticket__info__order__t">номер билета:</div>
                    <div class="data-sender__ticket__info__order__tx order-success__data-sender__ticket__info__order__tx"><?=$_GET['ticket']?></div>
                </div>
                <div class="data-sender__ticket__info__code order-success__data-sender__ticket__info__code">
                    <div class="data-sender__ticket__info__code__t order-success__data-sender__ticket__info__code__t">локатор:</div>
                    <div class="data-sender__ticket__info__code__tx order-success__data-sender__ticket__info__code__tx"><span><?=$_GET['loc']?></span></div>
                </div>
            </div>
            <div class="data-sender__ticket__price order-success__data-sender__ticket__price">
                <div class="data-sender__ticket__price__t order-success__data-sender__ticket__price__t">пассажиров:</div>
                <div class="data-sender__ticket__price__tx order-success__data-sender__ticket__price__tx">1</div>
                <div class="data-sender__ticket__price__t order-success__data-sender__ticket__price__t order-success__data-sender__ticket__price__t_next">цена:</div>
                <div class="data-sender__ticket__price__tx order-success__data-sender__ticket__price__tx"><?=$data->price?></div>
            </div>
        </div>
        <div>
            <div class="data-sender__flight order-success__data-sender__flight">
                <div class="data-sender__flight__top">
                    <div class="data-sender__flight__info">
                        <div class="data-sender__flight__info__t"><?=$data->fromCity?> &#8594; <?=$data->toCity?></div>
                        <div class="data-sender__flight__info__timeofflight">Прямой рейс. Время в пути 4 часа</div>
                        <div class="data-sender__flight__info__time">
                            <div class="data-sender__flight__info__time__clock">08:00 – 11:00</div>
                            <div class="data-sender__flight__info__time__day">23 ноября, понедельник</div>
                        </div>
                    </div>
                    <img src="/results/img/sirius.jpg" alt="">
                </div>
                <div class="data-sender__flight__bottom">
                    <div class="data-sender__flight__tarif">
                        тариф:
                        <span><?=$data->tarif?></span>
                    </div>
                    <div class="data-sender__flight__plane">
                        <div class="data-sender__flight__plane__params">
                            Heavy Jets<br>
                            Legacy 600<br>
                            VP-BGT
                        </div>
                        <img src="/results/img/data/pla.jpg" alt="">
                    </div>
                </div>
                <div class="data-sender__enter">Выставлено на продажу: <span>0</span></div>
            </div>
            <div class="data-itog-dop">Дополнительные услуги: <span>0</span></div>
        </div>
    </div>
    <div class="next-step"></div>
</div>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>