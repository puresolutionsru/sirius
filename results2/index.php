<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Результаты поиска");
$APPLICATION->SetTitle('Поиск');
?>
	<?
	// разбираем url
	$url = $_SERVER["REQUEST_URI"];
	$url = explode("/", $url);
	if($url[count($url)-1]) {
		$data = $url[count($url)-1];
	} else {
		$data = $url[count($url)-2];
	}
	$data = explode("a", $data);
	// дата
	$date = $data[3];
	$day = substr($date, 0, 2);
	$month = substr($date, 2, 2);
	$year = substr($date, 4, 4);
	$formatCal = $year . "-" . $month . "-" . $day;
	$date = new DateTime($formatCal."T00:00:00+00:00");
	$dateFrom = $date->getTimestamp();

	$dayofweek = array(
		'', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'
	);

	$_monthsList = array(
		"1"=>"января",
		"2"=>"февраля",
		"3"=>"марта",
		"4"=>"апреля",
		"5"=>"мая",
		"6"=>"июня",
		"7"=>"июля",
		"8"=>"августа",
		"9"=>"сентября",
		"10"=>"октября",
		"11"=>"ноября",
		"12"=>"декабря"
	);
	
	// получаем города и страны
	$cities = file($_SERVER["DOCUMENT_ROOT"] . '/enginetais/files/city.json');
	$cities = json_decode($cities[0], true);
	$countries = file($_SERVER["DOCUMENT_ROOT"] . '/enginetais/files/country.json');
	$countries = json_decode($countries[0], true);
	

	// данные для формы поиска поля откуда
	$fromInput = "";
	$country = "";
	$fromCity = "";
	foreach($cities as $item) {
		if($item['codeen'] == $data[1]) {
			$fromCity = mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE);
			$fromInput .= mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE) . ', ';
			$country = $item['country'];
			break;
		}
	}
	$fromInput .= mb_convert_case(mb_strtolower($countries[$country]['name']), MB_CASE_TITLE) . ', '. $data[1];



	// данные для формы поиска поля куда
	$toInput = "";
	$country = "";
	$toCity = "";
	foreach($cities as $item) {
		if($item['codeen'] == $data[2]) {
			$toCity = mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE);
			$toInput .= mb_convert_case(mb_strtolower($item['name']), MB_CASE_TITLE) . ', ';
			$country = $item['country'];
			break;
		}
	}
	$toInput .= mb_convert_case(mb_strtolower($countries[$country]['name']), MB_CASE_TITLE) . ', '. $data[2];
	?>





	<script>
		var from = "<?=$data[1]?>";
		var fromCity = "<?=$fromCity?>";
		var to = "<?=$data[2]?>";
		var toCity = "<?=$toCity?>";
		var day = "<?=$day?>";
		var month = "<?=$month?>";
		var year = "<?=$year?>";
		var formatDate = "<?=$year?>-"+"<?=$month?>-"+"<?=$day?>";
		var dateFrom = <?=$dateFrom?>;
		var dateTo = <?=$dateFrom+86399?>;
		var count = <?=$data[4]?>;
	</script>





	<div class="data-select-pl">
		<div class="data-select-pl__in wrapper">
			<div class="data-select-pl__t">Ваш выбор:</div>
			<div class="data-select-pl__plane">
				<img src="/img/plane.png" alt="" class="data-select-pl__plane__img">
				<div class="data-select-pl__plane__t"></div>
				<div class="data-select-pl__plane__engine"></div>
			</div>
			<div class="data-select-pl__tarif"></div>
			<div class="data-select-pl__info">
				<span class="data-select-pl__info__t">Туда:</span>
				<span class="data-select-pl__info__date"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="data-select-pl__info__time"></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="data-select-pl__info__route"></span>
			</div>
			<div class="data-select-pl__price">
				<span class="data-select-pl__price__t">цена:</span>
				<span class="data-select-pl__price__amount"></span> <span class="data-select-pl__price__currency"></span>
			</div>
			<div class="data-select-pl__btns">
				<a class="data-select-pl__btns__next" href="/order/">Продолжить</a>
				<a class="data-select-pl__btns__clear" href="#"></a>
			</div>
		</div>
	</div>




	<div class="b-search">
		<div class="wrapper">
			<div class="main-order-block__form">
				<div class="mof-input departure-inp">
					<label class="input-label <?=($data[1])?"label-active":"";?>" for="order_from">Откуда</label>
					<div>
						<input type ="text" class="mof-input-item city required" id="order_from" name="order_from" autocomplete="off" value="<?=$fromInput?>">
						<div class="modal-city-from modal-city-it"></div>
					</div>
				</div>
				<div class="mof-input arrival-inp">
					<label class="input-label <?=($data[2])?"label-active":"";?>" for="order_to">Куда</label>
					<div>
						<input type="text" class="mof-input-item city required" id="order_to" autocomplete="off" value="<?=$toInput?>">
						<div class="modal-city-to modal-city-it"></div>
					</div>
				</div>
				<div class="mof-input">
					<label class="input-label <?=($day)?"label-active":"";?>"  for="order_attrebute_date_0" >Дата</label>
					<div>
						<? if($MainConstruct->isPhone) : ?>
							<input class="mof-input-item mof-input-item_date required" type="date" id="order_attrebute_date_0" min="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d"), date("Y"))) ?>" autocomplete="off">
						<? else : ?>
							<input class="mof-input-item mof-input-item_date datepicker datepicker2 required" data-dp-class="custom" id="order_attrebute_date_0" autocomplete="off" <?=($day)?"value='".$day."/".$month."/".$year."'":"";?>/>
						<? endif; ?>
					</div>
				</div>
				<div class="mof-input leg-pass">
					<div class="mof-input_add-pass">
						<div class="mof-input_add-pass-btn"></div>
						<div class="mof-input_add-pass-btn"></div>
					</div>
					<label class="input-label <?=($data[4])?"label-active":"";?>" for="add_pass">Пассажиры</label>
					<div><input class="mof-input-item pass required" id="add_pass" autocomplete="off" type="number" <?=($data[4])?"value='".(int) $data[4]."'":"";?>></div>
				</div>
				<? if($MainConstruct->isPhone) : ?>
					<div class="for-cryak">
						<div class="for-cryak__routes"></div>
						<div class="for-cryak__date"></div>
					</div>
				<? endif; ?>
				<a href="#main-form-order" class="mof-btn">Изменить</a>
			</div>
			<script>
					//добавляем пассажиров
					let legPass = document.getElementsByClassName('leg-pass'); // дивы с пассажирами
					let passangers = document.getElementsByClassName('pass'); // инпуты с пассажирами
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.firstElementChild.onmousedown = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = (PasAmount.value == "") ? 1 : ++passValue;


								legPass[formNumber].children[1].classList.add("label-active");
							}
						} )();
					}
					//убавляем пассажиров
					for (let i = 0 ; i < legPass.length; i++){
						legPass[i].firstElementChild.lastElementChild.onclick = (function(){

							let formNumber = i;
							return function(){
								let PasAmount = legPass[formNumber].getElementsByClassName('pass')[0];
								let passValue = PasAmount.value;
								PasAmount.value = passValue > 1 ? --passValue : "";
							}
						} )();
					}

					let mainOrderBlock = document.getElementsByClassName('main-order-block')[0];// блок со всеми формами
					let forms = document.getElementsByClassName('main-order-block__form'); // формы 
					let dalee = document.getElementsByClassName('mof-btn')[0]; // кнопка далее
					let comeback = document.getElementsByClassName('comeback')[0]; //кнопка добавить обратный рейс
					let addTransfer = document.getElementsByClassName('main-order-block__add-transfer')[0];//кнопка добавить пересадку
					let addPreferences = document.getElementById('prefencesSubmit');//кнопка добавить предпочтения

					$(".main-order-block__add-days").click(function(){
						$(this).toggleClass("main-order-block__add-days_active");
					});

					// добавляем пересадочку
					var formCount = 1;
					var centerBlockHid = 0;

					// удаляем пересадочку
					let delBlock = document.getElementsByClassName('del-block');
					for (var i = 0; i < delBlock.length; i++){
						delBlock[i].onclick = (function(){
							let x = i;
							return function(){
								if(formCount == 3 && x == 0) {
									centerBlockHid = 1;
								} else {
									centerBlockHid = 0;
								}
								addTransfer.classList.remove('manage-btn_hidden');
								formCount--;
								if (formCount == 1)	{
									forms[0].classList.remove('main-order-block__form_cryak');
									mainOrderBlock.classList.remove('mob-whith-transfer');
									comeback.classList.remove('display-none');
									addTransfer.classList.remove('display-none');
								}
								forms[1].classList.remove('main-order-block__form_cryak');
								forms[x+1].classList.add('display-none');
								let inpts = forms[x+1].getElementsByTagName("input");
								for (let i = 0; i < inpts.length; i++ ){
									inpts[i].value = "";
									inpts[i].parentElement.previousElementSibling.classList.remove("label-active");
								}
							}

						})();
					}

					//делаем активный лейбл меньше
					let addActiveLabel = function(input){
						input.parentElement.previousElementSibling.classList.add("label-active");
					}

					$('body').on('click', '.add-preferences', function(e){
						e.preventDefault();
						$('#formMainOrderPreferences').addClass('form-orderMtl_active');
					});

					var mofInputs = document.getElementsByClassName('mof-input-item');
					for (var i = 0; i < mofInputs.length; i++){
						if(!mofInputs[i].value) {
							mofInputs[i].value = "";
						}
						mofInputs[i].onfocus = (function(){
							var x = i;
							return function(){
								mofInputs[x].parentElement.previousElementSibling.classList.add("label-active");
							}
						})();
					}
					let checkLebels = function(){
						for (var i = 0; i < mofInputs.length; i++){
							if(mofInputs[i].value==""){
								mofInputs[i].parentElement.previousElementSibling.classList.remove("label-active");
								mofInputs[i].parentElement.previousElementSibling.classList.remove("input-label__error");
							}
						}
					}
					var isDatepicker = function(element, className){
						while(element){
							if(element.matches(className)){
								return true;
								break;
							}else{
								element = element.parentElement;
							}
						}
						return false;
					}
					let doImputs = function(e){
							if(isDatepicker(e.target,'.ui-datepicker')||isDatepicker(e.target,'.ui-datepicker-header')||e.target.classList.contains('mof-btn')){
								}else if(e.target.matches('.mof-input-item')){
								checkLebels();
								e.target.parentElement.previousElementSibling.classList.add("label-active");
							}else{
								checkLebels();
							}
					}
					let doImputsOnPress = function(e){
						if(e.key == "Tab"){
							checkLebels();
						}
					}
					window.addEventListener('click', doImputs);
					dalee.addEventListener('click', checkLebels);
					window.addEventListener('keydown', doImputsOnPress);
			</script>
		</div>
	</div>
	<div class="wrapper">
		<div class="calendar">
			<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'- 4 days'))?>a<?=$data[4]?>/" class="calendar__arrow-left"></a>
			<div class="calendar__left">
				<div class="calendar__days calendar__prev-days">
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'- 3 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'- 3 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'- 3 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'- 3 days'))]?></div>
							</div>
						</div>
					</a>
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'- 2 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'- 2 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'- 2 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'- 2 days'))]?></div>
							</div>
						</div>
					</a>
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'- 1 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'- 1 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'- 1 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'- 1 days'))]?></div>
							</div>
						</div>
					</a>
				</div>
				<div class="list-var">
					<div class="list-var__i list-var__i_e">Место</div>
					<div class="list-var__i list-var__i_r">Каюта</div>
					<div class="list-var__i list-var__i_t">Салон</div>
				</div>
			</div>
			<div class="calendar__center">
				<div class="calendar__city calendar__city_top"><?=$fromCity?></div>
				<div class="calendar__date-form">
					<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal))?></div>
					<div>
						<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal))]?></div>
						<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal))]?></div>
					</div>
				</div>
				<div class="calendar__city calendar__city_bottom"><?=$toCity?></div>
			</div>
			<div class="calendar__right">
				<div class="calendar__days calendar__next-days">
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'+ 1 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'+ 1 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'+ 1 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'+ 1 days'))]?></div>
							</div>
						</div>
					</a>
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'+ 2 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'+ 2 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'+ 2 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'+ 2 days'))]?></div>
							</div>
						</div>
					</a>
					<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'+ 3 days'))?>a<?=$data[4]?>/" class="calendar__day">
						<div class="calendar__date-form">
							<div class="calendar__date-form__day"><?=date("d", strtotime($formatCal.'+ 3 days'))?></div>
							<div>
								<div class="calendar__date-form__month"><?=$_monthsList[date("n", strtotime($formatCal.'+ 3 days'))]?></div>
								<div class="calendar__date-form__dayofweek"><?=$dayofweek[date("N", strtotime($formatCal.'+ 3 days'))]?></div>
							</div>
						</div>
					</a>
				</div>
				<div class="list-var-r">
					<div class="list-var-r__i">Все классы</div>
					<div class="list-var-r__i">Все бортовые</div>
				</div>
			</div>
			<a href="/results/a<?=$data[1]?>a<?=$data[2]?>a<?=date("dmY", strtotime($formatCal.'+ 4 days'))?>a<?=$data[4]?>/" class="calendar__arrow-right"></a>
		</div>
		<div class="vydacha-items"></div>
	</div>





	<script src="/results/lib/currency.js"></script>
	<script src="/results/lib/planes.js"></script>
	<script src="/results/lib/services.js"></script>
	<script src="/results/functions.js"></script>
	<script src="/results/script.js"></script>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
