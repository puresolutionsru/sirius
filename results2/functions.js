function timeInFlight(traveltime) {
    let arr = traveltime.split("H");
    let hours = arr[0].split("T");
    hours = hours[1];
    let minutes = arr[1].split("M");
    minutes = minutes[0];

    let text = "";
    if(hours>0) {
        text += hours + "ч.";
    }
    if(minutes>0) {
        text += " " + minutes + "мин.";
    }
    return text;
}
function timePerUnix(timestamp) {
    var date = parseISO8601(timestamp);
    var hours = date[4];
    if(hours<10) {
        hours = "0"+hours;
    }
    var minutes = date[5];
    if(minutes<10) {
        minutes = "0"+minutes;
    }

    return hours + ':' + minutes;
}
function parseISO8601(s) {
    var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:.(\d+))?(Z|[+-]\d{2})(?::(\d{2}))?/,
        d = s.match(re);
    if(!d) return null;
    for(var i in d)
        d[i] = ~~d[i];

    return d;
}
function delimeterThousand(num) {
    var ns = num.toString();
    var l = ns.length;
    var first = (l % 3 === 0) ? 3 : l % 3;

    var s = ns.substr(0, first);
    for(var i=first; i<l; i+=3) {
        s += ' ' + ns.substr(i, 3);
    }
        
    return s;
}