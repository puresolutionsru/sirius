let itemsblock = document.getElementsByClassName('vydacha-items')[0]; // контейнер для списка рейсов
let companyCode = "7R"; // код авиакомпании
let sessionId = null; // сессия
let listFlightsData = {}; // данные о рейсах
let selectData = {};
let faresData = Array();
let availSeat = {};


function timeInFlight(traveltime) {
    let arr = traveltime.split("H");
    let hours = arr[0].split("T");
    hours = hours[1];
    let minutes = arr[1].split("M");
    minutes = minutes[0];

    let text = "";
    if(hours>0) {
        text += hours + "ч.";
    }
    if(minutes>0) {
        text += " " + minutes + "мин.";
    }
    return text;
}
function timePerUnix(timestamp) {
    var date = parseISO8601(timestamp);
    var hours = date[4];
    if(hours<10) {
        hours = "0"+hours;
    }
    var minutes = date[5];
    if(minutes<10) {
        minutes = "0"+minutes;
    }

    return hours + ':' + minutes;
}
function parseISO8601(s) {
    var re = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:.(\d+))?(Z|[+-]\d{2})(?::(\d{2}))?/,
        d = s.match(re);
    if(!d) return null;
    for(var i in d)
        d[i] = ~~d[i];

    return d;
}
function delimeterThousand(num) {
    var ns = num.toString();
    var l = ns.length;
    var first = (l % 3 === 0) ? 3 : l % 3;

    var s = ns.substr(0, first);
    for(var i=first; i<l; i+=3) {
        s += ' ' + ns.substr(i, 3);
    }
        
    return s;
}

function loadData() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_air_avail/?company='+companyCode+'&from='+from+'&to='+to+'&date='+formatDate+'&count='+count, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {} 
        else {
            let data = JSON.parse(xhr.responseText);
            console.log(data);
            sessionId = data['@attributes']['SessionID'];

            if(typeof data['ItineraryOptions']['ItineraryOption'].length !== "undefined") {
                for(let element in data['ItineraryOptions']['ItineraryOption']) {
                    templateItem(data['ItineraryOptions']['ItineraryOption'][element]);
                }
            } else {
                templateItem(data['ItineraryOptions']['ItineraryOption']);
            }
            // updateSeat();
            updateServices(0);
        }
    }
}
loadData();

function updateServices(num) {
    let count = 0;
    $(".vydacha-items__i__tarifs__i_sh").each(function(e){
        if(count===num) {
            let tarif = $(this).attr("data-ref");
            let flight = $(this).parents(".vydacha-items__i").attr("data-itineraryref");
            
            var obj = {};
            obj.itineraryRef = flight;
            obj.fare = tarif;
            obj.session = sessionId;
            var data = JSON.stringify(obj);

            var fr = new XMLHttpRequest();
            fr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_flight_for_seat/?data='+data, true);
            fr.send();
            fr.onreadystatechange = function() {
                if (fr.readyState != 4) return;
                if (fr.status != 200) {}
                else {
                    var frserv = new XMLHttpRequest();
                    frserv.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_servtarif/?session='+sessionId, true);
                    frserv.send();
                    frserv.onreadystatechange = function() {
                        if (frserv.readyState != 4) return;
                        if (frserv.status != 200) {}
                        else {
                            console.log(frserv.responseText);
                        }
                    }
                }
            }
        }
        count++;
    });
}

// function updateSeat() {
//     if(faresData.length>0) {
//         setFare(0);
//     }
// }
// function setFare(num) {
//     var obj = {};
//     obj.itineraryRef = faresData[num]['flight'];
//     obj.fare = faresData[num]['fare'];
//     obj.session = sessionId;
//     var data = JSON.stringify(obj);

//     var fr = new XMLHttpRequest();
//     fr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_flight_for_seat/?data='+data, true);
//     fr.send();
//     fr.onreadystatechange = function() {
//         if (fr.readyState != 4) return;
//         if (fr.status != 200) {} 
//         else {
//             var frserv = new XMLHttpRequest();
//             frserv.open('GET', 'https://sirius-aero.ru/enginetais/Describes/get_air_services/?session='+sessionId, true);
//             frserv.send();
//             frserv.onreadystatechange = function() {
//                 if (frserv.readyState != 4) return;
//                 if (frserv.status != 200) {} 
//                 else {
//                     let responseText = JSON.parse(frserv.responseText);
//                     console.log(responseText);
//                     for(let element in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row']) {
//                         if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['@attributes']['RowProps'] != "NoRow") {
//                             for(let i in responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat']) {
//                                 if(responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatAvailability'] == "Available") {
//                                     if(!availSeat[faresData[num]['flight']]) {
//                                         availSeat[faresData[num]['flight']] = {};
//                                     }
//                                     availSeat[faresData[num]['flight']][responseText['SIG_AirServicesRS']['Itinerary']['FlightSegment']['SeatMap']['CabinMap']['Row'][element]['Seat'][i]['@attributes']['SeatLabel']] = "available";
//                                 }
//                             }
//                         }
//                     }
//                     num++;
//                     if(num < faresData.length) {
//                         setFare(num);
//                     } else {
//                         setSeat();
//                     }
//                 }
//             }
//         }
//     }
// }
// function setSeat() {
//     for(let element in availSeat) {
//         $(".vydacha-items__i[data-itineraryref="+element+"] .vydacha-items__i__about-plane__salon__place").removeClass("vydacha-items__i__about-plane__salon__place_available");
//         $(".vydacha-items__i[data-itineraryref="+element+"] .vydacha-items__i__about-plane__salon__place").addClass("vydacha-items__i__about-plane__salon__place_noavailable");

//         $(".vydacha-items__i[data-itineraryref="+element+"] .vydacha-items__i__about-plane__salon__place").each(function() {
//             let num = $(this).attr("data-number");
//             if(availSeat[element][num] == "available") {
//                 $(this).removeClass("vydacha-items__i__about-plane__salon__place_noavailable");
//                 $(this).addClass("vydacha-items__i__about-plane__salon__place_available");
//             }
//         });

//         var keys = Object.keys(availSeat[element]);
//         $(".vydacha-items__i[data-itineraryref="+element+"] .vydacha-items__i__about-plane__salon__place[data-number="+keys[0]+"]").addClass("vydacha-items__i__about-plane__salon__place_selected");
//     }
// }




function templateItem(element) {
    let ItineraryRef = element["@attributes"]['ItineraryRef'];
    listFlightsData[ItineraryRef] = {
        'equipment': element["FlightSegments"]["FlightSegment"]["@attributes"]["Equipment"],
        'numberFlight': element["FlightSegments"]["FlightSegment"]["@attributes"]["Flight"],
        'timeFlight': timeInFlight(element["@attributes"]["TravelTime"]),
        'date': day+'.'+month+'.'+year,
        'timeDeparture': timePerUnix(element["FlightSegments"]["FlightSegment"]["Departure"]["@attributes"]["Time"]),
        'timeArrival': timePerUnix(element["FlightSegments"]["FlightSegment"]["Arrival"]["@attributes"]["Time"]),
        'airportDeparture': element["FlightSegments"]["FlightSegment"]["Departure"]["@attributes"]["Airport"],
        'airportArrival': element["FlightSegments"]["FlightSegment"]["Arrival"]["@attributes"]["Airport"],
        'fromCity': fromCity,
        'toCity': toCity,
        "direct": 0,


        // 'plane': null,
        'seatAvailCount': 0,
        'refFareFull': null,
    };
    listFlightsData[ItineraryRef].direct = element['FlightSegments']['FlightSegment']['FlightDetails']["@attributes"]["StopCount"];
    // listFlightsData[ItineraryRef].plane = planes[listFlightsData[ItineraryRef].equipment];

    // получаем доступное количество мест
    // for(var i=0; i<element['FlightSegments']['FlightSegment']['ReservationDetails']['Reservation'].length; i++) {
    //     if(listFlightsData[ItineraryRef].seatAvailCount < element['FlightSegments']['FlightSegment']['ReservationDetails']['Reservation'][i]['@attributes']['SeatCount']) {
    //         listFlightsData[ItineraryRef].seatAvailCount = element['FlightSegments']['FlightSegment']['ReservationDetails']['Reservation'][i]['@attributes']['SeatCount'];
    //     }
    // }

    // ищем самые дешевые тарифы
    var fares = Array();
    var minPriceSeat = false;
    var minFareSeat = false;
    var countSeat = 0;
    // var minPriceCabin = false;
    // var minFareCabin = false;
    // var minPriceAircraft = false;
    // var minFareAircraft = false;

    var brands = Array();

    for(var i=0; i<element['Fares']['Fare'].length; i++) {
        if(element['Fares']['Fare'][i]["@attributes"]["FareBasis"].includes("1OW")) {
            fares["seat"] = element['Fares']['Fare'][i];
            if(!minPriceSeat && element['Fares']['Fare'][i]["@attributes"]["FareRef"] != -1) {
                minPriceSeat = Number(element['Fares']['Fare'][i]["Price"]["@attributes"]["BaseFare"]);
                minFareSeat = element['Fares']['Fare'][i]["@attributes"]["FareRef"];
                let reservation = element['Fares']['Fare'][i]["Reservation"]["@attributes"]["Refs"];
                for(index in element['FlightSegments']['FlightSegment']['ReservationDetails']["Reservation"]) {
                    if(element['FlightSegments']['FlightSegment']['ReservationDetails']["Reservation"][index]["@attributes"]["ReservationRef"] === reservation) {
                        countSeat = element['FlightSegments']['FlightSegment']['ReservationDetails']["Reservation"][index]["@attributes"]["SeatCount"];
                    }
                }
                // faresData.push({'flight':ItineraryRef, 'fare':element['Fares']['Fare'][i]["@attributes"]["FareRef"]});
            }
        } else if(element['Fares']['Fare'][i]["@attributes"]["FareBasis"].includes("4OW") || element['Fares']['Fare'][i]["@attributes"]["FareBasis"].includes("3OW")) {
            // fares["cabin"] = element['Fares']['Fare'][i];
            // if((!minPriceCabin || minPriceCabin > Number(element['Fares']['Fare'][i]["Price"]["@attributes"]["BaseFare"])) && element['Fares']['Fare'][i]["@attributes"]["FareRef"] != -1) {
            //     minPriceCabin = Number(element['Fares']['Fare'][i]["Price"]["@attributes"]["BaseFare"]);
            //     minFareCabin = element['Fares']['Fare'][i]["@attributes"]["FareRef"];
            // }
        } else {
            // fares["aircraft"] = element['Fares']['Fare'][i];
            // if((!minPriceAircraft || minPriceAircraft > Number(element['Fares']['Fare'][i]["Price"]["@attributes"]["BaseFare"])) && element['Fares']['Fare'][i]["@attributes"]["FareRef"] != -1) {
            //     minPriceAircraft = Number(element['Fares']['Fare'][i]["Price"]["@attributes"]["BaseFare"]);
            //     minFareAircraft = element['Fares']['Fare'][i]["@attributes"]["FareRef"];
            //     listFlightsData[ItineraryRef].refFareFull = element['Fares']['Fare'][i]["@attributes"]["FareRef"];
            // }
        }
    }
    console.log(fares["seat"]);


            let html = '<div class="vydacha-items__i" data-ItineraryRef="'+ItineraryRef+'" data-type="Shuttle">'+
                            '<div class="vydacha-items__i__row vydacha-items__i__top">'+
                                '<div class="vydacha-items__i__cell-left vydacha-items__i__info vydacha-items__i__info-flight">'+
                                    '<div class="vydacha-items__i__info__plane">'+
                                        '<img src="'+planes[listFlightsData[ItineraryRef].equipment].small+'" alt="">'+
                                        '<div>'+planes[listFlightsData[ItineraryRef].equipment].class+'</div>'+
                                        '<div>'+planes[listFlightsData[ItineraryRef].equipment].name+'</div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__info__time">'+
                                        '<div class="vydacha-items__i__info__time__top">'+
                                            '<div class="vydacha-items__i__info__time__t">'+listFlightsData[ItineraryRef].timeDeparture+'</div>'+
                                            '<div class="vydacha-items__i__info__time__direct">прямой рейс</div>'+
                                            '<div class="vydacha-items__i__info__time__t align-right">'+listFlightsData[ItineraryRef].timeArrival+'</div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__info__time__center">'+
                                            '<div class="vydacha-items__i__info__time__line"></div>'+
                                            '<div class="vydacha-items__i__info__time__img-plane"></div>'+
                                            '<div class="vydacha-items__i__info__time__line"></div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__info__time__bottom">'+
                                            '<div class="vydacha-items__i__info__time__code">'+listFlightsData[ItineraryRef].airportDeparture+'</div>'+
                                            '<div class="vydacha-items__i__info__time__path">в пути '+listFlightsData[ItineraryRef].timeFlight+'</div>'+
                                            '<div class="vydacha-items__i__info__time__code align-right">'+listFlightsData[ItineraryRef].airportArrival+'</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                '<div class="vydacha-items__i__cell-right vydacha-items__i__tarifs">';
                                    if(typeof fares['seat'] !== "undefined") {
                                        if(typeof fares['seat']['@attributes'] !== "undefined" && minPriceSeat !== false) {
                                            let currencyBase = currency[fares['seat']['Price']['@attributes']['BaseCurrency']];
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_sh" data-ref="'+minFareSeat+'">'+
                                                '<input type="radio" name="'+element.id+'" id="shuttle'+element.id+'" class="vydacha-items__i__tarifs__i__radio">'+
                                                '<label for="shuttle'+element.id+'" class="vydacha-items__i__tarifs__i__label"></label>'+
                                                '<div class="vydacha-items__i__tarifs__i__t">«Место»</div>'+
                                                '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(minPriceSeat)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                            '</div>';
                                        } else {
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Место»</div>';
                                        }
                                    } else {
                                        html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Место»</div>';
                                    }


                                    if(typeof fares['cabin'] !== "undefined") {
                                        if(typeof fares['cabin']['@attributes'] !== "undefined" && minPriceCabin !== false) {
                                            let currencyBase = currency[fares['cabin']['Price']['@attributes']['BaseCurrency']];
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_em" data-ref="'+minFareCabin+'">'+
                                                '<input type="radio" name="'+element.id+'" id="kaute'+element.id+'" class="vydacha-items__i__tarifs__i__radio">'+
                                                '<label for="kaute'+element.id+'" class="vydacha-items__i__tarifs__i__label"></label>'+
                                                '<div class="vydacha-items__i__tarifs__i__t">«Каюта»</div>'+
                                                '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(minPriceCabin)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                            '</div>';
                                        } else {
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Каюта»</div>';
                                        }
                                    } else {
                                        html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Каюта»</div>';
                                    }

                                    if(typeof fares['aircraft'] !== "undefined") {
                                        if(typeof fares['aircraft']['@attributes'] !== "undefined" && minPriceAircraft !== false) {
                                            let currencyBase = currency[fares['aircraft']['Price']['@attributes']['BaseCurrency']];
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_pc" data-ref="'+minFareAircraft+'">'+
                                                '<input type="radio" name="'+element.id+'" id="charter'+element.id+'" class="vydacha-items__i__tarifs__i__radio">'+
                                                '<label for="charter'+element.id+'" class="vydacha-items__i__tarifs__i__label"></label>'+
                                                '<div class="vydacha-items__i__tarifs__i__t">«Салон»</div>'+
                                                '<div class="vydacha-items__i__tarifs__i__price"><span class="vydacha-items__i__tarifs__i__price__amount">'+delimeterThousand(minPriceAircraft)+'</span> <span class="vydacha-items__i__tarifs__i__price__currency">'+currencyBase+'</span></div>'+
                                            '</div>';
                                        } else {
                                            html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Салон»</div>';
                                        }
                                    } else {
                                        html += '<div class="vydacha-items__i__tarifs__i vydacha-items__i__tarifs__i_disabled">«Салон»</div>';
                                    }
                                html += '</div>'+
                            '</div>'+
                            '<div class="vydacha-items__i__bottom">'+
                                
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                                '<div class="vydacha-items__i__row vydacha-items__i__about-flight">'+
                                    '<div class="vydacha-items__i__cell-left vydacha-items__i__about-flight__left">'+
                                        '<div class="vydacha-items__i__info-plane">'+
                                            '<div class="vydacha-items__i__info-plane__transfer-company"><img src="/bitrix/templates/ibe/img/logo.svg" alt=""></div>'+
                                            '<div class="vydacha-items__i__info-plane__data">'+
                                                '<div class="vydacha-items__i__info-plane__data__type">'+planes[listFlightsData[ItineraryRef].equipment].class+'</div>'+
                                                '<div class="vydacha-items__i__info-plane__data__plane">'+planes[listFlightsData[ItineraryRef].equipment].number+'</div>'+
                                                '<div class="vydacha-items__i__info-plane__data__path">Всего в пути:</div>'+
                                                '<div class="vydacha-items__i__info-plane__data__izm">'+
                                                    '<span class="icon-route">2509 км</span>'+
                                                    '<span class="icon-flytime">'+listFlightsData[ItineraryRef].timeFlight+'</span>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__info-plane__img-plane"><img src="/bitrix/templates/ibe/img/photoplane.jpg" alt=""></div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__info-fly">'+
                                            '<div class="vydacha-items__i__info-fly__top">'+
                                                '<span>вылет</span>'+
                                                '<span>прилет</span>'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__info-fly__bottom">'+
                                                '<span class="icon-from">'+listFlightsData[ItineraryRef].fromCity+'</span>'+
                                                '<span class="icon-centerroute"></span>'+
                                                '<span class="icon-to">'+listFlightsData[ItineraryRef].toCity+'</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__cell-right vydacha-items__i__tarifs-list">'+
                                        // Добавляем услуги для тарифов из файла для более быстрой загрузки, после полной загрузки отправим ajax для сравнения и обновления файла
                                        '<div class="vydacha-items__i__tarifs-list__i" id="services-seat">';
                                            // for(index in services.SEAT) {
                                            //     let typeTar = '';
                                            //     let show = true;
                                            //     if(services.SEAT[index].status == 'Included') {

                                            //     } else if (services.SEAT[index].status == 'Available') {
                                            //         typeTar = 'list-services_pay';
                                            //     } else {
                                            //         show = false;
                                            //     }
                                            //     if(show) {
                                            //         html += '<div class="list-services '+typeTar+'">'+services.SEAT[index].name+'</div>';
                                            //     }
                                            // }
                                        html += '</div>'+
                                        '<div class="vydacha-items__i__tarifs-list__i">';
                                            // for(i in services.CABIN) {
                                            //     let typeTar = '';
                                            //     let show = true;
                                            //     if(services.SEAT[i].status == 'Included') {

                                            //     } else if (services.SEAT[i].status == 'Available') {
                                            //         typeTar = 'list-services_pay';
                                            //     } else {
                                            //         show = false;
                                            //     }
                                            //     if(show) {
                                            //         html += '<div class="list-services '+typeTar+'">'+services.SEAT[i].name+'</div>';
                                            //     }
                                            // }
                                        html += '</div>'+
                                        '<div class="vydacha-items__i__tarifs-list__i">';
                                            // for(i in services.AIRPLANE) {
                                            //     let typeTar = '';
                                            //     let show = true;
                                            //     if(services.SEAT[i].status == 'Included') {

                                            //     } else if (services.SEAT[i].status == 'Available') {
                                            //         typeTar = 'list-services_pay';
                                            //     } else {
                                            //         show = false;
                                            //     }
                                            //     if(show) {
                                            //         html += '<div class="list-services '+typeTar+'">'+services.SEAT[i].name+'</div>';
                                            //     }
                                            // }
                                        html += '</div>'+
                                    '</div>'+
                                '</div>'+












                                '<div class="vydacha-items__i__row vydacha-items__i__about-plane">'+
                                    '<div class="vydacha-items__i__cell-left">'+
                                        '<img src="/bitrix/templates/ibe/img/avf.png" class="vydacha-items__i__about-plane__img" alt="">'+
                                        '<div class="vydacha-items__i__about-plane__tx">'+planes[listFlightsData[ItineraryRef].equipment].description+'</div>'+
                                        '<div class="cl"></div>'+
                                        '<div class="btn-all">Подробнее</div>'+
                                    '</div>'+
                                    '<div class="vydacha-items__i__cell-right">'+
                                        '<div class="vydacha-items__i__about-plane__t">Конфигурация салона '+planes[listFlightsData[ItineraryRef].equipment].name+'</div>'+
                                        '<div class="vydacha-items__i__about-plane__change">Ваш выбор: «<span>Место</span>»</div>'+
                                        '<div class="vydacha-items__i__about-plane__salon">'+
                                            '<div class="vydacha-items__i__about-plane__salon__img">';
                                                for(numberSeat in planes[listFlightsData[ItineraryRef].equipment].map) {
                                                    html += '<div class="vydacha-items__i__about-plane__salon__place vydacha-items__i__about-plane__salon__place_available vydacha-items__i__about-plane__salon__place_'+planes[listFlightsData[ItineraryRef].equipment].map[numberSeat].route+'" data-number="'+numberSeat+'" style="top:'+planes[listFlightsData[ItineraryRef].equipment].map[numberSeat].top+'px;left:'+planes[listFlightsData[ItineraryRef].equipment].map[numberSeat].left+'px;"></div>';
                                                }
                                                html += '<img src="'+planes[listFlightsData[ItineraryRef].equipment].configuration+'" class="" alt="">'+
                                            '</div>'+
                                            '<div class="vydacha-items__i__about-plane__ch">'+
                                                '<img src="/bitrix/templates/ibe/img/people.svg" class="" alt="">'+
                                                '<div class="vydacha-items__i__about-plane__salon__tx">Количество мест</div>'+
                                                '<div class="vydacha-items__i__about-plane__salon__count">'+planes[listFlightsData[ItineraryRef].equipment].count+'</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="vydacha-items__i__about-plane__available">Свободных мест: <span>'+countSeat+' / '+planes[listFlightsData[ItineraryRef].equipment].count+'</span></div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

            itemsblock.innerHTML += html;
}


// выбор рейса и тарифа
$("body").on("click", ".vydacha-items__i__tarifs__i", function(e) {
    if(!$(this).hasClass("vydacha-items__i__tarifs__i_disabled")) {
        let selectTarif = $(this);
        let selectContainer = selectTarif.parents(".vydacha-items__i");
        selectData.itineraryRef = selectContainer.attr('data-ItineraryRef');
        selectData.fare = selectTarif.attr('data-ref');
        
        $(".vydacha-items__i").removeClass("vydacha-items__i_active");
        selectContainer.addClass("vydacha-items__i_active");

        $(".vydacha-items__i__tarifs__i").removeClass("vydacha-items__i__tarifs__i_selected");
        selectTarif.addClass("vydacha-items__i__tarifs__i_selected");
        $(".vydacha-items__i__tarifs__i__radio").attr("checked", false);
        selectTarif.find(".vydacha-items__i__tarifs__i__radio").attr("checked", "checked");

        selectData.priceBackspace = selectTarif.find(".vydacha-items__i__tarifs__i__price__amount").html();
        selectData.price = selectTarif.find(".vydacha-items__i__tarifs__i__price__amount").html().replace(/\s/g, '');
        selectData.currency = selectTarif.find(".vydacha-items__i__tarifs__i__price__currency").html();

        if($(this).hasClass("vydacha-items__i__tarifs__i_sh")) {
            selectData.tarif = "Место";
        }
        if($(this).hasClass("vydacha-items__i__tarifs__i_pc")) {
            selectData.tarif = "Салон";
        }
        if($(this).hasClass("vydacha-items__i__tarifs__i_em")) {
            selectData.tarif = "Каюта";
        }
        selectFlight();
    }
});
function selectFlight() {
    $(".vydacha-items__i__about-plane__change span").text(selectData.tarif)

    $(".data-select-pl").addClass("data-select-pl_active");
    $(".data-select-pl__plane__img").attr("src", listFlightsData[selectData.itineraryRef].plane.small);
    $(".data-select-pl__plane__t").html(listFlightsData[selectData.itineraryRef].plane.class);
    $(".data-select-pl__plane__engine").html(listFlightsData[selectData.itineraryRef].plane.number);

    $(".data-select-pl__info__date").html(listFlightsData[selectData.itineraryRef].date);
    $(".data-select-pl__info__time").html(listFlightsData[selectData.itineraryRef].timeDeparture);
    $(".data-select-pl__info__route").html(listFlightsData[selectData.itineraryRef].fromCity + " - " + listFlightsData[selectData.itineraryRef].toCity);
    $(".data-select-pl__tarif").html("«"+selectData.tarif+"»");
    $(".data-select-pl__price__amount").html(delimeterThousand(selectData.price));
    $(".data-select-pl__price__currency").html(selectData.currency);
}


// Кнопка продолжить
$(".data-select-pl__btns__next").click(function(e){
    e.preventDefault();
    var obj = listFlightsData[selectData.itineraryRef];
    obj.tarif = selectData.tarif;
    obj.itineraryRef = selectData.itineraryRef;
    obj.fare = selectData.fare;
    obj.priceBackspace = selectData.priceBackspace;
    obj.price = selectData.price;
    obj.currency = selectData.currency;
    obj.formatDate = formatDate;
    obj.session = sessionId;

    var data = JSON.stringify(obj);
    var xhr = new XMLHttpRequest();
    // console.log(data);
    xhr.open('GET', 'https://sirius-aero.ru/enginetais/Requests/set_ordersdata/?data='+data, true);
    xhr.send();
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {} 
        else {
            // console.log(xhr.responseText);
            // document.location.href = "/order/?order="+xhr.responseText;
        }
    }
});