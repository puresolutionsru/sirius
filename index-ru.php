        <!-- begin slider-->
        <div id="big_stuff">
        <div class="wrapper_1544">
          <div class="title title_stuff title_aero">
            <div class="main">
              <div class="tab aero"><span class="aero on">Флот Sirius Aero</span>
                <div class="tab others"><span class="others">Флот партнеров</span></div>
              </div>
            </div>
          </div>
        </div>

        <?
        CModule::IncludeModule('iblock');
        $infoblock = 7;
        $arSort = array('SORT' => 'ASC');
        $arFilter = array('IBLOCK_ID' => $infoblock, '!=SECTION_ID' => '0', 'ACTIVE' => 'Y');
        $sct_el = CIBlockSection::GetList($arSort, $arFilter);
        ?>
        <div class="stuff on" id="stuff_aero">
          <!-- BEGIN TOP SLIDER -->
          <div id="fleet">
            <div class="wrapper_1544">
              <div class="slider_wr">
                <div class="slider_fleet">
                  <?
                  while($ar_sct_el = $sct_el->GetNext()) {
                        $img_src = CFile::GetPath($ar_sct_el['PICTURE']); ?>
                        <div class="item">
                            <div class="img_wr">
                                <img src="<?= $img_src ?>" alt="<?= $ar_sct_el['NAME'] ?>" title="<?= $ar_sct_el['NAME'] ?>">
                            </div>
                            <p><?= $ar_sct_el['NAME'] ?></p>
                        </div>
                  <?}?>
                </div>
              </div>
            </div>
          </div>
          <!-- END TOP SLIDER -->

          <!-- BEGIN BOTTOM SLIDER -->
          <div id="details">
            <div class="wrapper_1920">
              <div class="slider_details">
                <?
                $sct_el = CIBlockSection::GetList($arSort, $arFilter);
                while($ar_sct_el = $sct_el->GetNext()) {
                    $elArrSort = array("SORT"=>"ASC");
                    $elArrFilter = array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $ar_sct_el['ID'], 'ACTIVE' => 'Y');
                    $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                    ?>
                    <div class="item">
                        <? $i=1;
                        while($item = $el_i->GetNextElement()) {
                            $arFields = $item->GetFields();
                            $arProp = $item->GetProperties();
                            if($MainConstruct->isPhone) :
                                $file = CFile::ResizeImageGet($arProp['IMG_SLIDE']['VALUE'], array('width'=>1000, 'height'=>1000), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);   
                                $img = $file['src'];
                            else :
                                $img = CFile::GetPath($arProp['IMG_SLIDE']['VALUE']);
                            endif;
                            $class='';
                            if($i==1):
                                $class='on';
                            endif;
                            ?>
                                <img class="bg bg_<?=$i?> <?=$class?>" src="<?=$img?>" alt="" title="">
                            <?
                            $i++;
                        }
                        ?>
                        <div class="lkforair">
                            <?
                            $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                            $i = 0;
                            while($item = $el_i->GetNextElement()) {
                                $arFields = $item->GetFields();
                                $class='';
                                if($i==0):
                                    $class='on';
                                endif;
                                ?>
                                <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>" class="<?=$class?>">Подробнее о <?=$arFields['NAME']?></a>
                                <?
                                $i++;
                            } ?>
                        </div>
                        <div class="main_title"><span><?= $ar_sct_el['NAME'] ?></span></div>
                        <div class="tabs tab_active_one">
                            <div class="tabs-in-bg">
                            <?
                            $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                            $i = 0;
                            while($item = $el_i->GetNextElement()) {
                                $arFields = $item->GetFields();
                                $class='';
                                if($i==0):
                                    $class='on';
                                endif;
                                ?>
                                <div class="tab_wr <?=$class?>">
                                    <div class="item_tab"><?=$arFields['NAME']?></div>
                                </div>
                                <?
                                $i++;
                            } ?>
                            </div>
                        </div>
                        <?
                        $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                        $i = 0;
                        while($item = $el_i->GetNextElement()) {
                            $arFields = $item->GetFields();
                            $arProp = $item->GetProperties();
                            $class='';
                            if($i==0):
                                $class='on';
                            endif;
                            ?>
                            <section class="cl <?=$class?> <? if(!$arProp['PANORAMS']['VALUE']) : ?>block-fl<? endif; ?>">
                                <div class="left_block cl">
                                  <div class="capacity item_mini">
                                    <div class="img_wr">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/capacity.png" alt="" title="">
                                    </div>
                                    <p>Вместимость <br>пассажиров</p>
                                    <div class="number"><?=$arProp['VMEST']['VALUE']?></div>
                                  </div>
                                  <div class="distance item_mini">
                                    <div class="img_wr">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/distance.png" alt="" title="">
                                    </div>
                                    <p>Дальность <br>перелетов (км)</p>
                                    <div class="number"><?=$arProp['DAL']['VALUE']?></div>
                                  </div>
                                  <div class="amount item_mini">
                                    <div class="img_wr">
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/amount.png" alt="" title="">
                                    </div>
                                    <p>Объем багажного <br>отделения (м3)</p>
                                    <div class="number"><?=$arProp['OB']['VALUE']?></div>
                                  </div>
                                </div>
                                <div class="right_block cl">
                                  <? if($arProp['PANORAMS']['VALUE']) : ?>
                                      <div class="item_mini view">
                                          <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>#aircraft3d<?= $arFields['CODE'] ?>">
                                              <p>Просмотр салона</p>
                                              <div class="img_wr"></div>
                                          </a>
                                      </div>
                                  <? endif; ?>
                                  <div class="item_mini calc">
                                      <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>#aircraftcalc">
                                          <p>Рассчитать <br>стоимость перелета</p>
                                          <div class="img_wr"></div>
                                      </a>
                                  </div>
                                </div>
                            </section>
                            <?
                            $i++;
                        }
                        ?>
                    </div>
                <?}?>
              </div>
              <div class="form_wr">
                <form class="cl" action="" method="">
                  <input type="text" name="name" placeholder="Ваше имя:">
                  <input type="text" name="phone_or_email" placeholder="Тел. или email:">
                  <button type="submit">Зарезервировать сейчас</button>
                  <img src="<?= SITE_TEMPLATE_PATH ?>/img/main/details/close.png" alt="" title="">
                </form>
              </div>
            </div>
          </div>
          <!-- END BOTTOM SLIDER -->
        </div>


        <? if(false) : ?>
            <?
            $infoblock = 7;
            $arSort = array('SORT' => 'ASC');
            $arFilter = array('IBLOCK_ID' => $infoblock, '!=SECTION_ID' => '0', 'ACTIVE' => 'Y');
            $sct_el = CIBlockSection::GetList($arSort, $arFilter);
            ?>
            <div class="stuff" id="stuff_partners">
                <div id="fleet_partners">
                    <div class="wrapper_1544">
                    <div class="slider_wr">
                        <div class="slider_fleet_partners">
                        <?
                        while($ar_sct_el = $sct_el->GetNext()) {
                                $img_src = CFile::GetPath($ar_sct_el['PICTURE']); ?>
                                <div class="item">
                                    <div class="img_wr">
                                        <img src="<?= $img_src ?>" alt="<?= $ar_sct_el['NAME'] ?>" title="<?= $ar_sct_el['NAME'] ?>">
                                    </div>
                                    <p><?= $ar_sct_el['NAME'] ?></p>
                                </div>
                        <?}?>
                        </div>
                    </div>
                    </div>
                </div>
                <div id="details_partners">
                    <div class="wrapper_1920">
                    <div class="slider_details_partners">
                        <?
                        $sct_el = CIBlockSection::GetList($arSort, $arFilter);
                        while($ar_sct_el = $sct_el->GetNext()) {
                            $elArrSort = array("SORT"=>"ASC");
                            $elArrFilter = array('IBLOCK_ID' => $infoblock, 'SECTION_ID' => $ar_sct_el['ID'], 'ACTIVE' => 'Y');
                            $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                            ?>
                            <div class="item">
                                <? $i=1;
                                while($item = $el_i->GetNextElement()) {
                                    $arFields = $item->GetFields();
                                    $arProp = $item->GetProperties();
                                    $img = CFile::GetPath($arProp['IMG_SLIDE']['VALUE']);
                                    $class='';
                                    if($i==1):
                                        $class='on';
                                    endif;
                                    ?>
                                        <img class="bg bg_<?=$i?> <?=$class?>" src="<?=$img?>" alt="" title="">
                                    <?
                                    $i++;
                                }
                                ?>
                                <div class="lkforair">
                                    <?
                                    $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                                    $i = 0;
                                    while($item = $el_i->GetNextElement()) {
                                        $arFields = $item->GetFields();
                                        $class='';
                                        if($i==0):
                                            $class='on';
                                        endif;
                                        ?>
                                        <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>" class="<?=$class?>">Подробнее о <?=$arFields['NAME']?></a>
                                        <?
                                        $i++;
                                    } ?>
                                </div>
                                <div class="main_title"><span><?= $ar_sct_el['NAME'] ?></span></div>
                                <div class="tabs tab_active_one">
                                    <div class="tabs-in-bg">
                                    <?
                                    $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                                    $i = 0;
                                    while($item = $el_i->GetNextElement()) {
                                        $arFields = $item->GetFields();
                                        $class='';
                                        if($i==0):
                                            $class='on';
                                        endif;
                                        ?>
                                        <div class="tab_wr <?=$class?>">
                                            <div class="item_tab"><?=$arFields['NAME']?></div>
                                        </div>
                                        <?
                                        $i++;
                                    } ?>
                                    </div>
                                </div>
                                <?
                                $el_i = CIBlockElement::GetList($elArrSort, $elArrFilter);
                                $i = 0;
                                while($item = $el_i->GetNextElement()) {
                                    $arFields = $item->GetFields();
                                    $arProp = $item->GetProperties();
                                    $class='';
                                    if($i==0):
                                        $class='on';
                                    endif;
                                    ?>
                                    <section class="cl <?=$class?>">
                                        <div class="left_block cl">
                                        <div class="capacity item_mini">
                                            <div class="img_wr">
                                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/capacity.png" alt="" title="">
                                            </div>
                                            <p>Вместимость <br>пассажиров</p>
                                            <div class="number"><?=$arProp['VMEST']['VALUE']?></div>
                                        </div>
                                        <div class="distance item_mini">
                                            <div class="img_wr">
                                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/distance.png" alt="" title="">
                                            </div>
                                            <p>Дальность <br>перелетов (км)</p>
                                            <div class="number"><?=$arProp['DAL']['VALUE']?></div>
                                        </div>
                                        <div class="amount item_mini">
                                            <div class="img_wr">
                                                <img src="<?= SITE_TEMPLATE_PATH ?>/img/amount.png" alt="" title="">
                                            </div>
                                            <p>Объем багажного <br>отделения (м3)</p>
                                            <div class="number"><?=$arProp['OB']['VALUE']?></div>
                                        </div>
                                        </div>
                                        <div class="right_block cl">
                                        <div class="item_mini view">
                                            <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>#aircraft3d<?= $arFields['CODE'] ?>">
                                                <p>Просмотр салона</p>
                                                <div class="img_wr"></div>
                                            </a>
                                        </div>
                                        <div class="item_mini calc">
                                            <a href="<?= $ar_sct_el['SECTION_PAGE_URL'] ?>?<?= $arFields['CODE'] ?>#aircraftcalc">
                                                <p>Рассчитать <br>стоимость перелета</p>
                                                <div class="img_wr"></div>
                                            </a>
                                        </div>
                                        </div>
                                    </section>
                                    <?
                                    $i++;
                                }
                                ?>
                            </div>
                        <?}?>
                    </div>
                    <div class="form_wr">
                        <form class="cl" action="" method="">
                        <input type="text" name="name" placeholder="Ваше имя:">
                        <input type="text" name="phone_or_email" placeholder="Тел. или email:">
                        <button type="submit">Зарезервировать сейчас</button>
                        <img src="/img/main/details/close.png" alt="" title="">
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
      </div>
      <!-- end slider-->

      <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "servlist",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "5",
                "IBLOCK_TYPE" => "-",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "-1",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Преимущества Sirius Aero",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                    2 => "",
                ),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "ASC",
                "STRICT_SECTION_CHECK" => "N",
                "COMPONENT_TEMPLATE" => "servlist",
                "BUTTON_TEXT" => "Заказать рейс",
                "TEXT_PHONE" => "Заказать рейс:&nbsp;&nbsp;&nbsp;",
                "PHONE" => "+7 495 989 61 91"
            ),
            false
      );?>
      <!-- begin offer-->
      <div class="b-offer cl">
        <div class="b-offer__in">
          <div class="col-xs-12 col-md-6 b-offer__left">
              <a href="#">
                  <img src="<?= SITE_TEMPLATE_PATH ?>/img/for-load/logo.png" alt="">
              </a>
          </div>
          <div class="col-xs-12 col-md-6 b-offer__right">
              <?$APPLICATION->IncludeFile(
                  SITE_DIR.'/include/mainpredl.php',
                  array(),
                  array(
                      "MODE"=>"html",
                  )
              );?>
          </div>
        </div>
      </div>
      <!-- end offer-->
      <!-- begin articles-->
      <div class="b-articles cl">
        <div class="col-xs-12 col-md-6">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"listmain",
            	array(
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"ADD_SECTIONS_CHAIN" => "Y",
            		"AJAX_MODE" => "N",
            		"AJAX_OPTION_ADDITIONAL" => "",
            		"AJAX_OPTION_HISTORY" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "36000000",
            		"CACHE_TYPE" => "A",
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"DISPLAY_DATE" => "Y",
            		"DISPLAY_NAME" => "Y",
            		"DISPLAY_PICTURE" => "Y",
            		"DISPLAY_PREVIEW_TEXT" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"FIELD_CODE" => array(
            			0 => "",
            			1 => "",
            		),
            		"FILTER_NAME" => "",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"IBLOCK_ID" => "1",
            		"IBLOCK_TYPE" => "-",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"MESSAGE_404" => "",
            		"NEWS_COUNT" => "3",
            		"PAGER_BASE_LINK_ENABLE" => "N",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => ".default",
            		"PAGER_TITLE" => "Новости",
            		"PAGER_TITLE_ALL" => "Все новости",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"PROPERTY_CODE" => array(
            			0 => "",
            			1 => "",
            		),
            		"SET_BROWSER_TITLE" => "Y",
            		"SET_LAST_MODIFIED" => "N",
            		"SET_META_DESCRIPTION" => "Y",
            		"SET_META_KEYWORDS" => "Y",
            		"SET_STATUS_404" => "N",
            		"SET_TITLE" => "Y",
            		"SHOW_404" => "N",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_BY2" => "ID",
            		"SORT_ORDER1" => "DESC",
            		"SORT_ORDER2" => "DESC",
            		"STRICT_SECTION_CHECK" => "N",
            		"COMPONENT_TEMPLATE" => "listmain"
            	),
            	false
            );?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?$APPLICATION->IncludeComponent(
            	"bitrix:news.list",
            	"listmain2",
            	array(
            		"ACTIVE_DATE_FORMAT" => "d.m.Y",
            		"ADD_SECTIONS_CHAIN" => "Y",
            		"AJAX_MODE" => "N",
            		"AJAX_OPTION_ADDITIONAL" => "",
            		"AJAX_OPTION_HISTORY" => "N",
            		"AJAX_OPTION_JUMP" => "N",
            		"AJAX_OPTION_STYLE" => "Y",
            		"CACHE_FILTER" => "N",
            		"CACHE_GROUPS" => "Y",
            		"CACHE_TIME" => "36000000",
            		"CACHE_TYPE" => "A",
            		"CHECK_DATES" => "Y",
            		"DETAIL_URL" => "",
            		"DISPLAY_BOTTOM_PAGER" => "N",
            		"DISPLAY_DATE" => "Y",
            		"DISPLAY_NAME" => "Y",
            		"DISPLAY_PICTURE" => "Y",
            		"DISPLAY_PREVIEW_TEXT" => "Y",
            		"DISPLAY_TOP_PAGER" => "N",
            		"FIELD_CODE" => array(
            			0 => "",
            			1 => "",
            		),
            		"FILTER_NAME" => "",
            		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
            		"IBLOCK_ID" => "3",
            		"IBLOCK_TYPE" => "-",
            		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            		"INCLUDE_SUBSECTIONS" => "Y",
            		"MESSAGE_404" => "",
            		"NEWS_COUNT" => "3",
            		"PAGER_BASE_LINK_ENABLE" => "N",
            		"PAGER_DESC_NUMBERING" => "N",
            		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            		"PAGER_SHOW_ALL" => "N",
            		"PAGER_SHOW_ALWAYS" => "N",
            		"PAGER_TEMPLATE" => ".default",
            		"PAGER_TITLE" => "СМИ о нас",
            		"PAGER_TITLE_ALL" => "Все публикации",
            		"PARENT_SECTION" => "",
            		"PARENT_SECTION_CODE" => "",
            		"PREVIEW_TRUNCATE_LEN" => "",
            		"PROPERTY_CODE" => array(
            			0 => "LINK_SITE",
            			1 => "",
            		),
            		"SET_BROWSER_TITLE" => "Y",
            		"SET_LAST_MODIFIED" => "N",
            		"SET_META_DESCRIPTION" => "Y",
            		"SET_META_KEYWORDS" => "Y",
            		"SET_STATUS_404" => "N",
            		"SET_TITLE" => "Y",
            		"SHOW_404" => "N",
            		"SORT_BY1" => "ACTIVE_FROM",
            		"SORT_BY2" => "ID",
            		"SORT_ORDER1" => "DESC",
            		"SORT_ORDER2" => "DESC",
            		"STRICT_SECTION_CHECK" => "N",
            		"COMPONENT_TEMPLATE" => "listmain"
            	),
            	false
            );?>
        </div>
      </div>
      <!-- end articles-->
      <!-- end content-->
