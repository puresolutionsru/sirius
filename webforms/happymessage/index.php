<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/PHPMailerAutoload.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\Loader;

Loader::includeModule('iblock');
$el = new CIBlockElement;

		$json = '';

		$url = 'https://www.google.com/recaptcha/api/siteverify?secret=6LeWrHUUAAAAACJpGEe7CDtuyRksmelpzGXmAU_7&response='.(array_key_exists('g-recaptcha-response', $_POST) ? $_POST["g-recaptcha-response"] : '').'&remoteip='.$_SERVER['REMOTE_ADDR'];
		$resp = json_decode(file_get_contents($url), true);

		if ($resp['success'] == true) {
				$json['success'] = true;
		}
		else {
			$result['success'] = 'succes_no_send_message';
			echo json_encode($result);
			die();
		}


		//Формируем сообщение
		$messageContent = '';
		$theme = $_POST['theme'];
		$message = $_POST['message'];



		$messageContent .= '<table>';

		$messageContent .= '<tr><td colspan="2" align="center"><strong>Информация</strong></td></tr>';
		$messageContent .= '<tr><td>Тема</td><td>'.$theme.'</td></tr>';
		$messageContent .= '<tr><td>Сообщение</td><td>'.$message.'</td></tr>';

		$messageContent .= '</table>';
		//Формируем сообщение

		//Отправка сообщения
        $subject = 'Сообщение из формы "Доброволтные сообщения"';
				$msg = $messageContent;

				$__smtp = array(
					"host" => 'mail.sirius-aero.ru', // SMTP сервер
					"debug" => 0, // Уровень логирования
					"auth" => true, // Авторизация на сервере SMTP. Если ее нет - false
					"port" => '465', // Порт SMTP сервера
					"username" => 'no-reply@sirius-aero.ru', // Логин запрашиваемый при авторизации на SMTP сервере
					"password" => '3f2208Ck7', // Пароль
					"addreply" => 'no-reply@sirius-aero.ru', // Почта для ответа
					"secure" => 'ssl', // Тип шифрования. Например ssl или tls
					"mail_title" => 'Тест', // Заголовок письма
					"mail_name" => 'Имя отправителя' // Имя отправителя
				);

				$mail = new PHPMailer();
				$mail->CharSet = 'UTF-8';
				$mail->IsSMTP();
				$mail->Host       = $__smtp['host'];  // Host SMTP сервера: ip или доменное имя
				$mail->SMTPDebug  = $__smtp['debug'];  // Уровень журнализации работы SMTP клиента PHPMailer
				$mail->SMTPAuth   = $__smtp['auth'];  // Наличие авторизации на SMTP сервере
				$mail->Port       = $__smtp['port'];  // Порт SMTP сервера
				$mail->Username   = $__smtp['username'];
				$mail->Password   = $__smtp['password'];
				$mail->SMTPSecure = $__smtp['secure'];

				// От кого
				$mail->setFrom('no-reply@sirius-aero.ru', 'Sirius Aero');
				// Кому
				$mail->addAddress('d.melnik@sirius-aero.ru');
				$mail->addAddress('i.timofeev@sirius-aero.ru');
				$mail->addAddress('a.demin@sirius-aero.ru');
				$mail->addAddress('s.korolev@sirius-aero.ru');
				// Тема письма
				$mail->Subject = $subject;
				// Тело письма
				$body = $msg;
				$mail->msgHTML($body);
				$mail->AddAttachment($_SERVER['DOCUMENT_ROOT'] . $file);

				if(!$mail->Send()) {
					$result['success'] = 'succes_no_send_message';
					echo json_encode($result);
					die();
				}



				// $snd = CEvent::Send('VAKANSII', "s1", $arEventFields,'N', '', $files);
				// if(!$snd) {
				// 	$result['success'] = 'succes_no_send_message';
				// 	echo json_encode($result);
				// 	die();
				// }

				$els = new CIBlockElement;

				$arLoadProductArray = Array(
					"MODIFIED_BY"    => 1,
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => 12,
					"NAME"           => $theme,
					"ACTIVE"         => "Y",
					"DETAIL_TEXT"    => $message,
				);
				$PRODUCT_ID = $els->Add($arLoadProductArray);

		$result['success'] = 'succes-send_message';
		echo json_encode($result);
		
		die();

?>
