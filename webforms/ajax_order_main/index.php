<?
// if(!$_SERVER["REMOTE_ADDR"]) {
// 	$result['success'] = 'succes_no_send_message';
// 	echo json_encode($result);
// 	die();
// }
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\Loader;

Loader::includeModule('iblock');
$el = new CIBlockElement;

include($_SERVER['DOCUMENT_ROOT'] . '/mailTemplate.php');

			//Формируем сообщение
			$messageContent = '';
			$preferences = $_POST['preferences'];
			$name = $_POST['name'];
			$phone = $_POST['phone'];
			$email = $_POST['email'];
			$from1 = $_POST['from1'];
			$from2 = $_POST['from2'];
			$from3 = $_POST['from3'];
			$to1 = $_POST['to1'];
			$to2 = $_POST['to2'];
			$to3 = $_POST['to3'];
			$date1 = $_POST['date1'];
			$date2 = $_POST['date2'];
			$date3 = $_POST['date3'];
			$pass1 = $_POST['pass1'];
			$pass2 = $_POST['pass2'];
			$pass3 = $_POST['pass3'];
			$callval = $_POST['callval'];


			$messageContent .= $messageHeader;
			$messageContent .= '<tr><td><table style="width:100%;"><tbody>';
			$messageContent .= '<tr><td colspan="2" align="center" style="padding-top:20px;padding-bottom:20px;"><strong>Информация о пользователе</strong></td></tr>';
			$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">ФИО:</td><td style="padding-right:30px;padding-bottom:10px;">'.$name.'</td></tr>';
			$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">Телефон:</td><td style="padding-right:30px;padding-bottom:10px;">'.$phone.'</td></tr>';
			$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">E-mail:</td><td style="padding-right:30px;padding-bottom:10px;">'.$email.'</td></tr>';
			$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">Предпочтения:</td><td style="padding-right:30px;padding-bottom:10px;">'.$preferences.'</td></tr>';
			$messageContent .= '<tr><td colspan="2" align="center" style="padding-top:20px;padding-bottom:20px;"><strong>Информация о перелете</strong></td></tr>';
			$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">Направление</td><td style="padding-right:30px;padding-bottom:10px;">'.$from1.' - '.$to1.'  '.$date1.'. Пассажиры: '.$pass1.'</td></tr>';
			if($from2) :
				$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">Пересадка</td><td style="padding-right:30px;padding-bottom:10px;">'.$from2.' - '.$to2.'  '.$date2.'. Пассажиры: '.$pass2.'</td></tr>';
			endif;
			if($from3) :
				$messageContent .= '<tr><td style="padding-left:30px;padding-bottom:10px;">Пересадка</td><td style="padding-right:30px;padding-bottom:10px;">'.$from3.' - '.$to3.'  '.$date3.'. Пассажиры: '.$pass3.'</td></tr>';
			endif;

			$messageContent .= '</tbody></table></td></tr>';
			$messageContent .= $messageFooter;

			$comment = 'Направление: '.$from1.' - '.$to1.'  '.$date1.'. ';
			if($from2) :
				$comment .= 'Пересадка: '.$from2.' - '.$to2.'  '.$date2.'. ';
			endif;
			if($from3) :
				$comment .= 'Пересадка: '.$from3.' - '.$to3.'  '.$date3.'.';
			endif;

			//Отправка сообщения
					$subject = 'Заказ перелета';
					$msg = $messageContent;

					$PROP = array();
					$PROP[47] = $name;
					$PROP[46] = $phone;
					$PROP[45] = $email;
					$PROP[81] = $from1;
					$PROP[82] = $to1;
					$PROP[48] = $_SERVER["REMOTE_ADDR"];
					$arLoadProductArray = Array(
						"IBLOCK_SECTION_ID" => false,
						"IBLOCK_ID"      => 9,
						"NAME"           => 'Заказ ' . $name . ' (' . $email . '),' . ' (' . $phone . ') ' . $_SERVER["REMOTE_ADDR"],
						"ACTIVE"         => "Y",
						"PROPERTY_VALUES"=> $PROP,
						"DETAIL_TEXT"    => $msg,
					);
					$el->Add($arLoadProductArray);


					$arEventFields = array(
						"MESSAGE"	=> $msg,
						"TITLE"		=> $subject,
					);
					$snd = CEvent::Send('ORDER_FLY', "s1", $arEventFields);
					if(!$snd) {
							$result['success'] = 'succes_no_send_message';
							echo json_encode($result);
							die();
					}
					$arEventFieldsUser = array(
						"MESSAGE"		=> $msg,
						"TITLE"			=> $subject,
						"USER_EMAIL"	=> $email,
					);
					$sndUser = CEvent::Send('ORDER_FLY_USER', "s1", $arEventFieldsUser);
					if(!$sndUser) {
							$result['success'] = 'succes_no_send_message';
							echo json_encode($result);
							die();
					}

				if (!empty($phone) || !empty($email)){
					$ch = curl_init("https://api-node11.calltouch.ru/calls-service/RestAPI/requests/31529/register/?subject=Заказ перелета&sessionId=" . $callval . "&fio=" . $name . "&email=" . $email . "&phoneNumber=" . $phone . "&comment=" . $comment . "&requestUrl=" . $_SERVER['HTTP_REFERER']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$data = curl_exec($ch);
					curl_close($ch);
				}

			$result['success'] = 'succes-send_message';
			echo json_encode($result);
			
			die();



?>
