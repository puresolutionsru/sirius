<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\FileTable;

// Loader::includeModule('CFile');
 
// Здесь нужно сделать все проверки передаваемых файлов и вывести ошибки если нужно
 
// Переменная ответа
 
$data = array();
 
if( isset( $_GET['uploadfiles'] ) ){
    $error = false;
    $files = array();
 
    $uploaddir = './uploads/'; // . - текущая папка где находится submit.php
 
    // Создадим папку если её нет
 
    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
 
    // переместим файлы из временной директории в указанную
    foreach( $_FILES as $file ){
        // $info = basename($file['name']);
        // $info = explode(".", strtolower($info));
        // $info = md5(microtime() . rand(0, 9999)) . '.' . $info[count($info)-1];
        // if( move_uploaded_file( $file['tmp_name'], $uploaddir . $info ) ){
            $param = array(
                "name" => $file['name'],
                "size" => $file['size'],
                "tmp_name" => $file['tmp_name'],
                "type" => $file['type'],
                "del" => N,
            );
            $iy = CFile::SaveFile($param);
            $files[] = CFile::GetPath($iy);
            // $files[] = CFile::SaveFile($param);
        // }
        // else{
        //     $error = true;
        // }
    }
 
    $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
 
    echo json_encode( $data );
}