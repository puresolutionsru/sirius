<?
// if(!$_SERVER["REMOTE_ADDR"]) {
// 	$result['success'] = 'succes-no_phone_no_send_message';
// 	echo json_encode($result);
// 	die();
// }
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use Bitrix\Main\Loader;

Loader::includeModule('iblock');
$el = new CIBlockElement;

include($_SERVER['DOCUMENT_ROOT'] . '/mailTemplate.php');

		//Формируем сообщение
		$messageContent = '';
		$title = $_POST['title'];
		$person_name = $_POST['person_name'];
		$person_phone = $_POST['person_phone'];

		//Проверка на непустоту номера телефона
		$person_phone_tmp = trim($person_phone);
		// if($person_phone_tmp == '') {
		// 	$result['success'] = 'succes-no_phone_no_send_message';
		// 	echo json_encode($result);
    //         die();
		// }
		//Проверка на непустоту номера телефона

		$person_email = $_POST['person_email'];
		$comment = $_POST['comment'];
		$city_to = $_POST['city_to'];
		$city_from = $_POST['city_from'];
		$date = $_POST['date'];
		$person_count = $_POST['person_count'];


		$messageContent .= $messageHeader;
		$messageContent .= '<tr><td><table style="width:100%;"><tbody>';

		$messageContent .= '<tr><td colspan="2" align="center"><strong>Информация о пользователе</strong></td></tr>';
		$messageContent .= '<tr><td>ФИО</td><td>'.$person_name.'</td></tr>';
		$messageContent .= '<tr><td>Телефон</td><td>'.$person_phone.'</td></tr>';
		$messageContent .= '<tr><td>E-mail</td><td>'.$person_email.'</td></tr>';
		$messageContent .= '<tr><td>Комментарий</td><td>'.$comment.'</td></tr>';

		$messageContent .= '<tr><td colspan="2" align="center"><strong>Информация о рейсе</strong></td></tr>';
		if(!is_array($city_from)) {
			$messageContent .= '<tr><td>Откуда</td><td>'.$city_to.'</td></tr>';
			$messageContent .= '<tr><td>Куда</td><td>'.$city_from.'</td></tr>';
			$messageContent .= '<tr><td>Дата вылета</td><td>'.$date.'</td></tr>';
			$messageContent .= '<tr><td>Пассажиры</td><td>'.$person_count.'</td></tr>';
		} else {
			if(count($city_from) == 2) {
				$messageContent .= '<tr><td colspan="2" align="center"><strong>Откуда</strong></td></tr>';
				$messageContent .= '<tr><td>Откуда</td><td>'.$city_to[0].'</td></tr>';
				$messageContent .= '<tr><td>Куда</td><td>'.$city_from[0].'</td></tr>';
				$messageContent .= '<tr><td>Дата вылета</td><td>'.$date[0].'</td></tr>';
				$messageContent .= '<tr><td>Пассажиры</td><td>'.$person_count[0].'</td></tr>';
				$messageContent .= '<tr><td colspan="2" align="center"><strong>Куда</strong></td></tr>';
				$messageContent .= '<tr><td>Откуда</td><td>'.$city_to[1].'</td></tr>';
				$messageContent .= '<tr><td>Куда</td><td>'.$city_from[1].'</td></tr>';
				$messageContent .= '<tr><td>Дата вылета</td><td>'.$date[1].'</td></tr>';
				$messageContent .= '<tr><td>Пассажиры</td><td>'.$person_count[1].'</td></tr>';
			} elseif(count($city_from) > 2) {
				for ($i = 0; $i <= count($city_from)-1; $i++) {
					$messageContent .= '<tr><td colspan="2" align="center"><strong>Перелет №'.($i+1).'</strong></td></tr>';
					$messageContent .= '<tr><td>Откуда</td><td>'.$city_to[$i].'</td></tr>';
					$messageContent .= '<tr><td>Куда</td><td>'.$city_from[$i].'</td></tr>';
					$messageContent .= '<tr><td>Дата вылета</td><td>'.$date[$i].'</td></tr>';
					$messageContent .= '<tr><td>Пассажиры</td><td>'.$person_count[$i].'</td></tr>';
				}
			}
		}

		$messageContent .= '</tbody></table></td></tr>';
		$messageContent .= $messageFooter;
		//Формируем сообщение

		//Отправка сообщения
        $subject = $_POST['title'];
        $msg = $messageContent;

				$PROP = array();
				$PROP[47] = $person_name;
				$PROP[45] = $person_email;
				$PROP[46] = $person_phone;
				$PROP[48] = $_SERVER["REMOTE_ADDR"];
				if(!is_array($city_from)) {
					$PROP[81] = $city_to;
					$PROP[82] = $city_from;
				} else {
					$PROP[81] = $city_to[0];
					$PROP[82] = $city_from[0];
				}
				$arLoadProductArray = Array(
					"IBLOCK_SECTION_ID" => false,
					"IBLOCK_ID"      => 9,
					"NAME"           => 'Заказ ' . $person_name . ' (' . $person_email . ')',
					"ACTIVE"         => "Y",
					"PROPERTY_VALUES"=> $PROP,
					"DETAIL_TEXT"    => $msg,
				);
				$el->Add($arLoadProductArray);

				$arEventFields = array(
					"MESSAGE"			=> $msg,
					"TITLE"				=> $subject,
				);
				$snd = CEvent::Send('ORDER_FLY', "s1", $arEventFields);
				if(!$snd) {
					$result['success'] = 'succes-no_phone_no_send_message';
					echo json_encode($result);
					die();
				}
				if($person_email) :
					$arEventFieldsUser = array(
						"USER_NAME"	=> $person_name,
						"USER_EMAIL"=> $person_email,
						"TITLE"				=> $subject,
						"MESSAGE"		=> $msg,
					);
					$snd = CEvent::Send('ORDER_FLY_USER', "s1", $arEventFieldsUser);
				endif;

		$result['success'] = 'succes-send_message';
		echo json_encode($result);
		
		die();

?>
