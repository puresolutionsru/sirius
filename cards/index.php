<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
\CModule::IncludeModule('iblock');
$arFilter = Array("IBLOCK_ID"=>14, "ACTIVE"=>"Y", "CODE"=>$_REQUEST["CODE"]);
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1));
while($ob = $res->GetNextElement()) {
  $arFields = $ob->GetFields();
  $arProp = $ob->GetProperties();
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $arProp['LASTNAME']['VALUE']." ".$arProp['FIRSTNAME']['VALUE']." ".$arProp['MIDDLENAME']['VALUE'] ?></title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/cards/css/vizitka.css">
  </head>
  <body>
    <?

	$img = CFile::GetPath($arFields['PREVIEW_PICTURE']);
	$fullimg = "https://" . $_SERVER['SERVER_NAME'] . $img;
	$imgbase = base64_encode(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $img));

	$phone = str_replace(' ', '', $arProp['PHONE']['VALUE']);
	$phone = str_replace('(', '', $phone);
	$phone = str_replace(')', '', $phone);

	$data  = "BEGIN:VCARD\n";
	$data .= "VERSION:3.0\n";
	$data .= "URL:https://sirius-aero.ru\n";
	$data .= "N;CHARSET=UTF-8:".$arProp['LASTNAME']['VALUE'].";".$arProp['FIRSTNAME']['VALUE'].";".$arProp['MIDDLENAME']['VALUE'].";;;\n";
	$data .= "FN;CHARSET=UTF-8:".$arProp['LASTNAME']['VALUE']." ".$arProp['FIRSTNAME']['VALUE']." ".$arProp['MIDDLENAME']['VALUE']."\n";
	$data .= "TEL;TYPE=CELL:".$phone."\n";
	$data .= "PHOTO;ENCODING=b;TYPE=JPEG:".$imgbase."\n";
	$data .= "EMAIL;TYPE=INTERNET:".$arProp['EMAIL']['VALUE']."\n";
	$data .= "TITLE;CHARSET=UTF-8:".$arProp['DOLZNOST']['VALUE']."\n";
	$data .= "ORG;CHARSET=UTF-8:Sirius-Aero\n";
	$data .= "END:VCARD";

	$path = "/cards/files/".$_REQUEST["CODE"].".vcf";
	file_put_contents($_SERVER['DOCUMENT_ROOT'].$path, $data);
  	?>
	<section class='vizitka'>
		<a href="/">
			<img class="brandName" src="/cards/img/logo.svg" alt="logoSirius">
		</a>
		
		<div class="user-image"><img src="<?=$img?>" alt="user"></div>
		<a href="<?=$path?>" class="btn btn-addContact">
			<div class="account-icon">
				<svg width="19" height="16" viewBox="0 0 19 16" fill="none">
				<path fill-rule="evenodd" clip-rule="evenodd"
					d="M9.14284 9.14286C11.6676 9.14286 13.7143 7.09616 13.7143 4.57143C13.7143 2.0467 11.6676 0 9.14284 0C6.61811 0 4.57141 2.0467 4.57141 4.57143C4.57141 7.09616 6.61811 9.14286 9.14284 9.14286ZM9.14286 16H18.2857C18.2857 12.7178 14.1923 10.0571 9.14286 10.0571C4.0934 10.0571 0 12.7178 0 16H9.14286Z" />
				</svg>
			</div>
			<span>СОХРАНИТЬ В КОНТАКТЫ</span>
			<div class="download-icon">
				<svg width="9" height="11" viewBox="0 0 9 11" fill="none">
				<path fill-rule="evenodd" clip-rule="evenodd"
					d="M3.6 0V4.90909H0L4.5 9L9 4.90909H5.4V0H3.6ZM9 11V10H0V11H9Z" />
				</svg>
			</div>
  		</a>
		<div class="user-name">
			<h3><?=$arProp['LASTNAME']['VALUE']?> <?=$arProp['FIRSTNAME']['VALUE']?> <?=$arProp['MIDDLENAME']['VALUE']?></h3>
			<h4><?=$arProp['DOLZNOST']['VALUE']?></h4>
		</div>
		<div class="user-contact">
			<div class="constact user-phone">
				<a href="tel:<?=$phone?>"><?=$arProp['PHONE']['VALUE']?></a>
			</div>
			<div class="constact user-mail">
				<a href="mailto:<?=$arProp['EMAIL']['VALUE']?>"><?=$arProp['EMAIL']['VALUE']?></a>
			</div>
		</div>
		<div class="user-adress">
			<h5>Адрес: 121354, Москва, ул. Дорогобужская 14, корпус 1, БЦ «Партнер» <br>
			<a href="/">www.sirius-aero.ru</a></h5>
		</div>
		<div class="container">

		</div>
	</section>
  <?}?>
</body>
</html>