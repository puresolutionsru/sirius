<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,700,900" rel="stylesheet">  
<div id="app-wl-avia" lang="ru" currency="RUR" nav="ow" altdomain="sirius-aero.ru" ></div>  
<link rel="stylesheet" type="text/css" href="https://cdn.biletix.ru/avia-wl/all.css" />  
<script type="text/javascript" src="https://cdn.biletix.ru/avia-wl/build.js"></script>

<style>
    #app-wl-avia .wrap-searchform {
        min-height: auto;
    }
    #app-wl-avia .searchform {
        margin-bottom: 0px;
    }
</style>