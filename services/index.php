<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Авиакомпания Sirius Aero оказывает услуги бизнес-авиации. Осуществляет частные перелеты в Европу, Азию и по России. Собственный флот.");
$APPLICATION->SetPageProperty("title", "Услуги бизнес авиации Sirius Aero");
$APPLICATION->SetTitle("Услуги бизнес авиации Sirius Aero");
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <h1>' . $APPLICATION->GetTitle(false) . '</h1>
    ';
?><p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/checklist.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span><a href="#form-order" class="fancymodal">ЧАРТЕРНЫЕ VIP-ПЕРЕЛЁТЫ</a><br>
	 Организация и выполнение чартерных авиаперевозок на собственных и партнёрских самолётах по всему миру</span>
</p>
<div class="cl">
</div>
<p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/globe.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span><a href="/menedzhment-vs/">МЕНЕДЖМЕНТ ВОЗДУШНЫХ СУДОВ</a><br>
	 Комплексная программа управления воздушными судами клиентов, оптимизирующая расходы и решающая все вопросы эксплуатации бортов</span>
</p>
<div class="cl">
</div>
<p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/calendar-check.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span>ОРГАНИЗАЦИОННОЕ ОБЕСПЕЧЕНИЕ ПОЛЁТОВ И НАЗЕМНОЕ ОБСЛУЖИВАНИЕ<br>
	 Получение слотов в российских и международных аэропортах, разрешений на выполнение полётов и организация наземного обслуживания воздушных судов и пассажиров</span>
</p>
<div class="cl">
</div>
<p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/stock.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span>ПОДДЕРЖАНИЕ ЛЁТНОЙ ГОДНОСТИ ВОЗДУШНЫХ СУДОВ<br>
	 Планирование и отслеживание выполнения технического обслуживания, организация выполнения сервисных бюллетеней и директив лётной годности, сопровождение гарантийных и финансовых программ, а также круглосуточная поддержка эксплуатации</span>
</p>
<div class="cl">
</div>
<p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/repair-tools.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span>ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ САМОЛЁТОВ<br>
	 Линейное и базовое техническое обслуживание самолётов на собственных базах в России и Европе</span>
</p>
<div class="cl">
</div>
<p class="flex-2b">
 <img src="/bitrix/templates/sirius/img/for-load/chat.svg" alt="" class="pull-left" style="margin-right:20px;" width="70"> <span>АВИАЦИОННЫЙ КОНСАЛТИНГ<br>
	 Широкий спектр услуг от информационно-консультативной поддержки до полного пакета решения задач выбора, приобретения и владения воздушными судами любых типов</span>
</p>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>