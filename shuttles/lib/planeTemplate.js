function templatePlane(id) {
	let startDate = formatDate(info[id].Date);
    var price = info[id].Currency+' '+info[id].Cost+'<span>'+lang.priceTypeThree+'</span>';
    var inFlightTimeHour = Math.floor(info[id].InFlightTime/60);
    var inFlightTime = '';
    if(inFlightTimeHour>0) {
        if(lang.whatislang=='RU') {
            inFlightTime = inFlightTimeHour + ' ' + declOfNum(inFlightTimeHour, ['час', 'часа', 'часов']);
        } else {
            inFlightTime = inFlightTimeHour + ' ' + declOfNum(inFlightTimeHour, ['hour', 'hours', 'hours']);
        }
    }
    var inFlightTimeMin = info[id].InFlightTime - inFlightTimeHour*60;
    if(inFlightTimeMin>0) {
        if(lang.whatislang=='RU') {
            inFlightTime += ' ' + inFlightTimeMin + ' мин';
        } else {
            inFlightTime += ' ' + inFlightTimeMin + ' min';
        }
    }

	let img = "";
	for(let i=0; i<info[id].Images.length; i++) {
		img += "<a href='"+info[id].linkFull+"' target='_blank'><img src='https://firebasestorage.googleapis.com/v0/b/sirius-app-4021f.appspot.com/o/planeImages%2F"+ info[id].Images[i] +"?alt=media&token=6ac3caa1-39ed-4f33-b1f4-1bd25af5697a' alt=''></a>";
    }
    let decr = '';
    for(let i=0; i<info[id].details[lang.whatislang].length; i++) {
        for(let m=0; m<info[id].details[lang.whatislang][i].length; m++) {
            info[id].details[lang.whatislang][i][m] = info[id].details[lang.whatislang][i][m].replace(/\u2028/g,"")
            if(m==0) {
                decr += '<div class="descr-undert">'+info[id].details[lang.whatislang][i][m]+'</div>';
            } else {
                if(m==1) {
                    decr += '<ul class="descr-list">';
                }
                decr += '<li>'+info[id].details[lang.whatislang][i][m]+'</li>';
                if(m+1 == info[id].details[lang.whatislang][i].length) {
                    decr += '</ul>';
                }
            }
        }
    }
	let html = '<div class="main-w main-w_plane main-w_shuttle">'+
					'<div class="alert-close"></div>'+
					'<div class="plane__period-w"><div class="plane__period plane__period_shttl">'+startDate.date+' '+lang.month[startDate.month]+' '+startDate.year+'<div class="mlt-item__time-period__info__time">'+startDate.dayOfWeek+" "+toHHMMSS(info[id].Date, 'hours')+':'+toHHMMSS(info[id].Date, 'minutes')+'</div></div></div>'+
					'<div class="plane__t">'+info[id].FromCity+' - '+info[id].ToCity+'</div>'+
                    '<div class="fly-time">'+lang.flightTime+': '+inFlightTime+'</div>'+
                    '<div class="plane__route"><div class="plane__route__from plane__route__ar"><span>'+info[id].FromCityCode+'</span>'+info[id].FromCity+'</div><div class="plane__route__center"></div><div class="plane__route__to plane__route__ar"><span>'+info[id].ToCityCode+'</span>'+info[id].ToCity+'</div></div>'+
                    '<div class="plane__info">'+
                        '<div class="shuttle-left"><div class="plane__img plane__info__column">'+img+'</div><div class="mlt-item_plane mlt-item_plane__shuttle"><a href="" target="_blank" class="link-plane" onclick="return false;">'+lang.availableSeats+' <span>'+info[id].SeatsFree+'</span></a></div></div>'+
                        '<div class="plane__info__column">'+
                            '<div class="plane__param">'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.class+'</div>'+
                                    '<div class="plane__param__value">'+info[id].PlaneClass+'</div>'+
                                '</div>'+
                                '<div class="plane__param__row">'+
                                    '<div class="plane__param__t">'+lang.maxpass+'</div>'+
                                    '<div class="plane__param__value">'+info[id].SeatsAll+'</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="shuttle-descr">'+decr+'</div>'+
                            '<div class="plane__bottom">'+
                                '<div class="plane__price">'+price+'</div>';
                                if(info[id].CostType == "request") {
                                    html += '<a href="#order" class="mlt-intem__buy-btn" data-id="'+id+'">'+lang.orderReq+'</a>';
                                } else {
                                    html += '<a href="#order" class="mlt-intem__buy-btn" data-id="'+id+'">'+lang.order+'</a>';
                                }
                        html += '</div>'+
                        '</div>'+
                    '</div>'+
				'</div>';
	return html;
}

function declOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}