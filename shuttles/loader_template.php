<?
include_once __DIR__."/lang/".LANGUAGE_ID.".php";
?>
<div id="mlt">
    <? for($i=0; $i<$count; $i++) { ?>
        <div class="mlt-item mlt-item_preloader">
            <div class="mlt-item__photo"><span class="animation-js animation-js-photo"><div class="line"></div></span></div> 
            <div class="mlt-item__info">
                <div class="mlt-item__flight">
                    <div class="mlt-item__cities"><span class="animation-js animation-js-city"><div class="line"></div></span></div>
                    <div class="mlt-item__time-period"><span class="animation-js animation-js-date"><div class="line"></div></span></div> 
                </div>
                <div class="mlt-item_plane"><span class="animation-js animation-js-plane"><div class="line"></div></span></div>
                <div class="mlt-item__buy-block">
                    <div class="mil-item__price"><span class="animation-js animation-js-price"><div class="line"></div></span></div>
                    <div class="mlt-intem__buy-btn"><?= $LANG_PAGE['button_order'] ?></div>
                </div>
            </div>
        </div>
    <? } ?>
</div>