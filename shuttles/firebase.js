var mltDescriptionButton = document.getElementsByClassName("mlt-description__button")[0];
mltDescriptionButton.onclick = function(){
	this.parentElement.classList.toggle("hide-description");
}
// keyboard event
document.onkeyup = function(e){
	if (e.keyCode === 27){//esc
		closeModal();
	}
}
function formatDate(timestap) {
	let time = new Object();
	let date = new Date(timestap * 1000);
	time.year = date.getUTCFullYear();
	time.date = date.getUTCDate();
	time.month = date.getUTCMonth()+1;
	if(time.month<10) {
		time.month = '0' + time.month;
	}

	console.log(date.getUTCDay());
	time.dayOfWeek = lang.dayOfWeeks[date.getUTCDay()];

	return time;
}
function alertUpScroll() {
	$('body').addClass('compensate-for-scrollbar');
	$('body').css('padding-right', '17px');
}
function alertDownScroll() {
	$('body').removeClass('compensate-for-scrollbar');
	$('body').css('padding-right', '0px');
}







// Start firebase
var alert;
var info = Array();

var config = {
    apiKey: "AIzaSyCFP1R4wHvhpU9t2BPFXI4LElbwIhfxD94",
    authDomain: "sirius-app-4021f.firebaseapp.com",
    databaseURL: "https://sirius-app-4021f.firebaseio.com",
    projectId: "sirius-app-4021f",
    storageBucket: "sirius-app-4021f.appspot.com",
    messagingSenderId: "91652656168"
};
firebase.initializeApp(config);
if(window.alertid) {
	pushAlert(alertid);
}

let details = firebase.database().ref('Details');
let dataDetails = '';
details.once('value').then(function(detailSnap) {
	dataDetails = detailSnap.val();
	start();
});


function start() {
	var starCountRef = firebase.database().ref('ShutleFull');
	starCountRef.orderByChild('Date').once('value').then(function(snapshot) {
		var values = {};
		snapshot.forEach(child => {
			values[child.key] = child.val();
		});

		// var values = snapshot.val();
		var length = Object.keys(values).length;
		var keys = Array();

		var m = 0;
		for(var key in values) {
			keys[m] = key;
			m++;
		}

		for(var i=0; i<length; i++) { 
			var key = keys[i].replace(/"/g, '');
			var data = values[keys[i]];
			info[key] = values[keys[i]];
			if(data.DetailsPath) {
				// let details = firebase.database().ref('Details/'+data.DetailsPath);
				if(i==0) {
					document.getElementById('mlt').innerHTML = '';
				}
				// details.once('value').then(function(detailSnap) {
				// });
				data.details = dataDetails[data.DetailsPath];
				info[key].details = dataDetails[data.DetailsPath];
				templateCard(data, key);
			}
		}
	});
}

function templateModal(id) {
	$('.alert').remove();
	var alert = document.createElement('div');
	alert.className = "alert alert-plane";
	alert.innerHTML = "<div class='alert-background'></div><div class='arrow-plane arrow-left-plane'></div><div class='arrow-plane arrow-right-plane'></div>";
	alert.innerHTML += templatePlane(id);

	document.body.appendChild(alert);

	let a = new Date(info[id].Date * 1000);

	var lk = window.location.href;
    var mod = 'SA' + id + '/' + info[id].FromCityCode + '-' + info[id].ToCityCode + '-' + a.getDate() + '-' + a.getMonth() + '-' + a.getFullYear();
    mod = mod.replace('#','');

    if(lk.indexOf("?") == "-1") {
        lk = lk + '' + mod;
    } else {
        var change = lk.split('?');
        lk = change[0] + '' + mod;
    }
	history.pushState(null, null, lk);
	alert.className += " alert_success";
}

function templateModalOrder(id) {
	$('.modal-form').removeClass('modal-form_hide');
	$('.modal-form__success').removeClass('modal-form__success_active');

	$('#modal-form__select-pass').html('');
	$('#raceid_shttl').val(id);

	let startDate = formatDate(info[id].Date);

	document.getElementById('modal-form__from_shttl').innerText = info[id].FromCity;
	document.getElementById('modal-form__to_shttl').innerText = info[id].ToCity;
	document.getElementById('modal-form__datefrom_shttl').innerText = startDate.date+'.'+startDate.month+'.'+startDate.year;
	if(info[id].CostType == "seat") {
		$('#modal-form__select-pass_shttl').html('');
		for(let i=1; i<=info[id].SeatsFree; i++) {
			$('.form__item-select').css('display', 'flex');
			let opt = $('#modal-form__select-pass_shttl');
			opt.html(opt.html() + '<option>'+i+'</option>');
		}
	} else {
		$('.form__item-select').css('display', 'none');
	}
	alertUpScroll();
	$('#form-orderShttl').addClass('form-orderMtl_active');
	
	yaCounter28830840.reachGoal('shuttle_open_form');
}
// click on plane card
$('body').on('click', '.mlt-item', function(e){
	e.preventDefault();
	if(e.target.classList.contains('mlt-intem__buy-btn')) {
		templateModalOrder($(this).attr('data-id'));
	} else if(e.target.classList.contains('link-plane')) {

	} else {
		alertUpScroll();
		templateModal($(this).attr("data-id"));
	}
});


// send order
$('body').on('click', '.form-orderMtl__submit', function(e){
	e.preventDefault();
	let id = $('#form-orderShttl input[name="raceid"]').val();
	let name = $('#form-orderShttl input[name="name"]').val();
	let phone = $('#form-orderShttl input[name="phone"]').val();
	let pass = $('#modal-form__select-pass').val();
	let email = '';
	if($('#form-orderShttl input[name="email"]').val()) {
		email = $('#form-orderShttl input[name="email"]').val();
	}
	
	let countError = 0;

	if(!id) {
		countError++;
	}

	if(!pass) {
		pass = 1;
	} 

	if(!phone) {
		$('#form-orderShttl input[name="phone"]').addClass('it_error');
		countError++;
	} else {
		$('#form-orderShttl input[name="phone"]').removeClass('it_error');
	}

	if(countError) {
		return;
	}

	var dealId = '"'+id+'"';
	let ref = firebase.database().ref('booking');
	ref.push().set({
		userName: name,
		userPhone: phone,
		userEmail: email,
		seats: pass,
		shutle: dealId,
		type: "seats",
	});

	yaCounter28830840.reachGoal('shuttle_order');

	$('.modal-form').addClass('modal-form_hide');
	$('.modal-form__success').addClass('modal-form__success_active');
});
// click on order button
$('body').on('click', '.plane__bottom .mlt-intem__buy-btn', function(e){
	e.preventDefault();
	closeModal();
	alertUpScroll();
	templateModalOrder($(this).attr('data-id'));
});
// close modal window
$('body').on('click', '.alert-close, .alert-background, .form-orderMtl-below', function(e){
	closeModal();
});
function closeModal() {
	$('.alert').addClass('alert_close');
	$('.form-orderMtl_active').removeClass('form-orderMtl_active');
	alertDownScroll();
	if(document.location.host.indexOf('.com') != -1) {
		lang.lang = '';
	}
	history.pushState(null, null, '/'+lang.lang+'shuttles/');
}
function pushAlert(id) {
	id = '"'+id+'"';
	var starCountRef = firebase.database().ref('DealsFull/'+id);
	starCountRef.orderByChild('StartDate').on('value', function(snapshot) {
		var values = snapshot.val();
		info[id] = values;
		var html = templatePlane(id);

		html = html.replace('<div class="main-w main-w_plane">', '');
		html = html.slice(0, -6);
		$("#planePage").html(html);

	});
}