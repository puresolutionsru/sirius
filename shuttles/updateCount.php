<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/crm/crest.php');
require $_SERVER['DOCUMENT_ROOT'].'/crm/vendor/autoload.php';

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

$serviceAccount = ServiceAccount::fromJsonFile($_SERVER['DOCUMENT_ROOT'] . '/crm/google-services.json');
$firebase = (new Factory)->withServiceAccount($serviceAccount)->create();
$database = $firebase->getDatabase();
$info = $database->getReference('ShutleFull')->getSnapshot()->getValue();

file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/shuttles/countAllow.txt', count($info));