<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Организация перелетов по системе Jet Sharing. Позволяет арендовать бизнес-джет по цене до 90% ниже от стоимости аренды целого борта.");
$APPLICATION->SetPageProperty("title", "Jet Sharing - покресельная аренда бизнес джета в Москве");
$APPLICATION->SetPageProperty("title_legs", "Что такое Shuttle Up");
$APPLICATION->SetTitle("Jet Sharing");

$count = (int)file_get_contents($_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['SERVER_NAME'].'/shuttles/countAllow.txt');
function plural_form($number,$before,$after) {
	$cases = array(2,0,1,1,1,2);
	return $before[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]].' '.$number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
}
$t = plural_form($count, array('Доступен','Доступно','Доступно'), array('рейс','рейса','рейсов'));
// $APPLICATION->SetTitle($t);
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <h1>' . $APPLICATION->GetTitle(false) . '</h1>
    ';

if($_REQUEST['mtlid']) {
    include_once "plane_loader_template.php";
}

echo '<div class="mlt-description">
        <div class="mlt-description__button">'.$APPLICATION->getProperty("title_legs").'</div>
        <div class="mlt-description__text">'
?><p>
	 Sirius Aero предлагает эксклюзивную услугу Shuttle UP, которой вы можете воспользоваться в своих целях. В ее основе лежит принцип джет-шеринга (<strong>jet sharing</strong>), который предполагает совместную покресельную аренду самолета пассажирами и перелет по определенному расписанию.
</p>
 <?
echo '  </div>
      </div>';

    ?>
<div class="b-app-info">
	<div class="b-app-info__t">
		 Установите наше приложение и будьте в курсе о новых рейсах:
	</div>
	<div class="b-app-info__list">
		<div class="b-app-info__i">
 <a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios_shuttle'); return true;" class="appstore" target="_blank"></a>
		</div>
		<div class="b-app-info__i">
 <a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android_shuttle'); return true;" class="gplay" target="_blank"></a>
		</div>
	</div>
</div>
 <br><?

include_once "loader_template.php";
echo '
    <script src="/shuttles/lang/ru.js" defer></script>
    <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js" defer></script>
    <script src="/shuttles/lib/cardTemplate.js" defer></script>
    <script src="/shuttles/lib/planeTemplate.js" defer></script>
    <script src="/shuttles/firebase.js" defer></script>
    </div>
</div>
';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>