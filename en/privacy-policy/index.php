<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Privacy policy");
$APPLICATION->SetTitle("Privacy policy");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><br>
 The basic concepts used in these Rules mean the following:<br>
 <br>
 "User" - a capable physical person who completes any forms on the site owned by the Operator;<br>
 <br>
 "Operator" - LLC Airline Sirius-Aero, the company responsible for collecting and processing data from the Forms on the site.<br>
 <br>
 "Site" - the website (s) owned (s) by LLC Airline "Sirius-Aero", posted (s) on the Internet at <a href="/en/">https://sirius-aero.ru/</a> and on all subdomains ( subdomains) relating to the domain name sirius-aero.ru.<br>
 <br>
 <b>1. GENERAL PROVISIONS</b><br>
 1.1 These Rules are a public offer in accordance with Article 435 and Part 2 of Article 437 of the Civil Code of the Russian Federation.<br>
 1.2 The User’s use of the Site constitutes acceptance of this Privacy Policy and the terms of processing User’s personal data.<br>
 1.3 The Operator has the right to make adjustments to these Rules, therefore, the Visitor should familiarize themselves with them with each new action on the Site.<br>
 1.4 In case of disagreement with the terms of the Privacy Policy, the User must stop using the Site.<br>
 <br>
 <b>2. SUBJECT OF PRIVACY POLICY</b><br>
 2.1 Provision of information by the User:<br>
 2.1.1 In case of any actions on the Site, the User has the right to provide the following personal information: name, e-mail address, mobile or landline number, desired cities and flight dates, additional information as desired by the User.<br>
 2.1.2 Any other personal data not specified above (flight history, operating systems used, etc.) is subject to reliable storage and non-proliferation. It is not considered a violation by the Operator to provide information to agents and third parties acting on the basis of an agreement with the Operator to fulfill obligations to the User.<br>
 2.2 By providing their personal data, the User consents to the processing by the Operator, including for the purpose of promoting the goods and services by the Operator.<br>
 2.2.1 By providing their personal data, the User consents to the processing by the Operator, including for the purpose of promoting the goods and services by the Operator.<br>
 2.3 Terms of use of information provided by the User.<br>
 2.3.1 The operator uses the information obtained solely for the following purposes:<br>
 • registration of the User on the Website;<br>
 • fulfillment of its obligations to the User;<br>
 • analysis of the performance of the Site.<br>
 <br>
 <b>3. RESPONSIBILITY OF THE PARTIES</b><br>
 3.1 The Operator undertakes not to disclose information received from the User.<br>
 3.2 The operator who has not fulfilled his obligations shall be liable in accordance with the legislation of the Russian Federation, except as provided for in paragraphs. 2.1.2. this Privacy Policy.<br>
 3.3 The operator has the right to use the technology «cookies», which does not contain confidential information and is not transmitted to third parties.<br>
 3.4 Operator is not responsible for information:<br>
 3.4.1 Become public domain through the fault of the User.<br>
 3.4.2 Received from a third part until they are received by the Operator.<br>
 <br>
 <br>
 &nbsp;<br>
 <b class="t-3">TERMS OF USE</b><br>
 <br>
 This Agreement defines the conditions for the use by Users of the materials and services of the  <a href="/en/">www.sirius-aero.ru</a> site (hereinafter referred to as the “Site”).<br>
 <br>
 <b>1. GENERAL CONDITIONS</b><br>
 1.1 The use of all materials and services of the Site is regulated in accordance with the norms of the current legislation of the Russian Federation.<br>
 1.2 This Agreement is a public offer. By accessing the Site materials, the User joins it.<br>
 1.3 Site Administration has the right to change the terms of the Agreement. They become effective after three days. The user who does not agree with the changes is obliged to stop using the materials and services of the Site.<br>
 <br>
 <b>2. USER OBLIGATIONS</b><br>
 2.1 The User agrees not to violate, by his actions and comments on the Site, Russian and international standards, including in the field of intellectual property, copyright and related rights, protect the honor and dignity of the person, as well as harm the work of the Site and services.<br>
 2.2 According to Article 1270 of the Civil Code, it is not allowed to use the materials of the Site without the consent of the Rights Holders, fixed in the contract.<br>
 2.3 When citing materials, reference to the Site is obligatory (sub-clause 1 of clause 1 of Article 1274 of the Civil Code).<br>
 2.4 The User must understand that all the materials and services of the Site may contain promotional materials, for which the Administration of the Site is not responsible.<br>
 <br>
 <b>3. OBLIGATIONS OF THE ADMINISTRATION</b><br>
 3.1 Administration of the Site is not responsible for the consequences caused by the transition to links to external resources.<br>
 3.2 The Administration is not responsible for losses and damages of the User caused by registration on the Site, obtained information or services available through the transition on external links.<br>
 3.3 The late response of the Site Administration to unlawful actions of the user does not exclude its right to protect the interests of the Rights Holders at any other time.<br>
 <br>
 <b>4. OTHER CONDITIONS</b><br>
 4.1 All disputes related to the Agreement are considered by judicial authorities within the framework of the legislation in force in the territory of the Russian Federation.<br>
 4.2 The information provided on the Site cannot be considered as a partnership and is for informational purposes only.<br>
 4.3 A court decision that invalidates any of the provisions of this Agreement does not annul it completely.<br>
 <br>
 <br>
 The user confirms that he is familiar with all clauses of this Agreement and accepts them.<br>
 &nbsp;<br>
 <b class="t-3">CONSENT TO PERSONAL DATA PROCESSING</b><br>
 <br>
 I give my consent LLC Airline "Sirius-Aero" (TIN 7707276670, KPP 770701001, 125047, Moscow, ul. Chayanova, 22 bld.4, Phone: 8 495 989 61 91) for the processing of my personal data (name, series and passport number, contact phone number, e-mail address), for the purpose of fulfilling LLC Sirius-Aero Airlines before me the obligations specified in the cooperation agreement, including the sale of goods and services.<br>
 <br>
 My consent LLC Airline "Sirius-Aero" can use when conducting various kinds of research, improving the quality of services provided, while ensuring compliance with the laws, regulations of the Russian Federation, as well as the internal charter of LLC Airline "Sirius-Aero".<br>
 <br>
 I agree to the collection, systematization, storage and use, blocking and deletion of my personal data in cases stipulated by the current legislation of the Russian Federation, committed using specialized means.<br>
 <br>
 I have been notified that all personal information about me as a customer may be transferred to third parties (intermediaries) for organizing the process of payment for services used or goods purchased.<br>
 <br>
 It has been brought to my notice that Sirius-Aero Airline LLC guarantees the security of my personal data. The validity of this consent is not limited. Consent may be revoked at any time on my written application in accordance with the requirements of the legislation of the Russian Federation.<br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
