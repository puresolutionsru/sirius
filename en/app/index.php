<!doctype html>
<!--[if IE 8]><html lang="ru" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="ru" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="ru"><!--<![endif]-->
<head>
	<title></title>

	<meta charset="utf-8">
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="styles.css">

	<!-- <script src="js/jquery-1.9.1.min.js"></script> -->
	<!-- <script src="js/modernizr.min.js"></script> -->
	<!-- <script src="js/jquery.functions.js"></script> -->

	<link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,700&amp;subset=cyrillic" rel="stylesheet">

	<!--[if lt IE 9]>
	script(src="js/html5shiv.min.js")
	script(src="js/selectivizr-min.js")
	script(src="js/respond.min.js")
	<![endif]-->
</head>
<body>
	<header class="header">
		<a href="/"><img src="/bitrix/templates/sirius/img/logo.jpg" alt="" class="logo"></a>
	</header>
	<section class="main-section b-top">
		<div class="b-top__t">У нас для вас приложение</div>
		<div class="arrow b-top__arrow"></div>
		<div class="iphonepic b-top__iphone">
			<div class="iphonepic__bg"></div>
			<div class="iphonepic__window">
				<img src="images/mainwindow.jpg" alt="">
			</div>
		</div>
		<div class="t-1 b-top__undert">Услуги Sirius Aero всегда под рукой</div>
		<div class="b-top__desc desc">Мы выполняем внутренние и международные чартерные авиаперевозки практически во все страны мира. Базовый аэропорт располагается в Москве – в крупнейшем центре бизнес-авиации России – Vnukovo-3</div>
	</section>
	<section class="b-emptylegs">
		<div class="b-emptylegs__top">
			<div class="b-emptylegs__top_w">
				<div class="t-1 b-emptylegs__t">Возвратные рейсы</div>
				<div class="under-t b-emptylegs__under-t">Экономте до 75% от стоимости обычного чартера.</div>
				<div class="desc b-emptylegs__desc">Возвратный рейс - это рейс без пассажиров. Это происходит, когда воздушное судно высаживает пассажиров в аэропорту назначения и возвращается на аэродром приписки или когда самолет летит, чтобы забрать пассажиров в другом аэропорту.</div>
				<div class="iphonepic b-emptylegs__iphone">
					<div class="iphonepic__bg"></div>
					<div class="iphonepic__window">
						<img src="images/emptylegs.jpg" alt="">
					</div>
				</div>
				<div class="iphonepic b-emptylegs__iphone-filter">
					<div class="iphonepic__bg"></div>
					<div class="iphonepic__window">
						<img src="images/filter.jpg" alt="">
					</div>
				</div>
			</div>
		</div>
		<div class="b-emptylegs__bottom">
			<div class="iphone-in-circle">
				<div class="iphonepic">
					<div class="iphonepic__bg"></div>
					<div class="iphonepic__window">
						<img src="images/emptylegs.jpg" alt="">
					</div>
				</div>
			</div>
			<div class="b-emptylegs__bottom__info">
				<div class="b-emptylegs__bottom__info__t">Настройте отображение  возвратных рейсов</div>
				<div class="list-item">
					<span>–</span> Из своих направлений или сразу все
				</div>
				<div class="list-item">
					<span>–</span> Задать период перелетов
				</div>
				<div class="list-item">
					<span>–</span> Установить ценовое ограничение
				</div>
			</div>
		</div>
	</section>
	<section class="main-section b-push">
		<div class="t-1 b-push__t">Уведомления о возвратных рейсах</div>
		<div class="b-push__desc desc">Вы можете настроить уведомления и получать push&nbsp;-&nbsp;уведомления только о тех направлениях которые будут вами добавлены в личном кабинете</div>
		<div class="b-push__info">
			<div class="iphonepic">
				<div class="iphonepic__bg"></div>
				<div class="iphonepic__window">
					<img src="images/push.jpg" alt="">
				</div>
				<div class="push iphone-push1">
					<div class="push__t">Deals: Moscow - Simferopol</div>
					<div class="push__tx">28.09.19-02.10.19 Hawker 750 $ 15 000</div>
				</div>
				<div class="push iphone-push2">
					<div class="push__t">Deals: Nice - Vienna</div>
					<div class="push__tx">20.09.19-22.09.19 Legacy 600 $ 20 000</div>
				</div>
				<div class="push iphone-push3">
					<div class="push__t">Deals: Minsk - London</div>
					<div class="push__tx">24.09.19-26.09.19 Hawker 1000 $ 48 000</div>
				</div>
				<div class="push iphone-push4">
					<div class="push__t">Deals: Milan - Dubai</div>
					<div class="push__tx">17.09.19-18.09.19 Legacy 600 $ 50 000</div>
				</div>
				<div class="push iphone-push5">
					<div class="push__t">Deals: Yerevan - Zurich</div>
					<div class="push__tx">19.09.19-19.09.19 Legacy 600 $ 18 000</div>
				</div>
			</div>
		</div>
	</section>
	<section class="b-charter">
		<div class="t-1">Заказ чартера</div>
		<div class="under-t">Чартерные авиаперевозки практически во все страны мира.</div>
		<div class="b-charter__desc desc">Организация и выполнение чартерных авиаперевозок на собственных и партнёрских самолётах по всему миру</div>
		<div class="iphonepic b-charter__iphone">
			<div class="iphonepic__bg"></div>
			<div class="iphonepic__window">
				<img src="images/charter.jpg" alt="">
			</div>
		</div>
	</section>
	<section class="b-load">
		<div class="t-1 b-load__t">Загрузите приложение</div>
		<div class="b-load__info">
			<div class="b-load__link-on-app">
				<div class="b-load__link-on-app__i">
					<a href="https://play.google.com/store/apps/details?id=com.siriusaero" class="gplay" target="_blank"></a>
				</div>
				<div class="separator"></div>
				<div class="b-load__link-on-app__i">
					<a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" class="appstore" target="_blank"></a>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer">
		
	</footer>
</body>
</html>