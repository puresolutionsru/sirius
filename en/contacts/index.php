<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Contacts");
$APPLICATION->SetTitle("Contacts");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><h3>CHARTER FLIGHTS:<br>
 </h3>
<ul>
	 <!-- <li><a href="mailto:sales@sirius-aero.ru">sales@sirius-aero.ru</a></li> -->
	<li><span onclick="window.location.href='tel:+74959896191'" class="tag-like-link">+7 (495) 989 61 91 </span></li>
</ul>
<h3>OUR OFFICE:</h3>
<ul>
	<li>Phone:&nbsp;<span onclick="window.location.href='tel:+74959896191'" class="tag-like-link" itemprop="telephone">+7 (495) 989 61 91</span></li>
	<li>Fax:&nbsp;<span onclick="window.location.href='tel:+74959896191'" class="tag-like-link" itemprop="faxNumber">+7 (495) 989 61 91</span>&nbsp;ex. 108</li>
	<li><a href="mailto:info@sirius-aero.ru">info@sirius-aero.ru</a></li>
	<li>Address: 121354, Moscow, ul. Dorogobuzhskaya, 14, building 1, Business Center “Partner”</li>
</ul>
<!--<h3>PR DEPARTMENT:</h3>
<ul>
	<li><a href="mailto:pr@sirius-aero.ru">pr@sirius-aero.ru</a></li>
	<li><a href="tel:+74959896191">+7 (495) 989-61-91</a> ex. 161</li>
	<li><a href="tel:+79060291650">+7 (906) 029-16-50</a></li>
</ul>-->
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aadc3d2cfd7d1651f5b25161336578ce33c7d64e9b731b3bb2e9d0a94a1bbf6be&amp;source=constructor&lang=en_US" height="503" style="width:100%;" frameborder="0"></iframe><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>