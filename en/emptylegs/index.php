<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetPageProperty("title", "Empty legs");
$APPLICATION->SetPageProperty("title_legs", "What it an empty leg");
$APPLICATION->SetTitle("Empty legs");
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';

if($_REQUEST['mtlid']) {
    include_once "../../emptylegs/plane_loader_template.php";
}

echo '<div class="mlt-description">
        <div class="mlt-description__button">'.$APPLICATION->getProperty("title_legs").'</div>
        <div class="mlt-description__text">'
?>

<p>Empty leg is a flight without passengers. After completing an order and discharging it’s passengers at the destination airport, the aircraft returns to its home base or flies to another airport to pick up the next customer. You can spare up to 75% of the usual charter price.</p>

<?
echo '  </div>
      </div>';

      ?>
      <div class="b-app-info">
          <div class="b-app-info__t">Install our App and find out about new flights:</div>
          <div class="b-app-info__list">
              <div class="b-app-info__i">
                  <a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios_emptyleg'); return true;" class="appstore" target="_blank"></a>
              </div>
              <div class="b-app-info__i">
                  <a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android_emptyleg'); return true;" class="gplay" target="_blank"></a>
              </div>
          </div>
      </div>
      <?


include_once "../../emptylegs/loader_template.php";
echo '
    <script src="https://sirius-aero.ru/emptylegs/lang/en.js" defer></script>
    <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js" defer></script>
    <script src="https://sirius-aero.ru/emptylegs/lib/cardTemplate.js" defer></script>
    <script src="https://sirius-aero.ru/emptylegs/lib/planeTemplate.js" defer></script>
    <script src="https://sirius-aero.ru/emptylegs/firebase.js" defer></script>
    </div>
</div>
';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>