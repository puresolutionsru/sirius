<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Business Aviation | Rent a private plane | Order a VIP charter - Sirius Aero");
$APPLICATION->SetTitle("Home");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide Aviation company Sirius Aero The largest business aviation operator in Eastern Europe");
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/css/slick-theme.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.dotdotdot.min.js');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/dot.js');
echo '<div class="main-info cl">
        <img class="main-info__bg" src="'.SITE_TEMPLATE_PATH.'/img/img.jpg" alt="" title="">
        <div class="main-info__col-1 col-xs-6">
            <div class="ttl">';
                $APPLICATION->IncludeFile(
                    SITE_DIR.'/include/indexttl.php',
                    array(),
                    array(
                        "MODE"=>"text",
                    )
                );
echo '      </div>
        </div>
        <div class="main-info__col-2 col-xs-6">
    ';
?><br>
 Sirius Aero is international, one of the first Russian business aviation companies.<br>
 <br>
 The main activities are private charter flights and aircraft management. For details, see sections <a href="/services/">services</a> and <a href="/menedzhment-vs/">aircraft management</a>. Also you will find there information about how we can be useful to you. <br>
 <br>
 We provide domestic and international charter flights to virtually all countries of the world. The base airport is located in Moscow and in Malta. <br>
 <br>
 Sirius Aero fleet is a modern business jets and Russian business liners with a premium cabin configuration. There are 6 aircraft types in our fleet: <br>
 <br>
 <a href="/fleet/#flot-list1">MIDSIZE JETS</a>: Hawker 750,&nbsp;Hawker 850&nbsp;XP and Hawker 1000 <br>
 <br>
 <a href="/fleet/#flot-list2">HEAVY JETS</a>: Legacy 600, <!--Challenger 601, -->Challenger 850 <br>
 <br>
 <a href="/fleet/#flot-list3">BIZNES LINERS</a>: Yak-42 <br>
 <br>
 The whole fleet represents the highest levels of luxury, safety and security. <br>
 <br>
<div class="details">
 <a href="/about/company/">Read more about Company</a>
</div>
<br><?
echo '  </div>
    </div>';

require('index-'.LANGUAGE_ID.'.php');

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
