<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero |  Company");
$APPLICATION->SetTitle("Company");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><p>
	Sirius Aero is international, one of the largest Russian business aviation companies. We provide domestic and international charter flights to virtually all countries of the world.
</p>
<p>
	<span class="bold">Sirius Aero fleet</span> is a modern business jets and Russian business liners with a premium cabin configuration. There are 6 aircraft types in our fleet. <a href="/en/fleet/">Look at Fleet</a>
</p>
<p>
	Sirius Aero is <span class="bold">the largest commercial operator</span> in Eastern Europe. Sirius Aero has 2 offices – in Moscow and Vienna. The base airport is located in Moscow and in Malta. The activities of our company comply with Russian and international air law. This is confirmed by the relevant certificates and licenses.
</p>
<div class="b-pic-gal">
 <a href="/about/company/img/cert1.jpg" class="fancybox" data-fancybox="group-gal" data-caption="СЕРТИФИКАТ КОММЕРЧЕСКОГО ЭКСПЛУАТАНТА">
    <img src="/about/company/img/cert1.jpg" alt="">
 </a>
 <a href="/about/company/img/sert2.jpg" class="fancybox" data-fancybox="group-gal" data-caption="ЛИЦЕНЗИЯ НА ПЕРЕВОЗКУ ПАССАЖИРОВ">
    <img src="/about/company/img/sert2.jpg" alt="">
 </a>
 <a href="/about/company/img/sert3.jpg" class="fancybox" data-fancybox="group-gal" data-caption="ЛИЦЕНЗИЯ НА ПЕРЕВОЗКУ ГРУЗОВ">
    <img src="/about/company/img/sert3.jpg" alt="">
 </a>
	<p>
		Sirius Aero always takes care of its passengers, so we comply with international safety standards.
	</p>
 <a href="/images/IS-BAO-Certificate2019.jpg" class="fancybox" data-fancybox="group-gal" data-caption="IS-BAO CERTIFICATE STAGE I"><img src="/upload/medialibrary/939/939f2fec5cf87d35790e91c396dc1c29.jpg" alt=""></a> <a href="/upload/medialibrary/ce7/ce7e18f6918266366a8ccf7265ed7c81.jpg" class="fancybox" data-fancybox="group-gal" data-caption="EASA-TCO CERTIFICATE"><img src="/upload/medialibrary/ce7/ce7e18f6918266366a8ccf7265ed7c81.jpg" alt=""></a> <a href="/upload/medialibrary/7c2/7c2bfd8b965f1f141ba86f3b31eea218.jpg" class="fancybox" data-fancybox="group-gal" data-caption="CONTINUING AIRWORTHINESS AUTHORISATION OTAR 39 BDA/SF/038"><img src="/upload/medialibrary/7c2/7c2bfd8b965f1f141ba86f3b31eea218.jpg" alt=""></a>
</div><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
