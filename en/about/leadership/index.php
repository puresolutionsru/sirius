<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Leadership");
$APPLICATION->SetTitle("Leadership");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w _mod1">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><div class="t-3">
	 Mihail Parnev
</div>
<p>
	 Chief Operating Officer
</p>
<div class="t-3">
	 Alexandr Smirnov
</div>
<p>
	 Flight Director
</p>
<div class="t-3">
	 Sergey Klokotov
</div>
<p>
	 Technical Director
</p>
<div class="t-3">
	 Stepnova Elena
</div>
<p>
	 Commercial Director
</p>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>