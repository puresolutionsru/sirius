<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | For passengers");
$APPLICATION->SetTitle("For passengers");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><p>
 <span class="bold">General liability insurances of «SIRIUS-AERO» Ltd.</span> is insured with a compulsory insurance agreements on the carrier’s legal liability towards an air passenger:
</p>
<ul>
	<li>№ 19AVK0124 dated&nbsp;12.07.2019&nbsp;Valid until&nbsp;16.07.2020</li>
	<li>№ 19AVK0127 dated 12.07.2019&nbsp;Valid until&nbsp;16.07.2020</li>
</ul>
<p class="bold">
	 For further information, please contact SOGAZ Insurance Group:
</p>
<p>
	 Marina Valerievna Sivitskaya<br>
	 Aviation and Commercial Risks Insurance Management Department Head<br>
	 E-mail: <a href="mailto:Sivitskaya.Marina@sogaz.ru">Sivitskaya.Marina@sogaz.ru</a><br>
	 Phone.: <a href="tel:+74957392140">(495) 739-21-40</a>, ext. 2768
</p>
<p>
	 Andrey Alexandrovich Serikov<br>
	 Aviation Risks Insurance Department Head<br>
	 E-mail: <a href="mailto:SerikovAA@sogaz.ru">SerikovAA@sogaz.ru</a><br>
	 Phone.: <a href="tel:+74957392140">(495) 739-21-40</a>, ext. 2764
</p>
<p>
	 In case of an occurrence meeting criteria of an insured event, please contact the head office or nearest office of SOGAZ Insurance Group at addresses specified on the insurance company’s website.
</p>
<p>
	 Head Office:<br>
	 10 Akademika Sakharova av., Moscow, 107078, Russia<br>
	 Phone: <a href="tel:+74957807880">(495) 780-78-80</a>, <a href="tel:+74957392140">739-21-40</a>, <a href="tel:+74955392900">539-29-00</a><br>
	 Fax: (495) 739-21-39<br>
	 E-mail: <a href="mailto:sogaz@sogaz.ru">sogaz@sogaz.ru</a><br>
	 Website: <a href="//www.sogaz.ru/" target="_blank">www.sogaz.ru</a>
</p>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
