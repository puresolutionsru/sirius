<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Vacancy");
$APPLICATION->SetTitle("Vacancy");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?><p class="p-ligne-center">
	 If you are interested in working in Sirius Aero, please, send your CV to our HR department:
</p>
<div class="two-clmn-block">
    <div class="two-clmn-block-item"><a href="mailto:hr@sirius-aero.ru">hr@sirius-aero.ru</a></div>
	<div class="two-clmn-block-item"><a href="tel:+74959896191">+7 495 989 61 91</a>&nbsp;ext. 234</div>


</div>
<br>
<br>



<p>
	 If you send your CV for any vacancy of Sirius Aero by any means available to you, you confirm that all information you have included into your CV is full, true, correct and up-to-date, and that you give Sirius Aero your consent to your personal data processing, including, without limitation, personal data collection, systematization, accumulation, storage, adjustment (including renewal, change), usage, distribution (including transfer), depersonalization, blocking, destruction.&nbsp; <br><br>
	 Any personal data contained in a CV will be used for the purpose of the employment application consideration in strict adherence to requirements of Federal Law “On Personal Data” dated&nbsp; 27.07.2006 No. 152-FZ.
</p>
 <br><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
