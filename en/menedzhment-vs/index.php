<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Aircraft Management");
$APPLICATION->SetTitle("Aircraft Management");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?>
<p>
	 Aircraft management is a comprehensive program of operating client’s aircraft that optimizes costs and solves all issues related to it.
</p>
<p>
	 The composition of the services included in Aircraft management determines individually for each client based on his needs and wishes.
</p>
<p>
	 Basically, we offer the following issues:&nbsp;
</p>
<ul>
    <li>choice of exploitation type (private or commercial) and jurisdiction</li>
    <li>development of contractual legal structure</li>
    <li>planning, organization and operational support of flights</li>
    <li>selection, training and retraining of the crew</li>
    <li>development of commercial loading conditions (if necessary)</li>
    <li>financial statements</li>
    <li>maintenance at our own bases in Russia and Europe</li>
</ul>
<p>For more information please contact with our Aircraft Management Department</p>
<ul>
	<li>
	<p>
		Phone: <a href="tel:+79067608160">+7 906 760 81 60</a>&nbsp;
	</p>
	</li>
	<li>
	<p>
		E-mail: <a href="mailto:yourjetmanagement@sirius-aero.ru">yourjetmanagement@sirius-aero.ru</a>
	</p>
	</li>
</ul><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
