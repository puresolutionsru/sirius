<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "");
$APPLICATION->SetPageProperty("title", "Shuttle Up");
$APPLICATION->SetPageProperty("title_legs", "What it a Shuttle Up");
$APPLICATION->SetTitle("Shuttle Up");

$count = (int)file_get_contents('https://sirius-aero.ru/shuttles/countAllow.txt');
function plural_form($number,$before,$after) {
	$cases = array(2,0,1,1,1,2);
	return $number.' '.$after[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]] . ' ' . $before[($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)]];
}
$t = plural_form($count, array('available','available','available'), array('flight','flights','flights'));
$APPLICATION->SetTitle($t);
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';

if($_REQUEST['mtlid']) {
    include_once "../../shuttles/plane_loader_template.php";
}

echo '<div class="mlt-description">
        <div class="mlt-description__button">'.$APPLICATION->getProperty("title_legs").'</div>
        <div class="mlt-description__text">'
?>

<p>Sirius Aero has an exclusive offer Shuttle UP which you can use for your own purposes. It is based on jet-sharing principle, where you rent the aircraft together with other passengers and travel on a particular schedule.</p>

<?
echo '  </div>
      </div>';

      ?>
      <div class="b-app-info">
          <div class="b-app-info__t">Install our App and find out about new flights:</div>
          <div class="b-app-info__list">
              <div class="b-app-info__i">
                  <a href="https://apps.apple.com/ru/app/siriusapp/id1451576658" onclick="ym(28830840, 'reachGoal', 'ios_shuttle'); return true;" class="appstore" target="_blank"></a>
              </div>
              <div class="b-app-info__i">
                  <a href="https://play.google.com/store/apps/details?id=com.siriusaero" onclick="ym(28830840, 'reachGoal', 'android_shuttle'); return true;" class="gplay" target="_blank"></a>
              </div>
          </div>
      </div>
      <?


include_once "../../shuttles/loader_template.php";
echo '
    <script src="https://sirius-aero.ru/shuttles/lang/en.js" defer></script>
    <script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js" defer></script>
    <script src="https://sirius-aero.ru/shuttles/lib/cardTemplate.js" defer></script>
    <script src="https://sirius-aero.ru/shuttles/lib/planeTemplate.js" defer></script>
    <script src="https://sirius-aero.ru/shuttles/firebase.js" defer></script>
    </div>
</div>
';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>