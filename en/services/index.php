<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("TITLE", "Sirius Aero | Services");
$APPLICATION->SetTitle("Services");
$APPLICATION->SetPageProperty("description", "Sirius Aero - international Charter flights worldwide ".$APPLICATION->GetTitle(false));
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <div class="t-1">' . $APPLICATION->GetTitle(false) . '</div>
    ';
?>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/checklist.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span><a href="#form-order" class="fancymodal">VIP CHARTER FLIGHTS</a><br />
        Organization and implementation of charter flights on own and partner’s aircraft around the world</span>
    </p>
    <div class="cl"></div>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/globe.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span><a href="/menedzhment-vs/">AIRCRAFT MANAGEMENT</a><br />
        Comprehensive management program of client’s aircraft including cost optimization, crew recruiting, flight operations, finance administration, aircraft maintenance and risk management</span>
    </p>
    <div class="cl"></div>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/calendar-check.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span>FLIGHT ORGANIZATION AND GROUND SERVICE<br />
        Receiving slots in domestic and international airports, landing and overflight permits. Ground handling arrangements and passenger VIP Terminal services</span>
    </p>
    <div class="cl"></div>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/stock.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span>CONTINUING AIRWORTHINESS MANAGMENT<br />
        Maintenance planning and controlling, implementation of service bulletins and airworthiness directives. Maintenance of warranty and financial programs and round the clock support of operation</span>
    </p>
    <div class="cl"></div>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/repair-tools.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span>AIRCRAFT MAINTENANCE<br />
        Line and base maintenance of aircraft at our own bases in Russia and Europe</span>
    </p>
    <div class="cl"></div>
    <p class="flex-2b">
        <img src="/bitrix/templates/sirius/img/for-load/chat.svg" alt="" class="pull-left" width="70" style="margin-right:20px;">
        <span>AVIATION CONSULTING<br />
        Comprehensive range of services from technical, financial and legal consulting to complete solutions of aircraft selection, purchase and ownership</span>
    </p>
    <div class="cl"></div>
<?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>
