<?php
// переделать после get_countries

class Requests {
	public function get_company_routes() {
		return file_get_contents(MAIN_DIRECTORY . '/files/'.$_GET['company'].'/routes.json');
	}
	public function get_schedule() {
		return file_get_contents(MAIN_DIRECTORY . '/files/'.$_GET['company'].'/schedule.json');
	}
	public function get_cities() {
		return file_get_contents(MAIN_DIRECTORY . '/files/city.json');
	}
	public function get_countries() {
		return file_get_contents(MAIN_DIRECTORY . '/files/country.json');
	}



























	/**
	 * Записываем присланные параметры в файл и бронируем подкласс
	 */
	public function set_ordersdata() {
	    $order = new Order();
	    $orderid = $order->AddOrder($_GET['data']);
        $data = json_decode($_GET['data']);
        $data->orderid = $orderid;
        $jsonData = json_encode($data, JSON_UNESCAPED_UNICODE);
		$token = md5(uniqid(rand(),1));
		file_put_contents(MAIN_DIRECTORY . '/cache/' . $token . '.json', $jsonData);

		$bron = new Describes;
		$setdata = $bron->set_flight(json_decode($_GET['data']));

		return $token;
	}
	/**
	 * Устанавливаем парметры
	 */
	public function set_flight_for_seat() {
		$_GET['params'] = json_decode($_GET['params']);
		$bron = new Describes;
		$setdata = $bron->set_flight($_GET['params']);

		return json_encode($setdata);
	}

	public function pay_deposit() {

	    $auth_key = 'awgwagawfaw23cr3x23rc432cfx3f2';
	    $user_id = $_GET['user_id'];
	    $contract_id = $_GET['contract_id'];
	    $amount = $_GET['amount'];
        $arr = [
            'auth_key' => $auth_key,
            'user_id' => $user_id,
            'contract_id' => $contract_id,
            'service_id' => 2853880,
            'status' => 'in_progress',
            'type_operation' => 'debit',
            'form_of_payment' => 'balance',
            'amount' => $amount
        ];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/billing/add/");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $arr);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3000);
		$billing = json_decode(curl_exec($ch), true);
		$billingId = $billing['billing_id'];
		curl_close($ch);

		$status = 'cancelled';
		if($billingId) {
            $status = 'success';
		}
		$arrChangeStatus = [
            'auth_key' => $auth_key,
            'user_id' => $user_id,
            'status' => $status
        ];

		$chs = curl_init();
		curl_setopt($chs, CURLOPT_URL, "https://corp.rusline.aero/rest/v1/cabinet_sirius/billing/edit/$billingId");
		curl_setopt($chs, CURLOPT_POSTFIELDS, $arrChangeStatus);
		curl_setopt($chs, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($chs, CURLOPT_CONNECTTIMEOUT, 3000);
        curl_exec($chs);
		curl_close($chs);
		if($billingId) {
			return true;
		}

		return false;
	}



















	
	/**
	 * Дописываем данные о пассажире в файл
	 */
	public function set_passdata() {print_r($_GET);
		$data = file_get_contents(MAIN_DIRECTORY . '/cache/' . $_GET['token'] . '.json');
		$data = json_decode($data);

        $order = new Order();
        $order->addPassenger($_GET['firstname'], $_GET['lastname'], $data->orderid);

		$data->firstname = $_GET['firstname'];
		$data->lastname = $_GET['lastname'];
		
		file_put_contents(MAIN_DIRECTORY . '/cache/' . $_GET['token'] . '.json', json_encode($data, JSON_UNESCAPED_UNICODE));
		return true;
	}
}