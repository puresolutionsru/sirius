<?php
// удалить, переделать методы после get_company_shedule

class Describes {

    /**
     * доступные маршруты для выбранной авиакомпании (список направлений для формы поиска)
     */
    public function get_company_routes() {
        $tais = new Tais;
        $params->company = $_GET['company'];
        $params->method = 'get_company_routes';
        $answer = $tais->get_data($params);
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        $data = array();
        foreach($answer['SIG_AirRoutesRS']['Routes']['Route'] as $element) :
            $element = Functions::xml2array($element);
            $data[$element['@attributes']['Departure']][$element['@attributes']['Arrival']] = $element['@attributes']['TypeOfService'];
        endforeach;

        file_put_contents(MAIN_DIRECTORY . '/files/'.$_GET['company'].'/routes.json', json_encode($data, JSON_UNESCAPED_UNICODE));

        $this->get_company_shedule($_GET['company']);
    }
    /**
     * формирование расписания
     */
    public function get_company_shedule($companyCode) {
        $tais = new Tais;
        $params->company = $companyCode;
        $params->method = 'get_company_shedule';
        $params->minDepDate = date("Y-m-d");
        $seconds = 3600*24*365;
        $params->maxDepDate = date("Y-m-d", strtotime(date("Y-m-d") . " + $seconds SECONDS"));

        // из файла со списком направлений обходим каждое направление и получаем для него расписание
        $data = array();
        $routes = json_decode(file_get_contents(MAIN_DIRECTORY . '/files/'.$companyCode.'/routes.json'));
        foreach($routes as $from=>$route) :
            $params->from = $from;
            foreach($route as $to=>$type) :
                $params->to = $to;
                $answer = $tais->get_data($params);
                $answer = Functions::xml2array(new SimpleXMLElement($answer));

                $schedule = $answer["SIG_AirScheduleRS"]["Schedules"]["Schedule"];
                $out = array();
                if($schedule["@attributes"]) {
                    $out[] = $schedule;
                } else {
                    foreach($schedule as $key=>$value) {
                        $out[$key] = (array) $value;
                    }
                }

                foreach($out as $key=>$value) :
                    $data[$params->from . $params->to][$key]["MinDepDate"] = $value["@attributes"]["MinDepDate"];
                    $data[$params->from . $params->to][$key]["MaxDepDate"] = $value["@attributes"]["MaxDepDate"];
                    $data[$params->from . $params->to][$key]["Days"] = $value["@attributes"]["Days"];
                    $days = str_split((string)$value["@attributes"]["Days"]);
                    foreach($days as $day) {
                        $data[$params->from . $params->to][$key]["daysar"][] = $day;
                    }
                endforeach;
            endforeach;
        endforeach;

        // из полученного расписания делаем более читаемый формат для дальнейшего использования
        $answer = array();
        foreach($data as $route=>$params) :
            foreach($params as $param) :
                if(!$answer[$route]['freq']) {
                    for($i=0; $i<7; $i++) {
                        $answer[$route]['freq'][$i] = false;
                    }
                }
                $days = str_split((string)$param['Days']);
                foreach($days as $day) {
                    $answer[$route]['freq'][$day-1] = true;
                }
                
                $today = date('d.m.y');
                $todaystrotime = (int)(strtotime($today)/86400);
                $todaystrotime = $todaystrotime * 86400;
                
                $ds = $param['MinDepDate'];
                $ds = substr($ds, 8, 2).".".substr($ds, 5, 2).".".substr($ds, 0, 4);
                $dsstrotime = (int)(strtotime($ds)/86400);
                $dsstrotime = $dsstrotime * 86400 + 86400;
                
                $de = $param['MaxDepDate'];
                $de = substr($de, 8, 2).".".substr($de, 5, 2).".".substr($de, 0, 4);
                $destrotime = (int)(strtotime($de)/86400);
                $destrotime = $destrotime * 86400 + 86400;
                
                for($i=0;$i<1000;$i++) {
                    $dd = $todaystrotime + ($i*86400);

                    if($dsstrotime > $dd) {
                        continue;
                    }

                    $date = new DateTime();
                    $date->setTimestamp($dd);
                    $f = $date->format('d.m.y');
                    $dayweek = $date->format('w');
                    if($dayweek==0) {
                        $dayweek = 7;
                    }

                    if(!$answer[$route]['calendar'][$f]) {
                        $answer[$route]['calendar'][$f] = array();
                    }
                    if(in_array($dayweek, $param["daysar"])) {
                        $answer[$route]['calendar'][$f]["flight"] = true;
                    }

                    if($destrotime == $dd) {
                        break;
                    }
                }
            endforeach;
        endforeach;

        file_put_contents(MAIN_DIRECTORY . '/files/'.$companyCode.'/schedule.json', json_encode($answer, JSON_UNESCAPED_UNICODE));
    }
    /**
     * получение типа самолета
     */
    public function get_equipment() {
        $setdata = json_decode($this->set_flight(json_decode($_GET["params"])));
        $answer = json_decode($this->get_air_services($_GET["params"]));
        
        return json_encode($answer->SIG_AirServicesRS);
    }


























































    /**
     * получение списка рейсов
     */
    public function get_results() {
        $tais = new Tais;
        $_GET["params"] = json_decode($_GET["params"]);
        $_GET["params"]->method = 'get_results';
        $answer = $tais->get_data($_GET["params"]);
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        $data['attributes'] = $answer['SIG_AirShopRS']['@attributes'];
        $data['ShopOptions'] = $answer['SIG_AirShopRS']['ShopOptions'];
        $data['Services'] = $answer['SIG_AirShopRS']['Services'];
        $data['ServiceSets'] = $answer['SIG_AirShopRS']['ServiceSets'];
        return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    /**
     * устанавливаем рейс и тариф
     */
    public function set_flight($data) {
        $tais = new Tais;
        $data->method = 'set_flight';
        $answer = $tais->get_data($data);
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer);
    }
    /**
     * карта мест
     */
    public function get_air_services() {
        $tais = new Tais;
        $_GET["params"] = json_decode($_GET["params"]);
        $_GET["params"]->method = 'get_air_services';
        $answer = $tais->get_data($_GET["params"]);
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }




























    
    /**
     * устанавливаем данные пассажира
     */
    public function booking_place() {
        $tais = new Tais;
        $gost = array(
            "а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
            "е"=>"e", "ё"=>"yo","ж"=>"j","з"=>"z","и"=>"i",
            "й"=>"i","к"=>"k","л"=>"l", "м"=>"m","н"=>"n",
            "о"=>"o","п"=>"p","р"=>"r","с"=>"s","т"=>"t",
            "у"=>"y","ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch",
            "ш"=>"sh","щ"=>"sh","ы"=>"i","э"=>"e","ю"=>"u",
            "я"=>"ya",
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
            "Е"=>"E","Ё"=>"Yo","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"I","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"Y","Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"Ch",
            "Ш"=>"Sh","Щ"=>"Sh","Ы"=>"I","Э"=>"E","Ю"=>"U",
            "Я"=>"Ya",
            "ь"=>"","Ь"=>"","ъ"=>"","Ъ"=>"",
            "ї"=>"j","і"=>"i","ґ"=>"g","є"=>"ye",
            "Ї"=>"J","І"=>"I","Ґ"=>"G","Є"=>"YE"
        );
        $_GET['lastname'] = strtr($_GET['lastname'], $gost);
        $_GET['firstname'] = strtr($_GET['firstname'], $gost);
        $data = file_get_contents(MAIN_DIRECTORY . '/cache/' . $_GET['token'] . '.json');
        $data = json_decode($data);
        $answer = $tais->get_data(array('session'=>$_GET['session'], 'flight'=>$_GET['flight'], 'lastname'=>$_GET['lastname'], 'firstname'=>$_GET['firstname'], 'tarif'=>$data->tarif, 'service'=>$data->service, 'method'=>'booking_place'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        // echo "<pre>";
        // print_r($answer);
        // echo "</pre>";

        return json_encode($answer['SIG_AirBookRS'], JSON_UNESCAPED_UNICODE);
    }
    /**
     * оформление билета
     */
    public function issue_ticket() {
        $tais = new Tais;
        $answer = $tais->get_data(array('bookingreference'=>$_GET['bookingreference'], 'leadpassenger'=>$_GET['leadpassenger'], 'currency'=>$_GET['currency'], 'amount'=>$_GET['amount'], 'SessionID'=>$_GET['session'], 'type'=>$_GET['type'], 'method'=>'issue_ticket'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer['SIG_AirBookRS'], JSON_UNESCAPED_UNICODE);
    }
    /**
     * оформление услуг
     */
    public function issue_services() {
		$data = file_get_contents(MAIN_DIRECTORY . '/cache/' . $_GET['token'] . '.json');
        $data = json_decode($data);
        foreach($data->service as $value) {
            $tais = new Tais;
            if(!$value->ActualPrice) {
                $value->ActualPrice = 0;
            }
            $answer = $tais->get_data(array('bookingreference'=>$_GET['bookingreference'], 'leadpassenger'=>$_GET['leadpassenger'], 'currency'=>$_GET['currency'], 'amount'=>$value->ActualPrice, 'SessionID'=>$_GET['session'], 'type'=>$_GET['type'], 'method'=>'issue_services'));
            $answer = Functions::xml2array(new SimpleXMLElement($answer));
        }

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }



    /**
     * 
     */
    public function add_services() {
		$data = file_get_contents(MAIN_DIRECTORY . '/cache/' . $_GET['token'] . '.json');
        $data = json_decode($data);
        $tais = new Tais;
        $answer = $tais->get_data(array('bookingreference'=>$_GET['bookingreference'], 'leadpassenger'=>$_GET['leadpassenger'], 'flightref'=>$_GET['flightref'], 'paxref'=>$_GET['paxref'], 'SessionID'=>$_GET['session'], 'method'=>'add_services', 'service'=>$data->service, 'tarif'=>$data->tarif));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }














































































    /**
     * (не используется)
     */
    public function get_company_schedule() {
        $tais = new Tais;
        $answer = $tais->get_data(array('company'=>$_GET['company'], 'from'=>$_GET['from'], 'to'=>$_GET['to'], 'date'=>$_GET['date'], 'method'=>'get_company_schedule'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer['SIG_AirScheduleRS']['Schedules'], JSON_UNESCAPED_UNICODE);
    }

    /**
     * поиск рейсов (выдача, вывод рейсов на конкретное направление и дату)
     */
    public function get_air_avail() {
        $tais = new Tais;
        $answer = $tais->get_data(array('company'=>$_GET['company'], 'from'=>$_GET['from'], 'to'=>$_GET['to'], 'date'=>$_GET['date'], 'count'=>$_GET['count'], 'method'=>'get_air_avail'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer['SIG_AirAvailRS'], JSON_UNESCAPED_UNICODE);
    }

    /**
     * устанавливаем рейс и тариф
     */
    public function set_flight1($data) {
        $tais = new Tais;
        $answer = $tais->get_data(array('flight'=>$data['flight'], 'fare'=>$data['fare'], 'session'=>$data['session'], 'method'=>'set_flight'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return $answer;
    }




























    public function get_rfisc() {
        $tais = new Tais;
        $answer = $tais->get_data(array('session'=>$_GET['session'], 'method'=>'get_rfisc'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));
        echo '<pre>';
        print_r($answer);
        echo '</pre>';

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }
    public function set_shopflight() {
        $tais = new Tais;
        $answer = $tais->get_data(array('session'=>$_GET['session'], 'method'=>'set_shopflight'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));
        echo '<pre>';
        print_r($answer);
        echo '</pre>';

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }
    public function get_pr() {
        $tais = new Tais;
        $answer = $tais->get_data(array('session'=>$_GET['session'], 'method'=>'get_pr'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));
        echo '<pre>';
        print_r($answer);
        echo '</pre>';

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }
    /**
     * карта мест самолета
     */
    public function get_servtarif() {
        $tais = new Tais;
        $answer = $tais->get_data(array('session'=>$_GET['session'], 'method'=>'get_servtarif'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }


    /**
     * поиск рейсов
     */
    public function get_queue() {
        $tais = new Tais;
        $answer = $tais->get_data(array('method'=>'get_queue'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));
        // echo '<pre>';
        // print_r($answer);
        // echo '</pre>';

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }
















    
    
    /**
     * 
     */
    public function get_fare_rules() {
        $tais = new Tais;
        $answer = $tais->get_data(array('method'=>'get_fare_rules'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));
        echo '<pre>';
        print_r($answer);
        echo '</pre>';

        // $data['attributes'] = $answer['SIG_AirShopRS']['@attributes'];
        // $data['ShopOptions'] = $answer['SIG_AirShopRS']['ShopOptions'];
        // $data['Services'] = $answer['SIG_AirShopRS']['Services'];
        // $data['ServiceSets'] = $answer['SIG_AirShopRS']['ServiceSets'];
        // return json_encode($data, JSON_UNESCAPED_UNICODE);
    }
    
    /**
     * получение цен
     */
    public function get_price() {
        $tais = new Tais;
        $answer = $tais->get_data(array('method'=>'get_price'));
        $answer = Functions::xml2array(new SimpleXMLElement($answer));

        return json_encode($answer, JSON_UNESCAPED_UNICODE);
    }










    

    

}
