<?
// убрать из get_data условие массив, объект. принимать только объект

class Tais
{
	/**
	 * Errors
	 * @var array
	 */
	public $error = array();
	/**
	 * An open socket
	 * @var resource
	 */
	public $socket;
	/**
	 * Constructor
	 */
	public function __constuct() {
		
	}
	/**
	 * Socket create and connect
	 */
	public function connect() {
		
	}
	/**
	 * Close socket
	 */
	public function closeConnect() {
		
	}
	/**
	 * Send request
	 * @param string $xml Xml document for request
	 * @return string
	 */
	public function sendRequest($xml) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, TAIS_URL);
		curl_setopt($ch, CURLOPT_USERPWD, TAIS_USER.":".TAIS_PASS);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
	/**
	 * Create request
	 * @param array $data
	 * @return string
	 */
	public function get_data($data) {
		if(is_array($data)) {
			$method = $data['method'];
		} else {
			$method = $data->method;
		}
		$answer = self::sendRequest(Xml::$method($data));

		return $answer;
	}
}