<?
// переделать, удалить методы после get_company_shedule

class Xml {
	/**
	 * Xml object for request
	 * @var object
	 */
	public static $xml;
	/**
	 * Begin constructor xml
	 */
	public function createBegin() {
		self::$xml = xmlwriter_open_memory();
		xmlwriter_set_indent(self::$xml, 0);
		xmlwriter_set_indent_string(self::$xml, ' ');
		xmlwriter_start_document(self::$xml, '1.0', 'UTF-8');
			xmlwriter_start_element(self::$xml, 'SIG_Request');
	}
	/**
	 * End construcor xml
	 */
	public function createEnd() {
			xmlwriter_end_element(self::$xml);
		xmlwriter_end_document(self::$xml);
	}
	/**
	 * Get company routes
	 */
	public function get_company_routes($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirRoutesRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'FlightPref');
					xmlwriter_start_attribute(self::$xml, 'Airline');
						xmlwriter_text(self::$xml, $data->company);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Get company shedulle
	 */
	public function get_company_shedule($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirScheduleRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'Itinerary');
					xmlwriter_start_element(self::$xml, 'OriginDestination');
						xmlwriter_start_attribute(self::$xml, 'ODRef');
							xmlwriter_text(self::$xml, "0");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'From');
							xmlwriter_text(self::$xml, $data->from);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'To');
							xmlwriter_text(self::$xml, $data->to);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'MinDepDate');
							xmlwriter_text(self::$xml, $data->minDepDate);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'MaxDepDate');
							xmlwriter_text(self::$xml, $data->maxDepDate);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'FlightPref');
					xmlwriter_start_attribute(self::$xml, 'DesiredAirlines');
						xmlwriter_text(self::$xml, $data->company);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}



























































	/**
	 * Get flights
	 */
	public function get_results($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirShopRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'Itinerary');
					xmlwriter_start_element(self::$xml, 'OriginDestination');
						xmlwriter_start_attribute(self::$xml, 'ODRef');
							xmlwriter_text(self::$xml, "0");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'From');
							xmlwriter_text(self::$xml, $data->from);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'To');
							xmlwriter_text(self::$xml, $data->to);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'Date');
							xmlwriter_text(self::$xml, $data->date);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'PaxTypes');
					xmlwriter_start_element(self::$xml, 'PaxType');
						xmlwriter_start_attribute(self::$xml, 'Count');
							xmlwriter_text(self::$xml, 1);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'AgeCat');
							xmlwriter_text(self::$xml, "ADT");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'PTRef');
							xmlwriter_text(self::$xml, "pax0");
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
					if($data->count > 1) {
						xmlwriter_start_element(self::$xml, 'PaxType');
							xmlwriter_start_attribute(self::$xml, 'Count');
								xmlwriter_text(self::$xml, ($data->count-1));
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'AgeCat');
								xmlwriter_text(self::$xml, "ADT");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'PTRef');
								xmlwriter_text(self::$xml, "ADT");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'PTC');
								xmlwriter_text(self::$xml, "ITX");
							xmlwriter_end_attribute(self::$xml);
						xmlwriter_end_element(self::$xml);
					}
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'FlightPref');
					xmlwriter_start_attribute(self::$xml, 'DesiredAirlines');
						xmlwriter_text(self::$xml, $data->company);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'FareBrand');
						xmlwriter_text(self::$xml, $data->brand);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Set flight and fare
	 */
	public function set_flight($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirShopRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'SelectedShopOption');
					xmlwriter_start_attribute(self::$xml, 'SessionID');
						xmlwriter_text(self::$xml, $data->SessionID);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'OptionRef');
						xmlwriter_text(self::$xml, $data->OptionRef);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_element(self::$xml, 'SelectedItinerary');
						xmlwriter_start_attribute(self::$xml, 'ItineraryRef');
							xmlwriter_text(self::$xml, $data->ItineraryRef);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Seats map
	 */
	public function get_air_services($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirServicesRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data->SessionID);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'ServiceGroup');
					xmlwriter_text(self::$xml, "SA");
				xmlwriter_end_element(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}




























































	





































	/**
	 * Get company schedule
	 */
	public function get_company_schedule($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirScheduleRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'Itinerary');
					xmlwriter_start_element(self::$xml, 'OriginDestination');
						xmlwriter_start_attribute(self::$xml, 'ODRef');
							xmlwriter_text(self::$xml, "1");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'From');
							xmlwriter_text(self::$xml, $data['from']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'To');
							xmlwriter_text(self::$xml, $data['to']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'MinDepDate');
							xmlwriter_text(self::$xml, $data['date']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'MaxDepDate');
							xmlwriter_text(self::$xml, $data['date']);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'FlightPref');
					xmlwriter_start_attribute(self::$xml, 'DesiredAirlines');
						xmlwriter_text(self::$xml, $data['company']);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Get flights
	 */
	public function get_air_avail($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirAvailRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'Itinerary');
					xmlwriter_start_element(self::$xml, 'OriginDestination');
						xmlwriter_start_attribute(self::$xml, 'ODRef');
							xmlwriter_text(self::$xml, "0");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'From');
							xmlwriter_text(self::$xml, $data['from']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'To');
							xmlwriter_text(self::$xml, $data['to']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'Date');
							xmlwriter_text(self::$xml, $data['date']);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'PaxTypes');
					xmlwriter_start_element(self::$xml, 'PaxType');
						xmlwriter_start_attribute(self::$xml, 'PTRef');
							xmlwriter_text(self::$xml, "adt0");
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'Count');
							xmlwriter_text(self::$xml, $data['count']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'AgeCat');
							xmlwriter_text(self::$xml, "ADT");
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);

				// xmlwriter_start_element(self::$xml, 'FlightPref');
				// 	xmlwriter_start_attribute(self::$xml, 'DesiredAirlines');
				// 		xmlwriter_text(self::$xml, $data['company']);
				// 	xmlwriter_end_attribute(self::$xml);
				// xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'SearchOptions');
					xmlwriter_start_attribute(self::$xml, 'Mode');
						xmlwriter_text(self::$xml, "Fares");
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Set flight and fare
	 */
	public function set_flight1($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirAvailRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['session']);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'SelectedAvailOptions');
					xmlwriter_start_element(self::$xml, 'SelectedAvailOption');
						xmlwriter_start_attribute(self::$xml, 'ItineraryRef');
							xmlwriter_text(self::$xml, $data['flight']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'FareRef');
							xmlwriter_text(self::$xml, $data['fare']);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}


























































	/**
	 * Seats map
	 */
	public function get_pr($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirPriceRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['session']);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'PriceOption');
					xmlwriter_start_attribute(self::$xml, 'OptionRef');
						xmlwriter_text(self::$xml, "1H");
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	
	/**
	 * 
	 */
	public function get_servtarif($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirServicesRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['session']);
				xmlwriter_end_attribute(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * 
	 */
	public function get_rfisc($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirServicesRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['session']);
				xmlwriter_end_attribute(self::$xml);

				// xmlwriter_start_element(self::$xml, 'ServiceGroup');
				// 	xmlwriter_text(self::$xml, "BG");
				// xmlwriter_end_element(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Get Services
	 * @param string $lang
	 * @return string
	 */
	public function get_queue($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_QueueRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * 
	 */
	public function get_fare_rules($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirFareRulesRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'Rules');
					xmlwriter_start_element(self::$xml, 'RuleKey');
						xmlwriter_text(self::$xml, "!2/000/00/РГ/3/1//0/0//0/0/9/0/ESR1OW//МОВ/LON/16/");
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}

	
	public function set_shopflight($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirShopRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'SelectedShopOption');
					xmlwriter_start_attribute(self::$xml, 'SessionID');
						xmlwriter_text(self::$xml, $data['session']);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'OptionRef');
						xmlwriter_text(self::$xml, 0);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_element(self::$xml, 'SelectedItinerary');
						xmlwriter_start_attribute(self::$xml, 'ItineraryRef');
							xmlwriter_text(self::$xml, 0);
						xmlwriter_end_attribute(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	
	public function get_price($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirPriceRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'BookingIdentity');
					xmlwriter_start_attribute(self::$xml, 'BookingReference');
						xmlwriter_text(self::$xml, "5555/7R");
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'LeadPassenger');
						xmlwriter_text(self::$xml, "");
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	public function booking_place($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirBookRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['session']);
				xmlwriter_end_attribute(self::$xml);

				// xmlwriter_start_element(self::$xml, 'SelectedShopOption');
				// 	xmlwriter_start_attribute(self::$xml, 'SessionID');
				// 		xmlwriter_text(self::$xml, $data['session']);
				// 	xmlwriter_end_attribute(self::$xml);
				// 	xmlwriter_start_attribute(self::$xml, 'OptionRef');
				// 		xmlwriter_text(self::$xml, "0");
				// 	xmlwriter_end_attribute(self::$xml);
					
				// 	xmlwriter_start_element(self::$xml, 'SelectedItinerary');
				// 		xmlwriter_start_attribute(self::$xml, 'ItineraryRef');
				// 			xmlwriter_text(self::$xml, "1");
				// 		xmlwriter_end_attribute(self::$xml);
				// 	xmlwriter_end_element(self::$xml);
				// xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'Add');
					xmlwriter_start_element(self::$xml, 'Passengers');
						xmlwriter_start_element(self::$xml, 'Passenger');
							xmlwriter_start_attribute(self::$xml, 'AgeType');
								xmlwriter_text(self::$xml, "ADT");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'PaxRef');
								xmlwriter_text(self::$xml, "pax0");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'Title');
								xmlwriter_text(self::$xml, "MR");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'LastName');
								xmlwriter_text(self::$xml, $data['lastname']);
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'FirstName');
								xmlwriter_text(self::$xml, $data['firstname']);
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'DOB');
								xmlwriter_text(self::$xml, "1990-10-16");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'DocType');
								xmlwriter_text(self::$xml, "НП");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'DocNumber');
								xmlwriter_text(self::$xml, "9876543210");
							xmlwriter_end_attribute(self::$xml);
						xmlwriter_end_element(self::$xml);
						if($data['tarif']=="Каюта") {
							for($i=0;$i<3;$i++) {
								$ar = array(
									"a","s","d","f","g","h","t"
								);
								xmlwriter_start_element(self::$xml, 'Passenger');
									xmlwriter_start_attribute(self::$xml, 'AgeType');
										xmlwriter_text(self::$xml, "ADT");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PTC');
										xmlwriter_text(self::$xml, "ITX");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PaxRef');
										xmlwriter_text(self::$xml, "pax0");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'Title');
										xmlwriter_text(self::$xml, "MR");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'LastName');
										xmlwriter_text(self::$xml, 'Testov'.$ar[$i]);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'FirstName');
										xmlwriter_text(self::$xml, 'Test');
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DOB');
										xmlwriter_text(self::$xml, "1991-10-16");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DocType');
										xmlwriter_text(self::$xml, "НП");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DocNumber');
										xmlwriter_text(self::$xml, (9876543211+$i+1));
									xmlwriter_end_attribute(self::$xml);
								xmlwriter_end_element(self::$xml);
							}
						}
						if($data['tarif']=="Салон") {
							for($i=0;$i<7;$i++) {
								$ar = array(
									"a","s","d","f","g","h","t"
								);
								xmlwriter_start_element(self::$xml, 'Passenger');
									xmlwriter_start_attribute(self::$xml, 'AgeType');
										xmlwriter_text(self::$xml, "ADT");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PTC');
										xmlwriter_text(self::$xml, "ITX");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PaxRef');
										xmlwriter_text(self::$xml, "ADT");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'Title');
										xmlwriter_text(self::$xml, "MR");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'LastName');
										xmlwriter_text(self::$xml, 'Testov'.$ar[$i]);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'FirstName');
										xmlwriter_text(self::$xml, 'Test');
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DOB');
										xmlwriter_text(self::$xml, "1991-10-16");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DocType');
										xmlwriter_text(self::$xml, "НП");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'DocNumber');
										xmlwriter_text(self::$xml, (9876543211+$i+1));
									xmlwriter_end_attribute(self::$xml);
								xmlwriter_end_element(self::$xml);
							}
						}
					xmlwriter_end_element(self::$xml);
					xmlwriter_start_element(self::$xml, 'Contacts');
						xmlwriter_start_element(self::$xml, 'Contact');
							xmlwriter_start_attribute(self::$xml, 'TypeOfContact');
								xmlwriter_text(self::$xml, "Mobile phone");
							xmlwriter_end_attribute(self::$xml);
							xmlwriter_start_attribute(self::$xml, 'ContactRef');
								xmlwriter_text(self::$xml, "31");
							xmlwriter_end_attribute(self::$xml);

							xmlwriter_text(self::$xml, "79207777777");
						xmlwriter_end_element(self::$xml);
					xmlwriter_end_element(self::$xml);
					// if($data['service']) {
					// 	xmlwriter_start_element(self::$xml, 'Remarks');
					// 		xmlwriter_start_element(self::$xml, 'Remark');
					// 			xmlwriter_start_attribute(self::$xml, 'ServiceRef');
					// 				xmlwriter_text(self::$xml, $data['service']);
					// 			xmlwriter_end_attribute(self::$xml);
					// 		xmlwriter_end_element(self::$xml);
					// 	xmlwriter_end_element(self::$xml);
					// }
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Set flight
	 * @param string $lang
	 * @return string
	 */
	public function issue_ticket($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirBookRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['SessionID']);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'BookingIdentity');
					xmlwriter_start_attribute(self::$xml, 'BookingReference');
						xmlwriter_text(self::$xml, $data['bookingreference']);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'LeadPassenger');
						xmlwriter_text(self::$xml, $data['leadpassenger']);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);

				// xmlwriter_start_element(self::$xml, 'Issue');
				// 	xmlwriter_start_element(self::$xml, 'Payment');
				// 		xmlwriter_start_attribute(self::$xml, 'Currency');
				// 			xmlwriter_text(self::$xml, $data['currency']);
				// 		xmlwriter_end_attribute(self::$xml);
				// 		xmlwriter_start_attribute(self::$xml, 'Amount');
				// 			xmlwriter_text(self::$xml, $data['amount']);
				// 		xmlwriter_end_attribute(self::$xml);
				// 		xmlwriter_start_attribute(self::$xml, 'FOP');
				// 			xmlwriter_text(self::$xml, $data['fop']);
				// 		xmlwriter_end_attribute(self::$xml);
				// 	xmlwriter_end_element(self::$xml);
				// xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'Issue');
					xmlwriter_start_element(self::$xml, 'TicketMods');
						xmlwriter_start_element(self::$xml, 'TicketMod');
							xmlwriter_start_element(self::$xml, 'Payments');
								xmlwriter_start_element(self::$xml, 'Payment');
									xmlwriter_start_attribute(self::$xml, 'Currency');
										xmlwriter_text(self::$xml, $data['currency']);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'Amount');
										xmlwriter_text(self::$xml, $data['amount']);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'FOP');
										if($data['amount'] == "hold") {
											xmlwriter_text(self::$xml, 'Invoice');
										} else {
											xmlwriter_text(self::$xml, 'Credit card');
										}
									xmlwriter_end_attribute(self::$xml);
									
									// xmlwriter_start_element(self::$xml, 'CreditCard');
									// 	xmlwriter_start_attribute(self::$xml, 'PaymentSystem');
									// 		xmlwriter_text(self::$xml, "VI");
									// 	xmlwriter_end_attribute(self::$xml);
									// 	xmlwriter_start_attribute(self::$xml, 'AccountNumber');
									// 		xmlwriter_text(self::$xml, "411111111111111");
									// 	xmlwriter_end_attribute(self::$xml);
									// xmlwriter_end_element(self::$xml);
								xmlwriter_end_element(self::$xml);
							xmlwriter_end_element(self::$xml);
						xmlwriter_end_element(self::$xml);
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
	/**
	 * Set flight
	 * @param string $lang
	 * @return string
	 */
	public function issue_services($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirBookRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['SessionID']);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'BookingIdentity');
					xmlwriter_start_attribute(self::$xml, 'BookingReference');
						xmlwriter_text(self::$xml, $data['bookingreference']);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'LeadPassenger');
						xmlwriter_text(self::$xml, $data['leadpassenger']);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'Issue');
					xmlwriter_start_attribute(self::$xml, 'DocType');
						xmlwriter_text(self::$xml, "EMD");
					xmlwriter_end_attribute(self::$xml);

					xmlwriter_start_element(self::$xml, 'Payment');
						xmlwriter_start_attribute(self::$xml, 'Amount');
							xmlwriter_text(self::$xml, $data['amount']);
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'FOP');
							if($data['amount'] == "hold") {
								xmlwriter_text(self::$xml, 'Invoice');
							} else {
								xmlwriter_text(self::$xml, 'Credit card');
							}
						xmlwriter_end_attribute(self::$xml);
						xmlwriter_start_attribute(self::$xml, 'Currency');
							xmlwriter_text(self::$xml, "EUR");
						xmlwriter_end_attribute(self::$xml);
						
						if($data['amount'] != "hold") {
							xmlwriter_start_element(self::$xml, 'CreditCard');
								xmlwriter_start_attribute(self::$xml, 'PaymentSystem');
									xmlwriter_text(self::$xml, "VI");
								xmlwriter_end_attribute(self::$xml);
								xmlwriter_start_attribute(self::$xml, 'AccountNumber');
									xmlwriter_text(self::$xml, "411111111111111");
								xmlwriter_end_attribute(self::$xml);
							xmlwriter_end_element(self::$xml);
						}
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}

	
	public function add_services($data) {
		self::createBegin();
			xmlwriter_start_element(self::$xml, 'SIG_AirBookRQ');
				xmlwriter_start_attribute(self::$xml, 'CustomerID');
					xmlwriter_text(self::$xml, TAIS_USER);
				xmlwriter_end_attribute(self::$xml);
				xmlwriter_start_attribute(self::$xml, 'SessionID');
					xmlwriter_text(self::$xml, $data['SessionID']);
				xmlwriter_end_attribute(self::$xml);

				xmlwriter_start_element(self::$xml, 'BookingIdentity');
					xmlwriter_start_attribute(self::$xml, 'BookingReference');
						xmlwriter_text(self::$xml, $data['bookingreference']);
					xmlwriter_end_attribute(self::$xml);
					xmlwriter_start_attribute(self::$xml, 'LeadPassenger');
						xmlwriter_text(self::$xml, $data['leadpassenger']);
					xmlwriter_end_attribute(self::$xml);
				xmlwriter_end_element(self::$xml);

				xmlwriter_start_element(self::$xml, 'Add');
					xmlwriter_start_element(self::$xml, 'Remarks');
						$pax = explode(",", $data['paxref']);
						if($data['tarif'] == "Салон") {
							$d = array("1A", "1B", "2A", "2B", "3A", "3B", "4A", "4B", "5A", "5B", "6A", "6B", "7A", "7B");
							foreach($d as $k=>$v) {
								xmlwriter_start_element(self::$xml, 'Remark');
									// xmlwriter_start_attribute(self::$xml, 'RemarkRef');
									// 	xmlwriter_text(self::$xml, $value->RemarkRef);
									// xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'FlightRef');
										xmlwriter_text(self::$xml, $data['flightref']);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PaxRefs');
										xmlwriter_text(self::$xml, $pax[$k]);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'ServiceCode');
										xmlwriter_text(self::$xml, "PFS");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'ServiceType');
										xmlwriter_text(self::$xml, "F");
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'Quantity');
										xmlwriter_text(self::$xml, "1");
									xmlwriter_end_attribute(self::$xml);

									xmlwriter_text(self::$xml, $v);
								xmlwriter_end_element(self::$xml);
							}
						} else {
							foreach($data['service'] as $key=>$value) {
								xmlwriter_start_element(self::$xml, 'Remark');
									xmlwriter_start_attribute(self::$xml, 'RemarkRef');
										xmlwriter_text(self::$xml, $value->RemarkRef);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'FlightRef');
										xmlwriter_text(self::$xml, $data['flightref']);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'PaxRefs');
										xmlwriter_text(self::$xml, $pax[$key]);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'ServiceCode');
										xmlwriter_text(self::$xml, $value->ServiceCode);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'ServiceType');
										xmlwriter_text(self::$xml, $value->ServiceType);
									xmlwriter_end_attribute(self::$xml);
									xmlwriter_start_attribute(self::$xml, 'Quantity');
										xmlwriter_text(self::$xml, "1");
									xmlwriter_end_attribute(self::$xml);

									xmlwriter_text(self::$xml, $value->Seat);
								xmlwriter_end_element(self::$xml);
							}
						}
					xmlwriter_end_element(self::$xml);
				xmlwriter_end_element(self::$xml);
			
			xmlwriter_end_element(self::$xml);
		self::createEnd();

		self::$xml = xmlwriter_output_memory(self::$xml);

		return self::$xml;
	}
}