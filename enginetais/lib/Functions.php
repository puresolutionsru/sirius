<?php

class Functions {
    /**
     * This is function transform an xml object to an array
     * @param object $xmlObject
     * @return array
     */
    static function xml2array($xmlObject, $out = array()) {
        foreach((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? self::xml2array($node) : $node;

        return $out;
    }
}
