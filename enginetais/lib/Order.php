<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use CModule;

class Order
{

    public function getCode($iata){
        if(CModule::IncludeModule('iblock')){
            $arSelect = Array("ID", "NAME", "PROPERTY_AIRPORT", "PROPERTY_CITY", "PROPERTY_ICAO");
            $arFilter = Array(
                Array("IBLOCK_ID"=>15, "NAME" => $iata)
            );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            while($ob = $res->GetNextElement()){
                return $ob->fields['ID'];
            }
        }
    }

    public function generateName(){
        $rand = random_int(10000,99999);
        $randname = '7355'.$rand;
        if(CModule::IncludeModule('iblock')){
            $arSelect = Array("ID", "NAME");
            $arFilter = Array(
                Array("IBLOCK_ID"=>16, "NAME" => $randname)
            );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);

            if($res->result->num_rows === 1){
                $this->generateName();
            }else{
                return $randname;
            }
        }
    }

    public function AddOrder($arr){
        $orderNumber = $this->generateName();
        $arr = json_decode($arr, true);
        $prop = [];
        $prop['DEPARTURE'] = $this->getCode($arr['selectFlight']['airportDeparture']);
        $prop['ARRIVAL'] = $this->getCode($arr['selectFlight']['airportArrival']);
        $prop['DATE_DEPARTURE'] = $arr['selectFlight']['date'].' '.$arr['selectFlight']['timeDeparture'];
        $prop['DATE_ARRIVAL'] = $arr['selectFlight']['date'].' '.$arr['selectFlight']['timeArrival'];
        $prop['FLIGHT_NUMBER'] = $arr['selectFlight']['numberFlight'];
        $prop['NAME_FLIGHT'] = $arr['plane']['name'];
        $prop['NUMBER_FLIGHT'] = $arr['plane']['number'];
        $prop['TICKET_NUMBER'] = null;
        $prop['LOCATOR'] = null;
        $prop['ORDER_NUMBER'] = null;
        $prop['P_NAME'] = null;
        $prop['P_SURNAME'] = null;
        $prop['EMAIL'] = null;
        $prop['BROKER'] = null;
        $prop['STATUS'] = ['PROPERTY_VALUES' => 1];

        $el = new CIBlockElement;
        $arLoadProductArray = Array(
            "IBLOCK_ID"      => 16,
            "PROPERTY_VALUES"=> $prop,
            "NAME"           => $orderNumber,
            "ACTIVE"         => "Y",
        );
        return $el->Add($arLoadProductArray);
    }

    public function addPassenger($name, $lastname, $orderId){
        $passdata = [];
        $passdata['PASS_NAME'] = $name;
        $passdata['PASS_SURNAME'] = $lastname;
        $passdata['ORDER_NUMBER'] = $orderId;

        if(CModule::IncludeModule('iblock')) {
            $el = new CIBlockElement;
            $arLoadProductArray = Array(
                "IBLOCK_ID" => 17,
                "PROPERTY_VALUES" => $passdata,
                "NAME" => $this->getOrderName($orderId),
                "ACTIVE" => "Y",
            );
            $el->Add($arLoadProductArray);
        }
    }

    private function getOrderName($id){
        if(CModule::IncludeModule('iblock')){
            $arSelect = Array("ID", "NAME");
            $arFilter = Array(
                Array("IBLOCK_ID"=>16, "ID" => $id)
            );

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            while($ob = $res->GetNextElement()){
                return $ob->fields['NAME'];
            }
        }
    }
}