<?
set_time_limit(100000);
ini_set('session.gc_maxlifetime', 100000);

include 'config.php';

if($_REQUEST['METHOD'] && $_REQUEST['TYPE']) :
    $type = $_REQUEST['TYPE'];
    $method = $_REQUEST['METHOD'];
    $obj = new $type;
    $answer = $obj->$method();
    if($answer) {
        echo $answer;
    }
endif;