<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("description", "Контактные данные, реквизиты - Авиакомпания Sirius Aero");
$APPLICATION->SetPageProperty("title", "Контакты - Авиакомпания Sirius Aero");
$APPLICATION->SetTitle("Контакты");
echo '
<div class="b-content _page">
    <div class="the-content main-w">
        <h1>' . $APPLICATION->GetTitle(false) . '</h1>
    ';
?><h3>АРЕНДА САМОЛЁТА</h3>
<ul>
	 <!-- <li><a href="mailto:sales@sirius-aero.ru">sales@sirius-aero.ru</a></li> -->
	<li><a href="tel:+74959896191 ">+7 (495) 989 61 91 </a></li>
</ul>
<h3>НАШ ОФИС:</h3>
<div>
	<ul itemscope="" itemtype="http://schema.org/Organization">
		<li style="display:none;"><span itemprop="name">Sirius Aero</span></li>
		<li>Телефон: <span onclick="window.location.href='tel:+74959896191'" class="tag-like-link" itemprop="telephone">+7 (495) 989 61 91</span></li>
		<li>Факс: <span onclick="window.location.href='tel:+74959896191'" class="tag-like-link" itemprop="faxNumber">+7 (495) 989 61 91</span> доб. 108</li>
		<li><a href="mailto:info@sirius-aero.ru"><span itemprop="email">info@sirius-aero.ru</span></a></li>
		<li>
		<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			 Адрес: <span itemprop="postalCode">121354</span>, <span itemprop="addressLocality">Москва</span>, ул. <span itemprop="streetAddress">Дорогобужская 14</span>, корпус 1, БЦ «Партнер»
		</div>
 </li>
	</ul>
</div>
<h3>Реквизиты:</h3>
<p>Юридический адрес: 248926, Калужская область г. Калуга ул. Взлетная д. 46 офис 205<br>
ИНН 7707276670<br>
КПП 402901001</p>
<!-- <h3>ПРЕСС-СЛУЖБА</h3>
<ul>
	<li><a href="mailto:pr@sirius-aero.ru">pr@sirius-aero.ru</a></li>
	<li><a href="tel:+74959896191">+7 (495) 989-61-91</a> доб. 161</li>
	<li><a href="tel:+79060291650">+7 (906) 029-16-50</a></li>
</ul>-->
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aadc3d2cfd7d1651f5b25161336578ce33c7d64e9b731b3bb2e9d0a94a1bbf6be&amp;source=constructor" height="503" style="width:100%;" frameborder="0"></iframe><?
echo '
    </div>
</div>
    ';
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>